/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */




import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;




/**
*	Frame which presents data pertaining to modem login info.
*/

public class KnownHostDialog extends JDialog
								implements ActionListener
{
	
	private TreeMap knownHostTreeMap;
	private KnownHostTable knownHostTable;
	private JButton okButton, cancelButton;
	
	private boolean isCanceled = false;
	
	
	public KnownHostDialog(Frame owner, TreeMap knownHostTreeMap)
	{
		
		super(owner, "Known Hosts", true /*modal*/); 
		
		// close only on Cancel or OK button press
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        
        this.knownHostTreeMap = knownHostTreeMap;
        
        knownHostTable = new KnownHostTable(knownHostTreeMap);
		 
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		
		okButton = new JButton("OK");
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(this);
		
		this.getRootPane().setDefaultButton(okButton);
		
		
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.getContentPane().setLayout(theLayout);
		
		// add components
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(knownHostTable, c);
		this.getContentPane().add(knownHostTable);
		
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(okButton, c);
		this.getContentPane().add(okButton);
		
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(cancelButton, c);
		this.getContentPane().add(cancelButton);
		
		
	}
	
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("ok"))
		{
			this.isCanceled = false;
			
			try
			{
				// set known host vector to that returned by table, if no errors
				this.knownHostTreeMap = knownHostTable.getKnownHostTreeMap();
				this.setVisible(false);
				this.dispose();
			}
			catch (ValueFormatException ve)
			{
				String messageString = "Problem with supplied value: " + ve.getMessage() + "\n";
				JOptionPane.showMessageDialog(this, messageString, "Problem with input", JOptionPane.ERROR_MESSAGE);
			}
			
			
		}
		
		
		if (command.equals("cancel"))
		{
			this.isCanceled = true;
			this.setVisible(false);
			this.dispose();
		}
		
	}
	
	
	
	public boolean isCanceled()
	{
	    return this.isCanceled;
	}
	
	
	
	public void addHost(KnownHostInfo knownHostInfo)
	{
	    knownHostTable.addHost(knownHostInfo);
	}
	
	
	
	public TreeMap getKnownHostTreeMap()
	{
	    return this.knownHostTreeMap;
	}
		
	
	
	
}