/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


import snmp.*;

import java.io.*;
import java.util.*;



public abstract class HostInfoRetriever
{
    
    private static TreeMap hostInfoRetrievers = new TreeMap();
    private static String name;
    
    
    /**
    *   Load all known retrievers
    */
    static 
    {
        Class retrieverClass;
        String[] retrieverClassNames = {"AirportExtremeHostInfoRetriever", "SnowAirportHostInfoRetriever", "GraphiteAirportHostInfoRetriever"};
        
        for (int i = 0; i < retrieverClassNames.length; i++)
        {
            try
            {
                retrieverClass = Class.forName(retrieverClassNames[i]);
            }
            catch (ClassNotFoundException e)
            {
                System.out.println("Class not found: " + retrieverClassNames[i]);
            }
        }
    }
    
    
    
    protected static void registerRetriever(String name, HostInfoRetriever newRetriever)
    {
        // just add this to retrievers vector
        hostInfoRetrievers.put(name, newRetriever);
        
        //System.out.println("Added a retriever: " + name);
    }
    
    
    public static TreeMap getRetrievers()
    {
        return hostInfoRetrievers;
    }
    
    
    public static HostInfoRetriever getRetriever(String name)
    {
        return (HostInfoRetriever)hostInfoRetrievers.get(name);
    }
    
    
    public static String getName()
    {
        return name;
    }
    
    
    public abstract int getWANInterfaceIndex();
    
    
    public abstract HostInfoTreeMap getHostInfo(SNMPv1CommunicationInterface communicationInterface)
            throws IOException, SNMPGetException, SNMPBadValueException;
}