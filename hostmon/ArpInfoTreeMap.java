/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.util.*;
import java.math.*;
import snmp.*;


public class ArpInfoTreeMap extends TreeMap
{
    
    public ArpInfoTreeMap()
    {
        // create empty Hashtable
        super();
    }
    
    
    public ArpInfoTreeMap(SNMPVarBindList varBindList)
    {
        // create and populate Hashtable of HostInfo objects
        super();
        
        for (int i = 0; i < varBindList.size(); i += 2)
        {
            SNMPSequence variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
            SNMPObjectIdentifier snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
            SNMPOctetString snmpMacAddress = (SNMPOctetString)variablePair.getSNMPObjectAt(1);
            MacAddress macAddress = new MacAddress((byte[])snmpMacAddress.getValue());
            
            if ( (i+1) >= varBindList.size() )
                break;
            
            variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i+1);
            snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
            SNMPIPAddress snmpValue = (SNMPIPAddress)variablePair.getSNMPObjectAt(1);
            String ipAddress = snmpValue.toString();
            
            ArpInfo arpInfo = new ArpInfo(ipAddress, macAddress);
            
            // add new ArpInfo object to hashtable
            this.put(ipAddress, arpInfo);
        
        }
        
        
    }
    
    
    
    
    public String toString()
    {
        String returnValue = new String();
        
        Iterator iterator = this.values().iterator();
        
        while (iterator.hasNext())
        {
            ArpInfo arpInfo = (ArpInfo)iterator.next();
            returnValue += arpInfo.toString() + "\n";
        }
        
        return returnValue;
        
    }


}