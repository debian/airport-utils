/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

import java.util.*;
import java.io.*;



public class MacAddress
    implements Comparable, Serializable
						
{

	private byte[] macAddress;
	
	
	
	public MacAddress(byte[] address)
	{
		macAddress = address;
	}
	
	
	
	public MacAddress(String macAddressString)
		throws ValueFormatException
	{
	    this.macAddress = parseMacAddress(macAddressString);
	}
	
	
	private byte[] parseMacAddress(String addressString)
		throws ValueFormatException
	{
		try
		{
			StringTokenizer st = new StringTokenizer(addressString, " .-"); // break on spaces, dots or dashes
			int size = 0;
			
			while (st.hasMoreTokens())
			{
				// figure out how many values are in string
				size++;
				st.nextToken();
			}
			
			if (size != 6)
			{
				throw new ValueFormatException("Wrong number of components supplied in MAC address");
			}
			
			byte[] returnBytes = new byte[size];
			
			st = new StringTokenizer(addressString, " .-");
			
			for (int i = 0; i < size; i++)
			{
				int addressComponent = (Integer.parseInt(st.nextToken(), 16));
				if ((addressComponent < 0) || (addressComponent > 255))
					throw new ValueFormatException("Invalid characters used in MAC address");
				returnBytes[i] = (byte)addressComponent;
			}
			
			return returnBytes;
			
		}
		catch (NumberFormatException e)
		{
			String messageString = "Invalid characters used in MAC address\n";
			messageString += "MAC address must be specified as pairs of hex digits separated by spaces.\n";
			messageString += "  Example: 00 02 2d 37 1f a6";
			throw new ValueFormatException(messageString);
		}
		catch (ValueFormatException e)
		{
		    // augment the message string
		    String messageString = e.getMessage() + "\n";
			messageString += "MAC address must be specified as pairs of hex digits separated by spaces.\n";
			messageString += "  Example: 00 02 2d 37 1f a6";
			throw new ValueFormatException(messageString);
		}
		
	}
	
	
	
	public boolean equals(Object other)
	{
	    if (!(other instanceof MacAddress))
	        return false;
	    
	    MacAddress otherAddress = (MacAddress)other;
	    
	    if (macAddress.length != otherAddress.macAddress.length)
	        return false;
	    
	    for (int i = 0; i < macAddress.length; i++)
	    {
	        if (this.macAddress[i] != otherAddress.macAddress[i])
	            return false;
	    }
	    
	    // all address components equal; return true
	    return true;
	}
	
	
	public int compareTo(Object other)
	{
	    MacAddress otherAddress = (MacAddress)other;
	    
	    if (macAddress.length != otherAddress.macAddress.length)
	    {
	        if (macAddress.length < otherAddress.macAddress.length)
	            return -1;
	        else
	            return 1;
	    }
	    
	    for (int i = 0; i < macAddress.length; i++)
	    {
	        int unsigned = this.macAddress[i];
	        if (unsigned < 0)
	            unsigned += 256;
	        
	        int otherUnsigned = otherAddress.macAddress[i];
	        if (otherUnsigned < 0)
	            otherUnsigned += 256;
	        
	        if (unsigned < otherUnsigned)
	            return -1;
	        else if (unsigned > otherUnsigned)
	            return 1;
	            
	    }
	    
	    // if get to here, they're equal
	    return 0;
	}
	
	
	
	private String hexByte(byte b)
	{
		int pos = b;
		if (pos < 0)
			pos += 256;
		String returnString = new String();
		returnString += Integer.toHexString(pos/16);
		returnString += Integer.toHexString(pos%16);
		return returnString;
	}
	
	
	
	/** 
	*	Returns a space-separated hex string
	*/
	
	public String toString()
	{
		String returnString = new String();
		
		
		for (int i = 0; i < macAddress.length; i++)
		{
			returnString += hexByte(macAddress[i]) + " ";
		}
		
		return returnString;
		
	}
			
}