/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import snmp.*;
import java.io.*;


public class AirportExtremeHostInfoRetriever
                    extends HostInfoRetriever
{
    
    // OIDs for retrieving info from Extreme base stations
	private static String macAddressBaseOID = "1.3.6.1.4.1.63.501.3.2.2.1.1";
	private static String timeAssociatedBaseOID = "1.3.6.1.4.1.63.501.3.2.2.1.4";
	
	private static String name = "Airport Extreme";
	
	
	
	/**
	*   Static initializer, to call base class routine registerRetriever with a name 
	*   to register this retriever in the list of retrievers when class is loaded
	*/
	static
	{
	    AirportExtremeHostInfoRetriever retriever = new AirportExtremeHostInfoRetriever();
	    registerRetriever(name, retriever);
	}
	
	
	
	public int getWANInterfaceIndex()
	{
	    // WAN interface is on index 2 for Extreme models
	    return 2;
	}
	
	
	
	public HostInfoTreeMap getHostInfo(SNMPv1CommunicationInterface communicationInterface)
        throws IOException, SNMPGetException, SNMPBadValueException
	{
	    
	    // fetch associated host info
		String[] baseOIDs = {macAddressBaseOID, timeAssociatedBaseOID};
		SNMPVarBindList snmpHostInfoTable = communicationInterface.retrieveMIBTable(baseOIDs);
		HostInfoTreeMap hostInfoHashtable = new HostInfoTreeMap(snmpHostInfoTable);	
        
		//System.out.println("SNMP table:");
		//System.out.println(snmpPortMapInfo.toString());
		
		// now fetch arp info table, to get IP addresses
		String[] arpBaseOIDs = {"1.3.6.1.2.1.4.22.1.2", "1.3.6.1.2.1.4.22.1.3"};
		SNMPVarBindList snmpArpInfo = communicationInterface.retrieveMIBTable(arpBaseOIDs);
		RarpInfoTreeMap rarpInfoTreeMap = new RarpInfoTreeMap(snmpArpInfo);	
        hostInfoHashtable.setIPAddresses(rarpInfoTreeMap);
		
		return hostInfoHashtable;
	}
    
}