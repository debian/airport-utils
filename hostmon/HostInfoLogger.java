/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

 
 
 import java.io.*;
 import java.util.*;



public class HostInfoLogger
                    implements HostInfoChangeListener
{
    String logFile;
    int logMode;
    
    
    
    
    public HostInfoLogger(String logFile, int logMode)
    {
        this.logFile = logFile;
        this.logMode = logMode;
    }
    
    
    // do whatever in response to a change in the retrieved info
    public void processHostInfoChange(HostInfoTreeMap oldInfo, HostInfoTreeMap newInfo)
        throws HostInfoChangeException
    {
        
        
        try
        {
        
            switch (logMode)
            {
                case Preferences2.LOG_ALL:
                case Preferences2.LOG_UNKNOWN:
                {
                    // log any hosts that are in the new info and not in the old
                    
                    // open file for writing; will throw exception if problem with that...
                    FileWriter fileWriter = new FileWriter(logFile, true /*append*/);
                    
                    Iterator iterator = newInfo.keySet().iterator();
                    while(iterator.hasNext())
                    {
                        Object key = iterator.next();
                        
                        //System.out.println("Processing key " + key);
                        if (oldInfo.containsKey(key))
                        {
                            // we already know about this guy; continue
                            //System.out.println("Got this host already");
                            continue;
                        }
                        
                        // a new arrival; get info
                        HostInfo newHostInfo = (HostInfo)newInfo.get(key);
                        
                        // if log mode is LOG_UNKNOWN, log only hosts that are unknown
                        if ((logMode == Preferences2.LOG_UNKNOWN) && (newHostInfo.known == true))
                        {
                            // we know this guy; continue
                            //System.out.println("Known host");
                            continue;
                        }
                        
                        //System.out.println("Writing new info");
                        
                        // write info out to file
                        String hostInfoString = (new Date()).toString();
                        
                        if (newHostInfo.known == true)
                            hostInfoString += ": " + newHostInfo.name + ", MAC address " + newHostInfo.macAddress;
                        else
                            hostInfoString += ": Unknown host, MAC address " + newHostInfo.macAddress;
                        
                        hostInfoString += ", IP address " + newHostInfo.ipAddress;
                        
                        if (newHostInfo.timeAssociated < 0)
                            hostInfoString += ",  time associated unknown\n";
                        else
                            hostInfoString += ",  time associated " + newHostInfo.timeAssociated + ".\n";
                        
                        fileWriter.write(hostInfoString);
                        
                    }
                    
                    fileWriter.flush();
                    fileWriter.close();
                    
                    break;
                }
                
                case Preferences2.LOG_NONE:
                default:
                {
                    // pretty boring choice, n'est-ce pas?
                    break;
                }
            }
        
        }
        catch (Exception e)
        {
            throw new HostInfoChangeException(e.toString());
        }
        
    }
    
    
    
    public void setLogFile(String logFile)
    {
        this.logFile = logFile;
    }
    
    
    
    public void setLogMode(int logMode)
    {
        this.logMode = logMode;
    }
}