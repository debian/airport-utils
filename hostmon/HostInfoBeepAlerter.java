/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

 
 
 import java.io.*;
 import java.util.*;
 import java.awt.Toolkit;



public class HostInfoBeepAlerter
                    implements HostInfoChangeListener
{
    int notifyMode;
    
    
    
    
    public HostInfoBeepAlerter(int notifyMode)
    {
        this.notifyMode = notifyMode;
    }
    
    
    // beep in response to a change in the retrieved info
    public void processHostInfoChange(HostInfoTreeMap oldInfo, HostInfoTreeMap newInfo)
        throws HostInfoChangeException
    {
        
        
        try
        {
        
            switch (notifyMode)
            {
                case Preferences2.NOTIFY_ALL:
                case Preferences2.NOTIFY_UNKNOWN:
                {
                    // beep for any hosts that are in the new info and not in the old
                    
                    Iterator iterator = newInfo.keySet().iterator();
                    while(iterator.hasNext())
                    {
                        Object key = iterator.next();
                        
                        //System.out.println("Processing key " + key);
                        if (oldInfo.containsKey(key))
                        {
                            // we already know about this guy; continue
                            continue;
                        }
                        
                        // a new arrival; get info
                        HostInfo newHostInfo = (HostInfo)newInfo.get(key);
                        
                        // if mode is NOTIFY_UNKNOWN, beep only for hosts that are unknown
                        if ((notifyMode == Preferences2.NOTIFY_UNKNOWN) && (newHostInfo.known == true))
                        {
                            // we know this guy; continue
                            continue;
                        }
                        
                        // previously unseen host - beep!
                        Toolkit.getDefaultToolkit().beep();
                        
                    }
                    
                    break;
                }
                
                case Preferences2.NOTIFY_NONE:
                default:
                {
                    // pretty boring choice, n'est-ce pas?
                    break;
                }
            }
        
        }
        catch (Exception e)
        {
            throw new HostInfoChangeException(e.toString());
        }
        
    }
    
    
    
    public void setNotifyMode(int notifyMode)
    {
        this.notifyMode = notifyMode;
    }
}