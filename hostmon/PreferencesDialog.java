/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

 
 

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;




public class PreferencesDialog extends JDialog
                        implements ActionListener
{

    JTextField ipAddressField;
    JPasswordField passwordField;
    JTextField intervalField;
    JLabel ipAddressLabel;
    JLabel passwordLabel;
    JLabel intervalLabel;
    JLabel accessPointTypeLabel;
    JComboBox accessPointTypeComboBox;
    
    JCheckBox logCheckBox;
    JLabel logFileLabel;
    JTextField logFileField;
    JButton chooseLogFileButton;
    JRadioButton logAllButton, logUnknownButton;
    
    JCheckBox notifyCheckBox;
    JRadioButton notifyAllButton, notifyUnknownButton;
    JCheckBox notifyBeepCheckBox;
    JCheckBox notifyEmailCheckBox;
    JLabel notifyEmailLabel;
    JTextField notifyEmailField;
    JLabel notifySMTPServerLabel;
    JTextField notifySMTPServerField;
    
    
    
    JButton okButton, cancelButton;
    
    boolean cancelled = false;
    Preferences2 newPrefs;
    
    
    public PreferencesDialog(Frame owner, Preferences2 currentPreferences, String[] accessPointTypes)
    {
        // create as modal dialog
        super(owner, "Preferences", true);
        
        // set new prefs to current
        newPrefs = currentPreferences;
        
        ipAddressField = new JTextField(12);
        ipAddressLabel = new JLabel("Base station IP address:");
        ipAddressField.setText(currentPreferences.ipAddress);
        ipAddressField.setMinimumSize(ipAddressField.getPreferredSize());
        
        passwordField = new JPasswordField(12);
        passwordLabel = new JLabel("Base station password:");
        passwordField.setText("");
        passwordField.setMinimumSize(passwordField.getPreferredSize());
        
        intervalField = new JTextField(12);
        intervalLabel = new JLabel("Base station query interval (seconds):");
        intervalField.setText("" + currentPreferences.queryInterval);
        intervalField.setMinimumSize(intervalField.getPreferredSize());
       
        logCheckBox = new JCheckBox("Log associated host information");
        logCheckBox.setSelected(currentPreferences.logFlag);
        
        logAllButton = new JRadioButton("Log all associated hosts");
        logUnknownButton = new JRadioButton("Log only unknown associated hosts");
        
        ButtonGroup logButtonGroup = new ButtonGroup();
        logButtonGroup.add(logAllButton);
        logButtonGroup.add(logUnknownButton);
        
        if (currentPreferences.logMode == Preferences2.LOG_ALL)
        {
            logAllButton.setSelected(true);
        }
        else
        {
            logUnknownButton.setSelected(true);
        }
        
        logFileField = new JTextField(12);
        logFileLabel = new JLabel("Log file:");
        logFileField.setText(currentPreferences.logFile);
        logFileField.setMinimumSize(logFileField.getPreferredSize());
       
        chooseLogFileButton = new JButton("Choose file...");
        chooseLogFileButton.setActionCommand("choose log file");
		chooseLogFileButton.addActionListener(this);
		
		
		notifyCheckBox = new JCheckBox("Notify when new hosts appear");
        notifyCheckBox.setSelected(currentPreferences.notifyFlag);
        
        notifyAllButton = new JRadioButton("Notify for all associated hosts");
        notifyUnknownButton = new JRadioButton("Notify only for unknown associated hosts");
        
        ButtonGroup notifyButtonGroup = new ButtonGroup();
        notifyButtonGroup.add(notifyAllButton);
        notifyButtonGroup.add(notifyUnknownButton);
        
        if (currentPreferences.notifyMode == Preferences2.NOTIFY_ALL)
        {
            notifyAllButton.setSelected(true);
        }
        else
        {
            notifyUnknownButton.setSelected(true);
        }
        
        notifyBeepCheckBox = new JCheckBox("Beep when new hosts appear");
        notifyBeepCheckBox.setSelected(currentPreferences.beepNotifyFlag);
        
        notifyEmailCheckBox = new JCheckBox("Send email when new hosts appear");
        notifyEmailCheckBox.setSelected(currentPreferences.emailNotifyFlag);
        
        notifyEmailField = new JTextField(12);
        notifyEmailLabel = new JLabel("Email address:");
        notifyEmailField.setText(currentPreferences.emailAddress);
        notifyEmailField.setMinimumSize(notifyEmailField.getPreferredSize());
       
        notifySMTPServerField = new JTextField(12);
        notifySMTPServerLabel = new JLabel("Email server (SMTP):");
        notifySMTPServerField.setText(currentPreferences.emailSMTPHost);
        notifySMTPServerField.setMinimumSize(notifySMTPServerField.getPreferredSize());
        
		
		
        okButton = new JButton("OK");
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		
		this.getRootPane().setDefaultButton(okButton);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(this);
		
		// populate combo box with access point types; set selected index to
		accessPointTypeComboBox = new JComboBox(accessPointTypes);
		accessPointTypeComboBox.setSelectedItem(currentPreferences.accessPointType);
		accessPointTypeLabel = new JLabel("Base station type:");
		
        
        // set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		// set up panel for logging info
		InfoPanel logPanel = new InfoPanel();
		
		// put stuff in panel
		logPanel.setLayout(theLayout);
		
		c.gridwidth = 3;
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(logUnknownButton, c);
		logPanel.add(logUnknownButton);
		
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(logAllButton, c);
		logPanel.add(logAllButton);
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 3;
		c.anchor = GridBagConstraints.EAST;
		theLayout.setConstraints(logFileLabel, c);
		logPanel.add(logFileLabel);
		
		c.gridx = 2;
		c.gridy = 3;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(logFileField, c);
		logPanel.add(logFileField);
		
		c.gridx = 3;
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		theLayout.setConstraints(chooseLogFileButton, c);
		logPanel.add(chooseLogFileButton);
		
		
		// make highlighting of panel dependent on logCheckBox
		logCheckBox.addItemListener(logPanel);
		logPanel.setEnabled(logCheckBox.isSelected());
		
		
		
		
		// set up panel for email notifying when new host appears
		InfoPanel emailNotifyPanel = new InfoPanel();
		emailNotifyPanel.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.EAST;
		theLayout.setConstraints(notifyEmailLabel, c);
		emailNotifyPanel.add(notifyEmailLabel);
		
		c.gridx = 2;
		c.gridy = 1;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(notifyEmailField, c);
		emailNotifyPanel.add(notifyEmailField);
		
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.EAST;
		theLayout.setConstraints(notifySMTPServerLabel, c);
		emailNotifyPanel.add(notifySMTPServerLabel);
		
		c.gridx = 2;
		c.gridy = 2;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(notifySMTPServerField, c);
		emailNotifyPanel.add(notifySMTPServerField);
		
		// make highlighting of panel dependent on logCheckBox
		logCheckBox.addItemListener(emailNotifyPanel);
		emailNotifyPanel.setEnabled(logCheckBox.isSelected());
		
		
		
		// make highlighting of panel dependent on notifyCheckBox
		notifyEmailCheckBox.addItemListener(emailNotifyPanel);
		emailNotifyPanel.setEnabled(notifyEmailCheckBox.isSelected());
		
		
		
		
		
		// set up panel for notifying when new host appears
		InfoPanel notifyPanel = new InfoPanel();
		notifyPanel.setLayout(theLayout);
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(notifyUnknownButton, c);
		notifyPanel.add(notifyUnknownButton);
		
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(notifyAllButton, c);
		notifyPanel.add(notifyAllButton);
		
		c.gridx = 1;
		c.gridy = 3;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(notifyBeepCheckBox, c);
		notifyPanel.add(notifyBeepCheckBox);
		
		c.gridx = 1;
		c.gridy = 4;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(notifyEmailCheckBox, c);
		notifyPanel.add(notifyEmailCheckBox);
		
		c.gridx = 1;
		c.gridy = 5;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(emailNotifyPanel, c);
		notifyPanel.add(emailNotifyPanel);
		
		
		// make highlighting of panel dependent on notifyCheckBox
		notifyCheckBox.addItemListener(notifyPanel);
		notifyPanel.setEnabled(notifyCheckBox.isSelected());
		
		
    
		
		this.getContentPane().setLayout(theLayout);
		
		c.anchor = GridBagConstraints.CENTER;
		
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.EAST;
		theLayout.setConstraints(ipAddressLabel, c);
		this.getContentPane().add(ipAddressLabel);
        
		c.gridx = 2;
		c.gridy = 1;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(ipAddressField, c);
		this.getContentPane().add(ipAddressField);
		
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.EAST;
		theLayout.setConstraints(passwordLabel, c);
		this.getContentPane().add(passwordLabel);
		
		c.gridx = 2;
		c.gridy = 2;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(passwordField, c);
		this.getContentPane().add(passwordField);
		
		c.gridx = 1;
		c.gridy = 3;
		c.anchor = GridBagConstraints.EAST;
		theLayout.setConstraints(accessPointTypeLabel, c);
		this.getContentPane().add(accessPointTypeLabel);
		
		c.gridx = 2;
		c.gridy = 3;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(accessPointTypeComboBox, c);
		this.getContentPane().add(accessPointTypeComboBox);
		
		
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 4;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(logCheckBox, c);
		this.getContentPane().add(logCheckBox);
		
		c.gridx = 1;
		c.gridy = 5;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(logPanel, c);
		this.getContentPane().add(logPanel);
		
		c.gridx = 1;
		c.gridy = 6;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(notifyCheckBox, c);
		this.getContentPane().add(notifyCheckBox);
		
		c.gridx = 1;
		c.gridy = 7;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(notifyPanel, c);
		this.getContentPane().add(notifyPanel);
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 8;
		c.anchor = GridBagConstraints.CENTER;
		theLayout.setConstraints(okButton, c);
		this.getContentPane().add(okButton);
		
		c.gridx = 2;
		c.gridy = 8;
		c.anchor = GridBagConstraints.CENTER;
		theLayout.setConstraints(cancelButton, c);
		this.getContentPane().add(cancelButton);
		
		this.pack();
		this.show();
		
		
		
    }
    
    
    
    public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes, menu selections
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("ok"))
		{
			if (validateNewPreferences())
			    this.hide();
		}
		
		
		
		if (command == "cancel")
		{
			cancelled = true;
			this.hide();
		}
		
		
		if (command == "choose log file")
		{
			
			FileDialog fd = new FileDialog((Frame)this.getOwner(), "Select log file...", FileDialog.LOAD);
			fd.setFile(logFileField.getText());
			fd.show();
			
			if (fd.getFile() != null)
			{
				logFileField.setText(fd.getFile());
			}
			
		}
		
		
	}
	
	
	
	private boolean validateNewPreferences()
	{
	    int interval;
	    
	    // check the interval parameter
	    try
	    {
	        interval = Integer.parseInt(intervalField.getText());
	        if (interval <= 0)
			    throw new NumberFormatException();
			
			newPrefs = new Preferences2();
        
            newPrefs.ipAddress = ipAddressField.getText();
            newPrefs.password = passwordField.getText();
            newPrefs.queryInterval = interval;
            newPrefs.accessPointType = (String)accessPointTypeComboBox.getSelectedItem();
            
            if (logCheckBox.isSelected())
                newPrefs.logFlag = true;
            else
                newPrefs.logFlag = false;
            
        	if (logAllButton.isSelected())
                newPrefs.logMode = Preferences2.LOG_ALL;
            else if (logUnknownButton.isSelected())
                newPrefs.logMode = Preferences2.LOG_UNKNOWN;
            else
                newPrefs.logMode = Preferences2.LOG_NONE;
            
            newPrefs.logFile = logFileField.getText();
            
            
            if (notifyCheckBox.isSelected())
                newPrefs.notifyFlag = true;
            else
                newPrefs.notifyFlag = false;
            
        	if (notifyAllButton.isSelected())
                newPrefs.notifyMode = Preferences2.NOTIFY_ALL;
            else if (notifyUnknownButton.isSelected())
                newPrefs.notifyMode = Preferences2.NOTIFY_UNKNOWN;
            else
                newPrefs.notifyMode = Preferences2.NOTIFY_NONE;
            
            if (notifyBeepCheckBox.isSelected())
                newPrefs.beepNotifyFlag = true;
            else
                newPrefs.beepNotifyFlag = false;
            
            if (notifyEmailCheckBox.isSelected())
                newPrefs.emailNotifyFlag = true;
            else
                newPrefs.emailNotifyFlag = false;
            
            newPrefs.emailAddress = notifyEmailField.getText();
            newPrefs.emailSMTPHost = notifySMTPServerField.getText();
           
            
    	    return true;
    	    
	    }
	    catch(NumberFormatException e)
	    {
	        JOptionPane.showMessageDialog(this, "Value supplied must be a positive integer.");
	        return false;
	    }
	    
	    
	}
    
    
    public Preferences2 getPreferences()
    {
        return newPrefs;
    }
    
    
    
    public boolean isCancelled()
    {
        return cancelled;
    }

}