/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


import snmp.*;

import java.io.*;
import java.util.*;


public class SnowAirportHostInfoRetriever
                extends HostInfoRetriever
{
    
    // OIDs for retrieving info from Snow base stations
	private String portMapLocalIPBaseOID = "1.3.6.1.4.1.731.100.3.1.1.2";
    
    private static String name = "Airport (Snow)";
	private static SnowAirportHostInfoRetriever retriever = new SnowAirportHostInfoRetriever();
	    
    
	/**
	*   Static initializer, to call base class routine registerRetriever to 
	*   register this retriever in the list of retrievers when class is loaded
	*/
	static
	{
	    registerRetriever(name, retriever);
	}
	
	
	
	public int getWANInterfaceIndex()
	{
	    // WAN interface is on index 3
	    return 3;
	}
	
	
	
	public HostInfoTreeMap getHostInfo(SNMPv1CommunicationInterface communicationInterface)
        throws IOException, SNMPGetException, SNMPBadValueException
	{
	    
	    // fetch portmap info - local IPs
		SNMPVarBindList varBindList = communicationInterface.retrieveMIBTable(portMapLocalIPBaseOID);
		
		// create a TreeMap to filter out duplicates
		TreeMap ipAddressTreeMap = new TreeMap();
		
		for (int i = 0; i < varBindList.size(); i++)
		{
		    SNMPSequence variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
            SNMPObjectIdentifier snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
            SNMPIPAddress snmpIPAddress = (SNMPIPAddress)variablePair.getSNMPObjectAt(1);
		    String ipAddress = snmpIPAddress.toString();
		    
		    if (!ipAddressTreeMap.containsKey(ipAddress))
		    {
		        HostInfo hostInfo = new HostInfo();
		        hostInfo.ipAddress = ipAddress;
		        hostInfo.timeAssociated = -1;
		        ipAddressTreeMap.put(ipAddress, hostInfo);
		    }
		}
		
		// now create host info hashtable from this tree map
		HostInfoTreeMap hostInfoHashtable = new HostInfoTreeMap(ipAddressTreeMap);	
		
		//System.out.println("SNMP table:");
		//System.out.println(snmpPortMapInfo.toString());
		
		// now fetch arp info table, to get MAC addresses
		String[] arpBaseOIDs = {"1.3.6.1.2.1.4.22.1.2", "1.3.6.1.2.1.4.22.1.3"};
		SNMPVarBindList snmpArpInfo = communicationInterface.retrieveMIBTable(arpBaseOIDs);
		ArpInfoTreeMap arpInfoTreeMap = new ArpInfoTreeMap(snmpArpInfo);	
        hostInfoHashtable.setMACAddresses(arpInfoTreeMap);
		
		return hostInfoHashtable;
	}
	
}