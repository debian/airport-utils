/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.util.*;
import java.math.*;
import snmp.*;


public class HostInfoTreeMap extends TreeMap
{
    
    
    public static final String UNKNOWN_HOST_NAME = "*****Unknown host*****";
    
    
    
    public HostInfoTreeMap()
    {
        // create empty Hashtable
        super();
    }
    
    
    public HostInfoTreeMap(SNMPVarBindList varBindList)
    {
        // create and populate Hashtable of HostInfo objects
        super();
        
        for (int i = 0; i < varBindList.size(); i += 2)
        {
            SNMPSequence variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
            SNMPObjectIdentifier snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
            SNMPOctetString snmpMacAddress = (SNMPOctetString)variablePair.getSNMPObjectAt(1);
            MacAddress macAddress = new MacAddress((byte[])snmpMacAddress.getValue());
            
            if ( (i+1) >= varBindList.size() )
                break;
            
            variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i+1);
            snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
            SNMPInteger snmpValue = (SNMPInteger)variablePair.getSNMPObjectAt(1);
            BigInteger timeAssociatedObject = (BigInteger)snmpValue.getValue();
            int timeAssociated = timeAssociatedObject.intValue();
            
            HostInfo hostInfo = new HostInfo();
            hostInfo.macAddress = macAddress;
            hostInfo.timeAssociated = timeAssociated;
            
            // add new HostInfo object to hashtable
            this.put(macAddress, hostInfo);
        
        }
        
        
    }
    
    
    
    public HostInfoTreeMap(TreeMap hostInfoTreeMap)
    {
        // create and populate Hashtable of HostInfo objects, using supplied tree map 
        super(hostInfoTreeMap);
    }
    
    
    
    public void setMACAddresses(ArpInfoTreeMap arpInfoTreeMap)
    {
        // for each entry in the hostInfoMap, find its corresponding MAC address in the arpInfoMap
        Iterator iterator = this.values().iterator();
        
        while (iterator.hasNext())
        {
            HostInfo hostInfo = (HostInfo)iterator.next();
            
            // find ArpInfo object corresponding to IP address
            ArpInfo arpInfo = (ArpInfo)arpInfoTreeMap.get(hostInfo.ipAddress);
            
            if (arpInfo != null)
            {
                hostInfo.macAddress = arpInfo.macAddress;
            }
            else
            {
                // no ARP entry; assume this is a spurious port map, and drop it
                iterator.remove();
                
                //byte[] nullAddress = {0,0,0,0,0,0};
                //hostInfo.macAddress = new MacAddress(nullAddress);
            }
        }
    }
    
    
    
    public void setIPAddresses(RarpInfoTreeMap rarpInfoTreeMap)
    {
        // for each entry in the hostInfoMap, find its corresponding MAC address in the arpInfoMap
        Iterator iterator = this.values().iterator();
        
        while (iterator.hasNext())
        {
            HostInfo hostInfo = (HostInfo)iterator.next();
            
            // find ArpInfo object corresponding to MAC address
            ArpInfo arpInfo = (ArpInfo)rarpInfoTreeMap.get(hostInfo.macAddress);
            
            if (arpInfo != null)
            {
                hostInfo.ipAddress = arpInfo.ipAddress;
            }
            else
            {
                // no IP address known; call it unknown
                hostInfo.ipAddress = "unknown";
            }
        }
    }
    
    
    
    public void setHostNames(TreeMap knownHostTreeMap)
    {
        // for each entry in the hostInfoMap, find its corresponding name in the knownHostTreeMap
        Iterator iterator = this.values().iterator();
        
        while (iterator.hasNext())
        {
            HostInfo hostInfo = (HostInfo)iterator.next();
            
            // find ArpInfo object corresponding to MAC address
            KnownHostInfo knownHostInfo = (KnownHostInfo)knownHostTreeMap.get(hostInfo.macAddress);
            
            if (knownHostInfo != null)
            {
                hostInfo.name = knownHostInfo.name;
                hostInfo.known = true;
            }
            else
            {
                hostInfo.name = UNKNOWN_HOST_NAME;
                hostInfo.known = false;
            }
        }
    }
    
    
    
    public String toString()
    {
        String returnValue = new String();
        
        Iterator iterator = this.values().iterator();
        
        while (iterator.hasNext())
        {
            HostInfo hostInfo = (HostInfo)iterator.next();
            returnValue += hostInfo.toString() + "\n";
        }
        
        return returnValue;
        
    }


}