/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


import snmp.*;
import java.io.*;


public class GraphiteAirportHostInfoRetriever
                    extends HostInfoRetriever
{
    
    
    // OIDs for retrieving info from Graphite base stations
	private static String macAddressBaseOID = "1.3.6.1.4.1.762.2.5.2.1.11";
	private static String ipAddressBaseOID = "1.3.6.1.4.1.762.2.5.2.1.15";
	
	private static String name = "Airport (Graphite) / RG-1000";
	
	
	
	/**
	*   Static initializer, to call base class routine registerRetriever with a name 
	*   to register this retriever in the list of retrievers when class is loaded
	*/
	static
	{
	    GraphiteAirportHostInfoRetriever retriever = new GraphiteAirportHostInfoRetriever();
	    registerRetriever(name, retriever);
	}
	
	
	
	public int getWANInterfaceIndex()
	{
	    // Ethernet interface is on index 1; unfortunately, the ipAddress table lists _all_ of the interfaces
	    // as interface 1, so the first listed IP address is what we'll get, and that seems to generally be
	    // the one corresponding to the LAN (wireless) interface.
	    return 1;
	}
	
	
	
	public HostInfoTreeMap getHostInfo(SNMPv1CommunicationInterface communicationInterface)
        throws IOException, SNMPGetException, SNMPBadValueException
	{
	    
	    
   		// do wireless host discovery process
		String itemID;
		SNMPVarBindList newVars;
		SNMPSequence pair;
		SNMPInteger snmpIntegerValue;
		SNMPObject snmpValue;

		// first, need to write values to appropriate OID's - why, I don't know...	
		try
		{
    		for (int i = 1; i <= 3; i++)
    		{
    			itemID = "1.3.6.1.4.1.762.2.5.5." + i;
    			communicationInterface.setMIBEntry(itemID, new SNMPInteger(500));	// was 50
    			
    			itemID = "1.3.6.1.4.1.762.2.5.4." + i;
    			communicationInterface.setMIBEntry(itemID, new SNMPInteger(3));
    		}
		}
		catch (SNMPSetException e)
		{
		    throw new SNMPGetException("Unable to configure base station to retrieve information");
		}
				
	    
		String[] baseOIDs = {macAddressBaseOID};
		SNMPVarBindList varBindList = communicationInterface.retrieveMIBTable(baseOIDs);
		
		// create a Vector with HostInfo
		HostInfoTreeMap hostInfoTreeMap = new HostInfoTreeMap();
		
		for (int i = 0; i < varBindList.size(); i++)
		{
		    
		    SNMPSequence variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
            SNMPObjectIdentifier snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
            SNMPOctetString snmpMacAddress = (SNMPOctetString)variablePair.getSNMPObjectAt(1);
            MacAddress macAddress = new MacAddress((byte[])snmpMacAddress.getValue());
		    
		    HostInfo hostInfo = new HostInfo();
	        hostInfo.macAddress = macAddress;
	        hostInfo.timeAssociated = -1;
	        hostInfoTreeMap.put(macAddress, hostInfo);
		        
		}
		
		// now fetch arp info table, to get IP addresses
		String[] arpBaseOIDs = {"1.3.6.1.2.1.4.22.1.2", "1.3.6.1.2.1.4.22.1.3"};
		SNMPVarBindList snmpArpInfo = communicationInterface.retrieveMIBTable(arpBaseOIDs);
		RarpInfoTreeMap rarpInfoTreeMap = new RarpInfoTreeMap(snmpArpInfo);	
        hostInfoTreeMap.setIPAddresses(rarpInfoTreeMap);
		
		
		return hostInfoTreeMap;
	}
    
}