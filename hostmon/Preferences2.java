/*
 * Wireless Host Monitor
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.io.*;
import java.net.*;


public class Preferences2 extends Preferences
                implements Serializable
{
	public boolean logFlag;
	public int logMode;
	public String logFile;
	
	public boolean notifyFlag;
	public int notifyMode;
	public boolean beepNotifyFlag;
	public boolean emailNotifyFlag;
	public String emailAddress;
	public String emailSMTPHost;
	
	
	public static final int LOG_ALL = 1;
    public static final int LOG_UNKNOWN = 2;
    public static final int LOG_NONE = 3;
    
    public static final int NOTIFY_ALL = 1;
    public static final int NOTIFY_UNKNOWN = 2;
    public static final int NOTIFY_NONE = 3;
    
    
	
	
	public Preferences2()
	{
	    super();
	    
	    logFlag = false;
    	logMode = LOG_NONE;
    	logFile = "";
    	
    	notifyFlag = false;
    	notifyMode = NOTIFY_NONE;
    	beepNotifyFlag = false;
    	emailNotifyFlag = false;
    	emailAddress = "";
    	emailSMTPHost = "";
	}
	
	
	
	public Preferences2(Preferences preferences)
	{
	    this.ipAddress = preferences.ipAddress;
    	this.password = preferences.password;
    	this.queryInterval = preferences.queryInterval;
    	this.accessPointType = preferences.accessPointType;
	    
	    logFlag = false;
    	logMode = LOG_NONE;
    	logFile = "";
    	
    	notifyFlag = false;
    	notifyMode = NOTIFY_NONE;
    	beepNotifyFlag = false;
    	emailNotifyFlag = false;
    	emailAddress = "";
    	emailSMTPHost = "";
	}
	
	
	
	public Preferences2(Preferences2 preferences)
	{
	    this.ipAddress = preferences.ipAddress;
    	this.password = preferences.password;
    	this.queryInterval = preferences.queryInterval;
    	this.accessPointType = preferences.accessPointType;
	    
	    this.logFlag = preferences.logFlag;
    	this.logMode = preferences.logMode;
    	this.logFile = preferences.logFile;
    	
    	this.notifyFlag = preferences.notifyFlag;
    	this.notifyMode = preferences.notifyMode;
    	this.beepNotifyFlag = preferences.beepNotifyFlag;
    	this.emailNotifyFlag = preferences.emailNotifyFlag;
    	this.emailAddress = preferences.emailAddress;
    	this.emailSMTPHost = preferences.emailSMTPHost;
	}
	
	
	public boolean equals(Object otherPrefsObject)
	{
	    if (!(otherPrefsObject instanceof Preferences2))
	    {
	        return false;
	    }
	       
	    Preferences2 otherPrefs = (Preferences2)otherPrefsObject;
	    
	    // check superclass fields
	    if (!super.equals(otherPrefsObject))
	        return false;
	    
	    // compare all fields but passwords
	    if 
	    (
	        (this.logFlag == otherPrefs.logFlag)
	      &&
	        (this.logMode == otherPrefs.logMode)
	      &&
	        (this.logFile.equals(otherPrefs.logFile))
	      &&
	        (this.notifyFlag == otherPrefs.notifyFlag)
	      &&
	        (this.notifyMode == otherPrefs.notifyMode)
	      &&
	        (this.beepNotifyFlag == otherPrefs.beepNotifyFlag)
	      &&
	        (this.emailNotifyFlag == otherPrefs.emailNotifyFlag)
	      &&
	        (this.emailAddress.equals(otherPrefs.emailAddress))
	      &&
	        (this.emailSMTPHost.equals(otherPrefs.emailSMTPHost))
	      
	    )
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	        
	}
	
}