/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
import java.io.*;
import java.util.*;


public class HostInfoChangeEmailer
                    implements HostInfoChangeListener
{
    
    String userEmailAddress;
    String mailHost;
    String baseStationName = "";
    int notifyMode;
    Writer errorWriter;
    
    MailerThread mailerThread;
    
    
    
    public HostInfoChangeEmailer(String userEmailAddress, String mailHost, int notifyMode, Writer errorWriter)
    {
        this.userEmailAddress = userEmailAddress;
        this.mailHost = mailHost;
        this.baseStationName = baseStationName;
        this.notifyMode = notifyMode;
        this.errorWriter = errorWriter;
    }
    
    
    // do whatever in response to a change in the retrieved info
    public void processHostInfoChange(HostInfoTreeMap oldInfo, HostInfoTreeMap newInfo)
        throws HostInfoChangeException
    {
        String messageText = "";
        
        // see if any new hosts have appeared
        switch (notifyMode)
        {
            case Preferences2.NOTIFY_ALL:
            case Preferences2.NOTIFY_UNKNOWN:
            {
                // send email for hosts that are in the new info and not in the old
                
                Iterator iterator = newInfo.keySet().iterator();
                while(iterator.hasNext())
                {
                    Object key = iterator.next();
                    
                    //System.out.println("Processing key " + key);
                    if (oldInfo.containsKey(key))
                    {
                        // we already know about this guy; continue
                        continue;
                    }
                    
                    // a new arrival; get info
                    HostInfo newHostInfo = (HostInfo)newInfo.get(key);
                    
                    // if mode is NOTIFY_UNKNOWN, beep only for hosts that are unknown
                    if ((notifyMode == Preferences2.NOTIFY_UNKNOWN) && (newHostInfo.known == true))
                    {
                        // we know this guy; continue
                        continue;
                    }
                    
                    // previously unseen host - add to message
                    if (newHostInfo.known == true)
                        messageText += "Name: " + newHostInfo.name + "\r\n";
                    else
                        messageText += "Name: Unknown host\r\n";;
                    
                    messageText += "MAC address: " + newHostInfo.macAddress + "\r\n";
                    messageText += "IP address: " + newHostInfo.ipAddress + "\r\n";
                    
                    if (newHostInfo.timeAssociated < 0)
                        messageText += "Time associated: unknown\r\n";
                    else
                        messageText += "Time associated: " + newHostInfo.timeAssociated + "\r\n";
                        
                    messageText += "\r\n";
                    
                }
                
                break;
            }
            
            case Preferences2.NOTIFY_NONE:
            default:
            {
                // pretty boring choice, n'est-ce pas?
                break;
            }
        }
        
        
        
        // see if have any hosts in message
        if (messageText.equals(""))
        {
            // no new hosts; return
            return;
        }
        
        // have new hosts; create message
        String messageHeader = "To: " + userEmailAddress + "\r\n";
        messageHeader += "From: Airport base station (" + baseStationName + ")\r\n";
        messageHeader += "Date: " + (new Date()).toString() + "\r\n";
        messageHeader += "Subject: New host(s) associated with base station\r\n\r\n\r\n";
        
        messageHeader += "Time: " + (new Date()).toString() + "\r\n";
        messageHeader += "New host(s) associated with base station " + baseStationName + ":\r\n\r\n\r\n";
        
        messageText = messageHeader + messageText;
            
        // start mailer thread
        mailerThread = new MailerThread(userEmailAddress, mailHost, messageText, errorWriter);
        mailerThread.start();
       
    }
    
    
    public void setBaseStationName(String baseStationName)
    {
        this.baseStationName = baseStationName;
    }
    
}