/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 



import java.util.*;
import java.net.*;
import java.awt.event.*;
import java.io.*;
import java.util.zip.*;


import airport.*;





public class AirportBaseStationCLI
							implements ActionListener, Runnable
{
	
	public static final String aboutString = "Airport Base Station Configurator ";
	
	
	AirportInfo airportInfo;
	
	String password = "";
	String baseStationAddress = "10.0.1.1";
	
	// don't allow updating until settings opened or retrieved
	boolean updateBaseStationEnabled = false;
	
	BufferedReader inStream;
	
	
	public AirportBaseStationCLI()
	{
		airportInfo = new AirportInfo();
		
	    inStream = new BufferedReader(new InputStreamReader(System.in));
	    
	}
	
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes, menu selections
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("quit"))
		{
			System.exit(0);
		}
		
		
		
		if (command == "about")
		{
			//AboutDialog aboutDialog = new AboutDialog(this);
		}
		
		
		
		if (command.equals("discover devices"))
		{
			//AirportDiscoverer discoverer = new AirportDiscoverer();
		}	
		
		
		
		if (command.equals("retrieve data"))
		{
			
			
			try
			{
				
				InetAddress hostAddress = InetAddress.getByName(baseStationAddress);
				
				System.out.println("Retrieving settings from base station " + hostAddress + "......\n");
				
				byte[] requestPayload = airportInfo.getRequestBytes();
				
				Airport2ProtocolMessage requestMessage = new Airport2ProtocolMessage(Airport2ProtocolMessage.READ, password, requestPayload, requestPayload.length);
				
				// open socket
				int port = 5009;
				
				Socket configSocket = new Socket(hostAddress, port);
				
				// set socket timeout to 15 seconds
				configSocket.setSoTimeout(15000);
				DataInputStream inputStream = new DataInputStream(configSocket.getInputStream());
				OutputStream outputStream = configSocket.getOutputStream();
				
				
				outputStream.write(requestMessage.getBytes());
				outputStream.flush();
				
				/*
				System.out.println("Request header bytes:");
				System.out.println(printHexBytes(requestMessage.getBytes()));
				*/
				
				outputStream.write(requestPayload);
				outputStream.flush();
				
				// get and process response
				byte[] responseHeader = new byte[128];
				
				inputStream.readFully(responseHeader);
				
				// create temp buffer to hold retrieved config; 20k should suffice...
				byte[] responsePayload = new byte[4096];
				
				// read info
				try
				{
					inputStream.readFully(responsePayload);
				}
				catch (EOFException e)
				{
					// this is expected; we _should_ reach the end of the stream before filling the buffer...
				}
				
				
				/*
				numBytes = 0;
				int newNumBytes = 0;
				while(  ((newNumBytes = inputStream.read(responsePayload, numBytes, responsePayload.length - numBytes)) >= 0 )
						&&
						(numBytes < responsePayload.length)
					 )
				{
					System.out.println("Read " + newNumBytes + " bytes.");
					numBytes += newNumBytes;
				}
				*/
				
				
				//System.out.println("done.");
				//System.out.println("Read " + numBytes + " bytes total.");
				
				
				inputStream.close();
				outputStream.close();
				
				configSocket.close();
				
				
				// check to see if we retrieved valid data from the base station
				try
				{
					airportInfo = new AirportInfo(responsePayload);	
				
					System.out.println("Settings retrieved.\n");
					
					// enable updating
					updateBaseStationEnabled = true;	
				
				}
				catch(IllegalArgumentException ex)
				{
					// thrown by AirportInfo constructor if no data retrieved
					System.out.println("Error retrieving settings (check password)\n");
				}
				
			}
			catch(UnknownHostException e)
			{
				System.out.println("Error retrieving settings: unknown host.\n");
			}
			catch(SocketException e)
			{
				System.out.println("Error retrieving settings: " + e + "\n");
			}
			catch(IOException e)
			{
				System.out.println("Error retrieving settings: timed out waiting for response.\n");
			}
			catch (Exception e)
			{
				System.out.println("Error retrieving settings: " + e.getMessage() + "\n");
			}
			
		}
		
		if (command.equals("save settings"))
		{
			
			if (!updateBaseStationEnabled)
			{
			    System.out.println("Must retrieve or open settings first.");
			    return;
			}
				
			try
			{
				
				
				System.out.print("Enter path to settings file (or hit return to cancel): ");
				
				String filePath = inStream.readLine();
				
				if (!filePath.equals(""))
				{
					
    				File newFile = new File(filePath);
    				DataOutputStream fStream = new DataOutputStream(new FileOutputStream(newFile));
    					
    				setDependentData();
    				setStaticData();	
    				performConsistencyChecks();
    				
    				byte[] fileBytes = airportInfo.getUpdateBytes();
    				
    				// compute a checksum on the data, and pre-pend it to the file
    				int checksum = computeChecksum(fileBytes);
    					
    				fStream.writeInt(checksum);
    				fStream.write(fileBytes);
    				fStream.flush();
    				fStream.close();
				
				}
				
			}
			catch (ValueFormatException ve)
			{
				System.out.println("Problem with supplied value: " + ve.getMessage() + "\n");
			}
			catch (Exception e)
			{
				System.out.println("Error saving settings: " + e.getMessage() + "\n");
			}
			
			
		}
		
		
		
		if (command.equals("update base station"))
		{
			
			if (!updateBaseStationEnabled)
			{
			    System.out.println("Must retrieve or open settings first.");
			    return;
			}
			
			try
			{

				setDependentData();
				setStaticData();
				
				performConsistencyChecks();
				
				InetAddress hostAddress = InetAddress.getByName(baseStationAddress);
				
				System.out.println("Updating settings to base station " + hostAddress + "......\n");
				
				byte[] requestPayload = airportInfo.getUpdateBytes();
				
				Airport2ProtocolMessage requestMessage = new Airport2ProtocolMessage(Airport2ProtocolMessage.WRITE, password, requestPayload, requestPayload.length);
				
				// open socket
				int port = 5009;
				
				Socket configSocket = new Socket(hostAddress, port);
				
				// set socket timeout to 15 seconds
				configSocket.setSoTimeout(15000);
				
				DataInputStream inputStream = new DataInputStream(configSocket.getInputStream());
				OutputStream outputStream = configSocket.getOutputStream();
				
				outputStream.write(requestMessage.getBytes());
				outputStream.flush();
				
				/*
				System.out.println("Request header bytes:");
				System.out.println(printHexBytes(requestMessage.getBytes()));
				*/
				
				outputStream.write(requestPayload);
				outputStream.flush();
				
				/*
				System.out.println("Request header payload:");
				System.out.println(printHexBytes(requestPayload));
				*/
				
				// get and process response
				byte[] responseHeader = new byte[128];
				
				inputStream.readFully(responseHeader);
				
				/*
				System.out.println("Response header bytes:");
				System.out.println(printHexBytes(responseHeader));
				*/
				
				// create temp buffer to hold retrieved config; 20k should suffice...
				byte[] responsePayload = new byte[4096];
				
				// read info
				int numBytes = inputStream.read(responsePayload);
				
				//System.out.println("done.");
				//System.out.println("Read " + numBytes + " bytes.");
				
				inputStream.close();
				outputStream.close();
				
				configSocket.close();
				
				
				if (numBytes > 0)
				{
					//airportInfo = new AirportInfo(responsePayload);	
				
					System.out.println("Settings updated.\n");
				}
				else
				{
					System.out.println("\nError updating settings (check password)\n");
				}
				
			}
			catch (ValueFormatException ve)
			{
				System.out.println("Problem with supplied value: " + ve.getMessage() + "\n");
			}
			catch(UnknownHostException e)
			{
				System.out.println("Error updating settings: unknown host.\n");
			}
			catch(SocketException e)
			{
				System.out.println("Error updating settings: " + e + "\n");
			}
			catch(IOException e)
			{
				System.out.println("Error updating settings: timed out waiting for response.\n");
			}
			catch (Exception e)
			{
				System.out.println("Error updating settings: " + e.getMessage() + "\n");
			}
			
		}
		
		
		
		if (command.equals("reboot base station"))
		{
			
			try
			{
				
				InetAddress hostAddress = InetAddress.getByName(baseStationAddress);
				
				System.out.println("Rebooting base station " + hostAddress + "......\n");
				
				// get just the reboot info record
				byte[] requestPayload = airportInfo.get("acRB").getUpdateBytes();
				
				Airport2ProtocolMessage requestMessage = new Airport2ProtocolMessage(Airport2ProtocolMessage.WRITE, password, requestPayload, requestPayload.length);
				
				// open socket
				int port = 5009;
				
				Socket configSocket = new Socket(hostAddress, port);
				
				// set socket timeout to 15 seconds
				configSocket.setSoTimeout(15000);
				
				DataInputStream inputStream = new DataInputStream(configSocket.getInputStream());
				OutputStream outputStream = configSocket.getOutputStream();
				
				outputStream.write(requestMessage.getBytes());
				outputStream.flush();
				
				/*
				System.out.println("Request header bytes:");
				System.out.println(printHexBytes(requestMessage.getBytes()));
				*/
				
				outputStream.write(requestPayload);
				outputStream.flush();
				
				/*
				System.out.println("Request header payload:");
				System.out.println(printHexBytes(requestPayload));
				*/
				
				// get and process response
				byte[] responseHeader = new byte[128];
				
				inputStream.readFully(responseHeader);
				
				/*
				System.out.println("Response header bytes:");
				System.out.println(printHexBytes(responseHeader));
				*/
				
				// create temp buffer to hold retrieved config; 20k should suffice...
				byte[] responsePayload = new byte[4096];
				
				// read info
				int numBytes = inputStream.read(responsePayload);
				
				//System.out.println("done.");
				//System.out.println("Read " + numBytes + " bytes.");
				
				inputStream.close();
				outputStream.close();
				
				configSocket.close();
				
				
				if (numBytes > 0)
				{
					System.out.println("Base station rebooting.\n");
				}
				else
				{
					System.out.println("\nError rebooting base station (check password)\n");
				}
				
			}
			catch(UnknownHostException e)
			{
				System.out.println("Error rebooting base station: unknown host.\n");
			}
			catch(SocketException e)
			{
				System.out.println("Error rebooting base station: " + e + "\n");
			}
			catch(IOException e)
			{
				System.out.println("Error rebooting base station: timed out waiting for response.\n");
			}
			catch (Exception e)
			{
				System.out.println("Error rebooting base station: " + e.getMessage() + "\n");
			}
			
		}
		
		
		
		if (command.equals("open settings"))
		{
			
			try
			{
				System.out.print("Enter path to settings file (or hit return to cancel): ");
				
				String filePath = inStream.readLine();
				
				if (!filePath.equals(""))
				{
					File newFile = new File(filePath);
					
					DataInputStream fStream = new DataInputStream(new FileInputStream(newFile));
					
					int existingChecksum = fStream.readInt();
					
					byte[] newBytes = new byte[(int)newFile.length() - 4];
					
					fStream.readFully(newBytes);
					
					fStream.close();
					
					// check checksum to make sure block is valid, at least...
					int computedChecksum = computeChecksum(newBytes);
					
					if (computedChecksum == existingChecksum)
					{
						airportInfo = new AirportInfo(newBytes);	
					
						System.out.println("Settings opened.\n");
						
						
						// enable updating
						updateBaseStationEnabled = true;
					}
					else
					{
						System.out.println("Invalid configuration file.\n");
					}
					
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Error opening settings: " + e.getMessage() + "\n");
			}
			
			
		}
		
		
		
		if (command.equals("upload firmware"))
		{
			
			try
			{
				System.out.println("Enter path to settings file (or hit return to cancel):");
				
				String filePath = inStream.readLine();
				
				if (!filePath.equals(""))
				{
					File firmwareFile = new File(filePath);
					
					DataInputStream fStream = new DataInputStream(new FileInputStream(firmwareFile));
					
					int fileLength = (int)firmwareFile.length();
					
					// read in firmware image
					int flashSize = 0x180000;
					byte[] firmwareBytes = new byte[flashSize];
					
					
					// check first to see if this should be interpreted as a MacBinary file;
					// read 128 bytes for header, and check if have type/creator fields
					
					// first make sure file big enough to hold MacBinary header
					if (fileLength < 128)
					    throw new Exception("invalid firmware file.");
					    
					// read in MacBinary header, for verification purposes
					fStream.read(firmwareBytes, 0, 128);
					
					/*
					System.out.println("Mac file header:");
        			System.out.println(printHexBytes(macFileHeader));
        			*/
        			
        			int dataLength = 0;
    					
					// get file type and creator
					String fileTypeCreator = new String(firmwareBytes, 65, 8);
					
					
					if (fileTypeCreator.equals("bin21wcu"))
					{
					    
					    System.out.println("Mac file");
					    
					    // it's a Mac file; get length of data fork
    					for (int i = 83; i < 87; i++)
    					{
    					    dataLength *= 256;
    					    
    					    int byteValue = (int)firmwareBytes[i];
    					    if (byteValue < 0)
    					        byteValue += 256;
    					    
    					    dataLength += byteValue;
    					}
					
    					/*
    					System.out.println("Data length bytes: " 
    					        + hexByte(macFileHeader[83]) + " "
    					        + hexByte(macFileHeader[84]) + " "
    					        + hexByte(macFileHeader[85]) + " "
    					        + hexByte(macFileHeader[86]) + " "
    					);
    					*/
    					
    					if (fileLength - 128 < dataLength)
    					    throw new Exception("invalid firmware file.");
					    
					    // read in firmware bytes, overwriting Mac header
					    fStream.read(firmwareBytes, 0, dataLength);
					
					}
					else
					{
					    // no Mac header - just straight firmware
					    // read in remaining stuff
					    System.out.println("Not a Mac file");
					    
					    dataLength = fileLength;
    					fStream.read(firmwareBytes, 128, fileLength);
					}
					
					
					//System.out.println("Read bytes: 0x" + Integer.toHexString(numbytes) + " of 0x" + Integer.toHexString(dataLength));
					
					fStream.close();
					
					
					// move checksum on firmware file to last 8 bytes ...
					firmwareBytes[flashSize - 8] = firmwareBytes[dataLength - 8];
					firmwareBytes[flashSize - 7] = firmwareBytes[dataLength - 7];
					firmwareBytes[flashSize - 6] = firmwareBytes[dataLength - 6];
					firmwareBytes[flashSize - 5] = firmwareBytes[dataLength - 5];
					firmwareBytes[flashSize - 4] = firmwareBytes[dataLength - 4];
					firmwareBytes[flashSize - 3] = firmwareBytes[dataLength - 3];
					firmwareBytes[flashSize - 2] = firmwareBytes[dataLength - 2];
					firmwareBytes[flashSize - 1] = firmwareBytes[dataLength - 1];
					
					firmwareBytes[dataLength - 8] = 0;
					firmwareBytes[dataLength - 7] = 0;
					firmwareBytes[dataLength - 6] = 0;
					firmwareBytes[dataLength - 5] = 0;
					firmwareBytes[dataLength - 4] = 0;
					firmwareBytes[dataLength - 3] = 0;
					firmwareBytes[dataLength - 2] = 0;
					firmwareBytes[dataLength - 1] = 0;
					
					
					/*
					// compute CRC32 on firmware image, up to the checksum...
					CRC32 crc = new CRC32();
					crc.update(firmwareBytes, 0, flashSize);
					System.out.println("CRC32 checksum: 0x" + Long.toHexString(crc.getValue()));
					
					Adler32 adler = new Adler32();
					adler.update(firmwareBytes, 0, flashSize);
					System.out.println("Adler32 checksum: 0x" + Long.toHexString(adler.getValue()));
					*/
					
					
					
					/*
					System.out.println("Firmware checksum: " 
					        + hexByte(firmwareBytes[flashSize - 8]) + " "
					        + hexByte(firmwareBytes[flashSize - 7]) + " "
					        + hexByte(firmwareBytes[flashSize - 6]) + " "
					        + hexByte(firmwareBytes[flashSize - 5]) + " "
					        + hexByte(firmwareBytes[flashSize - 4]) + " "
					        + hexByte(firmwareBytes[flashSize - 3]) + " "
					        + hexByte(firmwareBytes[flashSize - 2]) + " "
					        + hexByte(firmwareBytes[flashSize - 1]) + " "
					);
					*/
					
					
					
					
					// put up a dialog permitting the user to select uploading the current settings, if
					// they're valid...
					System.out.println("Upload firmware to base station " + baseStationAddress + "? (y or n)");
					
					String response = inStream.readLine();
					char responseChar = response.charAt(0);
					
					if ((responseChar == 'y') || (responseChar == 'Y'))
					{
						// transmit firmware info
						
						InetAddress hostAddress = InetAddress.getByName(baseStationAddress);
        				
        				// open socket
        				int port = 5009;
        				
        				Socket configSocket = new Socket(hostAddress, port);
        				
        				// set socket timeout to 45 seconds
        				configSocket.setSoTimeout(45000);
        				
        				
        				DataInputStream inputStream = new DataInputStream(configSocket.getInputStream());
        				OutputStream outputStream = configSocket.getOutputStream();
        				
        				Airport2ProtocolMessage requestMessage = new Airport2ProtocolMessage(Airport2ProtocolMessage.WRITE_RAW, password, firmwareBytes, firmwareBytes.length);
        				
        				outputStream.write(requestMessage.getBytes());
        				outputStream.flush();
        				
        				/*
        				System.out.println("Request header bytes:");
        				System.out.println(printHexBytes(requestMessage.getBytes()));
        				*/
        				
        				outputStream.write(firmwareBytes);
        				outputStream.flush();
        				
        				/*
        				System.out.println("Request header payload:");
        				System.out.println(printHexBytes(requestPayload));
        				*/
        				
        				// get and process response
        				byte[] responseHeader = new byte[128];
        				
        				inputStream.readFully(responseHeader);
        				
        				/*
        				System.out.println("Response header bytes:");
        				System.out.println(printHexBytes(responseHeader));
        				*/
        				
        				Airport2ProtocolMessage responseMessage = new Airport2ProtocolMessage(responseHeader);
                        //System.out.println(responseMessage.toString());
                        
                        // check status field, to see if successful
        				if (responseMessage.status < 0)
        				    throw new Exception("base station refused update - invalid firmware file");
        				
        				
        				
        				// now reboot base station to get it to read the new firmware image
        				System.out.println("Rebooting base station " + hostAddress + "......\n");
        				
        				// get just the reboot info record
        				byte[] requestPayload = airportInfo.get("acRB").getUpdateBytes();
        				
        				requestMessage = new Airport2ProtocolMessage(Airport2ProtocolMessage.WRITE, password, requestPayload, requestPayload.length);
        				
        				// open new socket
        			    configSocket = new Socket(hostAddress, port);
        				
        				// set socket timeout to 15 seconds
        				configSocket.setSoTimeout(15000);
        				inputStream = new DataInputStream(configSocket.getInputStream());
        				outputStream = configSocket.getOutputStream();
        				
        				
        				outputStream.write(requestMessage.getBytes());
        				outputStream.flush();
        				
        				/*
        				System.out.println("Request header bytes:");
        				System.out.println(printHexBytes(requestMessage.getBytes()));
        				*/
        				
        				outputStream.write(requestPayload);
        				outputStream.flush();
        				
        				/*
        				System.out.println("Request header payload:");
        				System.out.println(printHexBytes(requestPayload));
        				*/
        				
        				// get and process response
        				responseHeader = new byte[128];
        				
        				inputStream.readFully(responseHeader);
        				
        				/*
        				System.out.println("Response header bytes:");
        				System.out.println(printHexBytes(responseHeader));
        				*/
        				
        				
        				responseMessage = new Airport2ProtocolMessage(responseHeader);
                        //System.out.println(responseMessage.toString());
                        
                        
                        // check status field, to see if successful
        				if (responseMessage.status < 0)
        				    throw new Exception("base station refused to reboot!");
        				
        				
        				// create temp buffer to hold response message...
        				byte[] responsePayload = new byte[4096];
        				
        				// read info
        				int numBytes = inputStream.read(responsePayload);
        				
        				//System.out.println("done.");
        				//System.out.println("Read " + numBytes + " bytes.");
        				
        				inputStream.close();
        				outputStream.close();
        				
        				configSocket.close();
        				
        				
        				if (numBytes > 0)
        				{
        					//airportInfo = new AirportInfo(responsePayload);	
        				
        					System.out.println("Base station rebooting.\n");
        				}
        				else
        				{
        					System.out.println("\nError rebooting base station (check password)\n");
        				}
        			}
					
				}
				
			}
			catch(UnknownHostException e)
			{
				System.out.println("Error updating firmware: unknown host.\n");
			}
			catch(SocketException e)
			{
				System.out.println("Error updating firmware: " + e + "\n");
			}
			catch(IOException e)
			{
				System.out.println("Error updating firmware: timed out waiting for response - check password.\n");
			}
			catch (Exception e)
			{
				System.out.println("Error updating firmware: " + e.getMessage() + "\n");
			}
			
		}
		
		
		
		
		
	}
	
	
	
	
	private String hexByte(byte b)
	{
		int pos = b;
		if (pos < 0)
			pos += 256;
		String returnString = new String();
		returnString += Integer.toHexString(pos/16);
		returnString += Integer.toHexString(pos%16);
		return returnString;
	}
	
	
	
	
	private void setDependentData()
		throws ValueFormatException
	{
		
		// if connecting to WAN through modem, and DHCP service selected on Ethernet,
		// deliver also on WAN interface
		AirportInfoRecord configInterfaceRecord = airportInfo.get("waIn");
		AirportInfoRecord dhcpLANRecord = airportInfo.get("laDS");
		AirportInfoRecord dhcpWANRecord = airportInfo.get("waDS");
		
		if (configInterfaceRecord.toString().equals("00000004"))
		{
			// connecting through modem: turn on DHCP service through WAN interface
			// if also configured to go through LAN interface
			if (dhcpLANRecord.toString().equals("01"))
			{
				dhcpWANRecord.setBytesFromString("01");
			}
			else
			{
				dhcpWANRecord.setBytesFromString("00");
			}
		}
		
		
	}
	
	
	
	private void performConsistencyChecks()
		throws ValueFormatException
	{
		// check that manual Ethernet config is being used if DHCP without NAT is selected
		// necessary since the WAN address configured becomes the gateway delivered by DHCP
		AirportInfoRecord wanConfigRecord = airportInfo.get("waCV");
		AirportInfoRecord natRecord = airportInfo.get("raNA");
		AirportInfoRecord dhcpWirelessRecord = airportInfo.get("raDS");
		AirportInfoRecord dhcpLANRecord = airportInfo.get("laDS");
		AirportInfoRecord dhcpWANRecord = airportInfo.get("waDS");
		
		if (natRecord.toString().equals("00"))
		{
			// NAT off - wireless bridge mode
			// see if DHCP service is being provided
			if (dhcpWirelessRecord.toString().equals("01") || dhcpLANRecord.toString().equals("01") || dhcpWANRecord.toString().equals("01"))
			{
				// make sure manual Ethernet config is being provided;
				// if not, throw a ValueFormatException
				if (!wanConfigRecord.toString().equals("00000400"))
				{
					throw new ValueFormatException("Must use manual Ethernet configuration (Network Panel) when providing DHCP service (DHCP Panel)\nin transparent bridge mode (Bridging Panel)");
				}
			}
			
		}
		
		
		// if using manual Ethernet config, check that  base station IP address, router address and mask are consistent
		if (wanConfigRecord.toString().equals("00000400"))
		{
			
			byte[] wanIPBytes = airportInfo.get("waIP").getValue();
			byte[] wanRouterIPBytes = airportInfo.get("waRA").getValue();
			byte[] wanMaskBytes = airportInfo.get("waSM").getValue();
			
			wanRouterIPBytes = maskBytes(wanRouterIPBytes, wanMaskBytes);
			wanIPBytes = maskBytes(wanIPBytes, wanMaskBytes);
			
			if ( !arraysEqual(wanRouterIPBytes, wanIPBytes) )
			{
				throw new ValueFormatException("Router IP address inconsistent with base station IP address and subnet mask (Network Panel)");
			}
			
		}
		
		
		
		// check that  router address lies in DHCP subnet when providing DHCP service
		// look at different fields depending on whether providing NAT or doing transparent bridging
		
		// see if DHCP service is being provided
		if (dhcpWirelessRecord.toString().equals("01") || dhcpLANRecord.toString().equals("01") || dhcpWANRecord.toString().equals("01"))
		{
			
			byte[] dhcpBeginBytes = airportInfo.get("dhBg").getValue();
			byte[] dhcpEndBytes = airportInfo.get("dhEn").getValue();
			
			if (natRecord.toString().equals("00"))
			{
				// NAT off - wireless bridge mode
				// check WAN IP address, subnet mask; will have already checked above that 
				// manual config is being used
				byte[] wanRouterIPBytes = airportInfo.get("waRA").getValue();
				byte[] wanMaskBytes = airportInfo.get("waSM").getValue();
				
				wanRouterIPBytes = maskBytes(wanRouterIPBytes, wanMaskBytes);
				dhcpBeginBytes = maskBytes(dhcpBeginBytes, wanMaskBytes);
				dhcpEndBytes = maskBytes(dhcpEndBytes, wanMaskBytes);
				
				if ( !arraysEqual(wanRouterIPBytes, dhcpBeginBytes) || !arraysEqual(wanRouterIPBytes, dhcpEndBytes))
				{
					throw new ValueFormatException("DHCP range (DHCP Panel) inconssistent with router IP address and subnet mask (Network Panel)");
				}
				
			}
			else
			{
				// NAT on
				// check LAN IP address, subnet mask
				byte[] lanIPBytes = airportInfo.get("laIP").getValue();
				byte[] lanMaskBytes = airportInfo.get("laSM").getValue();
				
				lanIPBytes = maskBytes(lanIPBytes, lanMaskBytes);
				dhcpBeginBytes = maskBytes(dhcpBeginBytes, lanMaskBytes);
				dhcpEndBytes = maskBytes(dhcpEndBytes, lanMaskBytes);
				
				if ( !arraysEqual(lanIPBytes, dhcpBeginBytes) || !arraysEqual(lanIPBytes, dhcpEndBytes))
				{
					throw new ValueFormatException("DHCP range (DHCP Panel) inconsistent with base station private LAN IP address and subnet mask (Bridging Panel)");
				}	
			}
			
		}
		
	}
	
	
	
	private void setStaticData()
		throws ValueFormatException
	{
		// set read community password field
		AirportInfoRecord readCommunityRecord = airportInfo.get("syPR");
		readCommunityRecord.setBytesFromString("public");
		
		// Set community name into password field so base station will know what to use after reboot!
		// Find out if there's an entry in the main panel; if so, use this, otherwise use
		// current community name field
		AirportInfoRecord communityNameRecord = airportInfo.get("syPW");
		String communityName = communityNameRecord.toString();
		if (communityName.equals(""))
			communityName = password;
		communityNameRecord.setBytesFromString(communityName);
		
	}
	
	
	
	private int computeChecksum(byte[] fileBytes)
	{
		int checksum = 0; 
		
		for (int i = 0; i < fileBytes.length; i++)
		{
			checksum += fileBytes[i];
		}
		
		return checksum;
	}
	
	
	
	
	private String printHexBytes(byte[] bytes)
	{
		String returnString = new String();
		
		for(int i = 0; i < bytes.length; i++)
		{
			returnString += hexByte(bytes[i]) + " ";
			
			if (((i+1)%16) == 0)
				returnString += "\n";
				
		}
		
		return returnString;
		
	}
	
	
	
	private static boolean arraysEqual(byte[] a, byte[] b)
	{
		if (a.length != b.length)
		{
			return false;
		}
		else
		{
			for (int i = 0; i < a.length; i++)
			{
				if (a[i] != b[i])
					return false;
			}
		}
		
		return true;
	}
	
	
	
	private static byte[] maskBytes(byte[] inBytes, byte[] mask)
	{
		byte[] maskedBytes = new byte[inBytes.length];
		
		for (int i = 0; i < inBytes.length; i++)
		{
			maskedBytes[i] = (byte)(inBytes[i] & mask[i % inBytes.length]);
		}
		
		return maskedBytes;
	}
	
	
	
	
	
	private void setValue(StringTokenizer st)
	{
	    if (!updateBaseStationEnabled)
		{
		    System.out.println("Must retrieve or open settings first.");
		    return;
		}
		
		if (!st.hasMoreTokens())
	    {
	        System.out.println("Must supply a key and value (write <key> <value>)");
	        return;
	    }
	    
	    String key = st.nextToken();
	    AirportInfoRecord element = (AirportInfoRecord)airportInfo.get(key);
	    
	    if (element == null)
	    {
	        System.out.println(key + ":   No such element");
	        return;
	    }
	    
	    if (!st.hasMoreTokens())
	    {
	        System.out.println("Must supply a key and value (write <key> <value>)");
	        return;
	    }
	    
	    String value = st.nextToken();
	    
	    try
	    {
	        element.setBytesFromString(value);
	        System.out.println(key + ":   " + element.toString());
	        System.out.println(element.description);
            System.out.println("");
	        
	    }
	    catch (ValueFormatException e)
	    {
	        System.out.println("Problem with supplied value: " + e.getMessage());
	    }
	    
	    
	}
	
	
	
	private void getValue(StringTokenizer st)
	{
	    
	    if (!updateBaseStationEnabled)
		{
		    System.out.println("Must retrieve or open settings first.");
		    return;
		}
			
			
		if (!st.hasMoreTokens())
	    {
	        System.out.println("All values:");
	        
	        Enumeration keys = airportInfo.keys();
	        
	        while (keys.hasMoreElements())
	        {
	            String key = (String)keys.nextElement();
	            AirportInfoRecord element = (AirportInfoRecord)airportInfo.get(key);
	            
	            System.out.println(key + ":   " + element.toString());
	            System.out.println(element.description);
	            System.out.println("");
	        }
	        
	        return;
	    }
	    
	    String key = st.nextToken();
	    AirportInfoRecord element = (AirportInfoRecord)airportInfo.get(key);
	    
	    if (element != null)
	    {
	        System.out.println(key + ":   " + element.toString());
	        System.out.println(element.description);
            System.out.println("");
	    }
	    else
	    {
	        System.out.println(key + ":   No such element");
	    }
	    
	    
	}
	
	
	
	private void setBaseStationAddress(StringTokenizer st)
	{
	    
	    if (!st.hasMoreTokens())
	    {
	        System.out.println("Current base station address: " + baseStationAddress);
	        return;
	    }
	    
	    baseStationAddress = st.nextToken();
	    
	    System.out.println("Set base station address to " + baseStationAddress);
	    
	}
	
	
	
	private void setPassword(StringTokenizer st)
	{
	    if (!st.hasMoreTokens())
	    {
	        System.out.println("Current password: " + password);
	        return;
	    }
	    
	    password = st.nextToken();
	    
	    System.out.println("Set password to " + password);
	}
	
	
	
	private void printInfo()
	{
	    System.out.println("AirPort Base Station CLI");
	    System.out.println("Command-line configuration utility for Snow and Extreme AirPort base stations");
	    System.out.println("Version 1.0, October 2003");
	    System.out.println("");
	    System.out.println("Jonathan Sevy");
	    System.out.println("http://gicl.mcs.drexel.edu/people/sevy");
	    System.out.println("jsevy@mcs.drexel.edu");
	    System.out.println("");
	    
	 }
	
	
	
	
	private void printHelp()
	{
	    System.out.println("Commands:");
	    System.out.println("");
	    System.out.println(" quit");
	    System.out.println(" q");
	    System.out.println("   quit the application");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" boot");
	    System.out.println(" b");
	    System.out.println("   reboot the base station");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" address <base_station_address>");
	    System.out.println(" a <base_station_address>");
	    System.out.println("   set address of base station");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" password <base_station_password>");
	    System.out.println(" p <base_station_password>");
	    System.out.println("   set password to use for accessing base station");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" retrieve");
	    System.out.println(" r");
	    System.out.println("   retrieve settings from base station");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" update");
	    System.out.println(" u");
	    System.out.println("   update settings to base station");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" save <file_path>");
	    System.out.println(" s <file_path>");
	    System.out.println("   save settings to specified file");
	    System.out.println("   will be prompted for file path");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" open");
	    System.out.println(" o");
	    System.out.println("   open saved settings from file;");
	    System.out.println("   will be prompted for file path");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" write <key> <value>");
	    System.out.println(" w <key> <value>");
	    System.out.println("   change the value for the specified key");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" get");
	    System.out.println(" g");
	    System.out.println(" get <key>");
	    System.out.println(" g <key>");
	    System.out.println("   get the value for the specified key,");
	    System.out.println("   or get the values corresponding to all keys");
	    System.out.println("   if no key specified");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" info");
	    System.out.println(" i");
	    System.out.println("   print out application info");
	    System.out.println("");
	    System.out.println("");
	    System.out.println(" help");
	    System.out.println(" h");
	    System.out.println("   print out this help message");
	    System.out.println("");
	    System.out.println("");
	    
	}
	
	
	
	
	private void processInput(String inputString)
	{
	    StringTokenizer st = new StringTokenizer(inputString);
	    
	    if (!st.hasMoreTokens())
	        return;
	    
	    String command = st.nextToken();
	    char commandChar = command.charAt(0);
	    
	    switch (commandChar)
	    {
	        case 'a':
	        {
	            setBaseStationAddress(st);
	            break;
	        }
	        
	        case 'p':
	        {
	            setPassword(st);
	            break;
	        }
	        
	        case 'r':
	        {
	            ActionEvent event = new ActionEvent(this, 0, "retrieve data");
        	    actionPerformed(event);
	            break;
	        }
	        
	        case 'u':
	        {
	            ActionEvent event = new ActionEvent(this, 0, "update base station");
        	    actionPerformed(event);
	            break;
	        }
	        
	        case 'w':
	        {
	            setValue(st);
	            break;
	        }
	        
	        case 'g':
	        {
	            getValue(st);
	            break;
	        }
	        
	        case 'o':
	        {
	            ActionEvent event = new ActionEvent(this, 0, "open settings");
        	    actionPerformed(event);
	            break;
	        }
	        
	        case 's':
	        {
	            ActionEvent event = new ActionEvent(this, 0, "save settings");
        	    actionPerformed(event);
	            break;
	        }
	        
	        case 'q':
	        {
	            ActionEvent event = new ActionEvent(this, 0, "quit");
        	    actionPerformed(event);
	            break;
	        }
	        
	        case 'b':
	        {
	            ActionEvent event = new ActionEvent(this, 0, "reboot base station");
        	    actionPerformed(event);
	            break;
	        }
	        
	        case 'i':
	        {
	            printInfo();
	            break;
	        }
	        
	        case 'h':
	        {
	            printHelp();
	            break;
	        }
	        
	        default:
	        {
	            break;
	        }
	    }
	}
	
	
	
	
	public void run()
	{
	    String inputString;
	    
	    System.out.println("AirPort Base Station Configurator CLI");
	    
	    while (true)
	    {
	        
	        System.out.print("> ");
	    
	        try
	        {
	            inputString = inStream.readLine();
	            processInput(inputString);
	        }
	        catch (Exception e)
	        {
	            System.out.println("Problem reading input: " + e);
	        }
	    }
	}
	
	
	
	
	
	public static void main(String args[]) 
	{
		try
		{
			AirportBaseStationCLI theApp = new AirportBaseStationCLI();
			theApp.run();
		}
		catch (Exception e)
		{}
	}
	

}