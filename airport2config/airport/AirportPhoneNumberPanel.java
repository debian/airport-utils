/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Utility subpanel which maintains dial-up phone number info. Could be easily extended to also permit
*	secondary phone number and modem initialization string editting.
*/

public class AirportPhoneNumberPanel extends AirportInfoPanel
{
	
	private AirportInfo airportInfo;
	private AirportInfoTextField modemInitField, primaryPhoneField, secondaryPhoneField;
	private AirportInfoComboBox countryCodeBox;
	
	
	
	public AirportPhoneNumberPanel(AirportInfo airportInfo)
	{
		super();
		this.airportInfo = airportInfo;
		
		primaryPhoneField = new AirportInfoTextField(airportInfo.get("moPN"), 10);
		
		secondaryPhoneField = new AirportInfoTextField(airportInfo.get("moAP"), 10);
		
		// holds configuration info for telephone system
		countryCodeBox = new AirportInfoComboBox("Country", airportInfo.get("moCC"));
		
		// add settings for each different country system; each group is same!
		countryCodeBox.addItemAndValue("North America", "22");
		countryCodeBox.addItemAndValue("Guam (same as North America)", "22");
		countryCodeBox.addItemAndValue("Hong Kong (same as North America)", "22");
		countryCodeBox.addItemAndValue("India (same as North America)", "22");
		countryCodeBox.addItemAndValue("Latin America (same as North America)", "22");
		countryCodeBox.addItemAndValue("Phillipines (same as North America)", "22");
		countryCodeBox.addItemAndValue("Thailand (same as North America)", "22");
		
		countryCodeBox.addItemAndValue("Singapore", "47");
		countryCodeBox.addItemAndValue("China (same as Singapore)", "47");
		countryCodeBox.addItemAndValue("Czech Republic (same as Singapore)", "47");
		countryCodeBox.addItemAndValue("Slovak Republic (same as Singapore)", "47");
		countryCodeBox.addItemAndValue("Korea (same as Singapore)", "47");
		countryCodeBox.addItemAndValue("Malaysia (same as Singapore)", "47");
		countryCodeBox.addItemAndValue("Poland (same as Singapore)", "47");
		countryCodeBox.addItemAndValue("Taiwan (same as Singapore)", "47");
		
		countryCodeBox.addItemAndValue("Switzerland", "15");
		countryCodeBox.addItemAndValue("Portugal (same as Switzerland)", "15");
		countryCodeBox.addItemAndValue("Spain (same as Switzerland)", "15");
		
		countryCodeBox.addItemAndValue("Finland", "4");
		countryCodeBox.addItemAndValue("Germany (same as Finland)", "4");
		countryCodeBox.addItemAndValue("Iceland (same as Finland)", "4");
		countryCodeBox.addItemAndValue("Netherlands (same as Finland)", "4");
		
		countryCodeBox.addItemAndValue("Australia", "40");
		countryCodeBox.addItemAndValue("South Africa (same as Australia)", "40");
		
		countryCodeBox.addItemAndValue("United Kingdom", "16");
		countryCodeBox.addItemAndValue("Belgium (same as United Kingdom)", "16");
		countryCodeBox.addItemAndValue("Denmark (same as United Kingdom)", "16");
		countryCodeBox.addItemAndValue("Greece (same as United Kingdom)", "16");
		countryCodeBox.addItemAndValue("Ireland (same as United Kingdom)", "16");
		countryCodeBox.addItemAndValue("Norway (same as United Kingdom)", "16");
		
		countryCodeBox.addItemAndValue("New Zealand", "48");
		
		countryCodeBox.addItemAndValue("France", "5");
		
		countryCodeBox.addItemAndValue("Japan", "43");
		
		countryCodeBox.addItemAndValue("Austria", "1");
		
		countryCodeBox.addItemAndValue("Italy", "8");
		
		countryCodeBox.addItemAndValue("Sweden", "14");
		
		//System.out.println("moCI: " + airportInfo.get("moCI").toString());
		
		setUpDisplay();
	}
	
	
	private void setUpDisplay()
	{
		
		JLabel modemInitLabel = new JLabel("Modem init string");
		JLabel primaryPhoneLabel = new JLabel("Phone number");
		JLabel secondaryPhoneLabel = new JLabel("Secondary phone number");
		
		// set params for layout manager
		GridBagLayout theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.setLayout(theLayout);
		
		
		// add stuff: just single phone number for now!
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(primaryPhoneLabel, c);
		this.add(primaryPhoneLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(primaryPhoneField, c);
		this.add(primaryPhoneField);
		
		// also modem init string, for Steve Palm
		/*
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(modemInitLabel, c);
		this.add(modemInitLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(modemInitField, c);
		this.add(modemInitField);
		*/
		
		// and modem country code box
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(countryCodeBox, c);
		this.add(countryCodeBox);
		
		refreshDisplay();
		
		
	}
	
	
	
	public void writeValue()
		throws ValueFormatException
	{
		super.writeValue();
		
		// The following doesn't really need to be done; it just sets the
		// combobox index, so the exact correct entry will be displayed (e.g.,
		// even though UK and Greece use the same country code, make sure 
		// Greece is displayed when the configuration is retrieved if that is
		// what was selected when the base station was updated.
		// The index values just correspond to the index in the combo box
		// in Apple's software; thus the wrong stuff will be displayed only
		// if Apple's tool is used...
		
		/*
		AirportInfoRecord modemCountryFlagRecord = airportInfo.get("moCI");
		AirportInfoRecord modemCountryCodeRecord = airportInfo.get("moCC");
		
		if (modemCountryCodeRecord.toString().equals("22"))
		{
			modemCountryFlagRecord.setBytesFromString("00000000");
		}
		else
		{
			modemCountryFlagRecord.setBytesFromString("00000001");
		}
		*/
		
	}
	
	
	
		
}