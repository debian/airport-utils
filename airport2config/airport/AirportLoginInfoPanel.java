/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains data pertaining to modem connection login, with settings for
*	username, password and login string.
*/

public class AirportLoginInfoPanel extends AirportInfoPanel
{
	
	private AirportUsernamePanel usernamePanel;
	//private AirportLoginStringTable loginStringTable;
	//private AirportInfoRadioButton userPassButton, loginStringButton;
	//private ButtonGroup loginSelectGroup;
	
	public AirportLoginInfoPanel(AirportInfo theInfo)
	{
		/*
		loginStringButton = new AirportInfoRadioButton("Use login script:");
		
		userPassButton = new AirportInfoRadioButton("Use Username/Password:");
		
		loginSelectGroup = new ButtonGroup();
		loginSelectGroup.add(loginStringButton);
		loginSelectGroup.add(userPassButton);
		*/
		
		// panel holding dial-up username / password
		usernamePanel = new AirportUsernamePanel(theInfo);
		
		// table maintaining login string info
		//loginStringTable = new AirportLoginStringTable(theInfo);
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		JLabel label;
		
		this.setLayout(theLayout);
		
		// add components
		
		c.insets = new Insets(8,2,2,2);
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(usernamePanel, c);
		this.add(usernamePanel);
		/*
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(8,2,2,2);
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(userPassButton, c);
		this.add(userPassButton);
		
		c.insets = new Insets(2,2,2,2);
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(loginStringButton, c);
		this.add(loginStringButton);
		
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(8,2,2,2);
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(loginStringTable, c);
		this.add(loginStringTable);
		
		
		// make login string table enabling dependent on button settings
		loginStringButton.addItemListener(loginStringTable);
		loginStringTable.setEnabled(loginStringButton.isSelected());
		
		// make username/password panel enabling dependent on button settings
		userPassButton.addItemListener(usernamePanel);
		usernamePanel.setEnabled(userPassButton.isSelected());
		*/
		
	}
		
	
	
	
}