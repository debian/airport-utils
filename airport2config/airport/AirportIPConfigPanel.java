/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains data relevant to base station and router IP address, and subnet mask.
*	Contained as subpanel within AirportNetworkPanel.
*/

public class AirportIPConfigPanel extends AirportInfoPanel
{
	
	private AirportInfoTextField ipAddressField, subnetMaskField, routerIPAddressField;
	
	
	
	public 	AirportIPConfigPanel(AirportInfo theInfo)
	// use retrieved values as default values
	{
		
		ipAddressField = new AirportInfoTextField((AirportInfoRecord)theInfo.get("waIP"));
		subnetMaskField = new AirportInfoTextField((AirportInfoRecord)theInfo.get("waSM"));
		routerIPAddressField = new AirportInfoTextField((AirportInfoRecord)theInfo.get("waRA"));
		
		setUpDisplay();
		
	}
	
	
	
	public 	AirportIPConfigPanel(AirportInfo theInfo, String defaultIPAddress, String defaultSubnetMask, String defaultRouterIPAddress)
	// use supplied default values, unless empty string supplied, whence use retrieved value
	{
		
		
		ipAddressField = new AirportInfoTextField((AirportInfoRecord)theInfo.get("waIP"));
		if(!defaultIPAddress.equals(""))
			ipAddressField.setText(defaultIPAddress);
		
		subnetMaskField = new AirportInfoTextField((AirportInfoRecord)theInfo.get("waSM"));
		if(!defaultSubnetMask.equals(""))
			subnetMaskField.setText(defaultSubnetMask);
		
		routerIPAddressField = new AirportInfoTextField((AirportInfoRecord)theInfo.get("waRA"));
		if(!defaultRouterIPAddress.equals(""))
			routerIPAddressField.setText(defaultRouterIPAddress);
		
		
		
		setUpDisplay();
	}
		
		
		
		
		
	private void setUpDisplay()
	{
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		JLabel label;
		
		
		this.setLayout(theLayout);
		
		// add components
		
		c.gridwidth = 1;
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		label = new JLabel("IP address");
		theLayout.setConstraints(label, c);
		this.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(ipAddressField, c);
		this.add(ipAddressField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		label = new JLabel("Subnet mask");
		theLayout.setConstraints(label, c);
		this.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(subnetMaskField, c);
		this.add(subnetMaskField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 3;
		label = new JLabel("Router address");
		theLayout.setConstraints(label, c);
		this.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(routerIPAddressField, c);
		this.add(routerIPAddressField);
		
	}
	
	

}