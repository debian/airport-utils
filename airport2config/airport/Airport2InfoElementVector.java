/*
 * Airport2Analyzer
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.util.*;
import java.io.*;




public class Airport2InfoElementVector
{
	
	private Vector infoElementVector;
	
	
	
	public Airport2InfoElementVector()
	{
		infoElementVector = new Vector();
	}
	
	
	
	public Airport2InfoElementVector(byte[] retrievedBytes, int numBytes)
	{
		infoElementVector = new Vector();
		
		ByteArrayInputStream byteStream = new ByteArrayInputStream(retrievedBytes, 0, numBytes);
		
		while (byteStream.available() > 0)
		{
			
			Airport2InfoElement element = new Airport2InfoElement();
			
			// read the tag
			byte[] tagBytes = new byte[4];
			byteStream.read(tagBytes, 0, 4);
			element.tag = new String(tagBytes);
			
			//read the type
			byte[] typeBytes = new byte[4];
			byteStream.read(typeBytes, 0, 4);
			element.type = getIntegerValue(typeBytes);
			
			//read the length
			byte[] lengthBytes = new byte[4];
			byteStream.read(lengthBytes, 0, 4);
			element.length = getIntegerValue(lengthBytes);
			
			//read the value
			byte[] valueBytes = new byte[element.length];
			byteStream.read(valueBytes, 0, element.length);
			element.value = valueBytes;
			
			
			infoElementVector.add(element);
			
		}
		
	}
	
	
	
	public void add(Airport2InfoElement element)
	{
		infoElementVector.add(element);
	}
	
	
	public Airport2InfoElement elementAt(int i)
	{
		return (Airport2InfoElement)infoElementVector.elementAt(i);
	}
	
	
	public byte[] getBytes()
	{
		int totalSize = 0;
		
		for (int i = 0; i < infoElementVector.size(); i++)
		{
			totalSize += this.elementAt(i).getSize();
		}
		
		byte[] returnBytes = new byte[totalSize];
		
		int startPosition = 0;
		
		for (int i = 0; i < infoElementVector.size(); i++)
		{
			byte[] elementBytes = this.elementAt(i).getBytes();
			
			if (elementBytes != null)
			{
				for (int j = 0; j < elementBytes.length; j++)
					returnBytes[startPosition + j] = elementBytes[j];
					
				startPosition += elementBytes.length;
			}
			
		}
		
		return returnBytes;
	}
	
	
	public String toString()
	{
		String returnText = new String();
		
		for (int i = 0; i < infoElementVector.size(); i++)
		{
			Airport2InfoElement theElement = (Airport2InfoElement)infoElementVector.elementAt(i);
			returnText += theElement.toString();
			returnText += "\n";
		}
		
		return returnText;
		
	}
	
	
	
	private int getIntegerValue(byte[] valueBytes)
	{
		int value = 0;
		
		for (int i = 0; i < valueBytes.length; i++)
		{
			int absValue = valueBytes[i];
			
			if (absValue < 0)
				absValue += 256;
			
			value = value*256 + absValue;
		}
		
		return value;
	}
	
	
	public int size()
	{
		return infoElementVector.size();
	}
	
	

}