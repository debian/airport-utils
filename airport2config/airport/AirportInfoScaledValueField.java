/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;





/**
*	Creates a text box to display and edit information in an AirportInfoRecord.
*	Size of textfield is set automatically according to datatype.
*/

public class AirportInfoScaledValueField extends JTextField
								implements AirportInfoComponent
{
	private AirportInfoRecord theRecord;
	
	private int scalingFactor;
	
	
	/**
	*	Creates new textfield to display and edit value information in an AirportInfoRecord.
	*	Size of textfield is set automatically according to value size and scaling.
	*/
	
	public AirportInfoScaledValueField(AirportInfoRecord theRecord, int scalingFactor)
	{
		super();
		this.theRecord = theRecord;
		this.scalingFactor = scalingFactor;
		
		//determine how large the text field should be to display the value
		int fieldSize = theRecord.maxLength;
		
		fieldSize *= 3;	// to hold hex string: 2 chars per byte, plus space
		
		if (fieldSize > 40)
			fieldSize = 40;
			
		this.setColumns(fieldSize);
		
		refreshDisplay();
	}
	
	
	
	/**
	*	Creates new textfield to display and edit information in an AirportInfoRecord.
	*	Size of textfield is supplied as argument.
	*/
	
	public AirportInfoScaledValueField(AirportInfoRecord theRecord, int scalingFactor, int fieldSize)
	{
		super();
		this.theRecord = theRecord;
		this.scalingFactor = scalingFactor;
		
		this.setColumns(fieldSize);
		
		refreshDisplay();
	}
	
	
	
	/**
	*	Read value from associated AirportInfoRecord window, and display value.
	*/
	
	public void refreshDisplay()
	{
		
		
		try
		{
			long storedValue = Long.parseLong(theRecord.toString());
			long displayValue = storedValue/scalingFactor;
			String valueString = new String();
			valueString += displayValue;
			setText(valueString);
		}
		catch (NumberFormatException e)
		{
			// won't happen!
		}
		
	}
	
	
	
	/**
	*	Write value currently displayed into associated AirportInfoRecord window.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		long displayValue = 0;
		
		try
		{
			displayValue = Long.parseLong(getText());
		}
		catch (NumberFormatException e)
		{
			this.selectAll();
			this.requestFocus();
			throw new ValueFormatException("Bad number format.");
		}	
		
		
			
		// see how big the value can be
		int numBytes = theRecord.maxLength;
		
		long minValue = 0;
		long maxValue = 1;
		for (int i = 0; i < numBytes; i++)
		{
			maxValue *= 256;
		}
		maxValue -= 1;
		
		maxValue /= scalingFactor;
		
		if ((displayValue < minValue) || (displayValue > maxValue))
		{
			 this.selectAll();
			 this.requestFocus();
			 throw new ValueFormatException("Value must be between " + minValue + " and " + maxValue + ".");
		}
		
		
		long storedValue = displayValue*scalingFactor;
		String valueString = new String();
		valueString += storedValue;
		
		theRecord.setBytesFromString(valueString);
		
	}
	
	
	
	/**
	*	Write supplied value, rather than that currently displayed, into associated 
	*	AirportInfoRecord window.
	*/
	
	public void writeValue(String newValue)
		throws ValueFormatException
	{
		setText(newValue);
		writeValue();
		refreshDisplay();
	}
	
	
	
	
}