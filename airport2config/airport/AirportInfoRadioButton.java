/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import java.util.*;
import javax.swing.*;




/**
*	Radio button displaying state of and setting value of specific piece of Airport info. 
*	Provides facilities for reading from / writing to multiple AirportInfoRecords, and 
*	specifying the value for each corresponding to the selected state.
*/

public class AirportInfoRadioButton extends JRadioButton
								implements AirportInfoComponent
{
	private Vector theRecords;
	
	
	private class RecordSettings
	{
		public AirportInfoRecord theRecord;
		public String selectedValue;
		
		public void writeValue()
			throws ValueFormatException
		{
			theRecord.setBytesFromString(selectedValue);
		}
	}
	
	
	
	/**
	*	Create new radio button with given label and no associated AirportInfoRecords.
	*/
	
	public AirportInfoRadioButton(String label)
	{
		super(label);
		theRecords = new Vector();
		
		//redisplay done automatically when new record added
	}
	
	
	
	/**
	*	Create new radio button with given label and supplied AirportInfoRecord, with 
	*	selectedValue string stored for subsequent writing (as appropriate). State of
	*	radio button set according to value in underlying byte block.
	*/
	
	public AirportInfoRadioButton(String label, AirportInfoRecord theRecord, String selectedValue)

	{
		super(label);
		theRecords = new Vector();
		addInfoRecord(theRecord, selectedValue);
		//redisplay done automatically by addInfoRecord
	}
	
	

	/**
	*	Add additional AirportInfoRecord to list of byte block fields, with selectedValue
	*	string stored for subsequent writing (as appropriate). State of checkbox set according 
	*	to value in all underlying byte blocks - if inconsistent, set to unselected.
	*/
	
	public void addInfoRecord(AirportInfoRecord theRecord, String selectedValue)
	{
		RecordSettings recordSettings = new RecordSettings();
		recordSettings.theRecord = theRecord;
		recordSettings.selectedValue = selectedValue;
		
		theRecords.insertElementAt(recordSettings, theRecords.size());
		refreshDisplay();
	}
		
	
	
	
	public void refreshDisplay()
	{
		boolean selected = true;
		
		Enumeration elements = theRecords.elements();
		
		while (elements.hasMoreElements())
		{
			RecordSettings nextSettings = (RecordSettings)elements.nextElement();
			String currentValue = nextSettings.theRecord.toString();
			String selectedValue = nextSettings.selectedValue;
			
			// convert to integers to compare - but assume use hex for selectedValue!
			try
			{
				int selectedInt = Integer.parseInt(selectedValue, 16);
				int currentInt;
				
				if(nextSettings.theRecord.dataType == AirportInfoRecord.UNSIGNED_INTEGER)
				{
					// the toString method returns a base-10 value string
					currentInt = Integer.parseInt(currentValue, 10);
				}
				else
				{
					// others, either BYTE or BYTE_STRING, return a base-16 value
					currentInt = Integer.parseInt(currentValue, 16);
				}
					
				if(currentInt != selectedInt)
					selected = false;
			
			}
			catch (NumberFormatException e)
			{
				System.out.println("Radio button " + this.getLabel() + ", number format exception");
				selected = false;
			}
		}
		
		this.setSelected(selected);
		
	}
	
	
	
	
	/**
	*	Write values to all of associated AirportInfoRecords if checkbox selected.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		boolean selected = this.isSelected();
		
		if (selected)
		{
			Enumeration elements = theRecords.elements();
			
			while (elements.hasMoreElements())
			{
				RecordSettings nextSettings = (RecordSettings)elements.nextElement();
				nextSettings.writeValue();
			}
			
		}
	}
	
	
}