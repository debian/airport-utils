/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
*	Panel maintaining configuration information for Ethernet network connection.
*/

public class AirportPPPoEConfigPanel extends AirportInfoPanel
										implements ActionListener
{
	
	private JButton loginButton;
	private ButtonGroup connectionGroup;
	private AirportInfoRadioButton pppoeAlwaysStayConnectedButton, pppoeAutomaticConnectButton, pppoeManualConnectButton;
	private AirportInfoLabelledScaledValueField pppoeDisconnectTimeoutField;
	private AirportInfo theInfo;
	private AirportInfoPanel pppoeDisconnectTimeoutPanel;
	
	/**
	*	Create new panel with information from supplied AirportInfo object.
	*/
	
	public 	AirportPPPoEConfigPanel(AirportInfo theInfo)
	{
		this.theInfo = theInfo;
		
		pppoeAlwaysStayConnectedButton = new AirportInfoRadioButton("Always stay connected");
		pppoeAlwaysStayConnectedButton.addInfoRecord(theInfo.get("peSC"), "01");
		pppoeAlwaysStayConnectedButton.addInfoRecord(theInfo.get("peAC"), "01");
		pppoeAlwaysStayConnectedButton.addInfoRecord(theInfo.get("peID"), "0");
		
		pppoeAutomaticConnectButton = new AirportInfoRadioButton("Connect automatically");
		pppoeAutomaticConnectButton.addInfoRecord(theInfo.get("peAC"), "01");
		pppoeAutomaticConnectButton.addInfoRecord(theInfo.get("peSC"), "00");
		
		pppoeManualConnectButton = new AirportInfoRadioButton("Don't connect automatically");
		pppoeManualConnectButton.addInfoRecord(theInfo.get("peAC"), "00");
		pppoeManualConnectButton.addInfoRecord(theInfo.get("peSC"), "00");
		
		connectionGroup = new ButtonGroup();
		connectionGroup.add(pppoeAlwaysStayConnectedButton);
		connectionGroup.add(pppoeAutomaticConnectButton);
		connectionGroup.add(pppoeManualConnectButton);
		
		pppoeDisconnectTimeoutField = new AirportInfoLabelledScaledValueField("PPPoE idle timeout (minutes; 0 = never)", theInfo.get("peID"), 60);
		
		setUpDisplay();
	}
		
		
		
		
		
	private void setUpDisplay()
	{
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(1,1,1,1);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		JLabel theLabel;
		
		
		// Button to show login panel when pressed...
		loginButton = new JButton("Username/Password");
		loginButton.setActionCommand("show login panel");
		loginButton.addActionListener(this);
		
		
		// panel for PPPoE timeout field
		pppoeDisconnectTimeoutPanel = new AirportInfoPanel();
		pppoeDisconnectTimeoutPanel.add(pppoeDisconnectTimeoutField);
		
		
		
		this.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 1;
		theLayout.setConstraints(pppoeAlwaysStayConnectedButton, c);
		this.add(pppoeAlwaysStayConnectedButton);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(pppoeAutomaticConnectButton, c);
		this.add(pppoeAutomaticConnectButton);
		
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(pppoeManualConnectButton, c);
		this.add(pppoeManualConnectButton);
		
		c.gridx = 2;
		c.gridy = 2;
		c.gridheight = 3;
		c.anchor = GridBagConstraints.CENTER;
		theLayout.setConstraints(loginButton, c);
		this.add(loginButton);
		
		c.gridx = 1;
		c.gridy = 5;
		c.gridwidth = 2;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(pppoeDisconnectTimeoutPanel, c);
		this.add(pppoeDisconnectTimeoutPanel);
		
		
		// make pppoeDisconnectTimeoutPanel enabling dependent on connect status
		pppoeAutomaticConnectButton.addItemListener(pppoeDisconnectTimeoutPanel);
		pppoeManualConnectButton.addItemListener(pppoeDisconnectTimeoutPanel);
		
		// pppoeDisconnectTimeoutPanel.setEnabled(pppoeAutomaticConnectButton.isSelected() || pppoeManualConnectButton.isSelected());
		pppoeDisconnectTimeoutPanel.setEnabled(pppoeAutomaticConnectButton.isSelected() || pppoeManualConnectButton.isSelected());
		
	}
	
	
	
	
	/**
	*	Override base class method to enable and disable contained components.
	*/
	
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		
		if (enabled == true)
		{
			// if enabling, set enabling of pppoeDisconnectTimeoutPanel
			pppoeDisconnectTimeoutPanel.setEnabled(pppoeAutomaticConnectButton.isSelected() || pppoeManualConnectButton.isSelected());
		}
	}
	
	
	
	
	
	
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("show login panel"))
		{
			// get top-level Frame to associate modal dialog with so will be minimzed and
			// maximized correctly.
			Component owner = this;
			while ( !((owner == null) || (owner instanceof Frame)) )
				owner = owner.getParent();
			Frame owningFrame = (Frame)owner;
			
			// create and display modal dialog holding dial-up username / password and login script
			AirportPPPoELoginInfoDialog loginDialog = new AirportPPPoELoginInfoDialog(owningFrame, "PPPoE Login Information", true, theInfo);
			loginDialog.pack();
			
			// tweak app size to make it a little larger than necessary, to address the
			// "shrunken textfields" problem arising from the layout manager packing stuff
			// a little too tightly.
			Dimension dim = loginDialog.getSize();
			dim.height += 20;
			dim.width += 20;
			loginDialog.setSize(dim);
			
			loginDialog.show();
		}
		
	}
	
	

}