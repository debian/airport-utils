/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;



/**
*	Sub-panel of bridging panel which maintains range of addresses served by base station when 
*	acting as DHCP or NAT server.
*/

public class AirportDHCPRangePanel extends AirportInfoPanel
{
	private AirportInfoTextField dhcpStartField, dhcpEndField;
	
	
	/**
	*	Create new panel with start and end address for range taken from byte block referenced
	*	in supplied AirportInfo object.
	*/
	
	public AirportDHCPRangePanel(AirportInfo theInfo)
	{
		
		dhcpStartField = new AirportInfoTextField(theInfo.get("dhBg"));
		dhcpEndField = new AirportInfoTextField(theInfo.get("dhEn"));
		
		setUpDisplay();
	}
	
	
	
	
	/**
	*	Create new panel with specified start and end address for range. Used to set default values
	*	when required (e.g., range for NAT).
	*/
	
	public AirportDHCPRangePanel(AirportInfo theInfo, String defaultStartAddress, String defaultEndAddress)
	// use supplied default values, unless empty string supplied, whence use retrieved value
	{
		
		dhcpStartField = new AirportInfoTextField(theInfo.get("dhBg"));
		dhcpStartField.setText(defaultStartAddress);
		
		dhcpEndField = new AirportInfoTextField(theInfo.get("dhEn"));
		dhcpEndField.setText(defaultEndAddress);
		
		setUpDisplay();
	}
	
	
	
	
	private void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		JLabel theLabel;
		
		
		// textfields; put in panel
		
		this.setLayout(theLayout);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		theLabel = new JLabel("Address range start");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 1;
		c.gridy = 2;
		theLabel = new JLabel("Address range end");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(dhcpStartField, c);
		this.add(dhcpStartField);
		
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(dhcpEndField, c);
		this.add(dhcpEndField);
		
		
		
	}
	
	
	
}