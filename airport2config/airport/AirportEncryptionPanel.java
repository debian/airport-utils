/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains wireless LAN information: network name, channel, and encryption switch and key.
*	Note that encryption key is supplied as 10-digit hex string (with no "0x" prefix).
*/

public class AirportEncryptionPanel extends AirportInfoPanel
{
	private AirportInfoLabelledTextField encryptionKeyField;
	private AirportInfoRadioButton noEncryptionButton, keySize40Button, keySize128Button;
	private ButtonGroup buttonGroup;
	//private AirportInfoCheckBox allowUnencryptedDataBox;
	AirportInfo theInfo;
	
	public AirportEncryptionPanel(AirportInfo theInfo)
	{
		this.theInfo = theInfo;
		
		noEncryptionButton = new AirportInfoRadioButton("No encryption");
		noEncryptionButton.addInfoRecord(theInfo.get("raWM"), "00000000");
		
		keySize40Button = new AirportInfoRadioButton("Use 40-bit encryption");
		keySize40Button.addInfoRecord(theInfo.get("raWM"), "00000001");
		
		keySize128Button = new AirportInfoRadioButton("Use 128-bit encryption");
		keySize128Button.addInfoRecord(theInfo.get("raWM"), "00000002");
		
		
		buttonGroup = new ButtonGroup();
		buttonGroup.add(noEncryptionButton);
		buttonGroup.add(keySize40Button);
		buttonGroup.add(keySize128Button);
		
		
		encryptionKeyField = new AirportInfoLabelledTextField("Encryption key:", theInfo.get("raWE"), 20);
		
		/**
		*	Deny unencrypted data flag:	
		*		byte 6*16 + 9, mask 0x80 (bit 8)
		*			00 = allow all packets
		*			80 = allow only encrypted packets
		*/
		
		//allowUnencryptedDataBox = new AirportInfoCheckBox("Allow unencrypted data");
		//allowUnencryptedDataBox.addInfoRecord(theInfo.get("Deny unencrypted data flag"), "00", "80");
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		AirportInfoPanel encryptionPanel = new AirportInfoPanel();
		encryptionPanel.add(encryptionKeyField);
		
		
		
		this.setLayout(theLayout);
		
		
		JLabel theLabel;
		
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(noEncryptionButton, c);
		this.add(noEncryptionButton);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(keySize40Button, c);
		this.add(keySize40Button);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(keySize128Button, c);
		this.add(keySize128Button);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(encryptionPanel, c);
		this.add(encryptionPanel);
		
		
		keySize40Button.addItemListener(encryptionPanel);
		keySize128Button.addItemListener(encryptionPanel);
		encryptionPanel.setEnabled(keySize40Button.isSelected() || keySize128Button.isSelected());
		
		
	}
	
	
	
	public void writeValue()
		throws ValueFormatException
	{
		// set maxSize of encryption key field based on button selection
		AirportInfoRecord keyRecord = theInfo.get("raWE");
		
		// set max size accordingly; write operation will throw exception if needed
		if (keySize40Button.isSelected())
		{
			keyRecord.maxLength = 5;
			encryptionKeyField.writeValue();
		}
		else if (keySize128Button.isSelected())
		{
			keyRecord.maxLength = 13;
			encryptionKeyField.writeValue();
		}
		
		super.writeValue();
	}
	
	
	
}