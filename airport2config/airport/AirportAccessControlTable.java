/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
package airport;


import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
*	Handles display and updating of MAC addresses and host names used for access control on the
*	wireless LAN.
*/

public class AirportAccessControlTable extends AirportInfoPanel
{
	private AirportInfo airportInfo;
	private Vector addressVector;
	private JTable table;
	private AbstractTableModel tableModel;
	
	
	/**
	*	Table model which maintains list of MAC addresses and host names.
	*/
	
	private class AccessAddressTableModel extends AbstractTableModel
	{
		public int getColumnCount() 
		{ 
			return 2; 
		}
		
		public int getRowCount() 
		{ 
			return AirportInfo.MAX_NUM_MAC_ADDRESSES;
		}
		
		public boolean isCellEditable(int row, int col) 
		{ 
			if (row <= addressVector.size())
				return true;
			else
				return false;
		}
		
		public String getColumnName(int col) 
		{ 
			if (col == 0)
				return "MAC address";
			else
				return "Host name (opt)";
		}
		
		public Object getValueAt(int row, int col)
		{
			if (row < addressVector.size())
				return ((String[])addressVector.elementAt(row))[col];
			else
				return "";
		}
		
		public void setValueAt(Object newValue, int row, int col) 
		{
			if (newValue instanceof String)
			{
				if (row < addressVector.size())
				{
					String[] addressEntry = (String[])addressVector.elementAt(row);
					addressEntry[col] = (String)newValue;
				}
				else
				{
					String[] addressEntry = {"",""};
					addressEntry[col] = (String)newValue;
					addressVector.insertElementAt(addressEntry, addressVector.size());
				}
			}
		}
		
	}
	
	
	
	
	
	/**
	*	Create new table based on data in airportInfo.
	*/
	
	public AirportAccessControlTable(AirportInfo airportInfo)
	{
		this.airportInfo = airportInfo;
		setUpDisplay();
	}
	
	
	
	private void setUpDisplay()
	{
		refreshDisplay();
	}
	
	
	
	
	
	
	
	
	
	/**
	*	Write the MAC addresses and hostnames in the list as concatenated 40-byte strings. Format is:
	*		- first 8 bytes get number of port maps
	*		- next 8 bytes are 0
	*		- following are concatenated MAC address / hostname blocks
	*	@throws ValueFormatException Thrown if either a MAC address is malformed, or hostname
	*	is too long. Also sets editting focus to the offending cell.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		
		
		// first, record changes if cell currently being edited
		TableCellEditor editor = table.getCellEditor(); 
		if(editor != null)
			editor.stopCellEditing();
		
		
		
		int numMacAddresses = 0;
		
		Vector addressRecordVector = new Vector();
		Vector nameRecordVector = new Vector();
		
		
		
		for (int i = 0; i < addressVector.size(); i++)
		{
			
			try
			{
				// these only get augmented if address is OK (and non-null)
				if (! ((String[])this.addressVector.elementAt(i))[0].equals(""))
				{
					// create a new info record to qualify the address and convert to bytes
					AirportInfoRecord addressRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.BYTE_STRING, AirportInfoRecord.UNENCRYPTED, 6);
					addressRecord.setBytesFromString(((String[])this.addressVector.elementAt(i))[0]);
					addressRecordVector.add(addressRecord);
					
					numMacAddresses++;
				}
				else
				{
					// skip this record
					continue;
				}
				
			}
			catch (ValueFormatException e)
			{
				table.editCellAt(i,0);
				throw e;
			}
			
			try
			{
				
				AirportInfoRecord nameRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.CHAR_STRING, AirportInfoRecord.UNENCRYPTED, 32);
				nameRecord.setBytesFromString(((String[])this.addressVector.elementAt(i))[1]);
				
				// now expand size of byte array to 34 bytes by appending 0's
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				
				try
				{
					outStream.write(nameRecord.value);
					
					byte[] padBytes = new byte[34 - nameRecord.value.length];
					outStream.write(padBytes);
				}
				catch (IOException e)
				{
					// shouldn't ever happen...
				}
				
				nameRecord.value = outStream.toByteArray();
				
				nameRecordVector.add(nameRecord);
			}
			
			catch (ValueFormatException e)
			{
				table.editCellAt(i,1);
				throw e;
			}
			
			
				
		}
		
		
		// now create concatenation
		try
		{
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		
			// write 4-byte padding of 0's
			byte[] zeroBytes = new byte[4];
			outStream.write(zeroBytes);
			
			// do length; use info record to create 4-byte value
			AirportInfoRecord sizeRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.UNSIGNED_INTEGER, AirportInfoRecord.UNENCRYPTED, 4);
			sizeRecord.setBytesFromString(Integer.toString(numMacAddresses));
			outStream.write(sizeRecord.value);
			
			// write 8-byte padding of 0's
			zeroBytes = new byte[8];
			outStream.write(zeroBytes);
			
			// now write addresses and hostnames
			for (int i = 0; i < numMacAddresses; i++)
			{
				outStream.write(((AirportInfoRecord)addressRecordVector.elementAt(i)).value);
				outStream.write(((AirportInfoRecord)nameRecordVector.elementAt(i)).value);
			}
			
			// now use this to set the value of the associated info record
			AirportInfoRecord accessControlRecord = airportInfo.get("acTa");
			accessControlRecord.setValue(outStream.toByteArray());
			
			
			// also, set access control switch: on if any Mac entries, off otherwise
			/**
			*	Access control switch:  
			*		00 = no access control
			*		01 = access control used
			*/
			
			String accessSwitchValue = new String();
			
			if (numMacAddresses > 0)
				accessSwitchValue = "01";
			else
				accessSwitchValue = "00";
				
			AirportInfoRecord accessControlSwitchRecord = airportInfo.get("acEn");
			accessControlSwitchRecord.setBytesFromString(accessSwitchValue);
		
		}
		catch (IOException e)
		{
			// shouldn't ever happen...
		}
		
	}
		
		
		
	
	/**
	*	Refresh the display based on the current data in the associated info record
	*/
	
	public void refreshDisplay()
	{
		// create new addressVector, with appropriate size
		addressVector = new Vector();
		
		AirportInfoRecord accessControlRecord = airportInfo.get("acTa");
		byte[] accessControlBytes = accessControlRecord.getValue();
		
		DataInputStream inStream = new DataInputStream(new ByteArrayInputStream(accessControlBytes));
		
		try
		{
			// Read first 4 bytes of 0 padding
			inStream.skip(4);
			
			// Read in number of Mac addresses (next 4 bytes)
			int numMacAddresses = inStream.readInt();
			
			// Read next 8 bytes of 0 padding
			inStream.skip(8);
			
			
			for (int i = 0; i < numMacAddresses; i++)
			{
				byte[] addressBytes = new byte[6];
				inStream.read(addressBytes);
				AirportInfoRecord addressRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.BYTE_STRING, AirportInfoRecord.UNENCRYPTED, 6, addressBytes);
				
				byte[] nameBytes = new byte[34];
				inStream.read(nameBytes);
				AirportInfoRecord nameRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.CHAR_STRING, AirportInfoRecord.UNENCRYPTED, 34, nameBytes);
				
				String[] addressEntry = new String[2];
				addressEntry[0] = addressRecord.toString();
				addressEntry[1] = nameRecord.toString();
				addressVector.insertElementAt(addressEntry, addressVector.size());
			}
			
			this.removeAll();
			tableModel = new AccessAddressTableModel();
			table = new JTable(tableModel);
			table.setCellSelectionEnabled(true);
			table.setPreferredScrollableViewportSize(new Dimension(350,200));
			JScrollPane scrollPane = new JScrollPane(table);
			this.add(scrollPane);
		
		}
		catch (IOException e)
		{
			// shouldn't ever happen...
		}
		
	}
	
	
	
	

}