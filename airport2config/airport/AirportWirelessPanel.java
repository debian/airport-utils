/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains wireless LAN information: network name, channel, and encryption switch and key.
*	Note that encryption key is supplied as 10-digit hex string (with no "0x" prefix).
*/

public class AirportWirelessPanel extends AirportInfoPanel
{
	private AirportInfoLabelledTextField networkNameField;
	private AirportInfoCheckBox closedNetworkBox, microwaveRobustnessBox;
	private AirportInfoComboBox channelBox;
	private AirportEncryptionPanel encryptionPanel;
	
	
	public AirportWirelessPanel(AirportInfo theInfo)
	{
		
		networkNameField = new AirportInfoLabelledTextField("Network name", theInfo.get("raNm"));
		
		closedNetworkBox = new AirportInfoCheckBox("Closed network");
		closedNetworkBox.addInfoRecord(theInfo.get("raCl"), "01", "00");
		
		microwaveRobustnessBox = new AirportInfoCheckBox("Microwave robustness");
		microwaveRobustnessBox.addInfoRecord(theInfo.get("raRo"), "01", "00");
		
		
		channelBox = new AirportInfoComboBox("Channel", theInfo.get("raCh"));
		for (int i = 1; i < 12; i++)
			channelBox.addItemAndValue(Integer.toString(i), Integer.toString(i));
		
		
		encryptionPanel = new AirportEncryptionPanel(theInfo);
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		
		
		this.setLayout(theLayout);
		
		
		JLabel theLabel;
		
		
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(networkNameField, c);
		this.add(networkNameField);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(channelBox, c);
		this.add(channelBox);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(closedNetworkBox, c);
		this.add(closedNetworkBox);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(microwaveRobustnessBox, c);
		this.add(microwaveRobustnessBox);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(encryptionPanel, c);
		this.add(encryptionPanel);
		
		
	}
	
	
	
	
	
}