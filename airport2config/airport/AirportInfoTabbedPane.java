/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;




/**
*	Tabbed pane holding panels which present various configuration information.
*/

public class AirportInfoTabbedPane extends JTabbedPane
{

	
	/**
	*	Create new tabbed pane holding panels which present various configuration information.
	*	The supplied AirportInfo instance is passed to the contained panels.
	*/
	
	public AirportInfoTabbedPane(AirportInfo airportInfo)
	{
		setUpDisplay(airportInfo);
	}
	
	
	
	
	private void setUpDisplay(AirportInfo airportInfo)
	{
		
		AirportInfoPanel mainPanel = new AirportMainPanel(airportInfo);
		AirportInfoPanel wirelessPanel = new AirportWirelessPanel(airportInfo);
		AirportInfoPanel connectionPanel = new AirportNetworkPanel(airportInfo);
		AirportInfoPanel bridgingPanel = new AirportBridgingPanel(airportInfo);
		AirportInfoPanel dhcpPanel = new AirportDHCPPanel(airportInfo);
		AirportInfoPanel accessTablePanel = new AirportAccessControlTable(airportInfo);
		AirportInfoPanel portMapTablePanel = new AirportPortMappingPanel(airportInfo);
		AirportInfoPanel advancedPanel = new AirportAdvancedPanel(airportInfo);
		
		//AirportInfoPanel loginPanel = new AirportLoginInfoPanel(airportInfo);
		
		this.addTab("Main", mainPanel);
		this.addTab("Wireless LAN Settings", wirelessPanel);
		this.addTab("Network Connection", connectionPanel);
		this.addTab("Bridging Functions", bridgingPanel);
		this.addTab("DHCP Functions", dhcpPanel);
		this.addTab("Access Control", accessTablePanel);
		this.addTab("Port Mappings", portMapTablePanel);
		this.addTab("Advanced Settings", advancedPanel);
		
	}
	
	
	
	
	
	
	/**
	*	Instruct all contained panels to write their settings to the associated AirportInfoRecords, 
	*	and update displays accordingly. (Note that only the enabled AirportInfoComponents contained
	*	in the panels will write their values.)
	*/
	
	public void writeData()
		throws ValueFormatException
	{
		int numTabs = this.getTabCount();
		
		for (int i = 0; i < numTabs; i++)
		{
			Component nextPanel = this.getComponentAt(i);
			
			if (nextPanel instanceof AirportInfoPanel)
			{
				
				try
				{
					((AirportInfoPanel)nextPanel).writeValue();
				}
				catch (ValueFormatException e)
				{
					// display offending panel; component will have already requested focus
					this.setSelectedIndex(i);
					
					throw e;	// terminate operation
				}
				
			}
			
		}
		
	}
	

}