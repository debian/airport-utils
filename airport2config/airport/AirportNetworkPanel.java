/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;




/**
*	Panel which maintains data relevant to base station network connection. Has radio button selection
*	for modem vs Ethernet connection of base station to external network, plus DNS server fields.
*	Has subpanels for configuring the network connection: modem panel with settings for phone number,
*	username and password, and the like; Ethernet panel with choice of manual or DHCP config, plus
*	settings for base station IP address, router address and mask. These subpanels are enabled only
*	when associated connection method is selected.
*/

public class AirportNetworkPanel extends AirportInfoPanel
									implements ItemListener
									
{
	private AirportInfoRadioButton modemButton, ethernetButton;
	
	private AirportInfoTextField primaryDNSField, secondaryDNSField, domainNameField;
	
	private AirportInfoPanel ethernetConfigPanel, modemConfigPanel, dnsPanel;
	
	private ButtonGroup modemEthernetButtonGroup;
	
	
	
	
	public AirportNetworkPanel(AirportInfo theInfo)
	{
		
		
		modemButton = new AirportInfoRadioButton("Connect to network through modem");
		modemButton.addInfoRecord(theInfo.get("waIn"), "00000004");
		modemButton.addInfoRecord(theInfo.get("waCV"), "00000900");
		
		ethernetButton = new AirportInfoRadioButton("Connect to network through Ethernet port");
		ethernetButton.addInfoRecord(theInfo.get("waIn"), "00000010");
		// CV field set in sub-panel
		
		modemEthernetButtonGroup = new ButtonGroup();
		modemEthernetButtonGroup.add(modemButton);
		modemEthernetButtonGroup.add(ethernetButton);
		
		
		primaryDNSField = new AirportInfoTextField(theInfo.get("waD1"));
		secondaryDNSField = new AirportInfoTextField(theInfo.get("waD2"));
		domainNameField = new AirportInfoTextField(theInfo.get("waDN"));
		
		ethernetConfigPanel = new AirportEthernetConfigPanel(theInfo);
		
		modemConfigPanel = new AirportModemConfigPanel(theInfo);
		
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		JLabel label;
		
		
		/*
		// create and lay out button panel
		
		AirportInfoPanel buttonPanel = new AirportInfoPanel();
		buttonPanel.setLayout(theLayout);
		
		// add components
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(modemButton, c);
		buttonPanel.add(modemButton);
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(ethernetButton, c);
		buttonPanel.add(ethernetButton);
		*/
		
		
		
		
		
		// create and lay out DNS panel
		
		dnsPanel = new AirportInfoPanel();
		dnsPanel.setLayout(theLayout);
		
		// add components
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		label = new JLabel("Primary DNS server IP address");
		theLayout.setConstraints(label, c);
		dnsPanel.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(primaryDNSField, c);
		dnsPanel.add(primaryDNSField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		label = new JLabel("Secondary DNS server IP address");
		theLayout.setConstraints(label, c);
		dnsPanel.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(secondaryDNSField, c);
		dnsPanel.add(secondaryDNSField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 3;
		label = new JLabel("Domain name");
		theLayout.setConstraints(label, c);
		dnsPanel.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(domainNameField, c);
		dnsPanel.add(domainNameField);
		
		
		
		
		
		// now add panels to this
		
		this.setLayout(theLayout);
		
		c.anchor = GridBagConstraints.CENTER;
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(ethernetButton, c);
		this.add(ethernetButton);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(modemButton, c);
		this.add(modemButton);
		
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(ethernetConfigPanel, c);
		this.add(ethernetConfigPanel);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(modemConfigPanel, c);
		this.add(modemConfigPanel);
		
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(dnsPanel, c);
		this.add(dnsPanel);
		
		// make modem and ethernet config panel enablings dependent on button settings
		modemButton.addItemListener(modemConfigPanel);
		modemButton.addItemListener(this);
		modemConfigPanel.setEnabled(modemButton.isSelected());
		modemConfigPanel.setVisible(modemButton.isSelected());
		
		ethernetButton.addItemListener(ethernetConfigPanel);
		ethernetButton.addItemListener(this);
		ethernetConfigPanel.setEnabled(ethernetButton.isSelected());
		ethernetConfigPanel.setVisible(ethernetButton.isSelected());
		
		
	}
	
	
	
	
	/**
	*	Used to trigger contained panels to be visible or not; panels themselves
	*	are responsible for enabling/disabling themselves by virtue of being registered
	*	as ItemListeners themselves.
	*/
	
	public void itemStateChanged(ItemEvent e)
	{
		
		modemConfigPanel.setVisible(modemButton.isSelected());
		ethernetConfigPanel.setVisible(ethernetButton.isSelected());
		
	}
		
	
	
	
}