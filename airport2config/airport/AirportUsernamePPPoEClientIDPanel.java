/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;





/**
*	Utility subpanel which maintains dial-up username and password.
*/

public class AirportUsernamePPPoEClientIDPanel extends AirportInfoPanel
{
	
	private JTextField pppoeUsernameField, pppoePasswordField, pppoeClientIDField;
	
	
	public AirportUsernamePPPoEClientIDPanel(AirportInfo airportInfo)
	{
		super();
		
		pppoeUsernameField = new AirportInfoTextField(airportInfo.get("peUN"), 24);
		pppoePasswordField = new AirportInfoTextField(airportInfo.get("pePW"), 24);
		pppoeClientIDField = new AirportInfoTextField(airportInfo.get("peSN"), 24);
		
		setUpDisplay();
	}
	
	
	private void setUpDisplay()
	{
		
		JLabel usernameLabel = new JLabel("User name");
		JLabel passwordLabel = new JLabel("Password");
		JLabel pppoeClientIDLabel = new JLabel("Service name");
		
		// set params for layout manager
		GridBagLayout theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.setLayout(theLayout);
		
		
		// add stuff
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(usernameLabel, c);
		this.add(usernameLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(pppoeUsernameField, c);
		this.add(pppoeUsernameField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(passwordLabel, c);
		this.add(passwordLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(pppoePasswordField, c);
		this.add(pppoePasswordField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(pppoeClientIDLabel, c);
		this.add(pppoeClientIDLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(pppoeClientIDField, c);
		this.add(pppoeClientIDField);
		
		
		refreshDisplay();
		
		
	}
	
	
		
}