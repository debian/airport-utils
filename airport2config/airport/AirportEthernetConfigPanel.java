/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
*	Panel maintaining configuration information for Ethernet network connection.
*/

public class AirportEthernetConfigPanel extends AirportInfoPanel
										implements ItemListener
{
	
	private AirportInfoRadioButton dhcpConfigButton, manualConfigButton, pppoeButton;
	private AirportInfoPanel manualIPConfigPanel, ethernetButtonPanel, dhcpIPConfigPanel, dhcpClientIDPanel, pppoeClientIDPanel;
	private AirportInfoTextField dhcpClientIDField;
	private ButtonGroup dhcpManualButtonGroup;
	
	
	/**
	*	Create new panel with information from supplied AirportInfo object.
	*/
	
	public 	AirportEthernetConfigPanel(AirportInfo theInfo)
	{
		/**
		*	Base station configuration mode switch:  byte 1*16 + 8, MASK 20
		*			00 = manual or modem station config
		*			20 = DHCP station config
		*/	
		
		dhcpConfigButton = new AirportInfoRadioButton("DHCP configuration of base station");
		dhcpConfigButton.addInfoRecord(theInfo.get("waCV"), "00000300");
		
		manualConfigButton = new AirportInfoRadioButton("Manual configuration of base station");
		manualConfigButton.addInfoRecord(theInfo.get("waCV"), "00000400");
		
		pppoeButton = new AirportInfoRadioButton("PPPoE configuration of base station");
		pppoeButton.addInfoRecord(theInfo.get("waCV"), "00000900");
		
		dhcpManualButtonGroup = new ButtonGroup();
		dhcpManualButtonGroup.add(dhcpConfigButton);
		dhcpManualButtonGroup.add(manualConfigButton);
		dhcpManualButtonGroup.add(pppoeButton);
		
		manualIPConfigPanel = new AirportIPConfigPanel(theInfo);
		
		dhcpClientIDField = new AirportInfoTextField(theInfo.get("waDC"), 20);
		
		pppoeClientIDPanel = new AirportPPPoEConfigPanel(theInfo);
		
		
		/*
		Set base station IP address and router IP address:
		
		.1.3.6.1.4.1.762.2.3.1.1.5	
			Base station IP address: byte 6*16 + 11
			Router IP address and mask: byte 7*16 + 1
				Manual config: 
					set values as specified
				DHCP config, modem config: 
					set default IP address
					router is left as is
		*/
		
		// dhcp config panel just used to write default IP info into block when
		// dhcp config desired; panel not displayed!
		//String defaultIPAddress = "192.42.249.12";
		//dhcpIPConfigPanel = new AirportIPConfigPanel(theInfo, defaultIPAddress, "", "");
		// just use retrieved info for all fields
		dhcpIPConfigPanel = new AirportIPConfigPanel(theInfo, "", "", "");
		
		
		setUpDisplay();
	}
		
		
		
		
		
	private void setUpDisplay()
	{
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		JLabel theLabel;
		
		
		
		dhcpClientIDPanel = new AirportInfoPanel();
		dhcpClientIDPanel.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.EAST;
		theLabel = new JLabel("DHCP client ID");
		theLayout.setConstraints(theLabel, c);
		dhcpClientIDPanel.add(theLabel);
		
		c.gridx = 2;
		c.gridy = 1;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(dhcpClientIDField, c);
		dhcpClientIDPanel.add(dhcpClientIDField);
		
		
		
		
		ethernetButtonPanel = new AirportInfoPanel();
		ethernetButtonPanel.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(dhcpConfigButton, c);
		ethernetButtonPanel.add(dhcpConfigButton);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(manualConfigButton, c);
		ethernetButtonPanel.add(manualConfigButton);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(pppoeButton, c);
		ethernetButtonPanel.add(pppoeButton);
		
		
		
		this.setLayout(theLayout);
		
		// add components
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(ethernetButtonPanel, c);
		this.add(ethernetButtonPanel);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(manualIPConfigPanel, c);
		this.add(manualIPConfigPanel);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(dhcpClientIDPanel, c);
		this.add(dhcpClientIDPanel);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(pppoeClientIDPanel, c);
		this.add(pppoeClientIDPanel);
		
		// never visible
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(dhcpIPConfigPanel, c);
		this.add(dhcpIPConfigPanel);
		
		// panel never visible; just used to set default data when selected
		dhcpIPConfigPanel.setVisible(false);
		
		
		// make manual config panel visibility and enabling dependent on button setting
		manualConfigButton.addItemListener(manualIPConfigPanel);
		manualConfigButton.addItemListener(this);
		manualIPConfigPanel.setEnabled(manualConfigButton.isSelected());
		manualIPConfigPanel.setVisible(manualConfigButton.isSelected());
		
		// make dhcp config panel enabling dependent on button setting (but never visible)
		dhcpConfigButton.addItemListener(dhcpIPConfigPanel);
		pppoeButton.addItemListener(dhcpIPConfigPanel);
		dhcpIPConfigPanel.setEnabled(dhcpConfigButton.isSelected() || pppoeButton.isSelected());
		
		// make DHCP client ID panel visibility and enabling dependent on button setting
		dhcpConfigButton.addItemListener(dhcpClientIDPanel);
		dhcpConfigButton.addItemListener(this);
		dhcpClientIDPanel.setEnabled(dhcpConfigButton.isSelected());
		dhcpClientIDPanel.setVisible(dhcpConfigButton.isSelected());
		
		// make PPPoE client ID panel visibility and enabling dependent on button setting
		pppoeButton.addItemListener(pppoeClientIDPanel);
		pppoeButton.addItemListener(this);
		pppoeClientIDPanel.setEnabled(pppoeButton.isSelected());
		pppoeClientIDPanel.setVisible(pppoeButton.isSelected());
		
		
	}
	
	
	
	
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		
		// create special setting status for subpanels
		manualIPConfigPanel.setEnabled(enabled && manualConfigButton.isSelected());
		dhcpIPConfigPanel.setEnabled(enabled && (dhcpConfigButton.isSelected() || pppoeButton.isSelected()));
		dhcpClientIDPanel.setEnabled(enabled && dhcpConfigButton.isSelected());
		pppoeClientIDPanel.setEnabled(enabled && pppoeButton.isSelected());
	
	}
	
	
	
	
	/**
	*	Used to trigger contained panels to be visible or not; panels themselves
	*	are responsible for enabling/disabling themselves by virtue of being registered
	*	as ItemListeners themselves.
	*/
	
	public void itemStateChanged(ItemEvent e)
	{
		super.itemStateChanged(e);
		
		manualIPConfigPanel.setVisible(manualConfigButton.isSelected());
		dhcpClientIDPanel.setVisible(dhcpConfigButton.isSelected());
		pppoeClientIDPanel.setVisible(pppoeButton.isSelected());
		
	}
	


}