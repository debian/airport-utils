/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
package airport;


import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;


/**
*	Handles display and updating of IP addresses, masks and interface numbers used for SNMP access control.
*/

public class AirportSNMPAccessControlTable extends AirportInfoPanel
{
	private AirportInfo airportInfo;
	private Vector addressVector;
	private JTable table;
	private AbstractTableModel tableModel;
	private JComboBox interfaceComboBox;
	private Hashtable interfaceNameToNumberHashtable, interfaceNumberToNameHashtable;
		
	
	/**
	*	Table model which maintains list of IP addresses, masks and interface numbers.
	*/
	
	private class AccessAddressTableModel extends AbstractTableModel
	{
		public int getColumnCount() 
		{ 
			return 3; 
		}
		
		public int getRowCount() 
		{ 
			return AirportInfo.MAX_NUM_SNMP_ACCESS_ENTRIES;
		}
		
		public boolean isCellEditable(int row, int col) 
		{ 
			if (row <= addressVector.size())
				return true;
			else
				return false;
		}
		
		public String getColumnName(int col) 
		{ 
			switch (col)
			{
				case 0:
					return "IP address";
				case 1:
					return "Address mask";
				default:
					return "Interface";
			} 
		}
		
		
		/*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.
         */
         
        public Class getColumnClass(int column) 
        {
            String temp = "";
            return temp.getClass();
        }
        
		
		public Object getValueAt(int row, int col)
		{
			if (row < addressVector.size())
			{
				return ((String[])addressVector.elementAt(row))[col];
			}
			else
				return "";
		}
		
		public void setValueAt(Object newValue, int row, int col) 
		{
			if (row < addressVector.size())
			{
				String[] addressEntry = (String[])addressVector.elementAt(row);
				addressEntry[col] = (String)newValue;
			}
			else
			{
				String[] addressEntry = {"", "", ""};
				addressEntry[col] = (String)newValue;
				addressVector.insertElementAt(addressEntry, addressVector.size());
			}
			
			fireTableCellUpdated(row, col);
		
		}
		
	}
	
	
	
	
	
	
	
	/**
	*	Create new table based on data in airportInfo.
	*/
	
	public AirportSNMPAccessControlTable(AirportInfo airportInfo)
	{
		this.airportInfo = airportInfo;
		
		interfaceComboBox = new JComboBox();
		interfaceComboBox.addItem("All");
		interfaceComboBox.addItem("Ethernet");
		interfaceComboBox.addItem("Wireless");
		interfaceComboBox.addItem("Modem");
		
		interfaceNameToNumberHashtable = new Hashtable();
		interfaceNameToNumberHashtable.put("Ethernet", "0");
		interfaceNameToNumberHashtable.put("Wireless", "1");
		interfaceNameToNumberHashtable.put("Modem", "2");
		interfaceNameToNumberHashtable.put("All", "65535");
		
		interfaceNumberToNameHashtable = new Hashtable();
		interfaceNumberToNameHashtable.put("0", "Ethernet");
		interfaceNumberToNameHashtable.put("1", "Wireless");
		interfaceNumberToNameHashtable.put("2", "Modem");
		interfaceNumberToNameHashtable.put("65535", "All");
		
		
		setUpDisplay();
	}
	
	
	
	
	private void setUpDisplay()
	{
		refreshDisplay();
	}
	
	
	
	
	
	
	
	
	
	/**
	*	Write the IP addresses and masks in the list into the appropriate locations
	*	in the byte block referenced by the AirportInfo object supplied in the constructor.
	*	@throws ValueFormatException Thrown if an IP address or mask is malformed. Also sets 
	*	editting focus to the offending cell.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		
		int baseStartIndex, numRows, numCols, dataType;
		
		// first, record changes if cell currently being edited
		TableCellEditor editor = table.getCellEditor(); 
		if(editor != null)
			editor.stopCellEditing();
		
		
		
		ByteBlock baseBlock = airportInfo.baseBlock;
		
		// erase current blocks of info
		baseStartIndex = 0x0638;
		numRows = 1;	
		numCols = 10 * AirportInfo.MAX_NUM_SNMP_ACCESS_ENTRIES;
		dataType = AirportInfoRecord.BYTE_STRING;
		AirportInfoRecord portMapBlockRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		portMapBlockRecord.clearWindow();
		
		
		
		//create new info records
		
		/*
		*		
		*	SNMP access control list: require multi-block windows
		*
		*	0x0636:  number of entries (0-5)
		*	
		*	0x0638, 0x063a, 0x063c, 0x063e, 0x0640:  first two octets of IP addresses
		*	0x0642, 0x0644, 0x0646, 0x0648, 0x064a:  second two octets of IP addresses
		*	
		*	0x064c, 0x064e, 0x0650, 0x0652, 0x0654:  first two octets of masks
		*	0x0656, 0x0658, 0x065a, 0x065c, 0x065e:  second two octets of masks
		*	
		*	0x0660, 0x0662, 0x0664, 0x0666, 0x0668:   interfaces: 2-byte little-endian (any = ff ff)
		*/
		
		
		int numSNMPEntries = 0;
		
		int ipAddressStartIndex = 0x0638;
		int ipAddressBeginningNumRows = 1;	
		int ipAddressBeginningNumCols = 2;
		
		int ipAddressEndingIndex = 0x0642;
		int ipAddressEndingNumRows = 1;	
		int ipAddressEndingNumCols = 2;
		
		int ipAddressDataType = AirportInfoRecord.IP_ADDRESS;
		
		
		int ipMaskStartIndex = 0x064c;
		int ipMaskBeginningNumRows = 1;	
		int ipMaskBeginningNumCols = 2;
		
		int ipMaskEndingIndex = 0x0656;
		int ipMaskEndingNumRows = 1;	
		int ipMaskEndingNumCols = 2;
		
		int ipMaskDataType = AirportInfoRecord.IP_ADDRESS;
		
		
		int interfaceNumberStartIndex = 0x0660;
		int interfaceNumberNumRows = 1;	
		int interfaceNumberNumCols = 2;
		int interfaceNumberDataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
				
		
		for (int i = 0; i < addressVector.size(); i++)
		{
			
			String ipAddress = ((String[])addressVector.elementAt(i))[0];
			String ipMask = ((String[])addressVector.elementAt(i))[1];
			String interfaceName = ((String[])addressVector.elementAt(i))[2];
			
			// if null row, just ignore
			if (! (ipAddress.equals("")))
			{
				
				// null address or port throws exception when write to info records

				try
				{
					// need to have a multi window since address octets not contiguous!
					
					ByteBlockMultiWindow ipAddressWindow = new ByteBlockMultiWindow();
					
					ByteBlockWindow ipAddressBeginningWindow = new ByteBlockRectangularWindow(ipAddressStartIndex, ipAddressBeginningNumRows, ipAddressBeginningNumCols, baseBlock);
					ipAddressWindow.addWindow(ipAddressBeginningWindow);
					
					ByteBlockWindow ipAddressEndingWindow = new ByteBlockRectangularWindow(ipAddressEndingIndex, ipAddressEndingNumRows, ipAddressEndingNumCols, baseBlock);
					ipAddressWindow.addWindow(ipAddressEndingWindow);
					
					AirportInfoRecord ipAddressRecord = new AirportInfoRecord(ipAddressDataType, ipAddressWindow);
					ipAddressRecord.setBytesFromString(ipAddress);
					
				}
				catch (ValueFormatException e)
				{
					table.editCellAt(i,0);
					throw e;
				}
				
				
				
				
				try
				{
					// need to have a multi window since address octets not contiguous!
					
					ByteBlockMultiWindow ipMaskWindow = new ByteBlockMultiWindow();
					
					ByteBlockWindow ipMaskBeginningWindow = new ByteBlockRectangularWindow(ipMaskStartIndex, ipMaskBeginningNumRows, ipMaskBeginningNumCols, baseBlock);
					ipMaskWindow.addWindow(ipMaskBeginningWindow);
					
					ByteBlockWindow ipMaskEndingWindow = new ByteBlockRectangularWindow(ipMaskEndingIndex, ipMaskEndingNumRows, ipMaskEndingNumCols, baseBlock);
					ipMaskWindow.addWindow(ipMaskEndingWindow);
					
					AirportInfoRecord ipMaskRecord = new AirportInfoRecord(ipMaskDataType, ipMaskWindow);
					ipMaskRecord.setBytesFromString(ipMask);
					
				}
				catch (ValueFormatException e)
				{
					table.editCellAt(i,1);
					throw e;
				}
				
				
				try
				{
					AirportInfoRecord interfaceNumberRecord = new AirportInfoRecord(interfaceNumberStartIndex, interfaceNumberNumRows, interfaceNumberNumCols, interfaceNumberDataType, baseBlock);
					
					// need to convert name to number using Hashtable
					String interfaceNumber = (String)interfaceNameToNumberHashtable.get(interfaceName);
					
					interfaceNumberRecord.setBytesFromString(interfaceNumber);
				}
				catch (ValueFormatException e)
				{
					table.editCellAt(i,2);
					throw e;
				}
				
				
				
				
				// augment port map count, start indices
				ipAddressStartIndex += 2;
				ipAddressEndingIndex += 2;
				ipMaskStartIndex += 2;
				ipMaskEndingIndex += 2;
				interfaceNumberStartIndex += 2;
				numSNMPEntries++;
				
			
			}
				
		}
		
		// finally, write the number of addresses written
		AirportInfoRecord snmpAccessControlCountRecord = airportInfo.get("SNMP access control list length");
		String countString = new String();
		countString += numSNMPEntries;
		snmpAccessControlCountRecord.setBytesFromString(countString);
		
	}
		
		
		
	
	/**
	*	Refresh the display based on the current data in the underlying byte block.
	*/
	
	public void refreshDisplay()
	{
		
		ByteBlock baseBlock = airportInfo.baseBlock;
		
		
		/*
		*		
		*	SNMP access control list: require multi-block windows
		*
		*	0x0636:  number of entries (0-5)
		*	
		*	0x0638, 0x063a, 0x063c, 0x063e, 0x0640:  first two octets of IP addresses
		*	0x0642, 0x0644, 0x0646, 0x0648, 0x064a:  second two octets of IP addresses
		*	
		*	0x064c, 0x064e, 0x0650, 0x0652, 0x0654:  first two octets of masks
		*	0x0656, 0x0658, 0x065a, 0x065c, 0x065e:  second two octets of masks
		*	
		*	0x0660, 0x0662, 0x0664, 0x0666, 0x0668:   interfaces: 2-byte little-endian (any = ff ff)
		*/
		
		
		AirportInfoRecord snmpAccessControlCountRecord = airportInfo.get("SNMP access control list length");
		
		// Read in appropriate number of Mac addresses
		String countString = snmpAccessControlCountRecord.toString();
		int numSNMPEntries = 0;
		
		try
		{
			numSNMPEntries = Integer.parseInt(countString);
		}
		catch(NumberFormatException e)
		{
			System.out.println("Problem with number of SNMP access control entries");
		}
		
		
		// create new addressVector, with appropriate size
		addressVector = new Vector();
		
		
		
		int ipAddressStartIndex = 0x0638;
		int ipAddressBeginningNumRows = 1;	
		int ipAddressBeginningNumCols = 2;
		
		int ipAddressEndingIndex = 0x0642;
		int ipAddressEndingNumRows = 1;	
		int ipAddressEndingNumCols = 2;
		
		int ipAddressDataType = AirportInfoRecord.IP_ADDRESS;
		
		
		int ipMaskStartIndex = 0x064c;
		int ipMaskBeginningNumRows = 1;	
		int ipMaskBeginningNumCols = 2;
		
		int ipMaskEndingIndex = 0x0656;
		int ipMaskEndingNumRows = 1;	
		int ipMaskEndingNumCols = 2;
		
		int ipMaskDataType = AirportInfoRecord.IP_ADDRESS;
		
		
		int interfaceNumberStartIndex = 0x0660;
		int interfaceNumberNumRows = 1;	
		int interfaceNumberNumCols = 2;
		int interfaceNumberDataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		
		
		
		for (int i = 0; i < numSNMPEntries; i++)
		{
			// need to have a multi window since IP address octets not contiguous!		
			ByteBlockMultiWindow ipAddressWindow = new ByteBlockMultiWindow();
			
			ByteBlockWindow ipAddressBeginningWindow = new ByteBlockRectangularWindow(ipAddressStartIndex, ipAddressBeginningNumRows, ipAddressBeginningNumCols, baseBlock);
			ipAddressWindow.addWindow(ipAddressBeginningWindow);
			
			ByteBlockWindow ipAddressEndingWindow = new ByteBlockRectangularWindow(ipAddressEndingIndex, ipAddressEndingNumRows, ipAddressEndingNumCols, baseBlock);
			ipAddressWindow.addWindow(ipAddressEndingWindow);
			
			AirportInfoRecord ipAddressRecord = new AirportInfoRecord(ipAddressDataType, ipAddressWindow);
					
			
			ByteBlockMultiWindow ipMaskWindow = new ByteBlockMultiWindow();
					
			ByteBlockWindow ipMaskBeginningWindow = new ByteBlockRectangularWindow(ipMaskStartIndex, ipMaskBeginningNumRows, ipMaskBeginningNumCols, baseBlock);
			ipMaskWindow.addWindow(ipMaskBeginningWindow);
			
			ByteBlockWindow ipMaskEndingWindow = new ByteBlockRectangularWindow(ipMaskEndingIndex, ipMaskEndingNumRows, ipMaskEndingNumCols, baseBlock);
			ipMaskWindow.addWindow(ipMaskEndingWindow);
			
			AirportInfoRecord ipMaskRecord = new AirportInfoRecord(ipMaskDataType, ipMaskWindow);
			
			
			AirportInfoRecord interfaceNumberRecord = new AirportInfoRecord(interfaceNumberStartIndex, interfaceNumberNumRows, interfaceNumberNumCols, interfaceNumberDataType, baseBlock);
			
							
			String[] addressEntry = new String[3];
			addressEntry[0] = ipAddressRecord.toString();
			addressEntry[1] = ipMaskRecord.toString();
			
			// need to convert number to name using Hashtable
			String interfaceNumber = interfaceNumberRecord.toString();
			
			System.out.println("Interface number: " + interfaceNumber);
			
			addressEntry[2] = (String)interfaceNumberToNameHashtable.get(interfaceNumber);
			
			System.out.println("Interface value: " + addressEntry[2]);
			
			addressVector.insertElementAt(addressEntry, addressVector.size());
			
			ipAddressStartIndex += 2;
			ipAddressEndingIndex += 2;
			ipMaskStartIndex += 2;
			ipMaskEndingIndex += 2;
			interfaceNumberStartIndex += 2;
			
			
		}
		
		this.removeAll();
		tableModel = new AccessAddressTableModel();
		table = new JTable(tableModel);
		table.setCellSelectionEnabled(true);
		
		//Set up the editor for the interface column.
		table.getColumn("Interface").setCellEditor(new DefaultCellEditor(interfaceComboBox));
		
		table.setPreferredScrollableViewportSize(new Dimension(300,80));
		JScrollPane scrollPane = new JScrollPane(table);
		this.add(scrollPane);
		
		
	}
	
	
	

}