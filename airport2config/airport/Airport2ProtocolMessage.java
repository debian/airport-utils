

package airport;


import java.io.*;
import java.util.zip.*;



public class Airport2ProtocolMessage
{
	public String messageTag = "acpp";
	
	public int unknownField1 = 1;
	
	public int messageChecksum = 0;
	
	public int payloadChecksum = 0;
	
	public int payloadSize;
	
	public byte[] unknownField2 = new byte[8];
	
	public int messageType;     // download = 0x14, upload = 0x15, upload raw bytes = 0x03
	
	public int status = 0;      // non-zero indicastes error
	
	public byte[] unknownField3 = new byte[12];
	
	public byte[] password = new byte[32];
	
	public byte[] unknownField4 = new byte[48];
	
	
	
	public static final int READ = 0x14;
	public static final int WRITE = 0x15;
	public static final int WRITE_RAW = 0x03;
	
	
	
	public Airport2ProtocolMessage(int messageType, String password, byte[] payloadBytes, int payloadSize)
	{
		// set payload size and checksum
		if ((messageType == READ) || (messageType == WRITE_RAW))
		{
			this.payloadSize = payloadSize;
			this.payloadChecksum = computeChecksum(payloadBytes, payloadSize);
		}
		else
		{
			this.payloadSize = -1;
			this.payloadChecksum = 1;
		}
		
		// set message type
		this.messageType = messageType;
						
		// set encrypted password bytes
		byte[] passwordBytes = password.getBytes();
		
		int length = passwordBytes.length;
		
		if (length > 32)
			length = 32;
			
		for (int i = 0; i < length; i++)
		{
			this.password[i] = passwordBytes[i];
		}
		
						
		this.password = AirportInfoRecord.encryptBytes(AirportInfoRecord.cipherBytes, this.password);
		
		// get current message bytes, and use to compute checksum (including magic number)
		this.messageChecksum = computeChecksum(this.getBytes(), 128);
        
	}
	
	
	
	public Airport2ProtocolMessage(byte[] headerBytes)
	    throws ValueFormatException
	{
		// read fields from supplied header
		if (headerBytes.length != 128)
		    throw new ValueFormatException("Bad header size");
		
		DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(headerBytes));
		
		try
		{
    		byte[] tagBytes = new byte[4];
    		inputStream.readFully(tagBytes);
    		messageTag = new String(tagBytes);
    		
    		if (!messageTag.equals("acpp"))
    		    throw new ValueFormatException("Header tag incorrect");
    		
    		unknownField1 = inputStream.readInt();
    		messageChecksum = inputStream.readInt();
	        payloadChecksum  = inputStream.readInt();
	        payloadSize = inputStream.readInt();
	        
	        inputStream.read(unknownField2, 0, unknownField2.length);
	        
	        messageType = inputStream.readInt();
	        status = inputStream.readInt();
	        
    		inputStream.read(unknownField3, 0, unknownField3.length);
    		inputStream.read(password, 0, password.length);
    		inputStream.read(unknownField4, 0, unknownField4.length);
    		
		}
		catch (IOException e)
		{
		    throw new ValueFormatException("Bad header field encountered");
		}
	}
	
	
	
	public byte[] getBytes()
	{
		// serialize all the fields
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(128);
		DataOutputStream outStream = new DataOutputStream(byteStream);
		
		try
		{
			outStream.writeBytes(messageTag);
			outStream.writeInt(unknownField1);
			outStream.writeInt(messageChecksum);
			outStream.writeInt(payloadChecksum);
			outStream.writeInt(payloadSize);
			outStream.write(unknownField2);
			outStream.writeInt(messageType);
			outStream.writeInt(status);
			outStream.write(unknownField3);
			outStream.write(password);
			outStream.write(unknownField4);
		}
		catch(IOException e)
		{
		    System.out.println("Error getting protocol message bytes");
		}
		
		return byteStream.toByteArray();
	}
	
	
	
	private int computeChecksum(byte[] fileBytes, int length)
	{
		Adler32 adler = new Adler32();
		adler.update(fileBytes, 0, length);
		return (int)adler.getValue();
	}
	
					
					
	private int oldComputeChecksum(byte[] fileBytes, int length)
	{
		int checksum;
		
		// multiply each byte by descending sequence for weighted checksum
		
	    long weightedChecksum = 0;
		long ordinaryChecksum = 0;
	
		for (int i = 0; i < length; i++)
		{
			int byteValue = fileBytes[i];
			if (byteValue < 0)
				byteValue += 256;
			
			ordinaryChecksum += byteValue;	
			weightedChecksum += byteValue * (length - i);
			
		}
		
		//System.out.println("Checksums: ordinary = 0x" + Long.toHexString(ordinaryChecksum) + ", weighted = 0x" + Long.toHexString(weightedChecksum));
		
		ordinaryChecksum += 1;
		weightedChecksum += length;
		
		
		// boil checksums down to 16 bits
		
		while (ordinaryChecksum > 0x0000FFFF)
		{
			long upperHalf = ordinaryChecksum >> 16;
			long lowerHalf = ordinaryChecksum & 0x0000FFFF;
			ordinaryChecksum = lowerHalf + (upperHalf * 0x0F);
		}
		
		while (weightedChecksum > 0x0000FFFF)
		{
			long upperHalf = weightedChecksum >> 16;
			long lowerHalf = weightedChecksum & 0x0000FFFF;
			weightedChecksum = lowerHalf + (upperHalf * 0x0F);
		}
		
		// combine 16-bit checksums into 32 bit checksum
		checksum = (int)(ordinaryChecksum | (weightedChecksum << 16));
		
		return checksum;
	}
	
	
	
	public String toString()
	{
	    String result = new String("Airport protocol message:\n");
	    result += "   messageTag:       " + messageTag + "\n";
	    result += "   messageChecksum:  0x" + Integer.toHexString(messageChecksum) + "\n";
	    result += "   payloadChecksum:  0x" + Integer.toHexString(payloadChecksum) + "\n";
	    result += "   payloadSize:      0x" + Integer.toHexString(payloadSize) + "\n";
	    result += "   messageType:      0x" + Integer.toHexString(messageType) + "\n";
	    result += "   status:           0x" + Integer.toHexString(status) + "\n";
	    
	    return result;
	}
	
}