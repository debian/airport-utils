/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains basic base station information: name, location, and contact individual.
*	Also provides textfield for entering new community name (base station password).
*/

public class AirportMainPanel extends AirportInfoPanel
{
	private JTextField baseStationNameField, baseStationLocationField;
	private JTextField baseStationContactField, baseStationPasswordField;
	
	
	public AirportMainPanel(AirportInfo theInfo)
	{
		AirportInfoRecord nextRecord;
		
		nextRecord = theInfo.get("syNm");
		baseStationNameField = new AirportInfoTextField(nextRecord);
		
		nextRecord = theInfo.get("syLo");
		baseStationLocationField = new AirportInfoTextField(nextRecord);
		
		nextRecord = theInfo.get("syCt");
		baseStationContactField = new AirportInfoTextField(nextRecord);
		
		nextRecord = theInfo.get("syPW");
		baseStationPasswordField = new AirportInfoTextField(nextRecord);
		baseStationPasswordField.setText("");
		
		setUpDisplay();
	}
	
	
	private void setUpDisplay()
	{
		
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		JLabel theLabel;
		
		this.setLayout(theLayout);
		
		
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		theLabel = new JLabel("Base station name");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(baseStationNameField, c);
		this.add(baseStationNameField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		theLabel = new JLabel("Base station location");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(baseStationLocationField, c);
		this.add(baseStationLocationField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 3;
		theLabel = new JLabel("Contact person name");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(baseStationContactField, c);
		this.add(baseStationContactField);
		
		c.insets = new Insets(20,2,2,2);
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 4;
		theLabel = new JLabel("Password");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 4;
		theLayout.setConstraints(baseStationPasswordField, c);
		this.add(baseStationPasswordField);
		
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 5;
		theLabel = new JLabel("(enter only if to be changed)");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		
	}
		
		
		
}