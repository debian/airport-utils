/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
package airport;


import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
*	Handles display and updating of entries used in login string.
*/

public class AirportLoginStringTable extends AirportInfoPanel
{
	private AirportInfo airportInfo;
	private Vector tableContentsVector;
	private JTable table;
	private JScrollPane tableScrollPane;
	private AbstractTableModel tableModel;
	private JComboBox elementTypeComboBox;
	
	private final int MAX_NUM_LOGIN_STRING_ENTRIES = 20;
		
	
	/**
	*	Table model which maintains list of login string elements.
	*/
	
	private class LoginStringTableModel extends AbstractTableModel
	{
		public int getColumnCount() 
		{ 
			return 3; 
		}
		
		public int getRowCount() 
		{ 
			return MAX_NUM_LOGIN_STRING_ENTRIES;
		}
		
		public boolean isCellEditable(int row, int col) 
		{ 
			if (row <= tableContentsVector.size())
			{
				if (col < 2)
					return true;
				else 	// carriage return column; editable only if send or receive (not delay)
				{
					
					if (row == tableContentsVector.size())
						return false;
					else 
					{
						Object[] currentRow = (Object[])tableContentsVector.elementAt(row);
						if ( ((String)currentRow[0]).equals("Send string") || ((String)currentRow[0]).equals("Expect string") )
							return true;
						else
							return false;
					}
				}
			}
			else
				return false;
		}
		
		public String getColumnName(int col) 
		{ 
			switch (col)
			{
				case 0:
					return "Element type";
				case 1:
					return "Value";
				default:
					return "Carriage return";
			} 
		}
		
		
		/*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.
         */
         
        public Class getColumnClass(int column) 
        {
            String stringDummy = "";
            Boolean booleanDummy = new Boolean(true);
            
            switch (column)
            {
            	case 0:
            		return stringDummy.getClass();
            	
            	case 1:
            		return stringDummy.getClass();
            	
            	default:
            		return booleanDummy.getClass();
            }
            
        }
        
		
		public Object getValueAt(int row, int col)
		{
			//System.out.println("Get value: " + row + ", " + col);
			
			if (row < tableContentsVector.size())
			{
				return ((Object[])tableContentsVector.elementAt(row))[col];
			}
			else
			{
				if (col < 2)
					return "";
				else
					return new Boolean(false);
			}
		}
		
		public void setValueAt(Object newValue, int row, int col) 
		{
			if (row < tableContentsVector.size())
			{
				Object[] addressEntry = (Object[])tableContentsVector.elementAt(row);
				addressEntry[col] = newValue;
				
				// if change type to "delay", erase cr entry
				if ( (col == 0) && (((String)newValue).equals("Delay (seconds)")) )
				{
					addressEntry[2] = new Boolean(false);
					fireTableCellUpdated(row, 2);
				}
			}
			else
			{
				Object[] addressEntry = {"", "", new Boolean(false)};
				addressEntry[col] = newValue;
				tableContentsVector.insertElementAt(addressEntry, tableContentsVector.size());
			}
			
			//System.out.println("New value: " + newValue);
			
			fireTableCellUpdated(row, col);
		
		}
		
	}
	
	
	
	
	
	
	
	/**
	*	Create new table based on data in airportInfo.
	*/
	
	public AirportLoginStringTable(AirportInfo airportInfo)
	{
		this.airportInfo = airportInfo;
		
		tableContentsVector = new Vector();
		
		elementTypeComboBox = new JComboBox();
		elementTypeComboBox.addItem("");
		elementTypeComboBox.addItem("Expect string");
		elementTypeComboBox.addItem("Send string");
		elementTypeComboBox.addItem("Delay (seconds)");
		
		
		setUpDisplay();
	}
	
	
	
	
	private void setUpDisplay()
	{
		refreshDisplay();
	}
	
	
	
	
	
	
	
	
	
	/**
	*	Write the modem string information into the appropriate location
	*	in the byte block referenced by the AirportInfo object supplied in the constructor.
	*	@throws ValueFormatException Thrown if an entry is malformed. Also sets 
	*	editting focus to the offending cell.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		
		// first, record changes if cell currently being edited
		TableCellEditor editor = table.getCellEditor(); 
		if(editor != null)
			editor.stopCellEditing();
		
		
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		
		for (int i = 0; i < tableContentsVector.size(); i++)
		{
			
			String elementTypeString = (String)((Object[])tableContentsVector.elementAt(i))[0];
			String elementValueString = (String)((Object[])tableContentsVector.elementAt(i))[1];
			boolean carriageReturnFlag = ((Boolean)((Object[])tableContentsVector.elementAt(i))[2]).booleanValue();
			
			// if null type, just ignore
			if (!(elementTypeString.equals("")))
			{
				
				
				try
				{
					
					// set start byte: 
					//		Send string = 0x40, Expect string = 0x80, Delay = 0xC0
					//			plus number of characters to follow, or delay value
					
					if (elementTypeString.equals("Expect string") || elementTypeString.equals("Send string"))
					{
						byte startByte;
						
						if (elementTypeString.equals("Expect string"))
							startByte = (byte)0x80;
						else if (elementTypeString.equals("Send string"))
							startByte = (byte)0x40;
						else
							startByte = (byte)0x00;
						
						startByte += elementValueString.length();
						
						if (carriageReturnFlag == true) 
							++startByte;
							
						outStream.write(startByte);
						
						byte[] valueBytes = elementValueString.getBytes();
						
						try
						{
							outStream.write(valueBytes);
						}
						catch (IOException e)
						{
							// won't happen
						}
						
						if (carriageReturnFlag == true)
						{
							outStream.write((byte)0x0D);	// CR in ACSCII
						}
						
					}
					else if (elementTypeString.equals("Delay (seconds)"))
					{
						int delayValue = Integer.parseInt(elementValueString);
						
						outStream.write((byte)(0xC0 + delayValue));
						
						if ((delayValue < 0) || (delayValue > 63))
						{
							table.editCellAt(i,1);
							throw new ValueFormatException("Delay must be between 0 and 63 seconds");
						}
					}
					
					
				}
				catch (NumberFormatException e)
				{
					table.editCellAt(i,1);
					throw new ValueFormatException("Bad delay value");
				}
				catch (ArrayIndexOutOfBoundsException e)
				{
					throw new ValueFormatException("Login string too long (maximum size 127 characters)");
				}
				
				
			}
				
		}
		
		
		byte[] bytes = outStream.toByteArray();
		
		// set modem and pppoe username fields with this stuff; erase password records
		AirportInfoRecord modemUsernameRecord = (AirportInfoRecord)airportInfo.get("moUN");
		modemUsernameRecord.setValue(bytes); 
		
		AirportInfoRecord modemPasswordRecord = (AirportInfoRecord)airportInfo.get("moPW");
		modemPasswordRecord.setValue(new byte[0]); 
		
		AirportInfoRecord pppoeUsernameRecord = (AirportInfoRecord)airportInfo.get("peUN");
		pppoeUsernameRecord.setValue(bytes); 
		
		AirportInfoRecord pppoePasswordRecord = (AirportInfoRecord)airportInfo.get("pePW");
		pppoePasswordRecord.setValue(new byte[0]);
		
	}
		
		
		
	
	/**
	*	Refresh the display based on the current data in the underlying byte block.
	*/
	
	public void refreshDisplay()
	{
		
		AirportInfoRecord modemUsernameRecord = (AirportInfoRecord)airportInfo.get("moUN");
		
		byte[] loginBytes = modemUsernameRecord.getValue();
		
		int currentPosition = 0;
		
	WhileLoop:
	
		while (currentPosition < loginBytes.length)
		{
			
			int typeAndLength = loginBytes[currentPosition];
			++currentPosition;
			
			// make positive
			if (typeAndLength < 0)
				typeAndLength += 256;
				
			int length = typeAndLength % 64;
			int type = typeAndLength - length;
			
					
			switch (type)
			{
				case 0xC0:	// delay entry
				{
					Object[] addressEntry = {"Delay (seconds)", Integer.toString(length), new Boolean(false)};
					tableContentsVector.insertElementAt(addressEntry, tableContentsVector.size());
					break;
				}
				
				case 0x40:	// send string, receive string entry
				case 0x80:
				{
					String valueString = new String(loginBytes);
					valueString = valueString.substring(currentPosition, currentPosition + length);
					currentPosition += length;
					boolean crFlag;
					
					if (loginBytes[currentPosition - 1] == 0x0d)	// see if last char is CR
					{
						// if last char is CR, strip it off and set crFlag
						valueString = valueString.substring(0, length - 1);
						crFlag = true;
					}
					else
					{
						crFlag = false;
					}
					
					Object[] addressEntry = new Object[3];
					
					addressEntry[1] = valueString;
					addressEntry[2] = new Boolean(crFlag);
						
					if (type == 0x40)
					{
						addressEntry[0] = "Send string";
					}
					else	// (type == 0x80)
					{
						addressEntry[0] = "Expect string";
					}
						
					tableContentsVector.insertElementAt(addressEntry, tableContentsVector.size());
					break;
				}
				
				
				default:
				{
					// break out of while loop...
					break WhileLoop;
				}
				
			}	
			
		}
		
		
		
		
		// remove all components...
		this.removeAll();
		
		// create new table model and table...
		tableModel = new LoginStringTableModel();
		table = new JTable(tableModel);
		table.setCellSelectionEnabled(true);
		
		//Set the editor for the interface column.
		table.getColumn("Element type").setCellEditor(new DefaultCellEditor(elementTypeComboBox));
		
		table.setPreferredScrollableViewportSize(new Dimension(300,150));
		
		// add it to a scroll pane
		tableScrollPane = new JScrollPane(table);
		
		
		this.add(tableScrollPane);
		
		
		
	}
	
	
	
	/**
	*	Enable and disable JTable as well as any other contained components.
	*/
	
	public void setEnabled(boolean enabled)
	{
		super.setEnabled(enabled);
		
		table.setEnabled(enabled);
		
	}
	
	
	

}