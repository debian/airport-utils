/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 


package airport;


import java.util.*;
import java.io.*;
//import AirportBaseStationConfigurator;





public class AirportInfo extends Hashtable
{
	
	public static final int MAX_NUM_MAC_ADDRESSES = 256;
	public static final int MAX_NUM_PORT_MAPS = 20;
	public static final int MAX_NUM_SNMP_ACCESS_ENTRIES = 5;
	public static final int MAX_NUM_LOGIN_CHARS = 127;
	
	
	
	public AirportInfo()
	{
		initializeHashtable();
	}	
	
	
	
	
	public AirportInfo(byte[] retrievedBytes)
		throws IllegalArgumentException
	{
		int count = 0;
		byte[] invalidBytes = {(byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xF6};
		
		ByteArrayInputStream byteStream = new ByteArrayInputStream(retrievedBytes);
		
		
		initializeHashtable();
		
		
		while (byteStream.available() > 0)
		{
			
			// read the tag
			byte[] tagBytes = new byte[4];
			byteStream.read(tagBytes, 0, 4);
			String tag = new String(tagBytes);
			
			// break if tag is all zeros: end of info in array
			if (tag.equals("\0\0\0\0"))
				break;
			
			// get the corresponding element
			AirportInfoRecord element = this.get(tag);
			
			// increment count; used at end to determine if we got any valid info
			count++;
			
			// check to make sure the element's not null, in case have received
			// unknown tag: just ignore if null
			if (element != null)
			{
				//read the encryption
				byte[] encryptionBytes = new byte[4];
				byteStream.read(encryptionBytes, 0, 4);
				element.encryption = getIntegerValue(encryptionBytes);
				
				//read the length
				byte[] lengthBytes = new byte[4];
				byteStream.read(lengthBytes, 0, 4);
				int length = getIntegerValue(lengthBytes);
				
				//read the value
				byte[] valueBytes = new byte[length];
				byteStream.read(valueBytes, 0, length);
				
				if (element.encryption == AirportInfoRecord.ENCRYPTED)
					valueBytes = AirportInfoRecord.decryptBytes(AirportInfoRecord.cipherBytes, valueBytes);
				
				// check if the value being sent is 0xFFFFFF6; this indicates
				// the current value is invalid - just leave as 0. Ignore for
				// IP addresses, though...
				if (!arraysEqual(valueBytes, invalidBytes) || (element.dataType == AirportInfoRecord.IP_ADDRESS))
					element.value = valueBytes;
				
				//System.out.println("Tag " + tag + ": " + element.toString());
			}
			else
			{	
				
				// just add an entry in hashtable
				element = new AirportInfoRecord();
				
				// assign the tag
				element.tag = tag;
				
				//read the encryption
				byte[] encryptionBytes = new byte[4];
				byteStream.read(encryptionBytes, 0, 4);
				element.encryption = getIntegerValue(encryptionBytes);
				
				//read the length
				byte[] lengthBytes = new byte[4];
				byteStream.read(lengthBytes, 0, 4);
				int length = getIntegerValue(lengthBytes);
				element.maxLength = length;
				
				//read the value
				byte[] valueBytes = new byte[length];
				byteStream.read(valueBytes, 0, length);
				
				if (element.encryption == AirportInfoRecord.ENCRYPTED)
					valueBytes = AirportInfoRecord.decryptBytes(AirportInfoRecord.cipherBytes, valueBytes);
				
				// check if the value being sent is 0xFFFFFF6; this indicates
				// the current value is invalid - just leave as 0. Ignore for
				// IP addresses, though...
				if (!arraysEqual(valueBytes, invalidBytes) || (element.dataType == AirportInfoRecord.IP_ADDRESS))
					element.value = valueBytes;
				else
					element.value = new byte[element.maxLength];
				
				// add the element
				this.put(tag, element);
				
				//System.out.println("Unused tag " + tag + ": " + element.toString());
			}
			
		}
		
		
		if (count == 0)
		{
			// no data retrieved; throw exception!
			throw new IllegalArgumentException("No information retrieved from base station!");
		}
		
		
		
	}
	
	
	
	private void initializeHashtable()
	{
		
		// populate the hashtable
		int maxSize;
		int dataType;
		int encryption;
		String description;
		String tag;
		
		
		//	Trap community password: omitted
		
		
		
		
		//	Read community password: 
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Read community";
		tag = "syPR";
		
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		//	Read/write community password: 
		
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Read/write community";
		tag = "syPW";
		
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		//	Remaining community password count: omitted
		
		
		
		
		//	Remaining community password: omitted
		


		//	Configuration mode switch: 
		//		Modem config:     00 00 09 00
		//		Ethernet manual:  00 00 04 00
		//		Ethernet DHCP:    00 00 03 00
		//		Ethernet PPPoE:   00 00 09 00
		//		
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Configuration mode";
		tag = "waCV";
		
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		//	Ethernet/Modem switch: 
		//		00000004 = modem
		//		00000010 = Ethernet (hex)
		
		maxSize = 4;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Ethernet/Modem switch 1";
		tag = "waIn";
		
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		
		// 	Microwave robustness flag:  
		//			00 = off
		//			01 = on
		
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Microwave robustness flag";
		tag = "raRo";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		// 	RTS/CTS flag: not present 
		
		
		
		// 	Closed network flag:  
		//			00 = open
		//			01 = closed
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Closed network flag";
		tag = "raCl";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		

		
		//	Deny unencrypted data flag:	not present
		
		
		
		
		
		//	Access point density, multicast rate:
		//
		//		Multicast rate: 01 = 1 Mbps, 02 = 2 Mbps, 55 = 5.5 Mbps, 11 = 11 Mbps - all hex
		//		Density: 1 = low, 2 = medium, 3 = high
		//
		//				   large  medium   small
		//		1 Mbps		OK		OK		OK
		//		2 Mbps		OK		OK		OK
		//		5.5 Mbps	na		OK		OK
		//		11 Mbps		na		na		OK
		
		maxSize = 4;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Access point density";
		tag = "raDe";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		maxSize = 4;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Multicast rate";
		tag = "raMu";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		//	Select encryption key to use: not present
		
		
		
		//	Wireless channel:
		
		maxSize = 4;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Wireless channel";
		tag = "raCh";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		//	Modem timeout, in seconds:
		
		maxSize = 4;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Modem timeout";
		tag = "moID";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		//	Dialing type:
		//		 	00 = tone
		//			01 = pulse
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Dialing type (tone or pulse)";
		tag = "moPD";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		//	Dialing type:
		//		00 = auto dial off
		//		01 = auto dial on
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Automatic dial";
		tag = "moAD";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
			
		//	RTS Threshold: not present
		// 		max value 2347
		//		
		
		
		
		
			
		//	Phone country code:
		// 
		//		US standard = 	32 32 = 22 decimal
		//		Singapore = 	34 37
		//		Switzerland = 	31 35
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Phone country code";
		tag = "moCC";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
	
		//	Modem country code combo box index 
		
		maxSize = 4;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Modem country code combo box index";
		tag = "moCI";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		//	Network name:
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Network name";
		tag = "raNm";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		
		//	
		//	Modem stuff:
		//
			
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Primary phone number";
		tag = "moPN";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Secondary phone number";
		tag = "moAP";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
				
		//	
		//	PPPoE idle timeout, in seconds:
		//		0 = don't disconnect
		//
		
		maxSize = 4;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "PPPoE idle timeout";
		tag = "peID";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		//	PPPoE auto connect:
		//		00 = off
		//		01 = on
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "PPPoE auto connect";
		tag = "peAC";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		//	PPPoE stay connected:
		//		00 = no
		//		01 = yes
		
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "PPPoE stay connected";
		tag = "peSC";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		
		// 	Encryption flag field:
		//			00 = no encryption
		//			01 = 40-bit
		//			02 = 128-bit
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Encryption switch";
		tag = "raWM";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
			
		//	Encryption key:
		
		
		maxSize = 13;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Encryption key";
		tag = "raWE";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
			
		//	Private LAN base station address and subnet mask:
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Private LAN base station address";
		tag = "laIP";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Private LAN subnet mask";
		tag = "laSM";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
			
		
		
		
		//	syslog host facility(0 - 8): omitted
		//	
		
		
		
		
		
		//	Bridging switch:
		//		00 = don't bridge
		//		01 = bridge
		
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Wireless to Ethernet bridging switch";
		tag = "raWB";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		//	Access control switch:  
		//		00 = no access control
		//		01 = access control used
		
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Access control switch";
		tag = "acEn";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		//	Access control info:  
		
		
		maxSize = 16;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Access control info";
		tag = "acTa";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		//	DHCP service on wireless:
		//		00 = no DHCP service
		//		01 = DHCP on, using specified range of IP addresses 
		
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Wireless DHCP switch";
		tag = "raDS";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		//	DHCP service on LAN Ethernet:
		//		00 = no DHCP service
		//		01 = DHCP on 
		
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "LAN Ethernet DHCP switch";
		tag = "laDS";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		//	DHCP service on WAN Ethernet:
		//		00 = no DHCP service
		//		01 = DHCP on 
		
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "WAN Ethernet DHCP switch";
		tag = "waDS";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		//	NAT switch:
		//		00 = NAT off
		//		01 = NAT on
		
		
		maxSize = 1;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "NAT switch";
		tag = "raNA";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		
		
		//	Watchdog reboot timer switch: omit
		
		
		                 
		
		
		//	Base station IP address: 0x46A
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Base station IP address";
		tag = "waIP";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		//	Default TTL, for use with NAT(?): omitted
		
		
		
		
		
		//	Router IP address and mask: 0x470, 0x474
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Router IP address";
		tag = "waRA";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Subnet mask";
		tag = "waSM";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
	
		
		//	0x0478:  syslog IP address
		//	0x047C:  trap host IP address
		//	
		
		
		
		
		//	Names of base station, contact person
		
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Contact person name";
		tag = "syCt";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Base station name";
		tag = "syNm";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		//	Base station location:
		
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Base station location";
		tag = "syLo";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
	
		
		
		//	DHCP client ID:
		
		
		maxSize = 32;	// guess
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "DHCP client ID";
		tag = "waDC";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		
		
		
		//	DHCP address range to serve: 
		//		starting address: 0xCF2
		//		ending address: 0xCF6
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "DHCP address range start";
		tag = "dhBg";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "DHCP address range end";
		tag = "dhEn";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		
		//	DNS servers:
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Primary DNS server";
		tag = "waD1";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Secondary DNS server";
		tag = "waD2";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		//	DHCP lease time:
		//		4-byte unsigned integer giving lease time in seconds
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "DHCP lease time";
		tag = "dhLe";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		//	Domain name (from DNS setting window): 0xD0A
		
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Domain name";
		tag = "waDN";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		
		
			
		//	Port mapping functions:
		
		
		maxSize = 16;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Port mapping";
		tag = "pmTa";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		
		
		
		
		
		//	Username@domain, password
		
		
		
		maxSize = 64;	// guess
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Dial-up username";
		tag = "moUN";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		maxSize = 64;	// guess
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Dial-up password";
		tag = "moPW";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		maxSize = 64;	// guess
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "PPPoE username";
		tag = "peUN";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		maxSize = 64;	// guess
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "PPPoE password";
		tag = "pePW";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		
		//	PPPoE Service Name
		
		
		maxSize = 64;	// guess!
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "PPPoE service name";
		tag = "peSN";	
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		//	Reboot signal
	
		
		maxSize = 0;
		dataType = AirportInfoRecord.BYTE;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Reboot flag";
		tag = "acRB";	
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
	}
	
	
	
	
	public AirportInfoRecord get(String name)
	{
		//System.out.println("Get info record " + name);
		return (AirportInfoRecord)super.get(name);
	}
	
	
	
	public byte[] getUpdateBytes()
	{
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		
		Enumeration elementEnumeration = this.elements();
		
		while (elementEnumeration.hasMoreElements())
		{
			AirportInfoRecord nextElement = (AirportInfoRecord)elementEnumeration.nextElement();
			
			try
			{
				outStream.write(nextElement.getUpdateBytes());
				//System.out.println("Update bytes: tag " + nextElement.tag + ": " + nextElement.toString());
			}
			catch (IOException e)
			{
				// can't happen
			}
			
		}
		
		return outStream.toByteArray();
	}
	
	
	
	public byte[] getRequestBytes()
	{
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		
		Enumeration elementEnumeration = this.elements();
		
		while (elementEnumeration.hasMoreElements())
		{
			AirportInfoRecord nextElement = (AirportInfoRecord)elementEnumeration.nextElement();
			
			try
			{
				outStream.write(nextElement.getRequestBytes());
			}
			catch (IOException e)
			{
				// can't happen
			}
		}
		
		return outStream.toByteArray();
	}
	
	
	
	public String toString()
	{
		String returnText = new String();
		
		Enumeration elementEnumeration = this.elements();
		
		while (elementEnumeration.hasMoreElements())
		{
			AirportInfoRecord theElement = (AirportInfoRecord)elementEnumeration.nextElement();
			returnText += theElement.toString();
			returnText += "\n";
		}
		
		return returnText;
		
	}
	
	
	
	private int getIntegerValue(byte[] valueBytes)
	{
		int value = 0;
		
		for (int i = 0; i < valueBytes.length; i++)
		{
			int absValue = valueBytes[i];
			
			if (absValue < 0)
				absValue += 256;
			
			value = value*256 + absValue;
		}
		
		return value;
	}
	
	
	
	private static boolean arraysEqual(byte[] a, byte[] b)
	{
		if (a.length != b.length)
		{
			return false;
		}
		else
		{
			for (int i = 0; i < a.length; i++)
			{
				if (a[i] != b[i])
					return false;
			}
		}
		
		return true;
	}
	
	
	
}