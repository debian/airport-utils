/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
package airport;


import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
*	Handles display and updating of port mapping information.
*/

public class AirportPortMappingTable extends AirportInfoPanel
{
	private AirportInfo airportInfo;
	private Vector addressVector;
	private JTable table;
	private AbstractTableModel tableModel;
	
	
	/**
	*	Table model which maintains list of port mappings.
	*/
	
	private class PortMappingTableModel extends AbstractTableModel
	{
		public int getColumnCount() 
		{ 
			return 3; 
		}
		
		public int getRowCount() 
		{ 
			return AirportInfo.MAX_NUM_PORT_MAPS;
		}
		
		public boolean isCellEditable(int row, int col) 
		{ 
			if (row <= addressVector.size())
				return true;
			else
				return false;
		}
		
		public String getColumnName(int col) 
		{ 
			switch (col)
			{
				case 0:
					return "Public port number";
					
				case 1:
					return "Private IP address";
					
				case 2:
					return "Private port number";
					
				default:
					return "";
			}
			
		}
		
		public Object getValueAt(int row, int col)
		{
			if (row < addressVector.size())
				return ((String[])addressVector.elementAt(row))[col];
			else
				return "";
		}
		
		public void setValueAt(Object newValue, int row, int col) 
		{
			if (newValue instanceof String)
			{
				if (row < addressVector.size())
				{
					String[] addressEntry = (String[])addressVector.elementAt(row);
					addressEntry[col] = (String)newValue;
				}
				else
				{
					String[] addressEntry = {"","",""};
					addressEntry[col] = (String)newValue;
					addressVector.insertElementAt(addressEntry, addressVector.size());
				}
			}
		}
		
	}
	
	
	
	
	
	
	/**
	*	Create new table based on data in airportInfo.
	*/
	
	public AirportPortMappingTable(AirportInfo airportInfo)
	{
		this.airportInfo = airportInfo;
		setUpDisplay();
	}
	
	
	
	
	private void setUpDisplay()
	{
		refreshDisplay();
	}
	
	
	
	
	
	
	/**
	*	Write the MAC addresses and hostnames in the list into the appropriate locations
	*	in the byte block referenced by the AirportInfo object supplied in the constructor.
	*	@throws ValueFormatException Thrown if either an IP address is malformed, or ports
	*	are incorrect. Also sets editting focus to the offending cell.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		
		
		// first, record changes if cell currently being edited
		TableCellEditor editor = table.getCellEditor(); 
		if(editor != null)
			editor.stopCellEditing();
		
		
		
		int numMacAddresses = 0;
		
		Vector publicPortVector = new Vector();
		Vector privateIPVector = new Vector();
		Vector privatePortVector = new Vector();
		
		
		
		for (int i = 0; i < addressVector.size(); i++)
		{
			
			try
			{
				// these only get augmented if address is OK (and non-null)
				if (! ((String[])this.addressVector.elementAt(i))[0].equals(""))
				{
					// create a new info record to qualify the address and convert to bytes
					AirportInfoRecord publicPortRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.UNSIGNED_INTEGER, AirportInfoRecord.UNENCRYPTED, 2);
					publicPortRecord.setBytesFromString(((String[])this.addressVector.elementAt(i))[0]);
					publicPortVector.add(publicPortRecord);
					
					numMacAddresses++;
				}
				else
				{
					// skip this record
					continue;
				}
				
			}
			catch (ValueFormatException e)
			{
				table.editCellAt(i,0);
				throw e;
			}
			
			try
			{
				
				// create a new info record to qualify the address and convert to bytes
				AirportInfoRecord privateIPRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.IP_ADDRESS, AirportInfoRecord.UNENCRYPTED, 4);
				privateIPRecord.setBytesFromString(((String[])this.addressVector.elementAt(i))[1]);
				privateIPVector.add(privateIPRecord);
				
			}
			
			catch (ValueFormatException e)
			{
				table.editCellAt(i,1);
				throw e;
			}
			
			try
			{
				
				// create a new info record to qualify the address and convert to bytes
				AirportInfoRecord privatePortRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.UNSIGNED_INTEGER, AirportInfoRecord.UNENCRYPTED, 2);
				privatePortRecord.setBytesFromString(((String[])this.addressVector.elementAt(i))[2]);
				privatePortVector.add(privatePortRecord);
				
			}
			
			catch (ValueFormatException e)
			{
				table.editCellAt(i,2);
				throw e;
			}
			
			
				
		}
		
		// now create concatenation
		try
		{
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		
			byte[] zeroBytes = new byte[4];
			outStream.write(zeroBytes);
			
			// do length; use info record to create 8-byte value
			AirportInfoRecord sizeRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.UNSIGNED_INTEGER, AirportInfoRecord.UNENCRYPTED, 4);
			sizeRecord.setBytesFromString(Integer.toString(numMacAddresses));
			outStream.write(sizeRecord.value);
			
			// write 8-byte padding of 0's
			zeroBytes = new byte[8];
			outStream.write(zeroBytes);
			
			// now write addresses and hostnames
			for (int i = 0; i < numMacAddresses; i++)
			{
				outStream.write(((AirportInfoRecord)privateIPVector.elementAt(i)).value);
				outStream.write(((AirportInfoRecord)privatePortVector.elementAt(i)).value);
				outStream.write(((AirportInfoRecord)publicPortVector.elementAt(i)).value);
			}
			
			
			// now use this to set the value of the associated info record
			AirportInfoRecord accessControlRecord = airportInfo.get("pmTa");
			accessControlRecord.setValue(outStream.toByteArray());
			
		
		}
		catch (IOException e)
		{
			// shouldn't ever happen...
		}
		
	}
		
		
		
		
	
	/**
	*	Refresh the display based on the current data in the underlying byte block.
	*/
	
	public void refreshDisplay()
	{
		
		AirportInfoRecord portMapRecord = airportInfo.get("pmTa");
		byte[] portMapBytes = portMapRecord.getValue();
		
		DataInputStream inStream = new DataInputStream(new ByteArrayInputStream(portMapBytes));
		
		try
		{
			// Read first 4 bytes of 0 padding
			inStream.skip(4);
			
			// Read in number of Mac addresses (next 4 bytes)
			int numPortMaps = inStream.readInt();
			
			// Read next 8 bytes of 0 padding
			inStream.skip(8);
			
			
			// create new addressVector, with appropriate size
			addressVector = new Vector();
			
			
			
			for (int i = 0; i < numPortMaps; i++)
			{
				byte[] addressBytes = new byte[4];
				inStream.read(addressBytes);
				AirportInfoRecord privateIPRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.IP_ADDRESS, AirportInfoRecord.UNENCRYPTED, 4, addressBytes);
				
				byte[] privatePortBytes = new byte[2];
				inStream.read(privatePortBytes);
				AirportInfoRecord privatePortRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.UNSIGNED_INTEGER, AirportInfoRecord.UNENCRYPTED, 2, privatePortBytes);
				
				byte[] publicPortBytes = new byte[2];
				inStream.read(publicPortBytes);
				AirportInfoRecord publicPortRecord = new AirportInfoRecord("temp", "temp", AirportInfoRecord.UNSIGNED_INTEGER, AirportInfoRecord.UNENCRYPTED, 2, publicPortBytes);
				
				String[] addressEntry = new String[3];
				addressEntry[0] = publicPortRecord.toString();
				addressEntry[1] = privateIPRecord.toString();
				addressEntry[2] = privatePortRecord.toString();
				addressVector.insertElementAt(addressEntry, addressVector.size());
			}
			
			this.removeAll();
			tableModel = new PortMappingTableModel();
			table = new JTable(tableModel);
			table.setCellSelectionEnabled(true);
			table.setPreferredScrollableViewportSize(new Dimension(350,200));
			JScrollPane scrollPane = new JScrollPane(table);
			this.add(scrollPane);
		
		}
		catch (IOException e)
		{
			// shouldn't ever happen...
		}
		
	}
	
	
	
	

}