/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains wireless LAN information: network name, channel, and encryption switch and key.
*	Note that encryption key is supplied as 10-digit hex string (with no "0x" prefix).
*/

public class AirportDistanceRatePanel extends AirportInfoPanel
{
	private AirportInfoRadioButton large1MbpsButton, large2MbpsButton;
	private AirportInfoRadioButton medium1MbpsButton, medium2MbpsButton, medium5MbpsButton;
	private AirportInfoRadioButton small1MbpsButton, small2MbpsButton, small5MbpsButton, small11MbpsButton;
	private ButtonGroup buttonGroup;
	
	
	public AirportDistanceRatePanel(AirportInfo theInfo)
	{
		
		/**
		*	Access point density, multicast rate:
		*
		*		Multicast rate: 01 = 1 Mbps, 02 = 2 Mbps, 55 = 5.5 Mbps, 11 = 11 Mbps
		*		Density: 1 = low, 2 = medium, 3 = high
		*
		*				   large  medium   small
		*		1 Mbps		OK		OK		OK
		*		2 Mbps		OK		OK		OK
		*		5.5 Mbps	na		OK		OK
		*		11 Mbps		na		na		OK
		*/
		
		large1MbpsButton = new AirportInfoRadioButton("");
		large1MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000001");
		large1MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000001");
		
		medium1MbpsButton = new AirportInfoRadioButton("");
		medium1MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000002");
		medium1MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000001");
		
		small1MbpsButton = new AirportInfoRadioButton("");
		small1MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000003");
		small1MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000001");
		
		large2MbpsButton = new AirportInfoRadioButton("");
		large2MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000001");
		large2MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000002");
		
		medium2MbpsButton = new AirportInfoRadioButton("");
		medium2MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000002");
		medium2MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000002");
		
		small2MbpsButton = new AirportInfoRadioButton("");
		small2MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000003");
		small2MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000002");
		
		medium5MbpsButton = new AirportInfoRadioButton("");
		medium5MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000002");
		medium5MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000055");
		
		small5MbpsButton = new AirportInfoRadioButton("");
		small5MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000003");
		small5MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000055");
		
		small11MbpsButton = new AirportInfoRadioButton("");
		small11MbpsButton.addInfoRecord(theInfo.get("raDe"), "00000003");
		small11MbpsButton.addInfoRecord(theInfo.get("raMu"), "00000011");
		
		
		buttonGroup = new ButtonGroup();
		buttonGroup.add(large1MbpsButton);
		buttonGroup.add(medium1MbpsButton);
		buttonGroup.add(small1MbpsButton);
		buttonGroup.add(large2MbpsButton);
		buttonGroup.add(medium2MbpsButton);
		buttonGroup.add(small2MbpsButton);
		buttonGroup.add(medium5MbpsButton);
		buttonGroup.add(small5MbpsButton);
		buttonGroup.add(small11MbpsButton);
		
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.setLayout(theLayout);
		
		
		JLabel theLabel;
		
		c.gridwidth = 3;
		c.gridx = 2;
		c.gridy = 1;
		theLabel = new JLabel("Distance between access points");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridwidth = 1;
		
		c.insets = new Insets(2,10,8,10);
		
		c.gridx = 1;
		c.gridy = 2;
		theLabel = new JLabel("Rate:");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 2;
		c.gridy = 2;
		theLabel = new JLabel("Large  ");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 3;
		c.gridy = 2;
		theLabel = new JLabel("Medium");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 4;
		c.gridy = 2;
		theLabel = new JLabel("Small  ");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.insets = new Insets(2,2,2,2);
		
		c.gridx = 1;
		c.gridy = 3;
		theLabel = new JLabel("1 Mbps");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(large1MbpsButton, c);
		this.add(large1MbpsButton);
		
		c.gridx = 3;
		c.gridy = 3;
		theLayout.setConstraints(medium1MbpsButton, c);
		this.add(medium1MbpsButton);
		
		c.gridx = 4;
		c.gridy = 3;
		theLayout.setConstraints(small1MbpsButton, c);
		this.add(small1MbpsButton);
		
		
		c.gridx = 1;
		c.gridy = 4;
		theLabel = new JLabel("2 Mbps");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 2;
		c.gridy = 4;
		theLayout.setConstraints(large2MbpsButton, c);
		this.add(large2MbpsButton);
		
		c.gridx = 3;
		c.gridy = 4;
		theLayout.setConstraints(medium2MbpsButton, c);
		this.add(medium2MbpsButton);
		
		c.gridx = 4;
		c.gridy = 4;
		theLayout.setConstraints(small2MbpsButton, c);
		this.add(small2MbpsButton);
		
		
		c.gridx = 1;
		c.gridy = 5;
		theLabel = new JLabel("5.5 Mbps");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 3;
		c.gridy = 5;
		theLayout.setConstraints(medium5MbpsButton, c);
		this.add(medium5MbpsButton);
		
		c.gridx = 4;
		c.gridy = 5;
		theLayout.setConstraints(small5MbpsButton, c);
		this.add(small5MbpsButton);
		
		
		c.gridx = 1;
		c.gridy = 6;
		theLabel = new JLabel("11 Mbps");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 4;
		c.gridy = 6;
		theLayout.setConstraints(small11MbpsButton, c);
		this.add(small11MbpsButton);
		
		
	}
	
	
	
	
	
}