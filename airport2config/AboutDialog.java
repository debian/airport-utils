/*
 * SNMP Inquisitor
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class AboutDialog extends JDialog
								implements Runnable, ActionListener
{

	private JLabel aboutLabel1 = new JLabel(" ");
	private JLabel aboutLabel2 = new JLabel(" ");
	private JLabel aboutLabel3 = new JLabel(" ");
	private JLabel aboutLabel4 = new JLabel(" ");
	
	private String aboutString1 = AirportBaseStationConfigurator.aboutString;
	private String aboutString2 = "Version 2.1 ";
	private String aboutString3 = "J. Sevy ";
	private String aboutString4 = "April 2003 ";
	
	private JButton okButton;
	
	Thread displayThread;
	
	
	public AboutDialog(JFrame parent)
	{
		super(parent, "About...", true /*modal*/);
		
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		Font titleFont = new Font("SansSerif",Font.BOLD + Font.ITALIC, 12);
		Font labelFont = new Font("SansSerif",Font.PLAIN, 10);
			
		aboutLabel1.setText(aboutString1);
		aboutLabel2.setText(aboutString2);
		aboutLabel3.setText(aboutString3);
		aboutLabel4.setText(aboutString4);
		
		aboutLabel1.setFont(titleFont);
		aboutLabel2.setFont(labelFont);
		aboutLabel3.setFont(labelFont);
		aboutLabel4.setFont(labelFont);
			
		
		setUpDisplay();
		
		this.setLocation(Math.round((parent.size().width - this.size().width)/2), Math.round((parent.size().height - this.size().height)/2));
		
		// create and start display thread
		displayThread = new Thread(this);
		displayThread.start();
		
		this.show();
	
	}
	
	
	
	
	public void hide()
	{
		super.hide();
		
		// interrupt thread so it can exit..
		displayThread.interrupt();
	}
	
	
		
		
	private void setUpDisplay()
	{
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		
		JPanel aboutPanel = new JPanel();
		aboutPanel.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(aboutLabel1, c);
		aboutPanel.add(aboutLabel1);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(aboutLabel2, c);
		aboutPanel.add(aboutLabel2);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(aboutLabel3, c);
		aboutPanel.add(aboutLabel3);
		
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(aboutLabel4, c);
		aboutPanel.add(aboutLabel4);
		
		
		okButton = new JButton("OK");
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		this.getRootPane().setDefaultButton(okButton);
		
		this.getContentPane().setLayout(theLayout);
		
		c.insets = new Insets(8,8,8,8);
		
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(aboutPanel, c);
		this.getContentPane().add(aboutPanel);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(okButton, c);
		this.getContentPane().add(okButton);
		
		this.pack();
		
		//this.setSize(300, 180);
		
	}
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes, menu selections
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("ok"))
		{
			this.hide();
		}
	
	}
	
	
	
	public void run()
	{
		
		
		try
		{
			
			Color textColor = new Color(255, 0, 255);
		    aboutLabel1.setForeground(textColor);
			
			/*
			aboutLabel1.setSize(0, 30);
			
			for (int i = 0; i < this.getWidth() - 20; i++)
			{
				aboutLabel1.setSize(i, 30);
				Thread.currentThread().sleep(6);
			}
			*/
			
			
			// change color of other strings...
			int numSteps = 255;
			
			for (int i = 0; i < numSteps; i++)
			{
				textColor = new Color(255, 255-i, i);
				
				aboutLabel2.setForeground(textColor);
				aboutLabel3.setForeground(textColor);
				aboutLabel4.setForeground(textColor);
				
				Thread.currentThread().sleep(20);
			}
			
	
		}
		catch(Exception e)
		{
			// don't bother informing of exception; just exit...
			//System.out.println(e);
		}
			
		
		// later!
	}
			
			
			
}