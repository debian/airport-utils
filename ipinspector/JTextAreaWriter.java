/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
import java.io.*;
import javax.swing.text.*;



public class JTextAreaWriter extends Writer
{

    private JTextComponent textArea;
    
    
    public JTextAreaWriter(JTextComponent textArea)
    {
        this.textArea = textArea;
    }
    
    
    
    public void flush()
    {
        // do nothing
    }
    
    
    public void close()
    {
        // ditto
    }
    
    
    public void write (char[] charArray, int start, int end)
    {
        String messageString = new String(charArray);
        
        messageString = messageString.substring(start, end);
        textArea.setText(messageString);
    }
    
    
    public void write (String messageString)
    {
        textArea.setText(messageString);
    }

}