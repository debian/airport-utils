/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
import java.io.*;
import java.util.*;
import airport.*;


public class AirportInfoChangeEmailer
                    implements AirportInfoChangeListener
{
    
    String userEmailAddress;
    String mailHost;
    
    MailerThread mailerThread;
    Writer errorWriter;
    
    
    public AirportInfoChangeEmailer(String userEmailAddress, String mailHost, Writer errorWriter)
    {
        this.userEmailAddress = userEmailAddress;
        this.mailHost = mailHost;
        this.errorWriter = errorWriter;
    }
    
    
    // do whatever in response to a change in the retrieved info
    public synchronized void processInfoChange(IPInspectorAirportInfo oldInfo, IPInspectorAirportInfo newInfo)
    {
        // create message
        String messageText = "To: " + userEmailAddress + "\r\n";
        messageText = "From: Airport base station (" + newInfo.get("syNm").toString() + ")\r\n";
        messageText = "Date: " + (new Date()).toString() + "\r\n";
        messageText = "Subject: Change in Airport base station info\r\n\r\n\r\n";
        messageText += "Name: " + newInfo.get("syNm").toString() + "\r\n";
        messageText += "Location: " + newInfo.get("syLo").toString() + "\r\n";
        messageText += "Old WAN address: " + oldInfo.get("waIP").toString() + "\r\n";
        messageText += "New WAN address: " + newInfo.get("waIP").toString() + "\r\n";
            
        // start mailer thread
        mailerThread = new MailerThread(userEmailAddress, mailHost, messageText, errorWriter);
        mailerThread.start();
       
    }
    
}