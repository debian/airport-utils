/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 

import java.io.*;
import java.util.*;


public class MailerThread extends Thread
{
    
    String userEmailAddress;
    String mailHost;
    String messageText;
    
    POPWeasel mailman;
    
    Writer errorWriter;
    
    public MailerThread(String userEmailAddress, String mailHost, String messageText, Writer errorWriter)
    {
        this.userEmailAddress = userEmailAddress;
        this.mailHost = mailHost;
        this.messageText = messageText;
        this.errorWriter = errorWriter;

        mailman = new POPWeasel();
    }
    
    
    public void run()
    {
        
        Message changeMessage = new Message();
        changeMessage.recipients.add(userEmailAddress);
        changeMessage.text = messageText;
        
        try
        {
            mailman.sendMessage(userEmailAddress, mailHost, changeMessage);
        }
        catch (Exception e)
        {
            try
            {
                errorWriter.write("Error sending message: " + e.toString() + " (" + (new Date()).toString() + ").\n");
            }
            catch (IOException ex)
            {
                // print to System out
                System.out.println("Error sending message: " + e.toString() + " (" + (new Date()).toString() + ").");
            }
        }
        
        try
        {
            errorWriter.write("Sent notification of change to " + userEmailAddress + " (" + (new Date()).toString() + ").\n");
        }
        catch (IOException ex)
        {
            // print to System out
            System.out.println("Sent notification of change to " + userEmailAddress + " (" + (new Date()).toString() + ").");
        }
        
        
    }
    
}