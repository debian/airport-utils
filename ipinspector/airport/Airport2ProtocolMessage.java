

package airport;


import java.io.*;




public class Airport2ProtocolMessage
{
	public String messageTag = "acpp";
	
	public int unknownField1 = 1;
	
	public int messageChecksum = 0;
	
	public int payloadChecksum = 0;
	
	public int payloadSize;
	
	public byte[] unknownField2 = new byte[8];
	
	public int messageType;  // download = 0x14, upload = 0x15
	
	public byte[] unknownField3 = new byte[16];
	
	public byte[] password = new byte[32];
	
	public byte[] unknownField4 = new byte[48];
	
	
	
	public static final int READ = 0x14;
	public static final int WRITE = 0x15;
	
	
	
	public Airport2ProtocolMessage(int messageType, String password, byte[] payloadBytes, int payloadSize)
	{
		// set payload size and checksum
		if (messageType == READ)
		{
			this.payloadSize = payloadSize;
			this.payloadChecksum = computeChecksum(payloadBytes, payloadSize);
		}
		else
		{
			this.payloadSize = -1;
			this.payloadChecksum = 1;
		}
		
		// set message type
		this.messageType = messageType;
		
		// set encrypted password bytes
		byte[] passwordBytes = password.getBytes();
		
		int length = passwordBytes.length;
		
		if (length > 32)
			length = 32;
			
		for (int i = 0; i < length; i++)
		{
			this.password[i] = passwordBytes[i];
		}
		
		this.password = AirportInfoRecord.encryptBytes(AirportInfoRecord.cipherBytes, this.password);
		
		// get current message bytes, and use to compute checksum (including magic number)
		this.messageChecksum = computeChecksum(this.getBytes(), 128);
	}
	
	
	
	public byte[] getBytes()
	{
		// serialize all the fields
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(128);
		DataOutputStream outStream = new DataOutputStream(byteStream);
		
		try
		{
			outStream.writeBytes(messageTag);
			outStream.writeInt(unknownField1);
			outStream.writeInt(messageChecksum);
			outStream.writeInt(payloadChecksum);
			outStream.writeInt(payloadSize);
			outStream.write(unknownField2);
			outStream.writeInt(messageType);
			outStream.write(unknownField3);
			outStream.write(password);
			outStream.write(unknownField4);
		}
		catch(IOException e)
		{
		
		}
		
		return byteStream.toByteArray();
	}
	
	
	
	private int computeChecksum(byte[] fileBytes, int length)
	{
		int checksum;
		
		// just multiply each byte by descending sequence
		//int length = fileBytes.length;
		
		int weightedChecksum = 0;
		int ordinaryChecksum = 0;
	
		for (int i = 0; i < length; i++)
		{
			int byteValue = fileBytes[i];
			if (byteValue < 0)
				byteValue += 256;
			
			ordinaryChecksum += byteValue;	
			weightedChecksum += byteValue * (length - i);
		}
		
		ordinaryChecksum += 1;
		weightedChecksum += length;
		
		ordinaryChecksum = ordinaryChecksum & 0x0000FFFF;
		
		// boil checksum down to 16 bits
		while (weightedChecksum > 0x0000FFFF)
		{
			int upperHalf = (weightedChecksum & 0xFFFF0000) >> 16;
			int lowerHalf = (weightedChecksum & 0x0000FFFF);
			weightedChecksum = lowerHalf + (upperHalf * 0x0F);
		}
		
		checksum = ordinaryChecksum | (weightedChecksum << 16);
		
		return checksum;
	}
	
}