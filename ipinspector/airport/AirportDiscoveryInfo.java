/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.util.*;



/**
* 	Collects together information about the location of pieces of information 
* 	in the 116-byte Discovery memory block, and creates appropriate AirportInfoRecords
* 	(with ByteBlockWindows into the underlying ByteBlock) to read and write 
*	the information.
*/

public class AirportDiscoveryInfo extends Hashtable
{
	
	
	
	/**
	*	Create new
	*/
	
	public AirportDiscoveryInfo(byte[] baseBlock)
	{
	
		int baseStartIndex;
		int maxSize;
		int dataType;
		int encryption;
		byte[] bytes;
		String name;
		
	
		
		/**
		*	Base station MAC address
		*/

		baseStartIndex = 0x024;
		maxSize = 6;
		bytes = new byte[maxSize];
		for (int i = 0; i < maxSize; i++)
			bytes[i] = baseBlock[baseStartIndex + i];
		encryption = AirportInfoRecord.UNENCRYPTED;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Base station Mac address";
		this.put(name, new AirportInfoRecord(name, name, dataType, encryption, maxSize, bytes));
		
		
		
		/**
		*	Base station IP address: byte 2*16 + 13
		*/
		
		baseStartIndex = 0x02C;
		maxSize = 4;
		bytes = new byte[maxSize];
		for (int i = 0; i < maxSize; i++)
			bytes[i] = baseBlock[baseStartIndex + i];
		encryption = AirportInfoRecord.UNENCRYPTED;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Base station IP address";
		this.put(name, new AirportInfoRecord(name, name, dataType, encryption, maxSize, bytes));
		
		
		
		/**
		*	Base station name: byte 3*16 + 1
		*/
		
		baseStartIndex = 0x030;
		maxSize = 32;
		bytes = new byte[maxSize];
		for (int i = 0; i < maxSize; i++)
			bytes[i] = baseBlock[baseStartIndex + i];
		encryption = AirportInfoRecord.UNENCRYPTED;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "System name";
		this.put(name, new AirportInfoRecord(name, name, dataType, encryption, maxSize, bytes));
		
		
		
		/**
		*	Base station uptime: byte 5*16 + 1
 		*/
		
		baseStartIndex = 0x050;
		maxSize = 4;
		bytes = new byte[maxSize];
		for (int i = 0; i < maxSize; i++)
			bytes[i] = baseBlock[baseStartIndex + i];
		encryption = AirportInfoRecord.UNENCRYPTED;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Base station uptime";
  		this.put(name, new AirportInfoRecord(name, name, dataType, encryption, maxSize, bytes));
		

		
		/**
		*	Device identifying string: byte 5*16 + 5
		*/
		
		baseStartIndex = 0x054;
		maxSize = 32;
		bytes = new byte[maxSize];
		for (int i = 0; i < maxSize; i++)
			bytes[i] = baseBlock[baseStartIndex + i];
		encryption = AirportInfoRecord.UNENCRYPTED;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Device identifying string";
		this.put(name, new AirportInfoRecord(name, name, dataType, encryption, maxSize, bytes));
		
		
		
		
		
	}
	
	
	
	public AirportInfoRecord get(String name)
	{
		return (AirportInfoRecord)super.get(name);
	}
	
	
	
	
}