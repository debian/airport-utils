/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 


package airport;


import java.util.*;
import java.io.*;





public class IPInspectorAirportInfo extends Hashtable
{
	
	public static final int MAX_NUM_MAC_ADDRESSES = 256;
	public static final int MAX_NUM_PORT_MAPS = 20;
	public static final int MAX_NUM_SNMP_ACCESS_ENTRIES = 5;
	public static final int MAX_NUM_LOGIN_CHARS = 127;
	
	
	
	public IPInspectorAirportInfo()
	{
		initializeHashtable();
	}	
	
	
	
	
	public IPInspectorAirportInfo(byte[] retrievedBytes)
		throws IllegalArgumentException
	{
		int count = 0;
		byte[] invalidBytes = {(byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xF6};
		
		ByteArrayInputStream byteStream = new ByteArrayInputStream(retrievedBytes);
		
		
		initializeHashtable();
		
		
		while (byteStream.available() > 0)
		{
			
			// read the tag
			byte[] tagBytes = new byte[4];
			byteStream.read(tagBytes, 0, 4);
			String tag = new String(tagBytes);
			
			// break if tag is all zeros: end of info in array
			if (tag.equals("\0\0\0\0"))
				break;
			
			// get the corresponding element
			AirportInfoRecord element = this.get(tag);
			
			// increment count; used at end to determine if we got any valid info
			count++;
			
			// check to make sure the element's not null, in case have received
			// unknown tag: just ignore if null
			if (element != null)
			{
				//read the encryption
				byte[] encryptionBytes = new byte[4];
				byteStream.read(encryptionBytes, 0, 4);
				element.encryption = getIntegerValue(encryptionBytes);
				
				//read the length
				byte[] lengthBytes = new byte[4];
				byteStream.read(lengthBytes, 0, 4);
				int length = getIntegerValue(lengthBytes);
				
				//read the value
				byte[] valueBytes = new byte[length];
				byteStream.read(valueBytes, 0, length);
				
				if (element.encryption == AirportInfoRecord.ENCRYPTED)
					valueBytes = AirportInfoRecord.decryptBytes(AirportInfoRecord.cipherBytes, valueBytes);
				
				// check if the value being sent is 0xFFFFFF6; this indicates
				// the current value is invalid - just leave as 0. Ignore for
				// IP addresses, though...
				if (!arraysEqual(valueBytes, invalidBytes) || (element.dataType == AirportInfoRecord.IP_ADDRESS))
					element.value = valueBytes;
				
				//System.out.println("Tag " + tag + ": " + element.toString());
			}
			else
			{	
				
				// just add an entry in hashtable
				element = new AirportInfoRecord();
				
				// assign the tag
				element.tag = tag;
				
				//read the encryption
				byte[] encryptionBytes = new byte[4];
				byteStream.read(encryptionBytes, 0, 4);
				element.encryption = getIntegerValue(encryptionBytes);
				
				//read the length
				byte[] lengthBytes = new byte[4];
				byteStream.read(lengthBytes, 0, 4);
				int length = getIntegerValue(lengthBytes);
				element.maxLength = length;
				
				//read the value
				byte[] valueBytes = new byte[length];
				byteStream.read(valueBytes, 0, length);
				
				if (element.encryption == AirportInfoRecord.ENCRYPTED)
					valueBytes = AirportInfoRecord.decryptBytes(AirportInfoRecord.cipherBytes, valueBytes);
				
				// check if the value being sent is 0xFFFFFF6; this indicates
				// the current value is invalid - just leave as 0. Ignore for
				// IP addresses, though...
				if (!arraysEqual(valueBytes, invalidBytes) || (element.dataType == AirportInfoRecord.IP_ADDRESS))
					element.value = valueBytes;
				else
					element.value = new byte[element.maxLength];
				
				// add the element
				this.put(tag, element);
				
				//System.out.println("Unused tag " + tag + ": " + element.toString());
			}
			
		}
		
		
		if (count == 0)
		{
			// no data retrieved; throw exception!
			throw new IllegalArgumentException("No information retrieved from base station!");
		}
		
		
		
	}
	
	
	
	private void initializeHashtable()
	{
		
		// populate the hashtable
		int maxSize;
		int dataType;
		int encryption;
		String description;
		String tag;
		
		
		//	Configuration mode switch: 
		//		Modem config:     00 00 09 00
		//		Ethernet manual:  00 00 04 00
		//		Ethernet DHCP:    00 00 03 00
		//		Ethernet PPPoE:   00 00 09 00
		//		
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.BYTE_STRING;
		encryption = AirportInfoRecord.UNENCRYPTED;
		description = "Configuration mode";
		tag = "waCV";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
		
		//	Base station IP address: 0x46A
		
		
		maxSize = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Base station IP address";
		tag = "waIP";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		//	Name of base station
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Base station name";
		tag = "syNm";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
	
		
		
		//	Base station location:
		
		maxSize = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		encryption = AirportInfoRecord.ENCRYPTED;
		description = "Base station location";
		tag = "syLo";
		this.put(tag, new AirportInfoRecord(tag, description, dataType, encryption, maxSize));
		
		
	}
	
	
	
	
	public AirportInfoRecord get(String name)
	{
		//System.out.println("Get info record " + name);
		return (AirportInfoRecord)super.get(name);
	}
	
	
	
	public byte[] getUpdateBytes()
	{
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		
		Enumeration elementEnumeration = this.elements();
		
		while (elementEnumeration.hasMoreElements())
		{
			AirportInfoRecord nextElement = (AirportInfoRecord)elementEnumeration.nextElement();
			
			try
			{
				outStream.write(nextElement.getUpdateBytes());
				//System.out.println("Update bytes: tag " + nextElement.tag + ": " + nextElement.toString());
			}
			catch (IOException e)
			{
				// can't happen
			}
			
		}
		
		return outStream.toByteArray();
	}
	
	
	
	public byte[] getRequestBytes()
	{
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		
		Enumeration elementEnumeration = this.elements();
		
		while (elementEnumeration.hasMoreElements())
		{
			AirportInfoRecord nextElement = (AirportInfoRecord)elementEnumeration.nextElement();
			
			try
			{
				outStream.write(nextElement.getRequestBytes());
			}
			catch (IOException e)
			{
				// can't happen
			}
		}
		
		return outStream.toByteArray();
	}
	
	
	
	public String toString()
	{
		String returnText = new String();
		
		Enumeration elementEnumeration = this.elements();
		
		while (elementEnumeration.hasMoreElements())
		{
			AirportInfoRecord theElement = (AirportInfoRecord)elementEnumeration.nextElement();
			returnText += theElement.toString();
			returnText += "\n";
		}
		
		return returnText;
		
	}
	
	
	
	private int getIntegerValue(byte[] valueBytes)
	{
		int value = 0;
		
		for (int i = 0; i < valueBytes.length; i++)
		{
			int absValue = valueBytes[i];
			
			if (absValue < 0)
				absValue += 256;
			
			value = value*256 + absValue;
		}
		
		return value;
	}
	
	
	
	private static boolean arraysEqual(byte[] a, byte[] b)
	{
		if (a.length != b.length)
		{
			return false;
		}
		else
		{
			for (int i = 0; i < a.length; i++)
			{
				if (a[i] != b[i])
					return false;
			}
		}
		
		return true;
	}
	
	
	
}