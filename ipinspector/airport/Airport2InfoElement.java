/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.io.*;




public class Airport2InfoElement
{
	public String tag;
	
	public int dataType;
	
	public int encryption;
	
	public int length;
	
	public byte[] value;
	
	
	public static byte[] cipherBytes = {(byte)0x0e, (byte)0x39, (byte)0xf8, (byte)0x05, (byte)0xc4, (byte)0x01, (byte)0x55, (byte)0x4f, (byte)0x0c, (byte)0xac, (byte)0x85, (byte)0x7d, (byte)0x86, (byte)0x8a, (byte)0xb5, (byte)0x17, (byte)0x3e, (byte)0x09, (byte)0xc8, (byte)0x35, (byte)0xf4, (byte)0x31, (byte)0x65, (byte)0x7f, (byte)0x3c, (byte)0x9c, (byte)0xb5, (byte)0x6d, (byte)0x96, (byte)0x9a, (byte)0xa5, (byte)0x07, (byte)0x2e, (byte)0x19, (byte)0xd8, (byte)0x25, (byte)0xe4, (byte)0x21, (byte)0x75, (byte)0x6f, (byte)0x2c, (byte)0x8c, (byte)0xa5, (byte)0x9d, (byte)0x66, (byte)0x6a, (byte)0x55, (byte)0xf7, (byte)0xde, (byte)0xe9, (byte)0x28, (byte)0xd5, (byte)0x14, (byte)0xd1, (byte)0x85, (byte)0x9f, (byte)0xdc, (byte)0x7c, (byte)0x55, (byte)0x8d, (byte)0x76, (byte)0x7a, (byte)0x45, (byte)0xe7};
	
	
	
	public Airport2InfoElement(String tag, int encryption, int length, byte[] value)
	{
		this.tag = tag;
		this.encryption = encryption;
		this.length = length;
		this.value = value;
	}
	
	
	
	public Airport2InfoElement(String tag, int encryption, String stringValue)
	{
		this.tag = tag;
		this.encryption = encryption;
		this.length = stringValue.length();
		this.value = stringValue.getBytes();
		
		if (encryption == 2)
		{
			this.value = encryptBytes(cipherBytes, this.value);
		}
	}
	
	
	
	public Airport2InfoElement()
	{
		this.tag = "";
		this.encryption = 0;
		this.length = 0;
		this.value = null;
	}
	
	
	
	public int getSize()
	{
		int size = tag.length() + 4 + 4;
		
		if (value != null)
			size += value.length;
			
		return (size);
	}
	
	
	
	public byte[] getBytes()
	{
		// serialize all the fields
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(128);
		DataOutputStream outStream = new DataOutputStream(byteStream);
		
		try
		{
			outStream.writeBytes(tag);
			outStream.writeInt(encryption);
			outStream.writeInt(length);
			if (value != null)
				outStream.write(value);
		}
		catch(IOException e)
		{
			// IOException should never occur...
		}
		
		return byteStream.toByteArray();
	}
	
	
	
	public String toString()
	{
		String returnText = new String();
		
		returnText += "Name:   " + this.tag + "\n";
		returnText += "  encryption:         " + this.encryption + "\n";
		returnText += "  length:       " + this.length + "\n";
		if (this.length == 0)
		{
			returnText += "  value:  \n";
		}
		else
		{
			if (this.encryption == 2)
			{
				returnText += "  value (hex):  " + new String(hexBytes(decryptBytes(cipherBytes, this.value))) + "\n";
				returnText += "  value (char): " + new String(decryptBytes(cipherBytes, this.value)) + "\n";
			}
			else
			{
				returnText += "  value (hex):  " + new String(hexBytes(this.value)) + "\n";
				returnText += "  value (char): " + new String(this.value) + "\n";
			}
		}
		returnText += "\n";
		
		
		return returnText;
		
	}
	
	
	public static byte[] decryptBytes(byte[] cipherString, byte[] encryptedString)
	{
		byte[] returnBytes = new byte[encryptedString.length];
		
		// just xor each byte in encryptedString with cipherString
		int length = encryptedString.length;
		if(cipherString.length < encryptedString.length)
			length = cipherString.length;
		
		for (int i = 0; i < length; i++)
		{
			returnBytes[i] = (byte)(encryptedString[i] ^ cipherString[i]);
		}
		
		return returnBytes;
	}
	
	
	
	public static byte[] encryptBytes(byte[] cipherString, byte[] encryptedString)
	{
		return decryptBytes(cipherString, encryptedString);
	}
	
	
	
	private int getIntegerValue(byte[] valueBytes)
	{
		int value = 0;
		
		for (int i = 0; i < valueBytes.length; i++)
		{
			int absValue = valueBytes[i];
			
			if (absValue < 0)
				absValue += 256;
			
			value = value*256 + absValue;
		}
		
		return value;
	}
	
	
	
	private String hexByte(byte b)
	{
		int pos = b;
		if (pos < 0)
			pos += 256;
		String returnString = new String();
		returnString += Integer.toHexString(pos/16);
		returnString += Integer.toHexString(pos%16);
		return returnString;
	}
	
	
	
	private String hexBytes(byte[] bytes)
	{
		String returnString = new String();
		
		for(int i = 0; i < bytes.length; i++)
		{
			returnString += hexByte(bytes[i]) + " ";
		}
		
		return returnString;
		
	}
	
}