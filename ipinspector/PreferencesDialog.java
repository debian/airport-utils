/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;




public class PreferencesDialog extends JDialog
                        implements ActionListener
{

    JTextField ipAddressField = new JTextField(10);
    JPasswordField passwordField = new JPasswordField(10);
    JTextField smtpHostField = new JTextField(20);
    JTextField emailAddressField = new JTextField(20);
    JTextField intervalField = new JTextField(10);
    JLabel ipAddressLabel = new JLabel("Base station address:");
    JLabel passwordLabel = new JLabel("Base station password:");
    JLabel smtpHostLabel = new JLabel("SMTP (email) host:");
    JLabel emailAddressLabel = new JLabel("Email address for notices:");
    JLabel intervalLabel = new JLabel("Base station query interval (seconds):");
    
    JButton okButton, cancelButton;
    
    boolean cancelled = false;
    Preferences newPrefs;
    
    
    public PreferencesDialog(Frame owner, Preferences currentPreferences)
    {
        // create as modal dialog
        super(owner, "Preferences", true);
        
        // set new prefs to current
        newPrefs = currentPreferences;
        
        ipAddressField.setText(currentPreferences.ipAddress);
        passwordField.setText("");
        smtpHostField.setText(currentPreferences.smtpHost);
        emailAddressField.setText(currentPreferences.emailAddress);
        intervalField.setText("" + currentPreferences.queryInterval);
       
        
        okButton = new JButton("OK");
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		this.getRootPane().setDefaultButton(okButton);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(this);
        
        // set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.getContentPane().setLayout(theLayout);
		
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(ipAddressLabel, c);
		this.getContentPane().add(ipAddressLabel);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(ipAddressField, c);
		this.getContentPane().add(ipAddressField);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(passwordLabel, c);
		this.getContentPane().add(passwordLabel);
		
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(passwordField, c);
		this.getContentPane().add(passwordField);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(emailAddressLabel, c);
		this.getContentPane().add(emailAddressLabel);
		
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(emailAddressField, c);
		this.getContentPane().add(emailAddressField);
		
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(smtpHostLabel, c);
		this.getContentPane().add(smtpHostLabel);
		
		c.gridx = 2;
		c.gridy = 4;
		theLayout.setConstraints(smtpHostField, c);
		this.getContentPane().add(smtpHostField);
		
		c.gridx = 1;
		c.gridy = 5;
		theLayout.setConstraints(okButton, c);
		this.getContentPane().add(okButton);
		
		c.gridx = 2;
		c.gridy = 5;
		theLayout.setConstraints(cancelButton, c);
		this.getContentPane().add(cancelButton);
		
		this.pack();
		this.show();
		
    }
    
    
    
    public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes, menu selections
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("ok"))
		{
			if (validateNewPreferences())
			    this.hide();
		}
		
		
		
		if (command == "cancel")
		{
			cancelled = true;
			this.hide();
		}
		
		
	}
	
	
	
	private boolean validateNewPreferences()
	{
	    int interval;
	    
	    // check the interval parameter
	    try
	    {
	        interval = Integer.parseInt(intervalField.getText());
	        if (interval <= 0)
			    throw new NumberFormatException();
			
			newPrefs = new Preferences();
        
            newPrefs.ipAddress = ipAddressField.getText();
            newPrefs.password = passwordField.getText();
            newPrefs.smtpHost = smtpHostField.getText();
            newPrefs.emailAddress = emailAddressField.getText();
            newPrefs.queryInterval = interval;
            
    	    return true;
    	    
	    }
	    catch(NumberFormatException e)
	    {
	        JOptionPane.showMessageDialog(this, "Value supplied must be a positive integer.");
	        return false;
	    }
	    
	    
	}
    
    
    public Preferences getPreferences()
    {
        return newPrefs;
    }
    
    
    
    public boolean isCancelled()
    {
        return cancelled;
    }

}