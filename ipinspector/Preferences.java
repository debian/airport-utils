/*
 * IPInspector
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 /* Preferences class

Just a utility class

*/

import java.io.*;
import java.net.*;


public class Preferences 
                implements Serializable
{
	public String ipAddress;
	public String password;
	public String emailAddress;
	public String smtpHost;
	public int queryInterval;
	
	
	public Preferences()
	{
	    ipAddress = "";
    	password = "";
    	emailAddress = "";
    	smtpHost = "";
    	queryInterval = 5;
	}
	
	
	public boolean equals(Object otherPrefsObject)
	{
	    Preferences otherPrefs = (Preferences)otherPrefsObject;
	    
	    // compare all fields but passwords
	    if 
	    (
	        (this.ipAddress.equals(otherPrefs.ipAddress))
	      &&
	        (this.emailAddress.equals(otherPrefs.emailAddress))
	      &&
	        (this.smtpHost.equals(otherPrefs.smtpHost))
	      &&
	        (this.queryInterval == otherPrefs.queryInterval)
	    )
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	        
	}
	
}