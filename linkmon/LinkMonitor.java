/*
 * Airport Monitor Utility
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.util.*;
import java.net.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.plaf.*;
import java.awt.event.*;
import java.io.*;
import java.math.*;
import snmp.*;




public class LinkMonitor extends JFrame
							implements ActionListener, Runnable
{
	
	JButton newHostButton, newCommunityButton, newWirelessHostButton, scanButton;
	JTextArea messagesArea;
	JScrollPane messagesScroll;
	JTextField hostIDField, communityField, wirelessHostField, wirelessHostMACField;
	JLabel authorLabel, hostIDLabel, communityLabel, wirelessHostLabel, wirelessHostMACLabel;
	
	JProgressBar localSignalBar, localSNRBar, localNoiseBar;
	JProgressBar remoteSignalBar, remoteSNRBar, remoteNoiseBar;
	
	JLabel localSignalTextField, localSNRTextField, localNoiseTextField;
	JLabel remoteSignalTextField, remoteSNRTextField, remoteNoiseTextField;
	
	MenuBar theMenubar;
	Menu fileMenu;
	MenuItem quitItem, aboutItem;
	
	SNMPv1CommunicationInterface comInterface;
	String community;
	InetAddress hostAddress;
	int version;
	int wirelessHostNum = 1;
	
	Thread statusThread;
	
		
	
	// WindowCloseAdapter to catch window close-box closings
	private class WindowCloseAdapter extends WindowAdapter
	{ 
		public void windowClosing(WindowEvent evt)
		{
			// stop current statusThread, if alive, and wait for it to die:
			stopLinkTest();
			
			// then git!
			System.exit(0);
		}
	};
	
			
	
	public LinkMonitor() 
	{
		setUpDisplay();
		
		try
		{
			community = "public";
			version = 0;	// SNMPv1
			hostAddress = InetAddress.getByName(hostIDField.getText());
			
			comInterface = new SNMPv1CommunicationInterface(version, hostAddress, community);
			// set socket timeout to 5 seconds
			comInterface.setSocketTimeout(5000);
			
			statusThread = new Thread(this);
			
		}
		catch(Exception e)
		{
			messagesArea.setText("Exception during connection status polling startup:  " + e + "\n");
		}
		
		
		
	}
	
	
	
	private void setUpDisplay()
	{
		
		
		try
		{
			// set look-and-feel to native platform l&f, if possible
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e)
		{
			// or not...
		}

		
		this.getRootPane().setBorder(new BevelBorder(BevelBorder.RAISED));
		
		// set fonts to smaller-than-normal size, for compaction!
		UIManager manager = new UIManager();
		FontUIResource appFont = new FontUIResource("SansSerif", Font.PLAIN, 10);
		UIDefaults defaults = manager.getLookAndFeelDefaults();
		Enumeration keys = defaults.keys();
		
		while (keys.hasMoreElements())
		{
			String nextKey = (String)(keys.nextElement());
			if ((nextKey.indexOf("font") > -1) || (nextKey.indexOf("Font") > -1))
			{
				manager.put(nextKey, appFont);
			}
		}
		
		
		// add WindowCloseAdapter to catch window close-box closings
		addWindowListener(new WindowCloseAdapter());

		
		theMenubar = new MenuBar();
		this.setMenuBar(theMenubar);
		fileMenu = new Menu("File");
		
		aboutItem = new MenuItem("About...");
		aboutItem.setActionCommand("about");
		aboutItem.addActionListener(this);
		fileMenu.add(aboutItem);
		
		fileMenu.addSeparator();
		
		quitItem = new MenuItem("Quit");
		quitItem.setActionCommand("quit");
		quitItem.addActionListener(this);
		fileMenu.add(quitItem);
		
		theMenubar.add(fileMenu);
		
		
		hostIDLabel = new JLabel("Base station address:");
		hostIDField = new JTextField(20);
		hostIDField.setText("10.0.1.1");
		hostIDField.setEditable(false);
		
		communityLabel = new JLabel("Community (password):");
		communityField = new JTextField(20);
		communityField.setText("");
		communityField.setEditable(false);
		
		wirelessHostLabel = new JLabel("Wireless host name:");
		wirelessHostField = new JTextField(20);
		wirelessHostField.setText("");
		wirelessHostField.setEditable(false);
		
		wirelessHostMACLabel = new JLabel("Wireless host MAC:");
		wirelessHostMACField = new JTextField(20);
		wirelessHostMACField.setText("");
		wirelessHostMACField.setEditable(false);
		
		authorLabel = new JLabel(" Version 1.1        J. Sevy, May 2001");
		authorLabel.setFont(new Font("SansSerif", Font.ITALIC, 8));
			
		
		newHostButton = new JButton("   Set address  ");
		newHostButton.setActionCommand("new host");
		newHostButton.addActionListener(this);
		
		newCommunityButton = new JButton(" Set community ");
		newCommunityButton.setActionCommand("new community");
		newCommunityButton.addActionListener(this);
		
		newWirelessHostButton = new JButton("Select new host");
		newWirelessHostButton.setActionCommand("new wireless host");
		newWirelessHostButton.addActionListener(this);
		
		scanButton = new JButton("Test wireless link");
		scanButton.setActionCommand("test link");
		scanButton.addActionListener(this);
		
		
		localSignalBar = new JProgressBar(0, 100);
		localSignalBar.setValue(0);
		
		localSNRBar = new JProgressBar(0, 100);
		localSNRBar.setValue(0);
		
		localNoiseBar = new JProgressBar(0, 100);
		localNoiseBar.setValue(0);
		
		remoteSignalBar = new JProgressBar(0, 100);
		remoteSignalBar.setValue(0);
		
		remoteSNRBar = new JProgressBar(0, 100);
		remoteSNRBar.setValue(0);
		
		remoteNoiseBar = new JProgressBar(0, 100);
		remoteNoiseBar.setValue(0);
		
		
		Dimension labelDimension = new Dimension(20,20);
		
		localSignalTextField = new JLabel();
		localSignalTextField.setPreferredSize(labelDimension);
		
		localSNRTextField = new JLabel();
		localSNRTextField.setPreferredSize(labelDimension);
		
		localNoiseTextField = new JLabel();
		localNoiseTextField.setPreferredSize(labelDimension);
		
		remoteSignalTextField = new JLabel();
		remoteSignalTextField.setPreferredSize(labelDimension);
		
		remoteSNRTextField = new JLabel();
		remoteSNRTextField.setPreferredSize(labelDimension);
		
		remoteNoiseTextField = new JLabel();
		remoteNoiseTextField.setPreferredSize(labelDimension);
		
		
		
		messagesArea = new JTextArea(5,50);
		messagesScroll = new JScrollPane(messagesArea);
		 
		/*
        URL url = AirportBaseStationHangup.class.getResource("iconImage.gif");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(url));
		*/
		
		
		// now set up display
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.setTitle("Access Point Wireless Link Quality Monitoring Utility");
		
		
		Panel hostPanel = new Panel();
		hostPanel.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(hostIDLabel, c);
		hostPanel.add(hostIDLabel);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(hostIDField, c);
		hostPanel.add(hostIDField);
		
		c.gridx = 3;
		c.gridy = 1;
		theLayout.setConstraints(newHostButton, c);
		hostPanel.add(newHostButton);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(communityLabel, c);
		hostPanel.add(communityLabel);
		
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(communityField, c);
		hostPanel.add(communityField);
		
		c.gridx = 3;
		c.gridy = 2;
		theLayout.setConstraints(newCommunityButton, c);
		hostPanel.add(newCommunityButton);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(wirelessHostLabel, c);
		hostPanel.add(wirelessHostLabel);
		
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(wirelessHostField, c);
		hostPanel.add(wirelessHostField);
		
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(wirelessHostMACLabel, c);
		hostPanel.add(wirelessHostMACLabel);
		
		c.gridx = 2;
		c.gridy = 4;
		theLayout.setConstraints(wirelessHostMACField, c);
		hostPanel.add(wirelessHostMACField);
		
		c.gridheight = 2;
		c.gridx = 3;
		c.gridy = 3;
		theLayout.setConstraints(newWirelessHostButton, c);
		hostPanel.add(newWirelessHostButton);
		
		c.gridheight = 1;
		c.gridwidth = 3;
		c.gridx = 1;
		c.gridy = 5;
		theLayout.setConstraints(scanButton, c);
		hostPanel.add(scanButton);
		
		c.gridwidth = 1;
		
		
		JLabel barLabel;
		
		Panel localPanel = new Panel();
		localPanel.setLayout(theLayout);
		
		
		c.gridx = 1;
		c.gridy = 1;
		barLabel = new JLabel("Local signal level");
		theLayout.setConstraints(barLabel, c);
		localPanel.add(barLabel);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(localSignalBar, c);
		localPanel.add(localSignalBar);
		
		c.gridx = 3;
		c.gridy = 1;
		theLayout.setConstraints(localSignalTextField, c);
		localPanel.add(localSignalTextField);
		
		c.gridx = 1;
		c.gridy = 2;
		barLabel = new JLabel("Local SNR");
		theLayout.setConstraints(barLabel, c);
		localPanel.add(barLabel);
		
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(localSNRBar, c);
		localPanel.add(localSNRBar);
		
		c.gridx = 3;
		c.gridy = 2;
		theLayout.setConstraints(localSNRTextField, c);
		localPanel.add(localSNRTextField);
		
		c.gridx = 1;
		c.gridy = 3;
		barLabel = new JLabel("Local noise level");
		theLayout.setConstraints(barLabel, c);
		localPanel.add(barLabel);
		
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(localNoiseBar, c);
		localPanel.add(localNoiseBar);
		
		c.gridx = 3;
		c.gridy = 3;
		theLayout.setConstraints(localNoiseTextField, c);
		localPanel.add(localNoiseTextField);
		
		
		
		Panel remotePanel = new Panel();
		remotePanel.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 1;
		barLabel = new JLabel("Remote signal level");
		theLayout.setConstraints(barLabel, c);
		remotePanel.add(barLabel);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(remoteSignalBar, c);
		remotePanel.add(remoteSignalBar);
		
		c.gridx = 3;
		c.gridy = 1;
		theLayout.setConstraints(remoteSignalTextField, c);
		remotePanel.add(remoteSignalTextField);
		
		c.gridx = 1;
		c.gridy = 2;
		barLabel = new JLabel("Remote SNR");
		theLayout.setConstraints(barLabel, c);
		remotePanel.add(barLabel);
		
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(remoteSNRBar, c);
		remotePanel.add(remoteSNRBar);
		
		c.gridx = 3;
		c.gridy = 2;
		theLayout.setConstraints(remoteSNRTextField, c);
		remotePanel.add(remoteSNRTextField);
		
		c.gridx = 1;
		c.gridy = 3;
		barLabel = new JLabel("Remote noise level");
		theLayout.setConstraints(barLabel, c);
		remotePanel.add(barLabel);
		
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(remoteNoiseBar, c);
		remotePanel.add(remoteNoiseBar);
		
		c.gridx = 3;
		c.gridy = 3;
		theLayout.setConstraints(remoteNoiseTextField, c);
		remotePanel.add(remoteNoiseTextField);
		
		
		
		Panel barPanel = new Panel();
		barPanel.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(localPanel, c);
		barPanel.add(localPanel);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(remotePanel, c);
		barPanel.add(remotePanel);
		
		
		this.getContentPane().setLayout(theLayout);
		
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(hostPanel, c);
		this.getContentPane().add(hostPanel);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(barPanel, c);
		this.getContentPane().add(barPanel);
		
		c.gridx = 1;
		c.gridy = 3;
		JLabel messagesLabel = new JLabel("Messages:");
		theLayout.setConstraints(messagesLabel, c);
		this.getContentPane().add(messagesLabel);
		
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(messagesScroll, c);
		this.getContentPane().add(messagesScroll);				
		
		c.gridx = 1;
		c.gridy = 5;
		theLayout.setConstraints(authorLabel, c);
		this.getContentPane().add(authorLabel);
		
		
	}
	
	
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes, menu selections
	{
		String command = theEvent.getActionCommand();
		
	
		if (command == "quit")
		{
			// stop current link test thread, and wait for it to die:
			stopLinkTest();
			
			System.exit(0);
		}
		
		
		
		if (command == "about")
		{
			AboutDialog aboutDialog = new AboutDialog(this);
		}
		
		
		
		if (command == "new host")
		{
			
			String newHost = JOptionPane.showInputDialog("Input access point address:");
			if (newHost != null)
			{
						
				try
				{
					version = 0;	// SNMPv1
					hostAddress = InetAddress.getByName(newHost);
			
					// stop current link test thread, and wait for it to die:
					stopLinkTest();
					
					// set up polling parameters
					comInterface = new SNMPv1CommunicationInterface(version, hostAddress, community);
					// set socket timeout to 5 seconds
					comInterface.setSocketTimeout(5000);
					
					hostIDField.setText(newHost);					
				
				}
				catch(UnknownHostException e)
				{
					JOptionPane.showMessageDialog(this, "Unknown host supplied.");
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(this, "Error setting new access point address: " + e);
				}
				
			}
			
		}
		
		
		
		if (command == "new community")
		{
			
			String newCommunity = JOptionPane.showInputDialog("Input new community:");
			
			if (newCommunity != null)
			{
				try
				{
					version = 0;	// SNMPv1
					community = newCommunity;
					
					// stop current link test thread, and wait for it to die:
					stopLinkTest();
					
					// set up polling parameters
					comInterface = new SNMPv1CommunicationInterface(version, hostAddress, community);
					// set socket timeout to 5 seconds
					comInterface.setSocketTimeout(5000);
					
					communityField.setText(newCommunity);					
				
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(this, "Error setting new community: " + e);
				}
				
			}
			
		}
		
		
		
		
		if (command == "new wireless host")
		{
			
			try
			{
				// stop current link test thread, and wait for it to die:
				stopLinkTest();
				
				// clear messages area
				messagesArea.setText("");
				
				int k;
				
				// try 10 times
				for (k = 0; k < 10; k++)
				{
				
					// do wireless host discovery process
					String itemID;
					SNMPVarBindList newVars;
					SNMPSequence pair;
					SNMPInteger snmpIntegerValue;
					SNMPObject snmpValue;

					// first, need to write values to appropriate OID's - why, I don't know...	
					for (int i = 1; i <= 3; i++)
					{
						itemID = "1.3.6.1.4.1.762.2.5.5." + i;
						comInterface.setMIBEntry(itemID, new SNMPInteger(50));
						
						itemID = "1.3.6.1.4.1.762.2.5.4." + i;
						comInterface.setMIBEntry(itemID, new SNMPInteger(3));
					}
					
					// now, read the number of wireless hosts to select from
					itemID = "1.3.6.1.4.1.762.2.5.1.0";
					newVars = comInterface.getMIBEntry(itemID);
					pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
					snmpIntegerValue = (SNMPInteger)pair.getSNMPObjectAt(1);
					int numHosts = Integer.parseInt(snmpIntegerValue.toString());
					
					if (numHosts > 0)
					{
						// get info on these hosts: names should do...
						String[] hostNames = new String[numHosts];
						
						for (int i = 0; i < numHosts; i++)
						{
							itemID = "1.3.6.1.4.1.762.2.5.2.1.3." + (i + 1);
							newVars = comInterface.getMIBEntry(itemID);
							pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
							snmpValue = pair.getSNMPObjectAt(1);
							hostNames[i] = snmpValue.toString();
						}
						
						// put up a dialog with the names as chioces
						String newHost = (String)JOptionPane.showInputDialog(null, "Select new wireless host:", "", JOptionPane.PLAIN_MESSAGE, null, hostNames, hostNames[0]);
						
						if (newHost != null)
						{
							// determine which host number was selected
							for (int i = 0; i < numHosts; i++)
							{
								if (newHost.equals(hostNames[i]))
									wirelessHostNum = i + 1;
							}
							
							wirelessHostField.setText(newHost);
							
							itemID = "1.3.6.1.4.1.762.2.5.2.1.11." + wirelessHostNum;
							newVars = comInterface.getMIBEntry(itemID);
							pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
							snmpValue = pair.getSNMPObjectAt(1);
							wirelessHostMACField.setText(((SNMPOctetString)snmpValue).toHexString());
							
						}
						
						// found our host; break out of for loop
						break;
						
					}
					
				
				}
				
				
				if (k >= 10)
				{
					// didn't find a wireless host in 10 tries; display error message
					messagesArea.setText("Try again... no wireless hosts currently available");
				}	
						
			
			}
			catch(Exception e)
			{
				messagesArea.setText("Error selecting new wireless host: " + e);
			}
				
			
		}
		
		
		
		if (command == "test link")
		{
			startLinkTest();
		}
		
		
		
		if (command == "stop link test")
		{
			stopLinkTest();
		}
		
		
	}
	
	
	
	
	private void stopLinkTest()
	{
		if (statusThread.isAlive())
		{
			try
			{
				// stop current statusThread, and wait for it to die:
				statusThread.interrupt();
				statusThread.join();
			}
			catch(InterruptedException ex)
			{
				messagesArea.setText("Exception during link test thread exit:  " + ex + "\n");
			}
			
			scanButton.setText("Test wireless link");
			scanButton.setActionCommand("test link");
		
		}			
	}
	
	
	
	private void startLinkTest()
	{
		if (!statusThread.isAlive())
		{
			scanButton.setText("Stop link test");
			scanButton.setActionCommand("stop link test");
		
			// recreate and start status polling thread
			statusThread = new Thread(this);
			statusThread.start();
		}			
	}
	
	
	
	
	private String hexByte(byte b)
	{
		int pos = b;
		if (pos < 0)
			pos += 256;
		String returnString = new String();
		returnString += Integer.toHexString(pos/16);
		returnString += Integer.toHexString(pos%16);
		return returnString;
	}
	
	
	
	
	
	public void run()
	{
		
		
		while(!Thread.interrupted())
		{
		
			try
			{
			
				String itemID;
				SNMPVarBindList newVars;
				SNMPSequence pair;
				SNMPInteger snmpSetValue, snmpIntegerValue;
				SNMPObject snmpValue;
				
				
				messagesArea.setText("");
				
				/*
				// first, need to write values to appropriate OID's - why, I don't know...	
				for (int i = 1; i <= 3; i++)
				{
					itemID = "1.3.6.1.4.1.762.2.5.5." + i;
					newVars = comInterface.setMIBEntry(itemID, new SNMPInteger(50));
					
					itemID = "1.3.6.1.4.1.762.2.5.4." + i;
					newVars = comInterface.setMIBEntry(itemID, new SNMPInteger(3));
				}
				*/
				// now, should be able to read the desired values...
				
				
				// first, set up testing parameters
				// 1.3.6.1.4.1.762.2.5.2.1.27.1=1500:	packet size?
				itemID = "1.3.6.1.4.1.762.2.5.2.1.27." + wirelessHostNum;
				newVars = comInterface.setMIBEntry(itemID, new SNMPInteger(1500));
				
				// 1.3.6.1.4.1.762.2.5.2.1.26.1=25
				itemID = "1.3.6.1.4.1.762.2.5.2.1.26." + wirelessHostNum;
				newVars = comInterface.setMIBEntry(itemID, new SNMPInteger(25));
				
				// 1.3.6.1.4.1.762.2.5.2.1.25.1=88:   total number of packets to send? (half out, half back?)
				itemID = "1.3.6.1.4.1.762.2.5.2.1.25." + wirelessHostNum;
				newVars = comInterface.setMIBEntry(itemID, new SNMPInteger(88));
				
				itemID = "1.3.6.1.4.1.762.2.5.2.1.3." + wirelessHostNum;
				newVars = comInterface.getMIBEntry(itemID);
				pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
				snmpValue = pair.getSNMPObjectAt(1);
				wirelessHostMACField.setText(snmpValue.toString());
				
				itemID = "1.3.6.1.4.1.762.2.5.2.1.11." + wirelessHostNum;
				newVars = comInterface.getMIBEntry(itemID);
				pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
				snmpValue = pair.getSNMPObjectAt(1);
				wirelessHostMACField.setText(((SNMPOctetString)snmpValue).toHexString());
							
				// next, check the number of wireless hosts available
				itemID = "1.3.6.1.4.1.762.2.5.1.0";
				newVars = comInterface.getMIBEntry(itemID);
				pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
				snmpIntegerValue = (SNMPInteger)pair.getSNMPObjectAt(1);
				int numHosts = Integer.parseInt(snmpIntegerValue.toString());
				
				if (numHosts >= wirelessHostNum)
				{
					// now, read parameters
					//messagesArea.append("Local signal:\n");
						
					itemID = "1.3.6.1.4.1.762.2.5.2.1.32." + wirelessHostNum;
					newVars = comInterface.getMIBEntry(itemID);
					pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
					snmpIntegerValue = (SNMPInteger)pair.getSNMPObjectAt(1);
					//messagesArea.append(" local signal: " + snmpIntegerValue +"\n");
					localSignalBar.setValue(Integer.parseInt(snmpIntegerValue.toString()));
					localSignalTextField.setText(snmpIntegerValue.toString());
					
					itemID = "1.3.6.1.4.1.762.2.5.2.1.33." + wirelessHostNum;
					newVars = comInterface.getMIBEntry(itemID);
					pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
					snmpIntegerValue = (SNMPInteger)pair.getSNMPObjectAt(1);
					//messagesArea.append("  local noise: " + snmpIntegerValue +"\n");
					localNoiseBar.setValue(Integer.parseInt(snmpIntegerValue.toString()));
					localNoiseTextField.setText(snmpIntegerValue.toString());
					
					itemID = "1.3.6.1.4.1.762.2.5.2.1.35." + wirelessHostNum;
					newVars = comInterface.getMIBEntry(itemID);
					pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
					snmpIntegerValue = (SNMPInteger)pair.getSNMPObjectAt(1);
					//messagesArea.append("    local SNR: " + snmpIntegerValue +"\n");
					localSNRBar.setValue(Integer.parseInt(snmpIntegerValue.toString()));
					localSNRTextField.setText(snmpIntegerValue.toString());
					
					itemID = "1.3.6.1.4.1.762.2.5.2.1.44." + wirelessHostNum;
					newVars = comInterface.getMIBEntry(itemID);
					pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
					snmpIntegerValue = (SNMPInteger)pair.getSNMPObjectAt(1);
					//messagesArea.append(" remote signal: " + snmpIntegerValue +"\n");
					remoteSignalBar.setValue(Integer.parseInt(snmpIntegerValue.toString()));
					remoteSignalTextField.setText(snmpIntegerValue.toString());
					
					itemID = "1.3.6.1.4.1.762.2.5.2.1.45." + wirelessHostNum;
					newVars = comInterface.getMIBEntry(itemID);
					pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
					snmpIntegerValue = (SNMPInteger)pair.getSNMPObjectAt(1);
					//messagesArea.append("  remote noise: " + snmpIntegerValue +"\n");
					remoteNoiseBar.setValue(Integer.parseInt(snmpIntegerValue.toString()));
					remoteNoiseTextField.setText(snmpIntegerValue.toString());
					
					itemID = "1.3.6.1.4.1.762.2.5.2.1.47." + wirelessHostNum;
					newVars = comInterface.getMIBEntry(itemID);
					pair = (SNMPSequence)(newVars.getSNMPObjectAt(0));
					snmpIntegerValue = (SNMPInteger)pair.getSNMPObjectAt(1);
					//messagesArea.append("    local SNR: " + snmpIntegerValue +"\n");
					remoteSNRBar.setValue(Integer.parseInt(snmpIntegerValue.toString()));
					remoteSNRTextField.setText(snmpIntegerValue.toString());
					
				}
				
				
				try
				{
					// sleep for 10 seconds
					Thread.currentThread().sleep(10000);
				}
				catch(InterruptedException e)
				{
					// don't bother informing of interruption; will just exit...
					// but must reset this thread's interrupted status
					statusThread.interrupt();
					//messagesArea.setText("Exception during modem status polling:  " + e + "\n");
				}
				
			
			}
			catch(InterruptedIOException e)
			{
				// don't bother informing of interruption
				//messagesArea.setText("Exception during modem status polling:  " + e + "\n");
			}
			catch(Exception e)
			{
				messagesArea.append("Exception during link status polling:  " + e + "\n");
			}
			
			
			
			
		
		}
		
		
		try
		{
			// set total number of packets to send to 0
			String itemID = "1.3.6.1.4.1.762.2.5.2.1.25." + wirelessHostNum;
			comInterface.setMIBEntry(itemID, new SNMPInteger(0));
		}
		catch(Exception e)
		{
			messagesArea.append("Exception during modem status polling exit:  " + e + "\n");
		}
			
		//messagesArea.setText("Status thread exiting!\n");
		
	}
	

	
	
	
	
	public static void main(String args[]) 
	{
		try
		{
			LinkMonitor theApp = new LinkMonitor();
			theApp.pack();
			
			// tweak app size to make it a little larger than necessary, to address the
			// "shrunken textfields" problem arising from the layout manager packing stuff
			// a little too tightly.
			Dimension dim = theApp.getSize();
			dim.height += 20;
			dim.width += 20;
			theApp.setSize(dim);
			
			theApp.show();
		}
		catch (Exception e)
		{}
	}
	

}