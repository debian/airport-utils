/*
 * AirPort Port Inspector
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */




import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.applet.*;

public class AboutDialog extends JDialog
								implements Runnable, ActionListener
{

	private JLabel aboutLabel1 = new JLabel("AirPort Port Inspector");
	private JLabel aboutLabel2 = new JLabel("J. Sevy");
	private JLabel aboutLabel3 = new JLabel("January, 2003");
	private JLabel aboutLabel4 = new JLabel("");
	private JLabel aboutLabel5 = new JLabel("");
	
	private String descriptionString = "Who's surfing through _your_ ports? ";
	
	private JButton okButton;
	
	Thread displayThread;
	
	
	public AboutDialog(JFrame parent)
	{
		super(parent, "About Airport Port Inspector", true /*modal*/);
	    
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		setUpDisplay();
		
		this.setLocation(Math.round((parent.getSize().width - this.getSize().width)/2), Math.round((parent.getSize().height - this.getSize().height)/2));
		
		// create and start display thread
		displayThread = new Thread(this);
		displayThread.start();
		
		this.show();
	
	}
	
	
	
	
	public void hide()
	{
		super.hide();
		
		// interrupt thread so it can exit..
		displayThread.interrupt();
	}
	
	
		
		
	private void setUpDisplay()
	{
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		aboutLabel1.setBackground(Color.white);
		aboutLabel2.setBackground(Color.white);
		aboutLabel3.setBackground(Color.white);
		aboutLabel4.setBackground(Color.white);
		aboutLabel5.setBackground(Color.white);
		
		okButton = new JButton("OK");
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		okButton.setBackground(Color.white);
		
		this.getRootPane().setDefaultButton(okButton);
		
		
		JPanel aboutPanel = new JPanel();
		aboutPanel.setLayout(theLayout);
		aboutPanel.setBackground(Color.white);
		
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(aboutLabel1, c);
		aboutPanel.add(aboutLabel1);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(aboutLabel2, c);
		aboutPanel.add(aboutLabel2);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(aboutLabel3, c);
		aboutPanel.add(aboutLabel3);
		
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(aboutLabel4, c);
		aboutPanel.add(aboutLabel4);
		
		c.gridx = 1;
		c.gridy = 5;
		theLayout.setConstraints(aboutLabel5, c);
		aboutPanel.add(aboutLabel5);
		
		
		this.getContentPane().setLayout(theLayout);
		this.getContentPane().setBackground(Color.white);
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(aboutPanel, c);
		this.getContentPane().add(aboutPanel);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(okButton, c);
		this.getContentPane().add(okButton);
		
		this.pack();
		
		this.setSize(300, 200);
		
	}
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes, menu selections
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("ok"))
		{
			this.hide();
		}
	
	}
	
	
	
	public void run()
	{
		
		
		try
		{
			
			
			// simultaneously, write message out a character at a time...
			int numChars = descriptionString.length();
			
			for (int i = 0; i < numChars; i++)
			{
				aboutLabel4.setText(descriptionString.substring(0,i));
				Thread.currentThread().sleep(60);
			}
			
			
	
		}
		catch(Exception e)
		{
			// don't bother informing of exception; just exit...
			//System.out.println(e);
		}
			
		
		// later!
	}
			
			
			
}