/*
 * AirPort Port Inspector
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


public class PortCount
{

    public String localIP;
    public String localMAC;
    public int portCount;
    
    
    public PortCount(String localIP, String localMAC, int portCount)
    {
        this.localIP = localIP;
        this.localMAC = localMAC;
        this.portCount = portCount;
    }
    
    
    public PortCount()
    {
        this("", "", 0);
    }
    
    
    public PortCount(String localIP, String localMAC)
    {
        this(localIP, localMAC, 0);
    }
    
    
    public String toString()
    {
        String returnValue = new String();
        
        returnValue += localIP + " : " + localMAC + " : " + portCount;
        
        return returnValue;
        
    }

}