/*
 * AirPort Port Inspector
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */





public class PortInfo
{

    public String localIP;
    public int localPort;
    public String localMAC;
    public String remoteIP;
    public int remotePort;
    public String gatewayIP;
    public int gatewayPort;
    public int portType;
    public int lifetime;
    
    
    public PortInfo(String localIP, int localPort, String localMAC, String remoteIP, int remotePort, String gatewayIP, int gatewayPort, int portType, int lifetime)
    {
        this.localIP = localIP;
        this.localPort = localPort;
        this.localMAC = localMAC;
        this.remoteIP = remoteIP;
        this.remotePort = remotePort;
        this.gatewayIP = gatewayIP;
        this.gatewayPort = gatewayPort;
        this.portType = portType;
        this.lifetime = lifetime;
    }
    
    
    public PortInfo()
    {
        this("", 0, "", "", 0, "", 0, 0, 0);
    }
    
    
    public String toString()
    {
        String returnValue = new String();
        
        returnValue += localIP + " : " + localPort + " : " + localMAC + " : " + remoteIP + " : " + remotePort + " : " + gatewayIP + " : " + gatewayPort + " : " + portType + " : " + lifetime;
        
        return returnValue;
        
    }

}