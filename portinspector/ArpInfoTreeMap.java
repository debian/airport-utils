/*
 * AirPort Port Inspector
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.util.*;
import java.math.*;
import snmp.*;


public class ArpInfoTreeMap extends TreeMap
{
    
    public ArpInfoTreeMap()
    {
        // create empty Hashtable
        super();
    }
    
    
    public ArpInfoTreeMap(SNMPVarBindList varBindList)
    {
        // create and populate Hashtable of ArpInfo objects
        super();
        
        
        
	    // make list of MAC address / IP address pairs
	    if (varBindList.size() > 0)
        {
            int i = 0;
            SNMPSequence variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
            SNMPObjectIdentifier snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
            String oid = snmpOID.toString();
                
            String oidBase = "1.3.6.1.2.1.4.22.1.1";
            while(oid.startsWith(oidBase))
            {
                // use just to get index - note that it's the interface index followed by the IP address;
                // SO, we strip off the interface, and VOILA, we have the table hashed by IP.
                String tableIndex = oid.substring(oidBase.length() + 1 + 2); // extra 2 strips off the interface index and dot
                    
                // add a new ARPInfo object to hashtable, hashed by index
                this.put(tableIndex, new ArpInfo());
                
                i++;
                
                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
                
            }
            
            // get MAC addresses
            oidBase = "1.3.6.1.2.1.4.22.1.2";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1 + 2); // extra 2 strips off the interface index and dot
                    
                // modify ARPInfo in hashtable
                ArpInfo arpInfo = (ArpInfo)this.get(tableIndex);
                SNMPOctetString macAddress = (SNMPOctetString)variablePair.getSNMPObjectAt(1);
                arpInfo.macAddress = macAddress.toHexString();
                
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
            }
            
            // get IP addresses
            oidBase = "1.3.6.1.2.1.4.22.1.3";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1 + 2); // extra 2 strips off the interface index and dot
                    
                // modify ARPInfo in hashtable
                ArpInfo arpInfo = (ArpInfo)this.get(tableIndex);
                SNMPIPAddress ipAddress = (SNMPIPAddress)variablePair.getSNMPObjectAt(1);
                arpInfo.ipAddress = ipAddress.toString();
                
                i++;

                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
            }
            
        }
        
        
    }
    
    
    
    
    public String toString()
    {
        String returnValue = new String();
        
        Iterator iterator = this.values().iterator();
        
        while (iterator.hasNext())
        {
            ArpInfo arpInfo = (ArpInfo)iterator.next();
            returnValue += arpInfo.toString() + "\n";
        }
        
        return returnValue;
        
    }


}