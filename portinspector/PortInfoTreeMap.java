/*
 * AirPort Port Inspector
 *
 * Copyright (C) 2003, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.util.*;
import java.math.*;
import snmp.*;


public class PortInfoTreeMap extends TreeMap
{
    
    
    public PortInfoTreeMap()
    {
        // create empty Hashtable
        super();
    }
    
    
    public PortInfoTreeMap(SNMPVarBindList varBindList)
    {
        // create and populate Hashtable of PortInfo objects
        super();
        
        // get all OIDs for base OID  1.3.6.1.4.1.731.100.3.1.1.1;
        // index consists of  1.3.6.1.4.1.731.100.3.1.1.1.<local IP>.<local port>.<remote IP>.<remote port>,
        
        if (varBindList.size() > 0)
        {
            int i = 0;
            SNMPSequence variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
            SNMPObjectIdentifier snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
            String oid = snmpOID.toString();
                
            String oidBase = "1.3.6.1.4.1.731.100.3.1.1.1";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1);
                    
                // add a new PortInfo object to hashtable, hashed by index
                this.put(tableIndex, new PortInfo());
                
                // this entry also gives port type (TCP or UDP)
                PortInfo portInfo = (PortInfo)this.get(tableIndex);
                SNMPInteger port = (SNMPInteger)variablePair.getSNMPObjectAt(1);
                portInfo.portType = ((BigInteger)port.getValue()).intValue();
                i++;
                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.2";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1);
                    
                // modify PortInfo in hashtable
                PortInfo portInfo = (PortInfo)this.get(tableIndex);
                SNMPIPAddress ipAddress = (SNMPIPAddress)variablePair.getSNMPObjectAt(1);
                portInfo.localIP = ipAddress.toString();
                i++;
                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.3";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1);
                    
                // modify PortInfo in hashtable
                PortInfo portInfo = (PortInfo)this.get(tableIndex);
                SNMPInteger port = (SNMPInteger)variablePair.getSNMPObjectAt(1);
                portInfo.localPort = ((BigInteger)port.getValue()).intValue();
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.4";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1);
                    
                // modify PortInfo in hashtable
                PortInfo portInfo = (PortInfo)this.get(tableIndex);
                SNMPIPAddress ipAddress = (SNMPIPAddress)variablePair.getSNMPObjectAt(1);
                portInfo.remoteIP = ipAddress.toString();
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.5";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1);
                    
                // modify PortInfo in hashtable
                PortInfo portInfo = (PortInfo)this.get(tableIndex);
                SNMPInteger port = (SNMPInteger)variablePair.getSNMPObjectAt(1);
                portInfo.remotePort = ((BigInteger)port.getValue()).intValue();
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.6";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1);
                    
                // modify PortInfo in hashtable
                PortInfo portInfo = (PortInfo)this.get(tableIndex);
                SNMPIPAddress ipAddress = (SNMPIPAddress)variablePair.getSNMPObjectAt(1);
                portInfo.gatewayIP = ipAddress.toString();
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.7";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1);
                    
                // modify PortInfo in hashtable
                PortInfo portInfo = (PortInfo)this.get(tableIndex);
                SNMPInteger port = (SNMPInteger)variablePair.getSNMPObjectAt(1);
                portInfo.gatewayPort = ((BigInteger)port.getValue()).intValue();
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            // skip 8, 9, 10
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.8";
            while(oid.startsWith(oidBase))
            {
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.9";
            while(oid.startsWith(oidBase))
            {
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.10";
            while(oid.startsWith(oidBase))
            {
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
            
            oidBase = "1.3.6.1.4.1.731.100.3.1.1.11";
            while(oid.startsWith(oidBase))
            {
                // get index
                String tableIndex = oid.substring(oidBase.length() + 1);
                    
                // modify PortInfo in hashtable
                PortInfo portInfo = (PortInfo)this.get(tableIndex);
                SNMPInteger lifetime = (SNMPInteger)variablePair.getSNMPObjectAt(1);
                portInfo.lifetime = ((BigInteger)lifetime.getValue()).intValue();
                i++;

                if (i >= varBindList.size())
                    break;
                
                variablePair = (SNMPSequence)varBindList.getSNMPObjectAt(i);
                snmpOID = (SNMPObjectIdentifier)variablePair.getSNMPObjectAt(0);
                oid = snmpOID.toString();
                
            }
        
        }
        
        
    }
    
    
    
    public void setMACAddresses(ArpInfoTreeMap arpInfoTreeMap)
    {
        // for each entry in the portInfoMap, find its corresponding MAC address in the arpInfoMap
        Iterator iterator = this.values().iterator();
        
        while (iterator.hasNext())
        {
            PortInfo portInfo = (PortInfo)iterator.next();
            String ipAddress = portInfo.localIP;
            
            // find ArpInfo object corresponding to IP address
            ArpInfo arpInfo = (ArpInfo)arpInfoTreeMap.get(ipAddress);
            
            if (arpInfo != null)
            {
                portInfo.localMAC = arpInfo.macAddress;
            }
        }
    }
    
    
    
    public String toString()
    {
        String returnValue = new String();
        
        Iterator iterator = this.values().iterator();
        
        while (iterator.hasNext())
        {
            PortInfo portInfo = (PortInfo)iterator.next();
            returnValue += portInfo.toString() + "\n";
        }
        
        return returnValue;
        
    }


}