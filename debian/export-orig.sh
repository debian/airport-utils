#!/bin/bash

# Use this script to export all .orig.tar.gz.

pwd | egrep 'debian$' > /dev/null || { echo "You are not in debian/ directory. Exiting."; exit 1; }

pristine-tar -h 2>&1 | grep Usage > /dev/null || { echo "Please, install pristine-tar package."; exit 1; }

cd ..

gbp export-orig --pristine-tar --component=portinspector \
                               --component=modem \
                               --component=linkmon \
                               --component=ipinspector \
                               --component=hostmon \
                               --component=airportconfig \
                               --component=airport2config
