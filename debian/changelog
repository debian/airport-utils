airport-utils (2-8) unstable; urgency=medium

  * QA upload.
  * debian/export-orig.sh: using 'gbp export-orig' instead of pristine-tar.
    Thanks to Mattia Rizzolo and Hugh McMaster.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 29 Mar 2020 18:27:00 -0300

airport-utils (2-7) unstable; urgency=medium

  * QA upload.
  * New upstream homepage. Consequently:
      - debian/control: added Homepage field.
      - debian/copyright: updated Source field.
      - debian/watch: using a fake rule to point to 'No Tracking' status
        because upstream has several sources and not a pattern for its names
        and versions.
      - Thanks to Barak A. Pearlmutter" <barak@pearlmutter.net>.
        (Closes: #878231).
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Created VCS fields.
  * debian/copyright: updated packaging copyright years.
  * debian/export-orig.sh: created to export all needed upstream tarballs.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/*: added to perform some trivial tests.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 24 Mar 2020 20:25:18 -0300

airport-utils (2-6) unstable; urgency=medium

  * QA upload.
  * Bumped DH level to 10.
  * debian/control: bumped Standards-Version to 3.9.8.
  * debian/copyright: updated the packaging copyright years.
  * debian/rules.old: trash... Removed.
  * debian/watch: bumped to version 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 23 Dec 2016 23:44:18 -0200

airport-utils (2-5) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 05 May 2015 16:45:17 -0300

airport-utils (2-4) experimental; urgency=medium

  * QA upload.
  * Migrations:
      - debian/copyright to 1.0 format. All information updated too.
      - debian/rules to new (reduced) format. The old file was kept
        and renamed to debian/rules.old.
      - DH level to 9.
      - Standards-Version to 3.9.6.
  * debian/control: removed the obsolete java-compiler package from
      Build-Depends-Indep field.
  * debian/doc-base: added to specify the /usr/share/doc/ \
      airport-utils/index.html file.
  * debian/install: created to install some executable files.
  * debian/watch: added a fake site to explain about the current
      status of the original upstream homepage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 31 Jan 2015 11:11:13 -0200

airport-utils (2-3) unstable; urgency=low

  [ Colin Watson ]
  * QA upload (see #688537).
  * Add build-indep and build-arch targets to debian/rules.

  [ James Page ]
  * Transition to use default Java implementation (closes: #683504):
    - d/control: Switch build and runtime dependencies to use
      default-{jdk|jre} instead of openjdk-6 versions.
    - d/rules: Specify source/target = 1.5 to ensure backwards compatible
      bytecode is built.

 -- Colin Watson <cjwatson@debian.org>  Thu, 09 May 2013 12:17:17 +0100

airport-utils (2-2) unstable; urgency=low

  * debian/control:
    + Bump Standards-Version to 3.9.2 (no changes).
    + Add default-jdk as an alternative in Build-Depends-Indep.
    + Fix enumeration in long description (closes: #650370).

 -- Julien BLACHE <jblache@debian.org>  Fri, 02 Dec 2011 11:25:58 +0100

airport-utils (2-1) unstable; urgency=low

  * Package converted to source format 3.0 (quilt).
    + Patches now have DEP-3 headers.
    + Build system and source package reworked.

  * debian/control:
    + Drop dpatch build-dep.
    + Bump Standards-Version to 3.8.4 (no changes).
    + Depend on openjdk-6-jre | java5-runtime (closes: #573376).

 -- Julien BLACHE <jblache@debian.org>  Sat, 20 Mar 2010 08:13:55 +0100

airport-utils (1-8) unstable; urgency=low

  * debian/control:
    + Bump Standards-Version to 3.8.1 (no changes).
    + Build-depend on openjdk-6-jdk instead of jikes (closes: #528049).
    + Depend on openjdk-6-jre instead of gij.
  * debian/copyright:
    + Updated upstream website URL.
    + Put in proper copyright notice.

 -- Julien BLACHE <jblache@debian.org>  Sun, 10 May 2009 17:02:55 +0200

airport-utils (1-7) unstable; urgency=low

  * debian/control:
    + Drop dependency on libgcj7-1-awt, brought by gij anyway.
    + Bump Standards-Version to 3.7.3 (no changes).
  * debian/airport-utils.menu:
    + Move entries under Applications/System/Hardware.

 -- Julien BLACHE <jblache@debian.org>  Sat, 19 Jan 2008 10:54:45 +0100

airport-utils (1-6) unstable; urgency=low

  * debian/rules:
    + Test for build.xml existence before cleaning (closes: #436317).

 -- Julien BLACHE <jblache@debian.org>  Tue, 07 Aug 2007 11:55:32 +0200

airport-utils (1-5) unstable; urgency=low

  * debian/airport-utils.menu:
    + Update for the new menu structure.
    + Added descriptions.

 -- Julien BLACHE <jblache@debian.org>  Sun, 05 Aug 2007 19:07:32 +0200

airport-utils (1-4) unstable; urgency=low

  * debian/control:
    + Depend on the new libgcj7-1-awt (closes: #421727).

 -- Julien BLACHE <jblache@debian.org>  Sun, 13 May 2007 14:22:06 +0200

airport-utils (1-3) unstable; urgency=low

  * debian/control:
    + Added missing build-dependency on java-gcj-compat-dev (closes: #392112).

 -- Julien BLACHE <jblache@debian.org>  Tue, 10 Oct 2006 16:55:07 +0200

airport-utils (1-2) unstable; urgency=low

  * Depend on libgcj7-awt, needed by gij to run the apps (closes: #375383).
    I'm not sure this is the best solution, so if anyone has a better idea,
    feel free to mail me. Packaging Java apps feels ... experimental :)

 -- Julien BLACHE <jblache@debian.org>  Sun, 25 Jun 2006 22:29:45 +0200

airport-utils (1-1) unstable; urgency=low

  * Initial release (closes: #367116).

 -- Julien BLACHE <jblache@debian.org>  Sat, 20 May 2006 18:24:48 +0200
