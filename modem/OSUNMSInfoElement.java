/*
 * Airport2InfoElement
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */






public class OSUNMSInfoElement
{
	public int tag;
	
	public int length;
	
	public byte[] value;
	
	
	
	public OSUNMSInfoElement(int tag, int length, byte[] value)
	{
		this.tag = tag;
		this.length = length;
		this.value = value;
	}
	
	
	
	public OSUNMSInfoElement()
	{
		this.tag = 0;
		this.length = 0;
		this.value = new byte[0];
	}
	
	
	
	public int getIntegerValue()
	{
		int integerValue = 0;
		
		for (int i = 0; i < value.length; i++)
		{
			int absValue = value[i];
			
			if (absValue < 0)
				absValue += 256;
			
			integerValue = integerValue*256 + absValue;
		}
		
		return integerValue;
	}
	
	
	
	private String hexByte(byte b)
	{
		int pos = b;
		if (pos < 0)
			pos += 256;
		String returnString = new String();
		returnString += Integer.toHexString(pos/16);
		returnString += Integer.toHexString(pos%16);
		return returnString;
	}
	
	
	
	private String hexBytes(byte[] bytes)
	{
		String returnString = new String();
		
		for(int i = 0; i < bytes.length; i++)
		{
			returnString += hexByte(bytes[i]) + " ";
		}
		
		return returnString;
		
	}
	
}