/*
 * AirportBaseStationHangup Utility
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

import java.awt.*;
import java.awt.event.*;



class MessageDialog extends Dialog
					implements ActionListener
{
	
	public MessageDialog(Frame owner, String message)
	{
		super(owner, true);		// true -> modal dialog
		
		this.setLayout(new BorderLayout());
		this.add(new Label(message), BorderLayout.NORTH);
		
		Button okButton = new Button("OK");
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		this.add(okButton, BorderLayout.SOUTH);
	}
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes, menu selections
	{
		String command = theEvent.getActionCommand();
		
	
		if (command == "ok")
		{
			this.hide();
		}
		
	}
	
	
	
	public static void showMessageDialog(Frame owner, String message)
	{
		MessageDialog theDialog = new MessageDialog(owner, message);
		theDialog.pack();
		theDialog.show();
		
		// show() will block until hide() is called in response to a click on the
		// OK button
		
		theDialog.dispose();
		
	}
	
}