/*
 * AirportBaseStationHangup Utility
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

import java.awt.*;
import java.awt.event.*;



class InputDialog extends Dialog
					implements ActionListener
{
	
	private TextField responseField;
	private String initialResponse;
	private boolean cancelled;
	
	
	public InputDialog(Frame owner, String message, String initialResponse)
	{
		super(owner, true);		// true -> modal dialog
		
		this.setLayout(new BorderLayout());
		this.add(new Label(message), BorderLayout.NORTH);
		
		responseField = new TextField(20);
		responseField.setText(initialResponse);
		this.add(responseField, BorderLayout.CENTER);
		
		Panel buttonPanel = new Panel();
		buttonPanel.setLayout(new BorderLayout());
		
		Button okButton = new Button("OK");
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		buttonPanel.add(okButton, BorderLayout.WEST);
		
		Button cancelButton = new Button("Cancel");
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(this);
		buttonPanel.add(cancelButton, BorderLayout.EAST);
		
		this.add(buttonPanel, BorderLayout.SOUTH);
		
		this.initialResponse = initialResponse;
		this.cancelled = false;
	}
	
	
	
	public InputDialog(Frame owner, String message)
	{
		// just no initial response
		this(owner, message, "");
	}
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes, menu selections
	{
		String command = theEvent.getActionCommand();
		
	
		if (command == "ok")
		{
			this.hide();
		}
		
		if (command == "cancel")
		{
			cancelled = true;
			this.hide();
		}
		
	}
	
	
	public static String getInputFromDialog(Frame owner, String message, String initialResponse)
	{
		InputDialog theDialog = new InputDialog(owner, message, initialResponse);
		theDialog.pack();
		theDialog.show();
		
		// show() will block until hide() is called in response to a click on the
		// OK or Cancel buttons
		
		String responseString;
		
		if (theDialog.cancelled)
			responseString = null;
		else
			responseString = theDialog.responseField.getText();
			
		theDialog.dispose();
		
		return responseString;
	}
}