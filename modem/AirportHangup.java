/*
 * AirportHangup CLI Utility
 *
 * Copyright (C) 2001, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.net.*;



public class AirportHangup
{
	
	InetAddress hostAddress;
			
	
	
	
	private static void hangupModem(InetAddress hostAddress) 
	{
		try
		{
			DatagramSocket dSocket = new DatagramSocket();
			dSocket.setSoTimeout(15000);	//15 seconds
			int AIRPORT_PORT = 192;
			
			byte[] bytes = new byte[116];	// from sniffs
			bytes[0] = (byte)0x06;			// from S. Sexton - thanks!
			
			DatagramPacket outPacket = new DatagramPacket(bytes, bytes.length, hostAddress, AIRPORT_PORT);
					
			dSocket.send(outPacket);	
					
		}
		catch(Exception e)
		{
			System.out.println("Exception during hangup:  " + e + "\n");
		}
		
	}
	
	
	
	private static void connectModem(InetAddress hostAddress) 
	{
		try
		{
			DatagramSocket dSocket = new DatagramSocket();
			dSocket.setSoTimeout(15000);	//15 seconds
			int AIRPORT_PORT = 192;
			
			byte[] bytes = new byte[116];	// from sniffs
			bytes[0] = (byte)0x07;			// from P. Werz - thanks!
			
			
			DatagramPacket outPacket = new DatagramPacket(bytes, bytes.length, hostAddress, AIRPORT_PORT);
					
			dSocket.send(outPacket);
				
		}
		catch(Exception e)
		{
			System.out.println("Exception during modem connect:  " + e + "\n");
		}
		
	}
	
	
	
	
	private static void printUsage()
	{
		System.out.println("Usage: java -jar AirportModemUtilityCLI.jar <IP address> <command>");
		System.out.println("   where <command> is C, c, Connect, connect");
		System.out.println("                   or H, h, Hangup, hangup");
	}
	
	
	
	
	public static void main(String args[]) 
	{
		try
		{
			// make sure have at least 2 args
			if (args.length < 2)
			{
				printUsage();
			}
			else
			{
				// base station address is argument 0
				InetAddress hostAddress = InetAddress.getByName(args[0]);
		
				// command is arg 1: "h", "H" for hangup, "c", "C" for connect
				String command = (args[1]).substring(0,1);
				if (command.equalsIgnoreCase("c"))
				{
					connectModem(hostAddress);
				}
				else if (command.equalsIgnoreCase("h"))
				{
					hangupModem(hostAddress);
				}
				else
				{
					printUsage();
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Exception:  " + e + "\n");
		}
	}
	

}