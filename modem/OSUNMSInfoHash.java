/*
 * Airport2Analyzer
 *
 * Copyright (C) 2002, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */



import java.util.*;
import java.io.*;




public class OSUNMSInfoHash
				extends Hashtable
{
	
	
	public OSUNMSInfoHash(byte[] retrievedBytes, int numBytes)
	{
		ByteArrayInputStream byteStream = new ByteArrayInputStream(retrievedBytes, 0, numBytes);
		
		byte[] messageTypeBytes = new byte[2];
		byteStream.read(messageTypeBytes, 0, 2);
			
		while (byteStream.available() > 0)
		{
			
			OSUNMSInfoElement element = new OSUNMSInfoElement();
			
			//read the length
			byte[] lengthBytes = new byte[2];
			byteStream.read(lengthBytes, 0, 2);
			element.length = integerValue(lengthBytes);
			
			// read the tag: 2 bytes
			byte[] tagBytes = new byte[2];
			byteStream.read(tagBytes, 0, 2);
			element.tag = integerValue(tagBytes);
			
			//read the value
			byte[] valueBytes = new byte[element.length - 2];
			byteStream.read(valueBytes, 0, element.length - 2);
			element.value = valueBytes;
			
			//System.out.println("Added element with tag " + element.tag);
			
			// add the new element to the hashtable, using an Integer object as the key
			this.put(new Integer(element.tag), element);
			
		}
		
	}
	
	
	
	public OSUNMSInfoElement get(Integer integerTag)
	{
		return (OSUNMSInfoElement)super.get(integerTag);
	}
	
	
	
	private int integerValue(byte[] valueBytes)
	{
		int value = 0;
		
		for (int i = 0; i < valueBytes.length; i++)
		{
			int absValue = valueBytes[i];
			
			if (absValue < 0)
				absValue += 256;
			
			value = value*256 + absValue;
		}
		
		return value;
	}
	
	
	

}