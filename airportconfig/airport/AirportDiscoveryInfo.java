/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.util.*;
import byteblock.*;



/**
* 	Collects together information about the location of pieces of information 
* 	in the 116-byte Discovery memory block, and creates appropriate AirportInfoRecords
* 	(with ByteBlockWindows into the underlying ByteBlock) to read and write 
*	the information.
*/

public class AirportDiscoveryInfo extends Hashtable
{
	
	public ByteBlock baseBlock;
	
	
	/**
	*	Create new
	*/
	
	public AirportDiscoveryInfo(ByteBlock baseBlock)
	{
	
		this.baseBlock = baseBlock;
		
		int baseStartIndex;
		int numRows, numCols;
		int dataType;
		byte mask;
		String name;
		
	
		
		/**
		*	Base station MAC address: byte 2*16 + 5
		*/

		baseStartIndex = 0x024;
		numRows = 1;
		numCols = 6;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Base station Mac address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	Base station IP address: byte 2*16 + 13
		*/
		
		baseStartIndex = 0x02C;
		numRows = 1;
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Base station IP address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**
		*	Base station name: byte 3*16 + 1
		*/
		
		baseStartIndex = 0x030;
		numRows = 1;
		numCols = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Base station name";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	Base station uptime: byte 5*16 + 1
 		*/
		
		baseStartIndex = 0x050;
		numRows = 1;
		numCols = 4;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Base station uptime";
  		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));


		
		/**
		*	Device identifying string: byte 5*16 + 5
		*/
		
		baseStartIndex = 0x054;
		numRows = 1;
		numCols = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Device identifying string";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		
		
	}
	
	
	
	public AirportInfoRecord get(String name)
	{
		return (AirportInfoRecord)super.get(name);
	}
	
	
	
	
}