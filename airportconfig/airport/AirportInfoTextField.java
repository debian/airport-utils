/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;
import byteblock.*;




/**
*	Creates a text box to display and edit information in an AirportInfoRecord.
*	Size of textfield is set automatically according to datatype.
*/

public class AirportInfoTextField extends JTextField
								implements AirportInfoComponent
{
	private AirportInfoRecord theRecord;
	
	
	/**
	*	Creates new textfield to display and edit information in an AirportInfoRecord.
	*	Size of textfield is set automatically according to datatype.
	*/
	
	public AirportInfoTextField(AirportInfoRecord theRecord)
	{
		super();
		this.theRecord = theRecord;
		
		//determine how large the text field should be to display the value
		int fieldSize = theRecord.byteBlockWindow.getSize();
		
		switch (theRecord.dataType)
		{
			
			case AirportInfoRecord.CHAR_STRING:
			{
				// do nothing; field size OK as number of bytes
				break;
			}
			
			case AirportInfoRecord.PHONE_NUMBER:
			{
				fieldSize = 20;	// should be enough for all digits and spaces
				break;
			}
			
			
			case AirportInfoRecord.IP_ADDRESS:
			{
				fieldSize = 15;
				break;
			}
	
			
			case AirportInfoRecord.UNSIGNED_INTEGER:
			case AirportInfoRecord.BYTE:
			case AirportInfoRecord.BYTE_STRING:
			default:
			{
				fieldSize *= 3;	// to hold hex string: 2 chars per byte, plus space
				break;			// or integer value
			}
		}
		
		if (fieldSize > 40)
			fieldSize = 40;
			
		this.setColumns(fieldSize);
		
		refreshDisplay();
	}
	
	
	
	/**
	*	Creates new textfield to display and edit information in an AirportInfoRecord.
	*	Size of textfield is supplied as argument.
	*/
	
	public AirportInfoTextField(AirportInfoRecord theRecord, int fieldSize)
	{
		super();
		this.theRecord = theRecord;
		
		this.setColumns(fieldSize);
		
		refreshDisplay();
	}
	
	
	
	/**
	*	Read value from associated AirportInfoRecord window, and display value.
	*/
	
	public void refreshDisplay()
	{
		setText(theRecord.toString());
	}
	
	
	
	/**
	*	Write value currently displayed into associated AirportInfoRecord window.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		try
		{
			theRecord.setBytesFromString(getText());
		}
		catch (ValueFormatException e)
		{
			this.selectAll();
			this.requestFocus();
			throw e;
		}
	}
	
	
	
	/**
	*	Write supplied value, rather than that currently displayed, into associated 
	*	AirportInfoRecord window.
	*/
	
	public void writeValue(String newValue)
		throws ValueFormatException
	{
		setText(newValue);
		writeValue();
		refreshDisplay();
	}
	
	
	
	
}