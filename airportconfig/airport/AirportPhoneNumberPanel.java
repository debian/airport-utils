/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;
import byteblock.*;




/**
*	Utility subpanel which maintains dial-up phone number info. Could be easily extended to also permit
*	secondary phone number and modem initialization string editting.
*/

public class AirportPhoneNumberPanel extends AirportInfoPanel
{
	
	private AirportInfo airportInfo;
	private JTextField modemInitField, primaryPhoneField, secondaryPhoneField;
	private AirportInfoComboBox countryCodeBox;
	
	private static final int countryCodePrefixCount = 5;	// "*NC22", for example
	private static final int countFieldCount = 3;	// one byte each for phone number counts, modem init count
	
	
	
	
	public AirportPhoneNumberPanel(AirportInfo airportInfo)
	{
		super();
		this.airportInfo = airportInfo;
		
		// holds configuration info for telephone system
		countryCodeBox = new AirportInfoComboBox("Country", airportInfo.get("Phone country code"));
		
		// add settings for each different country system; each group is same!
		countryCodeBox.addItemAndValue("North America", "3232");
		countryCodeBox.addItemAndValue("Guam (same as North America)", "3232");
		countryCodeBox.addItemAndValue("Hong Kong (same as North America)", "3232");
		countryCodeBox.addItemAndValue("India (same as North America)", "3232");
		countryCodeBox.addItemAndValue("Latin America (same as North America)", "3232");
		countryCodeBox.addItemAndValue("Phillipines (same as North America)", "3232");
		countryCodeBox.addItemAndValue("Thailand (same as North America)", "3232");
		
		countryCodeBox.addItemAndValue("Singapore", "3437");
		countryCodeBox.addItemAndValue("China (same as Singapore)", "3437");
		countryCodeBox.addItemAndValue("Czech Republic (same as Singapore)", "3437");
		countryCodeBox.addItemAndValue("Slovak Republic (same as Singapore)", "3437");
		countryCodeBox.addItemAndValue("Korea (same as Singapore)", "3437");
		countryCodeBox.addItemAndValue("Malaysia (same as Singapore)", "3437");
		countryCodeBox.addItemAndValue("Poland (same as Singapore)", "3437");
		countryCodeBox.addItemAndValue("Taiwan (same as Singapore)", "3437");
		
		countryCodeBox.addItemAndValue("Switzerland", "3135");
		countryCodeBox.addItemAndValue("Portugal (same as Switzerland)", "3135");
		countryCodeBox.addItemAndValue("Spain (same as Switzerland)", "3135");
		
		countryCodeBox.addItemAndValue("Finland", "3034");
		countryCodeBox.addItemAndValue("Germany (same as Finland)", "3034");
		countryCodeBox.addItemAndValue("Iceland (same as Finland)", "3034");
		countryCodeBox.addItemAndValue("Netherlands (same as Finland)", "3034");
		
		countryCodeBox.addItemAndValue("Australia", "3430");
		countryCodeBox.addItemAndValue("South Africa (same as Australia)", "3430");
		
		countryCodeBox.addItemAndValue("United Kingdom", "3136");
		countryCodeBox.addItemAndValue("Belgium (same as United Kingdom)", "3136");
		countryCodeBox.addItemAndValue("Denmark (same as United Kingdom)", "3136");
		countryCodeBox.addItemAndValue("Greece (same as United Kingdom)", "3136");
		countryCodeBox.addItemAndValue("Ireland (same as United Kingdom)", "3136");
		countryCodeBox.addItemAndValue("Norway (same as United Kingdom)", "3136");
		
		countryCodeBox.addItemAndValue("New Zealand", "3438");
		
		countryCodeBox.addItemAndValue("France", "3035");
		
		countryCodeBox.addItemAndValue("Japan", "3433");
		
		countryCodeBox.addItemAndValue("Austria", "3031");
		
		countryCodeBox.addItemAndValue("Italy", "3038");
		
		countryCodeBox.addItemAndValue("Sweden", "3134");
		
		
		setUpDisplay();
	}
	
	
	private void setUpDisplay()
	{
		
		modemInitField = new JTextField(10);
		primaryPhoneField = new JTextField(10);
		secondaryPhoneField = new JTextField(10);
		JLabel modemInitLabel = new JLabel("Modem init string");
		JLabel primaryPhoneLabel = new JLabel("Phone number");
		JLabel secondaryPhoneLabel = new JLabel("Secondary phone number");
		
		// set params for layout manager
		GridBagLayout theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.setLayout(theLayout);
		
		
		// add stuff: just single phone number for now!
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(primaryPhoneLabel, c);
		this.add(primaryPhoneLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(primaryPhoneField, c);
		this.add(primaryPhoneField);
		
		// also modem init string, for Steve Palm
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(modemInitLabel, c);
		this.add(modemInitLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(modemInitField, c);
		this.add(modemInitField);
		
		// and modem country code box
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(countryCodeBox, c);
		this.add(countryCodeBox);
		
		refreshDisplay();
		
		
	}
	
	
	
	public void writeValue()
		throws ValueFormatException
	{
		
		
		// first, clear aggregate count, modem init, phone number field
		AirportInfoRecord aggregateInfoRecord = airportInfo.get("Modem init/phone number block");
		aggregateInfoRecord.clearWindow();
		
		
		
		
		// now write concatenated modem init string and phone numbers
		/**
		*	
		*	Modem stuff: starting at row 8, column 11
		*		continuation in columns 11 and 12 of subsequent rows
		*	First three bytes:
		*		Primary phone number count
		*		Secondary phone number count
		*		Modem init string count
		*	Modem initialization string next,
		*		continuation in columns 11 and 12 of subsequent rows
		*	Phone numbers: starting immediately after init string
		*		continuation in columns 11 and 12 of subsequent rows
		*		uses BCD, with D for space/paren and E for dash
		*		secondary number follows immediately
		*
		*	Some subfields defined separately:
		*		country code (above)
		*		string counts for modem init, phone numbers
		*
		*	SO
		*		this field includes counts, modem init string and phone numbers;
		*		count fields must overwrite first 3 bytes
		*
		*/	
		
		
		String modemInitString = modemInitField.getText();
		// add in prefix "   *NC22"; spaces will be overwritten with phone number abd init string lengths;
		// default 22 country code will be overwritten with correct value by cuntry code box
		modemInitString = "   *NC22" + modemInitString;
		
		String phoneNumberString = new String();
		phoneNumberString += primaryPhoneField.getText();
		phoneNumberString += secondaryPhoneField.getText();
		
		byte[] phoneBytes = convertFromPhoneNumber(phoneNumberString);
		byte[] modemInitBytes = modemInitString.getBytes();
		
		byte[] bytes = new byte[modemInitBytes.length + phoneBytes.length];
		
		for(int i = 0; i < modemInitBytes.length; i++)
			bytes[i] = modemInitBytes[i];
		
		for(int i = 0; i < phoneBytes.length; i++)
			bytes[i + modemInitBytes.length] = phoneBytes[i];
		
		aggregateInfoRecord.byteBlockWindow.writeBytes(bytes);
		
		
		// now determine counts of info; set appropriate fields (overwriting first three bytes
		// of modem init string
		
		int initStringCount = modemInitField.getText().length();
		// add country code string count, e.g., "*NC22" (not included in modem init string)
		initStringCount += countryCodePrefixCount;
		AirportInfoRecord initStringCountRecord = airportInfo.get("Modem init string length");
		String modemInitCountString = new String();
		modemInitCountString += initStringCount;
		initStringCountRecord.setBytesFromString(modemInitCountString);
		
		int primaryCount = primaryPhoneField.getText().length();
		AirportInfoRecord primaryCountRecord = airportInfo.get("Primary phone number length");
		String primaryCountString = new String();
		primaryCountString += primaryCount;
		primaryCountRecord.setBytesFromString(primaryCountString);
		
		int secondaryCount = secondaryPhoneField.getText().length();
		AirportInfoRecord secondaryCountRecord = airportInfo.get("Secondary phone number length");
		String secondaryCountString = new String();
		secondaryCountString += secondaryCount;
		secondaryCountRecord.setBytesFromString(secondaryCountString);
		
		// finally, write value from country code box
		countryCodeBox.writeValue();
		
	}
	
	
	
	
	public void refreshDisplay()
	{
		
		int OIDNum, OIDRow, OIDCol, numRows, numCols, dataType;
	
		
		// determine counts of info
		
		AirportInfoRecord initStringCountRecord = airportInfo.get("Modem init string length");
		int initStringCount = Integer.parseInt(initStringCountRecord.toString());
		
		AirportInfoRecord primaryCountRecord = airportInfo.get("Primary phone number length");
		int primaryCount = Integer.parseInt(primaryCountRecord.toString());
		
		AirportInfoRecord secondaryCountRecord = airportInfo.get("Secondary phone number length");
		int secondaryCount = Integer.parseInt(secondaryCountRecord.toString());
		
		
		// now get pieces out of aggregate field
		AirportInfoRecord aggregateInfoRecord = airportInfo.get("Modem init/phone number block");
		byte[] blockBytes = aggregateInfoRecord.byteBlockWindow.getBytes();
		
		
		// get modem init string, excluding country code part
		int partialInitStringCount = initStringCount - countryCodePrefixCount;
		if(partialInitStringCount < 0)
			partialInitStringCount = 0;
		
		byte[] initStringBytes = new byte[partialInitStringCount];
		for (int i = 0; i < partialInitStringCount; i++)
			initStringBytes[i] = blockBytes[i + countryCodePrefixCount + countFieldCount];
			
		
		String modemInitString = new String(initStringBytes);
		
		modemInitField.setText(modemInitString);
		
		
		// get phone number strings
		
		int startNibble;
		int endNibble;
		int startByte = partialInitStringCount + countryCodePrefixCount + countFieldCount;
		
		startNibble = 2 * startByte;
		endNibble = 2 * startByte + primaryCount;
		String primaryPhoneNumber = convertToPhoneNumber(blockBytes, startNibble, endNibble);
		
		primaryPhoneField.setText(primaryPhoneNumber);
		
		startNibble = 2 * startByte + primaryCount;
		endNibble = 2 * startByte + primaryCount + secondaryCount;
		String secondaryPhoneNumber = convertToPhoneNumber(blockBytes, startNibble, endNibble);
		
		secondaryPhoneField.setText(secondaryPhoneNumber);
	
		
	}
	
	
	
	private String convertToPhoneNumber(byte[] bytes, int startNibble, int endNibble)
	// since phone numbers are BCD encoded, the start and count indices are in NIBBLES, i.e.,
	// half-bytes!!!!
	{
		String returnString = new String();
		
		if ( 2 * (startNibble/2) != startNibble)	// odd start nibble
		{
			byte right = (byte)(bytes[startNibble/2] & 0x0F);
			
			returnString += AirportInfoRecord.decodePhoneDigit(right);
		}
		
		for (int i = (startNibble + 1)/2; i < (endNibble)/2; i++)
		{
			byte left = (byte)((bytes[i] & 0xF0) >>> 4);
			byte right = (byte)(bytes[i] & 0x0F);
			
			returnString += AirportInfoRecord.decodePhoneDigit(left);
			returnString += AirportInfoRecord.decodePhoneDigit(right);
		}
		
		if ( 2 * (endNibble/2) != endNibble)	// odd end nibble
		{
			byte left = (byte)((bytes[endNibble/2] & 0xF0) >>> 4);
			
			returnString += AirportInfoRecord.decodePhoneDigit(left);
		}
		
		return returnString;
	}
	
	
	
	private byte[] convertFromPhoneNumber(String phoneNumber)
		throws ValueFormatException
	// Phone numbers are BCD encoded, with E for dash, D for space or paren, C for comma
	{
		char[] chars = phoneNumber.toCharArray();
		byte[] bytes = new byte[(chars.length + 1)/2];
		
		for (int i = 0; i < chars.length; i++)
		{
			byte nibble;
			
			try
			{
				nibble = AirportInfoRecord.encodePhoneDigit(chars[i]);
			}
			catch(NumberFormatException e)
			{
				throw new ValueFormatException("Invalid phone number");
			}
				
			if (2 * (i/2) == i)		// i even, shift left
				nibble = (byte)(nibble << 4);
			
			bytes[i/2] |= nibble;
			
		}
		
		
		return bytes;
	}
	
	
	
		
}