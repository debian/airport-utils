/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
package airport;


import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import byteblock.*;

/**
*	Handles display and updating of MAC addresses and host names used for access control on the
*	wireless LAN.
*/

public class AirportAccessControlTable extends AirportInfoPanel
{
	private AirportInfo airportInfo;
	private Vector addressVector;
	private JTable table;
	private AbstractTableModel tableModel;
	
	
	/**
	*	Table model which maintains list of MAC addresses and host names.
	*/
	
	private class AccessAddressTableModel extends AbstractTableModel
	{
		public int getColumnCount() 
		{ 
			return 2; 
		}
		
		public int getRowCount() 
		{ 
			return AirportInfo.MAX_NUM_MAC_ADDRESSES;
		}
		
		public boolean isCellEditable(int row, int col) 
		{ 
			if (row <= addressVector.size())
				return true;
			else
				return false;
		}
		
		public String getColumnName(int col) 
		{ 
			if (col == 0)
				return "MAC address";
			else
				return "Host name (opt)";
		}
		
		public Object getValueAt(int row, int col)
		{
			if (row < addressVector.size())
				return ((String[])addressVector.elementAt(row))[col];
			else
				return "";
		}
		
		public void setValueAt(Object newValue, int row, int col) 
		{
			if (newValue instanceof String)
			{
				if (row < addressVector.size())
				{
					String[] addressEntry = (String[])addressVector.elementAt(row);
					addressEntry[col] = (String)newValue;
				}
				else
				{
					String[] addressEntry = {"",""};
					addressEntry[col] = (String)newValue;
					addressVector.insertElementAt(addressEntry, addressVector.size());
				}
			}
		}
		
	}
	
	
	
	
	
	/**
	*	Create new table based on data in airportInfo.
	*/
	
	public AirportAccessControlTable(AirportInfo airportInfo)
	{
		this.airportInfo = airportInfo;
		setUpDisplay();
	}
	
	
	
	private void setUpDisplay()
	{
		refreshDisplay();
	}
	
	
	
	
	
	
	
	
	
	/**
	*	Write the MAC addresses and hostnames in the list into the appropriate locations
	*	in the byte block referenced by the AirportInfo object supplied in the constructor.
	*	@throws ValueFormatException Thrown if either a MAC address is malformed, or hostname
	*	is too long. Also sets editting focus to the offending cell.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		
		int dataType, namedataType, addressdataType;
		int baseStartIndex, numRows, numCols;
		int addressStartIndex, addressnumRows, addressnumCols;
		int nameStartIndex, namenumRows, namenumCols;
		
		// first, record changes if cell currently being edited
		TableCellEditor editor = table.getCellEditor(); 
		if(editor != null)
			editor.stopCellEditing();
		
		
		
		ByteBlock baseBlock = airportInfo.baseBlock;
		
		
		// erase entire blocks of info
		addressStartIndex = 0xF8A;
		addressnumRows = 1;	
		addressnumCols = 6 * AirportInfo.MAX_NUM_MAC_ADDRESSES;
		addressdataType = AirportInfoRecord.BYTE_STRING;
		AirportInfoRecord addressBlockRecord = new AirportInfoRecord(addressStartIndex, addressnumRows, addressnumCols, addressdataType, baseBlock); 
		addressBlockRecord.clearWindow();
		
		nameStartIndex = 0x1CC8;
		namenumRows = 1;	
		namenumCols = 20 * AirportInfo.MAX_NUM_MAC_ADDRESSES;
		namedataType = AirportInfoRecord.CHAR_STRING;
		AirportInfoRecord nameBlockRecord = new AirportInfoRecord(nameStartIndex, namenumRows, namenumCols, namedataType, baseBlock);
		nameBlockRecord.clearWindow();
		
				
		
		//create new info records
		
		
		/**
		*	Mac addresses for access control
		*		count: byte 8*16 + 9 (maybe 8, with 2 digits)
		*		addresses: byte 8*16 + 11, in 6-byte blocks
		*	 		continue on from here.
		*	NOTE!! Apple claims can have up to 497 MAC addresses; this would require 497*6 = 2982 bytes = 11.65 (16x16) blocks
		*	In fact:
		*		Mac addresses start at byte 15*256 + 8*16 + 11 = 3979
		*		Names start at byte 28*256 + 12*16 + 9 = 7369
		*		Difference is 3390 = 565 6-byte addresses (or 13.24 blocks)
		*/
		
		int numMacAddresses = 0;
		
		addressStartIndex = 0xF8A;
		addressnumRows = 1;	
		addressnumCols = 6;
		addressdataType = AirportInfoRecord.BYTE_STRING;
		
		nameStartIndex = 0x1CC8;
		namenumRows = 1;	
		namenumCols = 20;
		namedataType = AirportInfoRecord.CHAR_STRING;
		
		
		for (int i = 0; i < addressVector.size(); i++)
		{
			
			try
			{
				// these only get augmented if address is OK (and non-null)
				if (! ((String[])addressVector.elementAt(i))[0].equals(""))
				{
					AirportInfoRecord addressRecord = new AirportInfoRecord(addressStartIndex, addressnumRows, addressnumCols, addressdataType, baseBlock);
					addressRecord.setBytesFromString(((String[])addressVector.elementAt(i))[0]);
					
					addressStartIndex += 6;
					numMacAddresses++;
				}
				
			}
			catch (ValueFormatException e)
			{
				table.editCellAt(i,0);
				throw e;
			}
			
			try
			{
				AirportInfoRecord nameRecord = new AirportInfoRecord(nameStartIndex, namenumRows, namenumCols, namedataType, baseBlock);
				nameRecord.setBytesFromString(((String[])addressVector.elementAt(i))[1]);
				
				nameStartIndex += 20;
			}
			catch (ValueFormatException e)
			{
				table.editCellAt(i,1);
				throw e;
			}
			
			
				
		}
		
		// finally, write the number of addresses written
		baseStartIndex = 0xF87;
		numRows = 1;	
		numCols = 2;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		AirportInfoRecord macCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		String addressCountString = new String();
		addressCountString += numMacAddresses;
		macCountRecord.setBytesFromString(addressCountString);
		
		
		// also, set access control switch: on if any Mac entries, off otherwise
		/**
		*	Access control switch: byte 4*16 + 9  
		*		00 = no access control
		*		80 = access control used
		*/
		
		String accessSwitchValue = new String();
		
		if (numMacAddresses > 0)
			accessSwitchValue = "80";
		else
			accessSwitchValue = "00";
			
		AirportInfoRecord accessControlSwitchRecord = airportInfo.get("Access control switch");
		accessControlSwitchRecord.setBytesFromString(accessSwitchValue);
		
		
		
	}
		
		
		
	
	/**
	*	Refresh the display based on the current data in the underlying byte block.
	*/
	
	public void refreshDisplay()
	{
		
		int addressStartIndex, addressnumRows, addressnumCols, addressdataType;
		int nameStartIndex, namenumRows, namenumCols, namedataType;
		int baseStartIndex, numRows, numCols, dataType;
		
		ByteBlock baseBlock = airportInfo.baseBlock;
		
		
		/**
		*	Mac addresses for access control
		*		count: byte 8*16 + 9 (maybe 8, with 2 digits)
		*		addresses: byte 8*16 + 11, in 6-byte blocks
		*	 		continue on from here.
		*	NOTE!! Apple claims can have up to 497 MAC addresses; this would require 497*6 = 2982 bytes = 11.65 (16x16) blocks
		*	In fact:
		*		Mac addresses start at byte 15*256 + 8*16 + 11 = 3979
		*		Names start at byte 28*256 + 12*16 + 9 = 7369
		*		Difference is 3390 = 565 6-byte addresses (or 13.24 blocks)
		*/
		
		baseStartIndex = 0xF87;
		numRows = 1;	
		numCols = 2;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		AirportInfoRecord macCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		
		
		// Read in appropriate number of Mac addresses
		String countString = macCountRecord.toString();
		int numMacAddresses = 0;
		
		try
		{
			numMacAddresses = Integer.parseInt(countString);
		}
		catch(NumberFormatException e)
		{
			System.out.println("Problem with number of Mac addresses");
		}
		
		// create new addressVector, with appropriate size
		addressVector = new Vector();
		
		
		
		addressStartIndex = 0xF8A;
		addressnumRows = 1;	
		addressnumCols = 6;
		addressdataType = AirportInfoRecord.BYTE_STRING;
		
		nameStartIndex = 0x1CC8;
		namenumRows = 1;	
		namenumCols = 20;
		namedataType = AirportInfoRecord.CHAR_STRING;
		
		
		for (int i = 0; i < numMacAddresses; i++)
		{
			AirportInfoRecord addressRecord = new AirportInfoRecord(addressStartIndex, addressnumRows, addressnumCols, addressdataType, baseBlock);
			AirportInfoRecord nameRecord = new AirportInfoRecord(nameStartIndex, namenumRows, namenumCols, namedataType, baseBlock);
			
			String[] addressEntry = new String[2];
			addressEntry[0] = addressRecord.toString();
			addressEntry[1] = nameRecord.toString();
			addressVector.insertElementAt(addressEntry, addressVector.size());
			
			addressStartIndex += 6;
			nameStartIndex += 20;
		}
		
		this.removeAll();
		tableModel = new AccessAddressTableModel();
		table = new JTable(tableModel);
		table.setCellSelectionEnabled(true);
		table.setPreferredScrollableViewportSize(new Dimension(350,200));
		JScrollPane scrollPane = new JScrollPane(table);
		this.add(scrollPane);
		
		
	}
	
	
	
	

}