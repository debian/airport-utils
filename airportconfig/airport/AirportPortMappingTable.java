/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
 
package airport;


import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import byteblock.*;

/**
*	Handles display and updating of port mapping information.
*/

public class AirportPortMappingTable extends AirportInfoPanel
{
	private AirportInfo airportInfo;
	private Vector addressVector;
	private JTable table;
	private AbstractTableModel tableModel;
	
	
	/**
	*	Table model which maintains list of port mappings.
	*/
	
	private class PortMappingTableModel extends AbstractTableModel
	{
		public int getColumnCount() 
		{ 
			return 3; 
		}
		
		public int getRowCount() 
		{ 
			return AirportInfo.MAX_NUM_PORT_MAPS;
		}
		
		public boolean isCellEditable(int row, int col) 
		{ 
			if (row <= addressVector.size())
				return true;
			else
				return false;
		}
		
		public String getColumnName(int col) 
		{ 
			switch (col)
			{
				case 0:
					return "Public port number";
					
				case 1:
					return "Private IP address";
					
				case 2:
					return "Private port number";
					
				default:
					return "";
			}
			
		}
		
		public Object getValueAt(int row, int col)
		{
			if (row < addressVector.size())
				return ((String[])addressVector.elementAt(row))[col];
			else
				return "";
		}
		
		public void setValueAt(Object newValue, int row, int col) 
		{
			if (newValue instanceof String)
			{
				if (row < addressVector.size())
				{
					String[] addressEntry = (String[])addressVector.elementAt(row);
					addressEntry[col] = (String)newValue;
				}
				else
				{
					String[] addressEntry = {"","",""};
					addressEntry[col] = (String)newValue;
					addressVector.insertElementAt(addressEntry, addressVector.size());
				}
			}
		}
		
	}
	
	
	
	
	
	
	/**
	*	Create new table based on data in airportInfo.
	*/
	
	public AirportPortMappingTable(AirportInfo airportInfo)
	{
		this.airportInfo = airportInfo;
		setUpDisplay();
	}
	
	
	
	
	private void setUpDisplay()
	{
		refreshDisplay();
	}
	
	
	
	
	
	
	/**
	*	Write the MAC addresses and hostnames in the list into the appropriate locations
	*	in the byte block referenced by the AirportInfo object supplied in the constructor.
	*	@throws ValueFormatException Thrown if either an IP address is malformed, or ports
	*	are incorrect. Also sets editting focus to the offending cell.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		
		int baseStartIndex, numRows, numCols, dataType;
		
		// first, record changes if cell currently being edited
		TableCellEditor editor = table.getCellEditor(); 
		if(editor != null)
			editor.stopCellEditing();
		
		
		
		ByteBlock baseBlock = airportInfo.baseBlock;
		
		
		// erase current info
		
		AirportInfoRecord eraseRecord;
		
		eraseRecord = airportInfo.get("Public port numbers");
		eraseRecord.clearWindow();
		
		eraseRecord = airportInfo.get("Private IP addresses, first two octets");
		eraseRecord.clearWindow();
		
		eraseRecord = airportInfo.get("Private IP addresses, last two octets");
		eraseRecord.clearWindow();
		
		eraseRecord = airportInfo.get("Private port numbers");
		eraseRecord.clearWindow();
		
		
		
		
		//create new info records
		
		/*	
		*	Port mapping functions: 
		*		count of port mappings: 0xD62
		*		public port numbers: 0xDC2, in 2-byte pairs
		*		first two octets of private IP addresses: 0xDEA,
		*			 in 2-byte pairs
		*		last two octets of private IP addresses: 0xE12,
		*			 in 2-byte pairs
		*		private port numbers: 0xE3A, in 2-byte pairs
		*/
		
		
		int numPortMaps = 0;
		
		int publicPortStartIndex = 0xDC2;
		int publicPortNumRows = 1;	
		int publicPortNumCols = 2;
		int publicPortDataType = AirportInfoRecord.UNSIGNED_INTEGER;
		
		
		int privateIPStartIndex = 0xDEA;
		int privateIPBeginningNumRows = 1;	
		int privateIPBeginningNumCols = 2;
		
		int privateIPEndingIndex = 0xE12;
		int privateIPEndingNumRows = 1;	
		int privateIPEndingNumCols = 2;
		
		int privateIPDataType = AirportInfoRecord.IP_ADDRESS;
		
		
		int privatePortStartIndex = 0xE3A;
		int privatePortNumRows = 1;	
		int privatePortNumCols = 2;
		int privatePortDataType = AirportInfoRecord.UNSIGNED_INTEGER;
		
		
		
		for (int i = 0; i < addressVector.size(); i++)
		{
			
			String publicPort = ((String[])addressVector.elementAt(i))[0];
			String privateIP = ((String[])addressVector.elementAt(i))[1];
			String privatePort = ((String[])addressVector.elementAt(i))[2];
			
			// if null row, just ignore
			if (! (publicPort.equals("") && privateIP.equals("") && privatePort.equals("")))
			{
				
				// null address or port throws exception when write to info records

				try
				{
					AirportInfoRecord publicPortRecord = new AirportInfoRecord(publicPortStartIndex, publicPortNumRows, publicPortNumCols, publicPortDataType, baseBlock);
					publicPortRecord.setBytesFromString(publicPort);
					
				}
				catch (ValueFormatException e)
				{
					table.editCellAt(i,0);
					throw e;
				}
				
				
				try
				{
					// need to have a multi window since address octets not contiguous!
					
					ByteBlockMultiWindow privateIPWindow = new ByteBlockMultiWindow();
					
					ByteBlockWindow privateIPBeginningWindow = new ByteBlockRectangularWindow(privateIPStartIndex, privateIPBeginningNumRows, privateIPBeginningNumCols, baseBlock);
					privateIPWindow.addWindow(privateIPBeginningWindow);
					
					ByteBlockWindow privateIPEndingWindow = new ByteBlockRectangularWindow(privateIPEndingIndex, privateIPEndingNumRows, privateIPEndingNumCols, baseBlock);
					privateIPWindow.addWindow(privateIPEndingWindow);
					
					AirportInfoRecord privateIPRecord = new AirportInfoRecord(privateIPDataType, privateIPWindow);
					privateIPRecord.setBytesFromString(privateIP);
					
				}
				catch (ValueFormatException e)
				{
					table.editCellAt(i,1);
					throw e;
				}
				
				try
				{
					AirportInfoRecord privatePortRecord = new AirportInfoRecord(privatePortStartIndex, privatePortNumRows, privatePortNumCols, privatePortDataType, baseBlock);
					privatePortRecord.setBytesFromString(privatePort);
				}
				catch (ValueFormatException e)
				{
					table.editCellAt(i,2);
					throw e;
				}
				
				
				
				
				
				// augment port map count, start indices
				publicPortStartIndex += 2;
				privateIPStartIndex += 2;
				privateIPEndingIndex += 2;
				privatePortStartIndex += 2;
				numPortMaps++;
				
				
			
			}
				
		}
		
		// finally, write the number of addresses written
		AirportInfoRecord portMapCountRecord = airportInfo.get("Count of port mappings");
		String countString = new String();
		countString += numPortMaps;
		portMapCountRecord.setBytesFromString(countString);
		
	}
		
		
		
	
	/**
	*	Refresh the display based on the current data in the underlying byte block.
	*/
	
	public void refreshDisplay()
	{
		
		ByteBlock baseBlock = airportInfo.baseBlock;
		
		
		/*	
		*	Port mapping functions: 
		*		count of port mappings: 0xD62
		*		public port numbers: 0xDC2, in 2-byte pairs
		*		first two octets of private IP addresses: 0xDEA,
		*			 in 2-byte pairs
		*		last two octets of private IP addresses: 0xE12,
		*			 in 2-byte pairs
		*		private port numbers: 0xE3A, in 2-byte pairs
		*/
		
		
		AirportInfoRecord portMapCountRecord = airportInfo.get("Count of port mappings");
		
		// Read in appropriate number of Mac addresses
		String countString = portMapCountRecord.toString();
		int numPortMaps = 0;
		
		try
		{
			numPortMaps = Integer.parseInt(countString);
		}
		catch(NumberFormatException e)
		{
			System.out.println("Problem with number of Mac addresses");
		}
		
		
		// create new addressVector, with appropriate size
		addressVector = new Vector();
		
		
		
		int publicPortStartIndex = 0xDC2;
		int publicPortNumRows = 1;	
		int publicPortNumCols = 2;
		int publicPortDataType = AirportInfoRecord.UNSIGNED_INTEGER;
		
		
		int privateIPStartIndex = 0xDEA;
		int privateIPBeginningNumRows = 1;	
		int privateIPBeginningNumCols = 2;
		
		int privateIPEndingIndex = 0xE12;
		int privateIPEndingNumRows = 1;	
		int privateIPEndingNumCols = 2;
		
		int privateIPDataType = AirportInfoRecord.IP_ADDRESS;
		
		
		int privatePortStartIndex = 0xE3A;
		int privatePortNumRows = 1;	
		int privatePortNumCols = 2;
		int privatePortDataType = AirportInfoRecord.UNSIGNED_INTEGER;
		
		
		
		for (int i = 0; i < numPortMaps; i++)
		{
			AirportInfoRecord publicPortRecord = new AirportInfoRecord(publicPortStartIndex, publicPortNumRows, publicPortNumCols, publicPortDataType, baseBlock);
			
			// need to have a multi window since IP address octets not contiguous!		
			ByteBlockMultiWindow privateIPWindow = new ByteBlockMultiWindow();
			
			ByteBlockWindow privateIPBeginningWindow = new ByteBlockRectangularWindow(privateIPStartIndex, privateIPBeginningNumRows, privateIPBeginningNumCols, baseBlock);
			privateIPWindow.addWindow(privateIPBeginningWindow);
			
			ByteBlockWindow privateIPEndingWindow = new ByteBlockRectangularWindow(privateIPEndingIndex, privateIPEndingNumRows, privateIPEndingNumCols, baseBlock);
			privateIPWindow.addWindow(privateIPEndingWindow);
			
			AirportInfoRecord privateIPRecord = new AirportInfoRecord(privateIPDataType, privateIPWindow);
			
			AirportInfoRecord privatePortRecord = new AirportInfoRecord(privatePortStartIndex, privatePortNumRows, privatePortNumCols, privatePortDataType, baseBlock);
							
			String[] addressEntry = new String[3];
			addressEntry[0] = publicPortRecord.toString();
			addressEntry[1] = privateIPRecord.toString();
			addressEntry[2] = privatePortRecord.toString();
			addressVector.insertElementAt(addressEntry, addressVector.size());
			
			publicPortStartIndex += 2;
			privateIPStartIndex += 2;
			privateIPEndingIndex += 2;
			privatePortStartIndex += 2;
				
			
		}
		
		this.removeAll();
		tableModel = new PortMappingTableModel();
		table = new JTable(tableModel);
		table.setCellSelectionEnabled(true);
		table.setPreferredScrollableViewportSize(new Dimension(350,200));
		JScrollPane scrollPane = new JScrollPane(table);
		this.add(scrollPane);
		
		
	}
	
	
	
	

}