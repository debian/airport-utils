/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;




/**
*	Panel which maintains data pertaining to modem configuration. Has settings for phone number,
*	username and password, and the like.
*/

public class AirportModemConfigPanel extends AirportInfoPanel
										implements ActionListener
{
	
	private AirportPhoneNumberPanel phoneNumberPanel;
	private AirportLoginInfoDialog loginDialog;
	private AirportInfoLabelledTextField modemTimeoutField;
	private AirportInfoComboBox tonePulseDialingBox;
	private AirportInfoCheckBox automaticDialingCheckbox;
	private JButton loginButton;
	
	private AirportInfo theInfo;

	
	
	
	
	public AirportModemConfigPanel(AirportInfo theInfo)
	{
		
		this.theInfo = theInfo;
		
		// panel holding phone number, modem init string info
		phoneNumberPanel = new AirportPhoneNumberPanel(theInfo);
		
		
		// Button to show login panel when pressed...
		loginButton = new JButton("Username/Password/Login Script");
		loginButton.setActionCommand("show login panel");
		loginButton.addActionListener(this);
		
		modemTimeoutField = new AirportInfoLabelledTextField("Modem timeout (in 10-second units)", theInfo.get("Modem timeout"));
		
		tonePulseDialingBox  = new AirportInfoComboBox("Tone or pulse (rotary) dialing", theInfo.get("Dialing type (tone or pulse)"));
		tonePulseDialingBox.addItemAndValue("Tone", "08");
		tonePulseDialingBox.addItemAndValue("Pulse", "00");
		
		automaticDialingCheckbox = new AirportInfoCheckBox("Automatic dialing", theInfo.get("Automatic dial"), "00", "02");
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,10,2,10);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.setLayout(theLayout);
		
		c.gridwidth = 2;
		// add components
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(phoneNumberPanel, c);
		this.add(phoneNumberPanel);
		
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(loginButton, c);
		this.add(loginButton);
		
		c.gridwidth = 1;
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(automaticDialingCheckbox, c);
		this.add(automaticDialingCheckbox);
		
		c.gridheight = 1;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(modemTimeoutField, c);
		this.add(modemTimeoutField);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(tonePulseDialingBox, c);
		this.add(tonePulseDialingBox);
		
	}
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("show login panel"))
		{
			// get top-level Frame to associate modal dialog with so will be minimzed and
			// maximized correctly.
			Component owner = this;
			while ( !((owner == null) || (owner instanceof Frame)) )
				owner = owner.getParent();
			Frame owningFrame = (Frame)owner;
			
			// create and display modal dialog holding dial-up username / password and login script
			loginDialog = new AirportLoginInfoDialog(owningFrame, "Login Information", true, theInfo);
			loginDialog.pack();
			
			// tweak app size to make it a little larger than necessary, to address the
			// "shrunken textfields" problem arising from the layout manager packing stuff
			// a little too tightly.
			Dimension dim = loginDialog.getSize();
			dim.height += 20;
			dim.width += 20;
			loginDialog.setSize(dim);
			
			loginDialog.show();
		}
		
	}
		
	
	
	
}