/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains basic base station information: name, location, and contact individual.
*	Also provides textfield for entering new community name (base station password).
*/

public class AirportPortMappingPanel extends AirportInfoPanel
{
	
	private AirportInfoCheckBox portMappingCheckbox;
	private AirportPortMappingTable portMappingTable;
	
	
	public AirportPortMappingPanel(AirportInfo theInfo)
	{
		portMappingCheckbox = new AirportInfoCheckBox("Use port mapping", theInfo.get("Port mapping switch"), "04", "00");
		
		portMappingTable = new AirportPortMappingTable(theInfo);
		
		setUpDisplay();
	}
	
	
	private void setUpDisplay()
	{
		
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		JLabel theLabel;
		
		
		this.setLayout(theLayout);
		
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.WEST;
		theLayout.setConstraints(portMappingCheckbox, c);
		this.add(portMappingCheckbox);
		
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.CENTER;
		theLayout.setConstraints(portMappingTable, c);
		this.add(portMappingTable);
		
	}
		
		
		
}