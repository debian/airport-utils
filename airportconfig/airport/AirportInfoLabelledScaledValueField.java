/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;
import byteblock.*;



/**
*	Creates a labelled text box to display and edit the information in an AirportInfoRecord.
*	Size of textfield is set automatically according to datatype.
*/

public class AirportInfoLabelledScaledValueField extends AirportInfoPanel
{
	public String name;
	public AirportInfoScaledValueField theField;
	private AirportInfoRecord theRecord;
	
	
	
	
	/**
	*	Creates new display for viewing and editing the information in the supplied 
	*	AirportInfoRecord. Size of textfield is supplied.
	*/
	
	public AirportInfoLabelledScaledValueField(String name, AirportInfoRecord theRecord, int scalingFactor, int fieldSize)
	{
		this.name = name;
		this.theRecord = theRecord;
		
		JLabel l = new JLabel(name);
		
		theField = new AirportInfoScaledValueField(theRecord, scalingFactor, fieldSize);
		
		this.setLayout(new BorderLayout(2,2));
		this.add("West", l);
		this.add("East", theField);
			
		refreshDisplay();
	}
	
	
	
	
	/**
	*	Creates new display for viewing and editing the information in the supplied 
	*	AirportInfoRecord. Size of textfield is set automatically according to datatype.
	*/
	
	public AirportInfoLabelledScaledValueField(String name, AirportInfoRecord theRecord, int scalingFactor)
	{
		this.name = name;
		this.theRecord = theRecord;
		JLabel l = new JLabel(name);
		
		theField = new AirportInfoScaledValueField(theRecord, scalingFactor);
		
		this.setLayout(new BorderLayout(5,5));
		this.add("West", l);
		this.add("East", theField);
		
		refreshDisplay();
	}
	
	
	
	
	
	
	
	
	/**
	*	Read value from associated AirportInfoRecord window, and display value.
	*/
	
	public void refreshDisplay()
	{
		theField.refreshDisplay();
	}
	
	
	
	
	/**
	*	Write value currently displayed into associated AirportInfoRecord window.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		theField.writeValue();
		
	}
	
	
	
	
	
	/**
	*	Write supplied value, rather than that currently displayed, into associated 
	*	AirportInfoRecord window.
	*/
	
	public void writeValue(String newValue)
		throws ValueFormatException
	{
		theField.writeValue(newValue);
	}
	
	
	
	
	/**
	*	Utility method to get text in enclosed textfield
	*/
	public String getText()
	{
		return theField.getText();
	}
	
	
	
	
}