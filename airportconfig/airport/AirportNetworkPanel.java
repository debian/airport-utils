/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;




/**
*	Panel which maintains data relevant to base station network connection. Has radio button selection
*	for modem vs Ethernet connection of base station to external network, plus DNS server fields.
*	Has subpanels for configuring the network connection: modem panel with settings for phone number,
*	username and password, and the like; Ethernet panel with choice of manual or DHCP config, plus
*	settings for base station IP address, router address and mask. These subpanels are enabled only
*	when associated connection method is selected.
*/

public class AirportNetworkPanel extends AirportInfoPanel
									implements ItemListener
									
{
	private AirportInfoRadioButton modemButton, ethernetButton;
	
	private AirportInfoTextField primaryDNSField, secondaryDNSField, domainNameField;
	
	private AirportInfoPanel ethernetConfigPanel, modemConfigPanel, dnsPanel;
	
	private ButtonGroup modemEthernetButtonGroup;
	
	
	
	
	public AirportNetworkPanel(AirportInfo theInfo)
	{
		
		/*
		
		.1.3.6.1.4.1.762.2.3.1.1.2	
		
			Ethernet/Modem switch 1: byte 11
				62 = modem
				60 = Ethernet
		
		.1.3.6.1.4.1.762.2.3.1.1.5	
		
			Configuration interface: byte 6*16 + 1
				03 = modem
				00 = Ethernet
				
		*/
		
		
		modemButton = new AirportInfoRadioButton("Connect to network through modem");
		modemButton.addInfoRecord(theInfo.get("Ethernet/Modem switch 1"), "62");
		modemButton.addInfoRecord(theInfo.get("Configuration interface"), "03");
		/**
		*	.1.3.6.1.4.1.762.2.3.1.1.2	
		*
		*
		*	also must handle base station configuration mode switch:  byte 1*16 + 8, MASK 20
		*			00 = manual or modem station config
		*			20 = DHCP station config
		*/	
		modemButton.addInfoRecord(theInfo.get("Base station configuration mode switch"), "00");
		
		
		ethernetButton = new AirportInfoRadioButton("Connect to network through Ethernet port");
		ethernetButton.addInfoRecord(theInfo.get("Ethernet/Modem switch 1"), "60");
		//ethernetButton.addInfoRecord(theInfo.get("Configuration interface"), "00");
		
		modemEthernetButtonGroup = new ButtonGroup();
		modemEthernetButtonGroup.add(modemButton);
		modemEthernetButtonGroup.add(ethernetButton);
		
		
		
		primaryDNSField = new AirportInfoTextField(theInfo.get("Primary DNS server"));
		secondaryDNSField = new AirportInfoTextField(theInfo.get("Secondary DNS server"));
		domainNameField = new AirportInfoTextField(theInfo.get("Domain name"));
		
		ethernetConfigPanel = new AirportEthernetConfigPanel(theInfo);
		
		modemConfigPanel = new AirportModemConfigPanel(theInfo);
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		JLabel label;
		
		
		/*
		// create and lay out button panel
		
		AirportInfoPanel buttonPanel = new AirportInfoPanel();
		buttonPanel.setLayout(theLayout);
		
		// add components
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(modemButton, c);
		buttonPanel.add(modemButton);
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(ethernetButton, c);
		buttonPanel.add(ethernetButton);
		*/
		
		
		
		
		
		// create and lay out DNS panel
		
		dnsPanel = new AirportInfoPanel();
		dnsPanel.setLayout(theLayout);
		
		// add components
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		label = new JLabel("Primary DNS server IP address");
		theLayout.setConstraints(label, c);
		dnsPanel.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(primaryDNSField, c);
		dnsPanel.add(primaryDNSField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		label = new JLabel("Secondary DNS server IP address");
		theLayout.setConstraints(label, c);
		dnsPanel.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(secondaryDNSField, c);
		dnsPanel.add(secondaryDNSField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 3;
		label = new JLabel("Domain name");
		theLayout.setConstraints(label, c);
		dnsPanel.add(label);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(domainNameField, c);
		dnsPanel.add(domainNameField);
		
		
		
		
		
		// now add panels to this
		
		this.setLayout(theLayout);
		
		c.anchor = GridBagConstraints.CENTER;
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(ethernetButton, c);
		this.add(ethernetButton);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(modemButton, c);
		this.add(modemButton);
		
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(ethernetConfigPanel, c);
		this.add(ethernetConfigPanel);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(modemConfigPanel, c);
		this.add(modemConfigPanel);
		
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(dnsPanel, c);
		this.add(dnsPanel);
		
		// make modem and ethernet config panel enablings dependent on button settings
		modemButton.addItemListener(modemConfigPanel);
		modemButton.addItemListener(this);
		modemConfigPanel.setEnabled(modemButton.isSelected());
		modemConfigPanel.setVisible(modemButton.isSelected());
		
		ethernetButton.addItemListener(ethernetConfigPanel);
		ethernetButton.addItemListener(this);
		ethernetConfigPanel.setEnabled(ethernetButton.isSelected());
		ethernetConfigPanel.setVisible(ethernetButton.isSelected());
		
		
	}
	
	
	
	
	/**
	*	Used to trigger contained panels to be visible or not; panels themselves
	*	are responsible for enabling/disabling themselves by virtue of being registered
	*	as ItemListeners themselves.
	*/
	
	public void itemStateChanged(ItemEvent e)
	{
		
		modemConfigPanel.setVisible(modemButton.isSelected());
		ethernetConfigPanel.setVisible(ethernetButton.isSelected());
		
	}
		
	
	
	
}