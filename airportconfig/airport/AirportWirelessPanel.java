/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains wireless LAN information: network name, channel, and encryption switch and key.
*	Note that encryption key is supplied as 10-digit hex string (with no "0x" prefix).
*/

public class AirportWirelessPanel extends AirportInfoPanel
{
	private AirportInfoLabelledTextField networkNameField, RTSThresholdField;
	private AirportInfoCheckBox encryptionBox, closedNetworkBox, RTSBox, microwaveRobustnessBox;
	private AirportInfoRadioButton encryptionKey1Button, encryptionKey2Button, encryptionKey3Button, encryptionKey4Button;
	private AirportInfoComboBox channelBox;
	private AirportEncryptionPanel encryptionPanel;
	
	
	public AirportWirelessPanel(AirportInfo theInfo)
	{
		
		networkNameField = new AirportInfoLabelledTextField("Network name", theInfo.get("Network name"));
		
		closedNetworkBox = new AirportInfoCheckBox("Closed network");
		closedNetworkBox.addInfoRecord(theInfo.get("Closed network flag"), "40", "00");
		
		RTSBox = new AirportInfoCheckBox("Use RTS/CTS");
		RTSBox.addInfoRecord(theInfo.get("RTS/CTS flag"), "20", "00");
		
		microwaveRobustnessBox = new AirportInfoCheckBox("Microwave robustness");
		microwaveRobustnessBox.addInfoRecord(theInfo.get("Microwave robustness flag"), "20", "00");
		
		RTSThresholdField = new AirportInfoLabelledTextField("RTS threshold", theInfo.get("RTS threshold"));
		
		/**
		* 	Encryption flag fields: 
		*		flag 1: masked with 0x08
		*			00 = no encryption
		*			08 = use encryption
		*		deny unencrypted data flag: masked with 0x80
		*			00 = don't deny (no encryption)
		*			80 = deny (use encryption)
		*		flag 3
		*			00 = no encryption
		*			01 = use encryption
		*		key size
		*			00 when no encryption
		*			05 when 40-bit encryption enabled
		*/
		
		encryptionBox = new AirportInfoCheckBox("Use encryption");
		encryptionBox.addInfoRecord(theInfo.get("Encryption flag 1"), "08", "00");
		encryptionBox.addInfoRecord(theInfo.get("Encryption flag 3"), "01", "00");
		// following is taken care of in the encryption panel
		// encryptionBox.addInfoRecord(theInfo.get("Deny unencrypted data flag"), "80", "00");
		
		
		channelBox = new AirportInfoComboBox("Wireless channel", theInfo.get("Wireless channel"));
		for (int i = 1; i < 12; i++)
			channelBox.addItemAndValue(Integer.toString(i), Integer.toString(i));
		
		
		encryptionPanel = new AirportEncryptionPanel(theInfo);
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		
		AirportInfoPanel RTSPanel = new AirportInfoPanel();
		RTSPanel.add(RTSThresholdField);
		
		
		AirportInfoPanel RTSMORPanel = new AirportInfoPanel();
		RTSMORPanel.setLayout(theLayout);
		
		c.insets = new Insets(0,0,0,10);
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(RTSBox, c);
		RTSMORPanel.add(RTSBox);
		
		c.insets = new Insets(0,10,0,10);
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(RTSPanel, c);
		RTSMORPanel.add(RTSPanel);
		
		c.insets = new Insets(0,10,0,0);
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 3;
		c.gridy = 1;
		theLayout.setConstraints(microwaveRobustnessBox, c);
		RTSMORPanel.add(microwaveRobustnessBox);
		
		
		this.setLayout(theLayout);
		
		
		JLabel theLabel;
		
		
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(networkNameField, c);
		this.add(networkNameField);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(channelBox, c);
		this.add(channelBox);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(closedNetworkBox, c);
		this.add(closedNetworkBox);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(RTSMORPanel, c);
		this.add(RTSMORPanel);
		
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(encryptionBox, c);
		this.add(encryptionBox);
		
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 2;
		c.gridy = 4;
		theLayout.setConstraints(encryptionPanel, c);
		this.add(encryptionPanel);
		
		
		
		// make encryption panel enabling dependent on encryption box setting
		encryptionBox.addItemListener(encryptionPanel);
		encryptionPanel.setEnabled(encryptionBox.isSelected());
		
		// make RTS threshold panel enabling dependent on RTS box setting
		RTSBox.addItemListener(RTSPanel);
		RTSPanel.setEnabled(RTSBox.isSelected());
		
		
	}
	
	
	
	
	
}