/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;
import AirportBaseStationConfigurator;




/**
*	Panel which maintains wireless LAN information: network name, channel, and encryption switch and key.
*	Note that encryption key is supplied as 10-digit hex string (with no "0x" prefix).
*/

public class AirportEncryptionPanel extends AirportInfoPanel
{
	private AirportInfoTextField encryptionKey1Field, encryptionKey2Field, encryptionKey3Field, encryptionKey4Field;
	private AirportInfoRadioButton encryptionKey1Button, encryptionKey2Button, encryptionKey3Button, encryptionKey4Button;
	private ButtonGroup buttonGroup;
	private AirportInfoCheckBox allowUnencryptedDataBox;
	
	
	public AirportEncryptionPanel(AirportInfo theInfo)
	{
		
		encryptionKey1Button = new AirportInfoRadioButton("Use encryption key 1:");
		encryptionKey1Button.addInfoRecord(theInfo.get("Encryption key selector"), "00");
		encryptionKey1Button.addInfoRecord(theInfo.get("Encryption key 1 size"), AirportBaseStationConfigurator.encryptionKeySizeString);
		
		encryptionKey2Button = new AirportInfoRadioButton("Use encryption key 2:");
		encryptionKey2Button.addInfoRecord(theInfo.get("Encryption key selector"), "40");
		encryptionKey2Button.addInfoRecord(theInfo.get("Encryption key 2 size"), AirportBaseStationConfigurator.encryptionKeySizeString);
		
		encryptionKey3Button = new AirportInfoRadioButton("Use encryption key 3:");
		encryptionKey3Button.addInfoRecord(theInfo.get("Encryption key selector"), "80");
		encryptionKey3Button.addInfoRecord(theInfo.get("Encryption key 3 size"), AirportBaseStationConfigurator.encryptionKeySizeString);
		
		encryptionKey4Button = new AirportInfoRadioButton("Use encryption key 4:");
		encryptionKey4Button.addInfoRecord(theInfo.get("Encryption key selector"), "C0");
		encryptionKey4Button.addInfoRecord(theInfo.get("Encryption key 4 size"), AirportBaseStationConfigurator.encryptionKeySizeString);
		
		
		buttonGroup = new ButtonGroup();
		buttonGroup.add(encryptionKey1Button);
		buttonGroup.add(encryptionKey2Button);
		buttonGroup.add(encryptionKey3Button);
		buttonGroup.add(encryptionKey4Button);
		
		
		encryptionKey1Field = new AirportInfoTextField(theInfo.get("Encryption key 1"));
		encryptionKey2Field = new AirportInfoTextField(theInfo.get("Encryption key 2"));
		encryptionKey3Field = new AirportInfoTextField(theInfo.get("Encryption key 3"));
		encryptionKey4Field = new AirportInfoTextField(theInfo.get("Encryption key 4"));
		
		/**
		*	Deny unencrypted data flag:	
		*		byte 6*16 + 9, mask 0x80 (bit 8)
		*			00 = allow all packets
		*			80 = allow only encrypted packets
		*/
		
		allowUnencryptedDataBox = new AirportInfoCheckBox("Allow unencrypted data");
		allowUnencryptedDataBox.addInfoRecord(theInfo.get("Deny unencrypted data flag"), "00", "80");
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.setLayout(theLayout);
		
		
		JLabel theLabel;
		
		
		if (AirportBaseStationConfigurator.encryptionKeySizeString.equals("0D"))
		{
			c.gridwidth = 2;
			c.gridx = 1;
			c.gridy = 1;
			theLabel = new JLabel("NOTE: wireless card in base station must be changed to Gold version for 128-bit encryption to function!");
			theLayout.setConstraints(theLabel, c);
			this.add(theLabel);
		}
		
		
		c.gridwidth = 1;
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(encryptionKey1Button, c);
		this.add(encryptionKey1Button);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(encryptionKey1Field, c);
		this.add(encryptionKey1Field);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(encryptionKey2Button, c);
		this.add(encryptionKey2Button);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(encryptionKey2Field, c);
		this.add(encryptionKey2Field);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(encryptionKey3Button, c);
		this.add(encryptionKey3Button);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 4;
		theLayout.setConstraints(encryptionKey3Field, c);
		this.add(encryptionKey3Field);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 5;
		theLayout.setConstraints(encryptionKey4Button, c);
		this.add(encryptionKey4Button);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 5;
		theLayout.setConstraints(encryptionKey4Field, c);
		this.add(encryptionKey4Field);
		
		c.gridwidth = 2;
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.gridy = 6;
		theLayout.setConstraints(allowUnencryptedDataBox, c);
		this.add(allowUnencryptedDataBox);
		
		
		
		
		
	}
	
	
	
	
	
}