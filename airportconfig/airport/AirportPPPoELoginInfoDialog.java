/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;




/**
*	Frame which presents data pertaining to modem login info.
*/

public class AirportPPPoELoginInfoDialog extends JDialog
								implements ActionListener
{
	
	private AirportUsernamePPPoEClientIDPanel loginPanel;
	private JButton okButton, cancelButton;
	private JTextArea messagesArea;
	
	
	public AirportPPPoELoginInfoDialog(Frame owner, String title, boolean modal, AirportInfo theInfo)
	{
		
		super(owner, title, modal); 
		
		// close only on Cancel or OK button press
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
          
        // panel holding login info
		loginPanel = new AirportUsernamePPPoEClientIDPanel(theInfo);
		 
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		
		okButton = new JButton("OK");
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(this);
		
		messagesArea = new JTextArea(4,60);
		JScrollPane messagesScroll = new JScrollPane(messagesArea);
		
		
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.getContentPane().setLayout(theLayout);
		
		// add components
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(loginPanel, c);
		this.getContentPane().add(loginPanel);
		
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(okButton, c);
		this.getContentPane().add(okButton);
		
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(cancelButton, c);
		this.getContentPane().add(cancelButton);
		
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 3;
		JLabel messageLabel = new JLabel("Messages");
		theLayout.setConstraints(messageLabel, c);
		this.getContentPane().add(messageLabel);
		
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(messagesScroll, c);
		this.getContentPane().add(messagesScroll);
		
	}
	
	
	
	
	public void actionPerformed(ActionEvent theEvent)
	// respond to button pushes
	{
		String command = theEvent.getActionCommand();
		
	
		if (command.equals("ok"))
		{
			try
			{
				loginPanel.writeValue();
				this.setVisible(false);
				this.dispose();
			}
			catch (ValueFormatException ve)
			{
				messagesArea.append("Problem with supplied value: " + ve.getMessage() + "\n");
			}
			
			
		}
		
		
		if (command.equals("cancel"))
		{
			this.setVisible(false);
			this.dispose();
		}
		
	}
		
	
	
	
}