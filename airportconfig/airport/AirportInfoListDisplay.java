/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.util.*;
import java.net.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;



/**
*	A utility class that displays all of the information in the associated AirportInfo
*	object. Creates an AirportInfoLabelledTextField (labelled text box) for each piece of 
*	information, and displays these in a scrollable pane.
*/

public class AirportInfoListDisplay extends JPanel
									implements Scrollable
{
	
	
		
	/**
	*	Create the display for the information contained in airportInfo.
	*/
	
	public AirportInfoListDisplay(AirportInfo airportInfo)
	{
		setUpDisplay(airportInfo);
	}
	
	
	
	private void setUpDisplay(AirportInfo airportInfo)
	{
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		//this.setBackground(Color.white);
		//this.setOpaque(true);
		this.setLayout(theLayout);
		
		Enumeration keys = airportInfo.keys();
		int i = 1;
			
		while (keys.hasMoreElements())
		{
			String name = (String)keys.nextElement();
			
			//if (name.endsWith("?"))
			{
			
				AirportInfoRecord nextRecord = airportInfo.get(name);
				
				AirportInfoLabelledTextField theDisplay = new AirportInfoLabelledTextField(name, nextRecord);
				
				c.gridx = 1;
				c.gridy = i;
				theLayout.setConstraints(theDisplay, c);
				this.add(theDisplay);
				
				i++;
			}
		}
		
		
	}
	
	
	
	public Dimension getPreferredScrollableViewportSize()
	{
	 	Dimension d;
	 	//d = getPreferredSize();
	 	
	 	d = new Dimension(600,200);
	 	
	 	//System.out.println("preferred size returned: " + d.getHeight() + ", " + d.getWidth());
		
		return d;
	 	
	}
	 
	 
	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction)
	// Components that display logical rows or columns should compute the scroll increment that 
	// will completely expose one block of rows or columns, depending on the value of orientation.
	{
		return 10;
	}
	
	
	public boolean getScrollableTracksViewportHeight() 
	// Return true if a viewport should always force the height of this Scrollable to 
	// match the height of the viewport.
	{
		return false;
	}


	public boolean getScrollableTracksViewportWidth()
	// Return true if a viewport should always force the width of this Scrollable to 
	// match the width of the viewport.
	{
		return false;
	}
	
	
	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) 
	// Components that display logical rows or columns should compute the scroll increment 
	// that will completely expose one new row or column, depending on the value of orientation.
	{
		return 10;
	}
	

}