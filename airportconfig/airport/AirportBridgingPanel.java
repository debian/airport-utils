/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.awt.*;
import javax.swing.*;



/**
*	Panel which maintains data relevant to bridging (plus address allocation via DHCP and NAT).
*/

public class AirportBridgingPanel extends AirportInfoPanel
{
	private AirportInfoRadioButton transparentBridgeButton, natButton;
	private ButtonGroup radioButtonGroup;
	private AirportInfoPanel natInfoPanel;
	private AirportInfoCheckBox natOnEthernetCheckbox, disableBridgingCheckbox;
	private AirportInfoLabelledTextField natInboundPrivateAddressBase, natInboundPrivateAddressMask;
	private AirportInfoTextField natOutboundPrivateAddressBase, natOutboundPrivateAddressMask;
	
	private AirportInfo theInfo;
	
	
	/**
	*	Create new panel based on data referenced through the supplied AirportInfo object.
	*/
	
	public AirportBridgingPanel(AirportInfo theInfo)
	{
		
		this.theInfo = theInfo;
		
		/**
		*	Transparent bridge/NAT switch: 0x449
		*		00 = act as transparent bridge
		*		02 = NAT on, using specified range of IP addresses as LAN subnet
		*/
		
		transparentBridgeButton = new AirportInfoRadioButton("Act as transparent bridge (no NAT)");
		transparentBridgeButton.addInfoRecord(theInfo.get("NAT switch"), "00");
		// make sure bridging is enabled
		transparentBridgeButton.addInfoRecord(theInfo.get("Bridging switch"), "01");
		
		natButton = new AirportInfoRadioButton("Provide network address translation (NAT)");
		natButton.addInfoRecord(theInfo.get("NAT switch"), "02");
		
		radioButtonGroup = new ButtonGroup();
		radioButtonGroup.add(transparentBridgeButton);
		radioButtonGroup.add(natButton);
		
		natInboundPrivateAddressBase = new AirportInfoLabelledTextField("Private LAN base station address", theInfo.get("NAT inbound private IP address"));
		natInboundPrivateAddressMask = new AirportInfoLabelledTextField("Private LAN subnet mask", theInfo.get("NAT inbound private subnet mask"));
		
		// these two won't appear, but will instead be set from above two when value is written
		natOutboundPrivateAddressBase = new AirportInfoTextField(theInfo.get("NAT outbound private IP address"));
		natOutboundPrivateAddressMask = new AirportInfoTextField(theInfo.get("NAT outbound private subnet mask"));
		
		//*****natOnEthernetCheckbox = new AirportInfoCheckBox("Provide NAT on Ethernet port, too",theInfo.get("Ethernet DHCP switch"), "40", "00");
		disableBridgingCheckbox = new AirportInfoCheckBox("Disable bridging between Ethernet port and wireless LAN",theInfo.get("Bridging switch"), "00", "01");
		
		// create panel to hold NAT info to be disabled when in transparent bridge mode
		natInfoPanel = new AirportInfoPanel();
		
		
		setUpDisplay();
	}
	
	
	
	
	private void setUpDisplay()
	{
		//this.setOpaque(true);
		//this.setBackground(Color.white);
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		JLabel theLabel;
		
		
		// add stuff to NAT panel
		natInfoPanel.setLayout(theLayout);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(natInboundPrivateAddressBase, c);
		natInfoPanel.add(natInboundPrivateAddressBase);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(natInboundPrivateAddressMask, c);
		natInfoPanel.add(natInboundPrivateAddressMask);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(disableBridgingCheckbox, c);
		natInfoPanel.add(disableBridgingCheckbox);
		
		
		
		
		// add stuff to this
		
		this.setLayout(theLayout);
		
		
		c.anchor = GridBagConstraints.WEST;
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(transparentBridgeButton, c);
		this.add(transparentBridgeButton);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(natButton, c);
		this.add(natButton);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(natInfoPanel, c);
		this.add(natInfoPanel);
		
		// make natInfoPanel enabled status dependent on button status
		natButton.addItemListener(natInfoPanel);
		natInfoPanel.setEnabled(natButton.isSelected());
		
	}
	
	
	
	public void writeValue()
		throws ValueFormatException
	{
		
		super.writeValue();
		
		if (natInfoPanel.isEnabled())
		{
			// sanity-check the private IP address to make sure it's not 0.0.0.0,
			// to correct an issue with the RG-1000; write and read the value to
			// eliminate spaces, etc.
			natInboundPrivateAddressBase.writeValue();
			String privateIP = theInfo.get("NAT inbound private IP address").toString();
			if (privateIP.equals("0.0.0.0"))
			{
				// select text in panel
				natInboundPrivateAddressBase.theField.selectAll();
				natInboundPrivateAddressBase.theField.requestFocus();
				
				// throw exception
				throw new ValueFormatException("Private LAN IP address in Bridging panel can't be 0.0.0.0");
			}
			
			// write private address base and subnet mask into outgoing NAT fields;
			// incoming fields are attached to the info text fields
			natOutboundPrivateAddressBase.writeValue(natInboundPrivateAddressBase.getText());
			natOutboundPrivateAddressMask.writeValue(natInboundPrivateAddressMask.getText());
			
		}
		
	}
	
	
	
}