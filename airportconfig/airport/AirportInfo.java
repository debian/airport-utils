/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.util.*;
import byteblock.*;
import AirportBaseStationConfigurator;


/**
* 	Collects together information about the location of pieces of information 
* 	in the Airport 17k memory block, and creates appropriate AirportInfoRecords
* 	(with ByteBlockWindows into the underlying ByteBlock) to read and write 
*	the information.
*/

public class AirportInfo extends Hashtable
{
	
	public static final int MAX_NUM_MAC_ADDRESSES = 256;
	public static final int MAX_NUM_PORT_MAPS = 20;
	public static final int MAX_NUM_SNMP_ACCESS_ENTRIES = 5;
	public static final int MAX_NUM_LOGIN_CHARS = 127;
	
	public ByteBlock baseBlock;
	
	
	
	public AirportInfo(ByteBlock baseBlock)
	{
	
		this.baseBlock = baseBlock;
		
		int baseStartIndex;
		int numRows, numCols;
		int dataType;
		byte mask;
		String name;
		
	
		/*
		
		Have 17408 bytes = 17k of ROM memory, communicated as 68 256-byte blocks in 68 OID blocks 
		*/
		
		/**
		* .1.3.6.1.4.1.762.2.3.1.1.1	
		*/
		
		
		/**
		*	Trap community password: 0x056
		*/
		
		baseStartIndex = 0x056;
		numRows = 1;
		numCols = 16;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Trap community";
		
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**
		*	Read community password: 0x076
		*/
		
		baseStartIndex = 0x076;
		numRows = 1;
		numCols = 16;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Read community";
		
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**
		*	Read/write community password: 0x096 (length????)
		*/
		
		baseStartIndex = 0x096;
		numRows = 1;
		numCols = 16;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Read/write community";
		
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	Remaining community password count: 0x0B6
		*/
		
		baseStartIndex = 0x0B6;
		numRows = 1;
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Remaining community count";
		
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	Remaining community password: 0x0B7 (length????)
		*/
		
		baseStartIndex = 0x0B7;
		numRows = 1;
		numCols = 16;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Remaining community";
		
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		
		// .1.3.6.1.4.1.762.2.3.1.1.2	Lots o' stuff!
		
		
		/*
		*	
		*	PPPoE switch 1: 0x107
		*		00 = off
		*		04 = on
		*/
		
		baseStartIndex = 0x107;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE switch 1";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/*
		*	
		*	Ethernet/Modem switch 1: 0x10A
		*		62 = modem
		*		60 = Ethernet
		*/
		
		baseStartIndex = 0x10A;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Ethernet/Modem switch 1";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		/**
		*	Set configuration mode for base station:
		*	Base station configuration mode switch:  0x117, MASK 20
		*			00 = manual or modem station config
		*			20 = DHCP station config
		*		NOTE: masked field: DHCP station config = 20, distribute on Ethernet = 40.
		*/	
		
		baseStartIndex = 0x117;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Base station configuration mode switch";
		mask = (byte)0x20;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		/**
		*	Set delivery of addresses on Ethernet:
		*	Ethernet DHCP switch:  0x117, MASK 40
		*			00 = don't give on Ethernet
		*			40 = give on Ethernet	
		*		Note: transparent mode, always 00
		*		NOTE: masked field: DHCP station config = 20, distribute on Ethernet = 40
		*/
		
		baseStartIndex = 0x117;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Ethernet DHCP switch";
		mask = (byte)0x40;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		/**
		* 	Encryption flag field: mask so only single bit affected
		*		0x158, mask 0x08 (bit 4)
		*			00 = no encryption
		*			08 = use encryption
		*/
		
		baseStartIndex = 0x158;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Encryption flag 1";
		mask = (byte)0x08;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		/**
		* 	Microwave robustness flag flag:  
		*		0x158, mask 0x20 (bit 6) 
		*			00 = off
		*			20 = on
		*/
		
		baseStartIndex = 0x158;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Microwave robustness flag";
		mask = (byte)0x20;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		/**
		* 	RTS/CTS flag:  
		*		0x168, mask 0x20 (bit 6) 
		*			00 = off
		*			20 = on
		*/
		
		baseStartIndex = 0x168;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "RTS/CTS flag";
		mask = (byte)0x20;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		

		
		/**
		* 	Closed network flag:  
		*		0x168, mask 0x40 (bit 7) 
		*			00 = open
		*			40 = closed
		*/
		
		baseStartIndex = 0x168;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Closed network flag";
		mask = (byte)0x40;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		

		
		/**
		*	Deny unencrypted data flag:	
		*		0x168, mask 0x80 (bit 8)
		*			00 = allow all packets
		*			80 = allow only encrypted packets
		*/
		
		baseStartIndex = 0x168;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Deny unencrypted data flag";
		mask = (byte)0x80;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		/**
		*	Distance between access points, rate: 0x0169
		*		mask 0x0F (lower nibble)
		*
		*				   large  medium   small
		*		1 Mbps		00		01		02
		*		2 Mbps		04		05		06
		*		5.5 Mbps	na		09		0a
		*		11 Mbps		na		na		0e
		*/
		
		baseStartIndex = 0x169;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Distance and rate field";
		mask = (byte)0x0F;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		
		/**
		*	Select encryption key to use: 0x169
		*		mask 0xF0 (upper nibble)
		*			00 = use key 1
		*			40 = use key 2
		*			80 = use key 3
		*			C0 = use key 4
		*/
		
		baseStartIndex = 0x169;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Encryption key selector";
		mask = (byte)0xF0;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		/**
		*	Wireless channel: 0x178
		*/
		
		baseStartIndex = 0x178;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Wireless channel";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		
		/**
		*	Modem timeout, in 10-second chunks: 0x17A
		*/
		
		baseStartIndex = 0x17A;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Modem timeout";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	Dialing type: 0x17B
		*		original: 	0D = tone
		*					04 = pulse
		*
		*		new:	mask 0x08
		*				08 = tone
		*				00 = pulse
		*/
		
		baseStartIndex = 0x17B;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Dialing type (tone or pulse)";
		mask = (byte)0x08;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		/**
		*	Dialing type: 0x17B
		*		mask 0x02
		*		00 = auto dial on
		*		02 = auto dial off
		*/
		
		baseStartIndex = 0x17B;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Automatic dial";
		mask = (byte)0x02;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		
		/**	
		*	RTS Threshold: 0x1A8
		* 		max value 2347
		*		
		*/
		
		baseStartIndex = 0x1A8;
		numRows = 1;
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "RTS threshold";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**	
		*	Phone country code: 0x1BA
		* 
		*		US standard = 	32 32
		*		Singapore = 	34 37
		*		Switzerland = 	31 35
		*/
		
		baseStartIndex = 0x1BA;
		numRows = 1;
		numCols = 2;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Phone country code";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**
		*	Network name: 0x1C8
		*		continuation in columns 8 and 9 of subsequent rows
		*/
		
		baseStartIndex = 0x1C8;
		numRows = 16;	// assumed
		numCols = 2;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Network name";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	
		*	Modem stuff: starting at row 8, column 11
		*		continuation in columns 10 and 11 of subsequent rows
		*
		*	First three bytes:
		*		Primary phone number count
		*		Secondary phone number count
		*		Modem init string count
		*
		*	Modem initialization string next,
		*		continuation in columns 11 and 12 of subsequent rows
		*
		*	Phone numbers: starting immediately after init string
		*		continuation in columns 11 and 12 of subsequent rows
		*		uses BCD, with D for space/paren and E for dash
		*		secondary number follows immediately
		*
		*	Some subfields defined separately:
		*		country code (above)
		*		string counts for modem init, phone numbers
		*
		*	SO
		*		this field all of above; counts must then overwrite first
		*		three bytes
		*
		*/	
		
		baseStartIndex = 0x18A;
		numRows = 31;	// assumed
		numCols = 2;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Modem init/phone number block";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		/**
		*	Phone number lengths:
		*		primary: 0x18A;  secondary: 0x18B
		*	Modem init string length: 0x19A
		*/
		
		baseStartIndex = 0x18A;
		numRows = 1;
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Primary phone number length";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x18B;
		numRows = 1;
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Secondary phone number length";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x19A;
		numRows = 1;
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Modem init string length";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/** .1.3.6.1.4.1.762.2.3.1.1.3	Extension of 2, plus:
		*/
		
		/**	Encryption flag field: 0x2C8
		*		00 = no encryption
		*		01 = use encryption
		*/
		
		baseStartIndex = 0x2C8;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Encryption flag 3";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		
		
		
		/** .1.3.6.1.4.1.762.2.3.1.1.4
		*/
		
		
		/*
		*	
		*	PPPoE switch 2: 0x336
		*		00 = off
		*		03 = on
		*/
		
		baseStartIndex = 0x336;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE switch 2";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/*
		*	
		*	PPPoE switch 3: 0x337
		*		00 = off
		*		0a = on
		*/
		
		baseStartIndex = 0x337;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE switch 3";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/*
		*	
		*	PPPoE idle timeout, in 10-second units: 0x346
		*		0 = don't disconnect
		*
		*/
		
		baseStartIndex = 0x346;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "PPPoE idle timeout";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/*
		*	
		*	PPPoE connection maintenance: 0x347
		*		00 = PPPoE off
		*		04 = PPPoE on, auto connect on
		*		06 = PPPoE on, auto connect off
		*		40 = PPPoE on, always stay connected
		*
		*	Break into three overlapping fields, so value will be set correctly
		*		
		*/
		
		baseStartIndex = 0x347;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE switch 4";
		mask = (byte)0xFF;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		baseStartIndex = 0x347;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE auto connect";
		mask = (byte)0x06;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		baseStartIndex = 0x347;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE stay connected";
		mask = (byte)0x40;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		
		/*
		*	
		*	PPPoE switch 5: 0x356
		*		00 = off
		*		a0 = on
		*/
		
		baseStartIndex = 0x356;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE switch 5";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/*
		*	
		*	PPPoE switch 6: 0x357
		*		00 = off
		*		05 = on
		*/
		
		baseStartIndex = 0x357;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE switch 6";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		
		
		
		
		/**	
		*	Encryption keys: up to 4
		*		which key used determined by 0x169
		*		number of bytes: 0x374, 0x384, 0x394, 0x3A4
		*			00 for no encryption
		*			05 for 40-bit (64-bit) encryption
		*			0D for 128-bit encryption
		*		keys: 0x376, 0x386, 0x396, 0x3A6
		*/
		
		baseStartIndex = 0x374;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Encryption key 1 size";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x376;
		numRows = 1;	
		numCols = AirportBaseStationConfigurator.encryptionKeySize;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Encryption key 1";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0x384;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Encryption key 2 size";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x386;
		numRows = 1;	
		numCols = AirportBaseStationConfigurator.encryptionKeySize;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Encryption key 2";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0x394;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Encryption key 3 size";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x396;
		numRows = 1;	
		numCols = AirportBaseStationConfigurator.encryptionKeySize;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Encryption key 3";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0x3A4;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Encryption key 4 size";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x3A6;
		numRows = 1;	
		numCols = AirportBaseStationConfigurator.encryptionKeySize;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Encryption key 4";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		
		/*
		*	
		*	PPPoE switch 7: 0x43d
		*		mask 0x20
		*		00 = off
		*		20 = on
		*/
		
		baseStartIndex = 0x43d;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "PPPoE switch 7";
		mask = (byte)0x20;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		/*
		*	syslog host facility(0 - 8): 0x0440
		*	
		*/
		
		baseStartIndex = 0x440;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Syslog host facility";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/** .1.3.6.1.4.1.762.2.3.1.1.5
		*/
		
		/**
		*	Bridging switch: 0x448
		*		mask 0x01
		*		01 = bridging on
		*		00 = bridging off
		*/
		
		baseStartIndex = 0x448;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Bridging switch";
		mask = (byte)0x01;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		/**
		*	Access control switch: 0x448  
		*		mask 0x80
		*		00 = no access control
		*		80 = access control used
		*/
		
		baseStartIndex = 0x448;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Access control switch";
		mask = (byte)0x80;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		/**
		*	DHCP switch: 0x449
		*		mask 0x80
		*		00 = no DHCP service
		*		80 = DHCP on, using specified range of IP addresses 
		*/
		
		baseStartIndex = 0x449;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "DHCP switch";
		mask = (byte)0x80;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		/**
		*	NAT switch: 0x449
		*		mask 0x02
		*		00 = NAT off
		*		02 = NAT on
		*/
		
		baseStartIndex = 0x449;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "NAT switch";
		mask = (byte)0x02;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		/**
		*	Port mapping switch: 0x449
		*		mask = 0x04
		*		00 = off
		*		04 = on 
		*/
		
		baseStartIndex = 0x449;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Port mapping switch";
		mask = (byte)0x04;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		
		/*
		*	Configuration interface: 0x460; seems to be configuration interface
		*		03 = modem
		*		00 = Ethernet
		*		01 = PPPoE		
		*/
		
		baseStartIndex = 0x460;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Configuration interface";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/*
		*	Watchdog reboot timer switch: 0x0462
		*		01 = on
		*		00 = off		
		*/
		
		baseStartIndex = 0x0462;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Watchdog reboot timer switch";
		mask = (byte)0x01;
		this.put(name, new AirportInfoRecord(dataType, new ByteBlockMaskedWindow(baseStartIndex, numRows, numCols, baseBlock, mask)));
		
		
		                 
		
		/**
		*	Base station IP address: 0x46A
		*/
		
		baseStartIndex = 0x46A;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Base station IP address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		/**
		*	Default TTL, for use with NAT(?): 0x46E
		*/
		
		baseStartIndex = 0x46E;
		numRows = 1;	
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "Default TTL";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		
		
		/**
		*	Router IP address and mask: 0x470, 0x474
		*/
		
		baseStartIndex = 0x470;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Router IP address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0x474;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Subnet mask";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
	
		/**
		*	0x0478:  syslog IP address
		*	0x047C:  trap host IP address
		*	
		*/
		
		baseStartIndex = 0x478;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Syslog host IP address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
	
		baseStartIndex = 0x47C;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Trap host IP address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**
		*	Names of base station, contact person
		*		Contact person name: 0x48C
		*		Base station name: 0x4CC
		*/
		
		baseStartIndex = 0x48C;
		numRows = 1;	
		numCols = 64;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Contact person name";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0x4CC;
		numRows = 1;	
		numCols = 64;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Base station name";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
			
		
		/**
		* .1.3.6.1.4.1.762.2.3.1.1.6	
		*/
		
		/**
		*	Base station location: 0x50C
		*/
		
		baseStartIndex = 0x50C;
		numRows = 1;	
		numCols = 64;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Base station location";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	DHCP lease time: 0x5B4
		*/
		
		baseStartIndex = 0x5B4;
		numRows = 1;	
		numCols =2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "DHCP lease time";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
	
	
		
		/**
		*	DHCP client ID: 0x5B6
		*/
		
		baseStartIndex = 0x5B6;
		numRows = 1;	
		numCols =32;	// guess
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "DHCP client ID";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		
		/**
		*		
		*	SNMP access control list: require multi-block windows
		*
		*	0x0636:  number of entries (0-5)
		*	
		*	0x0638, 0x063a, 0x063c, 0x063e, 0x0640:  first two octets of IP addresses
		*	0x0642, 0x0644, 0x0646, 0x0648, 0x064a:  second two octets of IP addresses
		*	
		*	0x064c, 0x064e, 0x0650, 0x0652, 0x0654:  first two octets of masks
		*	0x0656, 0x0658, 0x065a, 0x065c, 0x065e:  second two octets of masks
		*	
		*	0x0660, 0x0662, 0x0664, 0x0666, 0x0668:   interfaces: 2-byte little-endian (any = ff ff)
		*/
		
		baseStartIndex = 0x0636;
		numRows = 1;
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "SNMP access control list length";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		// IP addresses
		
		ByteBlockMultiWindow ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x638;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x642;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access host 1";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x63a;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x644;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access host 2";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x63c;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x646;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access host 3";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x63e;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x648;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access host 4";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x640;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x64a;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access host 5";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		// masks
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x064c;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x656;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access mask 1";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x64e;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x658;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access mask 2";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x650;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x65a;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access mask 3";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x652;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x65c;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access mask 4";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		ipWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0x654;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0x65e;
		numRows = 1;	
		numCols = 2;
		ipWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "SNMP access mask 5";
		this.put(name, new AirportInfoRecord(dataType, ipWindow));
		
		
		// 0x0660, 0x0662, 0x0664, 0x0666, 0x0668:   interfaces: 2-byte little-endian (any = ff ff)
		
		baseStartIndex = 0x0660;
		numRows = 1;
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "SNMP access interface 1";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x0662;
		numRows = 1;
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "SNMP access interface 2";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x0664;
		numRows = 1;
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "SNMP access interface 3";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x0666;
		numRows = 1;
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "SNMP access interface 4";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x0668;
		numRows = 1;
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "SNMP access interface 5";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		* .1.3.6.1.4.1.762.2.3.1.1.13	
		*/
		
		/**
		*	DHCP address range to serve: 
		*		starting address: 0xCF2
		*		ending address: 0xCF6
		*/
		
		baseStartIndex = 0xCF2;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "DHCP address range start";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xCF6;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "DHCP address range end";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**
		*	DNS servers: require multi-block windows
		*
		* .1.3.6.1.4.1.762.2.3.1.1.13	
		*	First two octets of primary and secondary DNS server
		*		primary: 0xCFA
		*		secondary: 0xCFC
		*
		* .1.3.6.1.4.1.762.2.3.1.1.14	
		*	Second two octets of primary and secondary DNS server
		*		primary: 0xD00
		*		secondary: 0xD02
		*/
		
		ByteBlockMultiWindow primaryWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0xCFA;
		numRows = 1;	
		numCols = 2;
		primaryWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0xD00;
		numRows = 1;	
		numCols = 2;
		primaryWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Primary DNS server";
		this.put(name, new AirportInfoRecord(dataType, primaryWindow));
		
		
		ByteBlockMultiWindow secondaryWindow = new ByteBlockMultiWindow();
		
		baseStartIndex = 0xCFC;
		numRows = 1;	
		numCols = 2;
		secondaryWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		baseStartIndex = 0xD02;
		numRows = 1;	
		numCols = 2;
		secondaryWindow.addWindow(new ByteBlockRectangularWindow(baseStartIndex, numRows, numCols, baseBlock));
		
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "Secondary DNS server";
		this.put(name, new AirportInfoRecord(dataType, secondaryWindow));
		
	
		
		/**
		* .1.3.6.1.4.1.762.2.3.1.1.13	
		*/
		
		
		/**
		*	DHCP lease time: 0xD06
		*		4-byte unsigned integer giving lease time in seconds
		*		Note that it's NOT little-endian
		*/
		
		baseStartIndex = 0xD06;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "DHCP lease time";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		/**
		*	Domain name (from DNS setting window): 0xD0A
		*/
		
		baseStartIndex = 0xD0A;
		numRows = 1;	
		numCols = 32;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Domain name";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**	
		*	Wireless subnet router IP address, when NAT enabled; 00's else: 0xD2A
		*	Wireless subnet mask, when NAT enabled; FF's else: 0xD2E
		*/
		
		
		baseStartIndex = 0xD2A;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "DHCP server router address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xD2E;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "DHCP server subnet mask";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0xD4A;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "NAT outbound public IP address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xD4E;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "NAT outbound public subnet mask";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0xD52;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "NAT outbound private IP address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xD56;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "NAT outbound private subnet mask";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0xD66;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "NAT inbound public subnet mask";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		baseStartIndex = 0xD6A;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "NAT inbound private IP address";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xD6E;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.IP_ADDRESS;
		name = "NAT inbound private subnet mask";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		
		
		
		
		/*	
		*	Port mapping functions: 
		*		count of port mappings: 0xD62
		*		public port numbers: 0xDC2, in 2-byte pairs
		*		first two octets of private IP addresses: 0xDEA,
		*			 in 2-byte pairs
		*/
		
		baseStartIndex = 0xD62;
		numRows = 1;	
		numCols = 2;
		dataType = AirportInfoRecord.LITTLE_ENDIAN_UNSIGNED_INTEGER;
		name = "Count of port mappings";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xD72;
		numRows = 1;	
		numCols = 2 * MAX_NUM_PORT_MAPS;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Public IP addresses, first two octets";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		baseStartIndex = 0xD9A;
		numRows = 1;	
		numCols = 2 * MAX_NUM_PORT_MAPS;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Public IP addresses, last two octets";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xDC2;
		numRows = 1;	
		numCols = 2 * MAX_NUM_PORT_MAPS;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Public port numbers";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xDEA;
		numRows = 1;	
		numCols = 2 * MAX_NUM_PORT_MAPS;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Private IP addresses, first two octets";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**
		* .1.3.6.1.4.1.762.2.3.1.1.15
		*/
		
		/**
		*	Port mapping functions: 
		*		last two octets of private IP addresses: 0xE12,
		*			 in 2-byte pairs
		*		private port numbers: 0xE3A, in 2-byte pairs
		*/
		
		baseStartIndex = 0xE12;
		numRows = 1;	
		numCols = 2 * MAX_NUM_PORT_MAPS;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Private IP addresses, last two octets";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		baseStartIndex = 0xE3A;
		numRows = 1;	
		numCols = 2 * MAX_NUM_PORT_MAPS;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Private port numbers";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		
		/**	
		*.1.3.6.1.4.1.762.2.3.1.1.16	
		*/
		
		/**
		* 	Login string to transmit, if selected: 0xF08
		*	
		*/
		
		baseStartIndex = 0xF08;
		numRows = 1;	
		numCols = MAX_NUM_LOGIN_CHARS;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Login information string";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
	
		
		
		/**
		*	The following are dealt with directly in the Access Control panel
		*
		*
		*	Mac addresses for access control
		*		count: byte 8*16 + 9 (maybe 8, with 2 digits)
		*		addresses: byte 8*16 + 11, in 6-byte blocks
		*	 		continue on from here.
		*	NOTE!! Apple claims can have up to 497 MAC addresses; this would require 497*6 = 2982 bytes = 11.65 (16x16) blocks
		*	In fact:
		*		Mac addresses start at byte 15*256 + 8*16 + 11 = 3979
		*		Names start at byte 28*256 + 12*16 + 9 = 7369
		*		Difference is 3390 = 565 6-byte addresses (or 13.24 blocks)
		*
		
		baseStartIndex = 0xF87;
		numRows = 1;	
		numCols = 2;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "Access control MAC address count";
		AirportInfoRecord macCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		this.put(name, macCountRecord);
	
		
		// Read in number of existing Mac addresses
		String countString = macCountRecord.toString();
		int numMacAddresses = 0;
		try
		{
			numMacAddresses = Integer.parseInt(countString);
		}
		catch(NumberFormatException e)
		{
			System.out.println("Problem with number of Mac addresses");
		}
		
		baseStartIndex = 0xF8A;
		numRows = 1;	
		numCols = 4;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Access control MAC address";
		
		for (int i = 0; i < numMacAddresses; i++)
		{
			String nameString = name + i;
			this.put(nameString, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
			baseStartIndex += 6;
		}
		
		
		
		**
		* .1.3.6.1.4.1.762.2.3.1.1.29	
		*
		
		**
		*	Host names for access control: byte 12*16 + 9
		*		Start here and continue over subsequent blocks...
		*		20 bytes for each (including null terminator, I think...)
		*	using Apple's stated 497 Mac address limit, gives 497*20 = 9940 bytes;
		*		= 38.83 blocks, putting us near the end;
		*	In fact:
		*		Names start at byte 28*256 + 12*16 + 9 = 7369
		*		Adding 9940 gives byte 17309 = 67*256 + 9*16 + 13
		*		Checksum seemingly starts at byte 11*16 + 6 of block 68, or at byte 17334 - or does it? maybe it's longer....the entry at position 2 is nonzero....that gives byte 17330, which is perfect if can have 498 MAC addresses....so maybe it's the whole 11th row that's the checksum stuff...
		*
		
		baseStartIndex = 0x1CC8;
		numRows = 1;	
		numCols = 20;
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "Access control host name";
		for (int i = 0; i < numMacAddresses; i++)
		{
			String nameString = name + i;
			this.put(nameString, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
			baseStartIndex += 20;
		}
		*/
		
		
		/**
		*	.1.3.6.1.4.1.762.2.3.1.1.63	
		*/
		
		/**
		*	Username@domain, password
		*				0x3EAA: Character count of dial-up username@domain, username@domain, 
		*						character count of dial-up password, password.
		*						Followed by 00 00 00 00, character count of PPPoE Client ID, 
		*						and then PPPoE Client ID, if PPPoE being used
		*	Treat as single block; parsing done in display panel!
		*/
		
		
		baseStartIndex = 0x3EAA;
		numRows = 1;	
		numCols = 128;	// guess
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Dial-up username/password/PPPoE client ID";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		/**
		*	PPPoE Client ID
		*				0x3ED2: character count
		*				0x3ED3: characters
		*/
		
		
		baseStartIndex = 0x3ED2;
		numRows = 1;	
		numCols = 1;	
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		name = "PPPoE Client ID character count";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x3ED3;
		numRows = 1;	
		numCols = 128;	// guess!
		dataType = AirportInfoRecord.CHAR_STRING;
		name = "PPPoE Client ID";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		* .1.3.6.1.4.1.762.2.3.1.1.68
		*/
		
		
		/**
		*	Unknown fields, but sometimes aren't set...: 0x43b1, 0x43b3
		*		value always 01 in working config files
		*/
		
		baseStartIndex = 0x43b1;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Miscellaneous switch 1";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		baseStartIndex = 0x43b3;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Miscellaneous switch 2";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	Login string approach: 0x43b5
		*		01 = none
		*		02 = ?
		*		03 = send username, password
		*		04 = send username, password, ppp
		*		05 = send username, password, ppp, with waits for server info
		*/
		
		baseStartIndex = 0x43b5;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.BYTE;
		name = "Login string switch";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
		
		/**
		*	Checksum:
		*/
		
		baseStartIndex = 0x43B6;
		numRows = 1;	
		numCols = 2;
		dataType = AirportInfoRecord.BYTE_STRING;
		name = "Checksum";
		this.put(name, new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock));
		
		
	}
	
	
	
	
	public AirportInfoRecord get(String name)
	{
		//System.out.println("Get info record " + name);
		return (AirportInfoRecord)super.get(name);
	}
	
	
	
	
	
}