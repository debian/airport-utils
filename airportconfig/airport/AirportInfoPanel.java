/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import byteblock.*;


/**
*	Superclass for all panels displaying info with AirportInfoComponents. When
*	enabled, has all AirportInfoComponents write their values; if not enabled,
*	component values not written.
*/

public class AirportInfoPanel extends JPanel
							implements AirportInfoComponent,
										ItemListener
{
	
	
	/**
	*	If panel enabled, instruct all contained AirportInfoComponents to write 
	*	their values; if not enabled, no writing.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		if (this.isEnabled())
		{
			Component[] components = this.getComponents();
			
			
			for (int i = 0; i < components.length; i++)
				if (components[i] instanceof AirportInfoComponent)
				{
					try
					{
						((AirportInfoComponent)components[i]).writeValue();
					}
					catch (ValueFormatException e)
					{
						// set focus to offending component (if it's not itself a container)
						if ( !(components[i] instanceof Container))
							components[i].requestFocus();
							
						throw e;	// so can display this panel
					}
				}
			
			
		}
		
	}
	
	
	
	/**
	*	Instruct all contained AirportInfoComponents to read their values from 
	*	their associated AirportInfoRecords, and update their displays accordingly.
	*/
	
	public void refreshDisplay()
	{
		Component[] components = this.getComponents();
		
		for (int i = 0; i < components.length; i++)
			if (components[i] instanceof AirportInfoComponent)
				((AirportInfoComponent)components[i]).refreshDisplay();
	}
	
	
	
	
	
	/**
	*	Used to trigger this panel to be enabled or disabled, by registering it
	*	as a ChangeListener with an AbstractButton instance. When disabled, 
	*	contained components won't write their values to underlying ByteBlock.
	*/
	
	public void itemStateChanged(ItemEvent e)
	{
		if (e.getStateChange() == ItemEvent.SELECTED)
			this.setEnabled(true);
		else
			this.setEnabled(false);
	}
	
	
	
	/**
	*	Enable and disable contained components.
	*/
	
	public void setEnabled(boolean enabled)
	{
		Component[] components = this.getComponents();
		
		super.setEnabled(enabled);
		
		for (int i = 0; i < components.length; i++)
			components[i].setEnabled(enabled);
		
	}
		
}