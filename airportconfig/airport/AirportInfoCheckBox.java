/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import byteblock.*;



/**
*	Checkbox displaying and setting specific piece of Airport info. Provides facilities
*	for reading from / writing to multiple AirportInfoRecords, and specifying the value(s) 
*	for each corresponding to the selected and unselected states.
*/

public class AirportInfoCheckBox extends JCheckBox
								implements AirportInfoComponent
{
	private Vector theRecords;
	
	
	private class RecordSettings
	{
		public AirportInfoRecord theRecord;
		public String selectedValue;
		public String unselectedValue;
		
		public void writeValue(boolean selected)
			throws ValueFormatException
		{
			if (selected)
			{
				theRecord.setBytesFromString(selectedValue);
			}
			else
			{
				theRecord.setBytesFromString(unselectedValue);
			}
		}
	}
	
	
	
	/**
	*	Create new checkbox with given label and no associated AirportInfoRecords.
	*/
	
	public AirportInfoCheckBox(String label)
	{
		super(label);
		theRecords = new Vector();
		
		//redisplay done automatically when records added
	}
	
	
	
	/**
	*	Create new checkbox with given label and supplied AirportInfoRecord, with selectedValue
	*	and unselectedValue strings stored for subsequent writing (as appropriate). State of
	*	checkbox set according to value in underlying byte block.
	*/
	
	public AirportInfoCheckBox(String label, AirportInfoRecord theRecord, String selectedValue, String unselectedValue)
	{
		super(label);
		theRecords = new Vector();
		addInfoRecord(theRecord, selectedValue, unselectedValue);
		//redisplay done automatically by addInfoRecord
	}
	
	

	
	/**
	*	Add additional AirportInfoRecord to list of byte block fields, with selectedValue
	*	and unselectedValue strings stored for subsequent writing (as appropriate). State of
	*	checkbox set according to value in all underlying byte blocks - if inconsistent,
	*	set to unselected.
	*/
	
	public void addInfoRecord(AirportInfoRecord theRecord, String selectedValue, String unselectedValue)
	{
		RecordSettings recordSettings = new RecordSettings();
		recordSettings.theRecord = theRecord;
		recordSettings.selectedValue = selectedValue;
		recordSettings.unselectedValue = unselectedValue;
		
		theRecords.insertElementAt(recordSettings, theRecords.size());
		refreshDisplay();
		
	}	
	
	
	
	
	public void refreshDisplay()
	{
		boolean selected = true;
		
		Enumeration elements = theRecords.elements();
		
		
		while (elements.hasMoreElements())
		{
			RecordSettings nextSettings = (RecordSettings)elements.nextElement();
			String currentValue = nextSettings.theRecord.toString();
			String selectedValue = nextSettings.selectedValue;
			
			// just byte fields; so convert to integers to compare
			try
			{
				if(Integer.parseInt(currentValue, 16)!=Integer.parseInt(selectedValue, 16))
				{
					selected = false;
					break;
				}
			}
			catch (NumberFormatException e)
			{
				selected = false;
			}
		}
		
		this.setSelected(selected);
		
	}
	
	
	
	
	/**
	*	Write values to all of associated AirportInfoRecords according to selected state
	*	of checkbox.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		boolean selected = this.isSelected();
		Enumeration elements = theRecords.elements();
		
		while (elements.hasMoreElements())
			((RecordSettings)elements.nextElement()).writeValue(selected);
	}
	
	
}