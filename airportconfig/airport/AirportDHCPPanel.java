/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.awt.*;
import javax.swing.*;



/**
*	Panel which maintains data relevant to bridging (plus address allocation via DHCP and NAT).
*/

public class AirportDHCPPanel extends AirportInfoPanel
{
	private AirportInfoCheckBox dhcpCheckbox, dhcpOnEthernetCheckbox;
	private AirportDHCPRangePanel dhcpRangePanel;
	private AirportInfoPanel dhcpInfoPanel;
	private AirportInfoLabelledScaledValueField dhcpLeaseTime;
	
	
	/**
	*	Create new panel based on data referenced through the supplied AirportInfo object.
	*/
	
	public AirportDHCPPanel(AirportInfo theInfo)
	{
		
		/**
		*	DHCP switch: 0x449
		*		00 = no DHCP service provided
		*		80 = DHCP on, using specified range of IP addresses 
		*/
		
		
		
		dhcpCheckbox = new AirportInfoCheckBox("Provide DHCP address delivery to wireless hosts",theInfo.get("DHCP switch"), "80", "00");
		
		dhcpRangePanel = new AirportDHCPRangePanel(theInfo);
		
		// create textfield for DHCP lease time
		dhcpLeaseTime = new AirportInfoLabelledScaledValueField("DHCP lease time (minutes)", theInfo.get("DHCP lease time"), 60);
		
		dhcpOnEthernetCheckbox = new AirportInfoCheckBox("Distribute addresses on Ethernet port, too",theInfo.get("Ethernet DHCP switch"), "40", "00");
		
		// create panel to hold info to be disabled when no DHCP service provided
		dhcpInfoPanel = new AirportInfoPanel();
		
		
		setUpDisplay();
	}
	
	
	
	
	private void setUpDisplay()
	{
		
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		JLabel theLabel;
		
		
		// add stuff to panel
		dhcpInfoPanel.setLayout(theLayout);
		
		c.anchor = GridBagConstraints.WEST;
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(dhcpRangePanel, c);
		dhcpInfoPanel.add(dhcpRangePanel);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(dhcpLeaseTime, c);
		dhcpInfoPanel.add(dhcpLeaseTime);
		
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(dhcpOnEthernetCheckbox, c);
		dhcpInfoPanel.add(dhcpOnEthernetCheckbox);
		
		
		
		// add stuff to this
		
		this.setLayout(theLayout);
		
		
		c.anchor = GridBagConstraints.WEST;
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(dhcpCheckbox, c);
		this.add(dhcpCheckbox);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(dhcpInfoPanel, c);
		this.add(dhcpInfoPanel);
		
		// make dhcpInfoPanel highlighting dependent on button setting
		dhcpCheckbox.addItemListener(dhcpInfoPanel);
		dhcpInfoPanel.setEnabled(dhcpCheckbox.isSelected());
		
	}
	
	
	
}