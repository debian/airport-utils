/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import byteblock.*;


/**
*	A ComboBox subclass for reading from and writing to an AirportInfoRecord. 
*/

public class AirportInfoComboBox extends AirportInfoPanel
								implements AirportInfoComponent
{
	private AirportInfoRecord theRecord;
	private JComboBox comboBox;
	
	/** 
	*	Associates ComboBox entry with value to be written; toString() method ensures
	*	that itemLabel is displayed in ComboBox list
	*/
	private class Association
	{
		public Association(String itemLabel, String valueString)
		{
			this.itemLabel = itemLabel;
			this.valueString = valueString;
		}
		
		String itemLabel;
		String valueString;
		
		public String toString()
		{
			return itemLabel;
		}
	}
	
	
	
	
	/** 
	*	Creates ComboBox with given label associated with specified AirportInfoRecord.
	*/
	
	public AirportInfoComboBox(String label, AirportInfoRecord theRecord)
	{
		JLabel theLabel = new JLabel(label);
		comboBox = new JComboBox();
		
		this.add("West", theLabel);
		this.add("East", comboBox);
		
		this.theRecord = theRecord;
		
		// display refreshed when items added
	}
	
	
	
	
	/** 
	*	Add item to ComboBox list, with specified item label and associated value string to
	*	be written if item is selected.
	*/
	
	public void addItemAndValue(String itemLabel, String valueString)
	{
		// create an association object, and add it to the combo box
		Association newItem = new Association(itemLabel, valueString);
		comboBox.addItem(newItem);
		refreshDisplay();
	}
	

	
	
	
	/** 
	*	Read current value in associated AirportInfoRecord, and set ComboBox to display
	*	corresponding item.
	*/
	
	public void refreshDisplay()
	{
		// determine which label is associated with the current value, and select it
		String currentValue = theRecord.toString();
					
		for (int i = 0; i < comboBox.getItemCount(); i++)
		{
			Association nextItem = (Association)(comboBox.getItemAt(i));
			
			if (nextItem.valueString.equalsIgnoreCase(currentValue))
			{
				comboBox.setSelectedIndex(i);
				break;
			}
		}
		
	}
	
	
	
	
	/** 
	*	Write the value corresponding to the currently selected item into the associated
	*	AirportInfoRecord.
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		Association theItem = (Association)(comboBox.getSelectedItem());
		theRecord.setBytesFromString(theItem.valueString);
	}
	
	
}