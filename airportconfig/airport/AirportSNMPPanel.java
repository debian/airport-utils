/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;




/**
*	Panel which maintains wireless LAN information: network name, channel, and encryption switch and key.
*	Note that encryption key is supplied as 10-digit hex string (with no "0x" prefix).
*/

public class AirportSNMPPanel extends AirportInfoPanel
{
	private AirportInfoLabelledTextField trapHostPasswordField, trapHostIPAddressField, syslogHostIPAddressField;
	private AirportInfoComboBox syslogHostFacilityBox;
	//private AirportSNMPAccessControlTable snmpAccessControlTable;
	
	public AirportSNMPPanel(AirportInfo theInfo)
	{
		
		/*
			0x0478:  syslog IP address
			0x0440:  syslog host facility(0 - 8)
			0x047C:  trap host IP address
			
			0x0056:  trap host password
			0x0076:  read password
		*/
		
		trapHostPasswordField = new AirportInfoLabelledTextField("Trap host password:", theInfo.get("Trap community"));
		trapHostIPAddressField = new AirportInfoLabelledTextField("Trap host IP address:", theInfo.get("Trap host IP address"));
		syslogHostIPAddressField = new AirportInfoLabelledTextField("Syslog host IP address:", theInfo.get("Syslog host IP address"));
		
		syslogHostFacilityBox = new AirportInfoComboBox("Syslog host facility", theInfo.get("Syslog host facility"));
		for (int i = 0; i < 9; i++)
			syslogHostFacilityBox.addItemAndValue(Integer.toString(i), Integer.toString(i));
		
		/**
		*		
		*	SNMP access control list: require multi-block windows
		*
		*	0x0636:  number of entries (0-5)
		*	
		*	0x0638, 0x063a, 0x063c, 0x063e, 0x0640:  first two octets of IP addresses
		*	0x0642, 0x0644, 0x0646, 0x0648, 0x064a:  second two octets of IP addresses
		*	
		*	0x064c, 0x064e, 0x0650, 0x0652, 0x0654:  first two octets of masks
		*	0x0656, 0x0658, 0x065a, 0x065c, 0x065e:  second two octets of masks
		*	
		*	0x0660, 0x0662, 0x0664, 0x0666, 0x0668:   interfaces: 2-byte little-endian (any = ff ff)
		*/
		
		// Nuts! The base station doesn't restrict SNMP access....  :-(
		//snmpAccessControlTable = new AirportSNMPAccessControlTable(theInfo);
		
		setUpDisplay();
	}
	
	
	
	
	public void setUpDisplay()
	{
		// set params for layout manager
		GridBagLayout  theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		c.insets = new Insets(8,2,2,2);
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0;
		c.weighty = 0;
		
		
		this.setLayout(theLayout);
		
		
		JLabel theLabel;
		
		
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(trapHostIPAddressField, c);
		this.add(trapHostIPAddressField);
		
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(trapHostPasswordField, c);
		this.add(trapHostPasswordField);
		
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(syslogHostIPAddressField, c);
		this.add(syslogHostIPAddressField);
		
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(syslogHostFacilityBox, c);
		this.add(syslogHostFacilityBox);
		
		c.gridwidth = 2;
		
		/*
		c.gridx = 1;
		c.gridy = 3;
		theLabel = new JLabel("SNMP Access Control List");
		theLayout.setConstraints(theLabel, c);
		this.add(theLabel);
		
		c.gridx = 1;
		c.gridy = 4;
		theLayout.setConstraints(snmpAccessControlTable, c);
		this.add(snmpAccessControlTable);
		*/
		
		
		
	}
	
	
	
	
	
}