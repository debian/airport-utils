/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package airport;


import java.awt.*;
import javax.swing.*;
import byteblock.*;




/**
*	Utility subpanel which maintains dial-up username and password.
*/

public class AirportUsernamePPPoEClientIDPanel extends AirportInfoPanel
{
	
	private AirportInfo airportInfo;
	private JTextField usernameField, passwordField, pppoeClientIDField;
	
	
	public AirportUsernamePPPoEClientIDPanel(AirportInfo airportInfo)
	{
		super();
		this.airportInfo = airportInfo;
		setUpDisplay();
	}
	
	
	private void setUpDisplay()
	{
		
		usernameField = new JTextField(24);
		passwordField = new JTextField(24);
		pppoeClientIDField = new JTextField(24);
		JLabel usernameLabel = new JLabel("User name");
		JLabel passwordLabel = new JLabel("Password");
		JLabel pppoeClientIDLabel = new JLabel("PPPoE client ID");
		
		// set params for layout manager
		GridBagLayout theLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.NONE;
		c.ipadx = 0;
		c.ipady = 0;
		Insets theMargin = new Insets(2,2,2,2);
		c.insets = theMargin;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = .5;
		c.weighty = .5;
		
		
		this.setLayout(theLayout);
		
		
		// add stuff
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 1;
		theLayout.setConstraints(usernameLabel, c);
		this.add(usernameLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 1;
		theLayout.setConstraints(usernameField, c);
		this.add(usernameField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		theLayout.setConstraints(passwordLabel, c);
		this.add(passwordLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 2;
		theLayout.setConstraints(passwordField, c);
		this.add(passwordField);
		
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 3;
		theLayout.setConstraints(pppoeClientIDLabel, c);
		this.add(pppoeClientIDLabel);
		
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 2;
		c.gridy = 3;
		theLayout.setConstraints(pppoeClientIDField, c);
		this.add(pppoeClientIDField);
		
		
		refreshDisplay();
		
		
	}
	
	
	
	
	/**
	*	Method writes character count of dial-up username, username, character count of dial-up password, 
	*	and password, terminated by hex byte "01" (not included in either count) if strings not empty.
	*	Then followed by 4 nulls, then PPPoE client ID character count, then PPPoE client ID
	*/
	
	public void writeValue()
		throws ValueFormatException
	{
		
		int baseStartIndex, numRows, numCols, dataType;
		
		
		
		// erase entire block
		AirportInfoRecord usernamePasswordRecord = airportInfo.get("Dial-up username/password/PPPoE client ID");
		usernamePasswordRecord.clearWindow();
		
		//create new info records, since sizes of some fields may have changed
		
		ByteBlock baseBlock = airportInfo.baseBlock;
		
		String username = usernameField.getText();
		String password = passwordField.getText();
		String pppoeClientID = pppoeClientIDField.getText();
		
		/**
		*	.1.3.6.1.4.1.762.2.3.1.1.63	
		*
		*	Username@domain, password
		*		byte 10*16 + 11: character count of dial-up username, username, 
		*						 character count of dial-up password, password.
		*						terminated by hex byte "01" (not included in either count)
		*/
		
		baseStartIndex = 0x3EAA;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		AirportInfoRecord usernameCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		int usernameCount = username.length();
		String usernameCountString = new String();
		usernameCountString += usernameCount;
		usernameCountRecord.setBytesFromString(usernameCountString);
		
		
		baseStartIndex = 0x3EAA + 1;
		numRows = 1;	
		numCols = usernameCount + 1;	// annoying; to handle null-terminator-room check, unneeded here!
		dataType = AirportInfoRecord.CHAR_STRING;
		// name = "Dial-up username";
		AirportInfoRecord usernameRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		usernameRecord.setBytesFromString(username);
		
		
		baseStartIndex = 0x3EAA + 1 + usernameCount;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		// name = "Dial-up password character count";
		AirportInfoRecord passwordCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		int passwordCount = password.length();
		String passwordCountString = new String();
		passwordCountString += passwordCount;
		passwordCountRecord.setBytesFromString(passwordCountString);
		
		
		baseStartIndex = 0x3EAA + 1 + usernameCount + 1;
		numRows = 1;	
		numCols = passwordCount + 6;
		dataType = AirportInfoRecord.CHAR_STRING;
		// name = "Dial-up password";
		AirportInfoRecord passwordRecord =  new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		
		// terminate password with hex byte "01", then padding "00 00 00 00"
		
		char[] terminator = new char[5];
		terminator[0] = (char)0x01;
		password += new String(terminator);
		passwordRecord.setBytesFromString(password);
		
		
		baseStartIndex = 0x3EAA + 1 + usernameCount + 1 + passwordCount + 5;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		// name = "PPPoE client ID character count";
		AirportInfoRecord pppoeClientIDCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		int pppoeClientIDCount = pppoeClientID.length();
		String pppoeClientIDCountString = new String();
		pppoeClientIDCountString += pppoeClientIDCount;
		pppoeClientIDCountRecord.setBytesFromString(pppoeClientIDCountString);
		
		
		baseStartIndex = 0x3EAA + 1 + usernameCount + 1 + passwordCount + 5 + 1;
		numRows = 1;	
		numCols = pppoeClientIDCount + 1;
		dataType = AirportInfoRecord.CHAR_STRING;
		// name = "PPPoE client ID";
		AirportInfoRecord pppoeClientIDRecord =  new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		pppoeClientIDRecord.setBytesFromString(pppoeClientID);
		
		
	}
	
	
	
	
	/**
	*	Method reads string and parses into: character count of dial-up username, username, 
	*	character count of dial-up password, and password, terminated by hex byte "01" 
	*	(not included in either count).
	*/
	
	public void refreshDisplay()
	{
		
		int baseStartIndex, numRows, numCols, dataType;
	
		
		//create info records to use to read info for JTextFields
		
		ByteBlock baseBlock = airportInfo.baseBlock;
		
		
		/**
		*	.1.3.6.1.4.1.762.2.3.1.1.63	
		*
		*	Username@domain, password
		*		byte 10*16 + 11: character count of dial-up username, username, 
		*						 character count of dial-up password, password.
		*/
		
		baseStartIndex = 0x3EAA;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		AirportInfoRecord usernameCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		int usernameCount = Integer.parseInt(usernameCountRecord.toString());
		
		baseStartIndex = 0x3EAA + 1;
		numRows = 1;	
		numCols = usernameCount;
		dataType = AirportInfoRecord.CHAR_STRING;
		// name = "Dial-up username";
		AirportInfoRecord usernameRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		usernameField.setText(usernameRecord.toString());
		
		
		baseStartIndex = 0x3EAA + 1 + usernameCount;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		// name = "Dial-up password character count";
		AirportInfoRecord passwordCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		int passwordCount = Integer.parseInt(passwordCountRecord.toString());
		
		
		baseStartIndex = 0x3EAA + 1 + usernameCount + 1;
		numRows = 1;	
		numCols = passwordCount;
		dataType = AirportInfoRecord.CHAR_STRING;
		// name = "Dial-up password";
		AirportInfoRecord passwordRecord =  new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		passwordField.setText(passwordRecord.toString());
		
		
		baseStartIndex = 0x3EAA + 1 + usernameCount + 1 + passwordCount + 5;
		numRows = 1;	
		numCols = 1;
		dataType = AirportInfoRecord.UNSIGNED_INTEGER;
		// name = "Dial-up password character count";
		AirportInfoRecord pppoeClientIDCountRecord = new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		int pppoeClientIDCount = Integer.parseInt(pppoeClientIDCountRecord.toString());
		
		
		baseStartIndex = 0x3EAA + 1 + usernameCount + 1 + passwordCount + 5 + 1;
		numRows = 1;	
		numCols = pppoeClientIDCount;
		dataType = AirportInfoRecord.CHAR_STRING;
		// name = "Dial-up password";
		AirportInfoRecord pppoeClientIDRecord =  new AirportInfoRecord(baseStartIndex, numRows, numCols, dataType, baseBlock);
		pppoeClientIDField.setText(pppoeClientIDRecord.toString());
		
	}
	
	
	
	
	
	
		
}