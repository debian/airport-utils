/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package byteblock;


import java.util.*;

/**
*	Groups a list of byte block windows into a single logical window. Windows are added
*	to a window list; bytes are written and read sequentially from the windows in the list.
*/

public class ByteBlockMultiWindow extends ByteBlockWindow
{
	
	private Vector windowList;
	private int totalSize;
	
	
	/**
	* 	Create a new multi-window, with an initially empty window list.
	*/
	public ByteBlockMultiWindow()
	{
		windowList = new Vector();
		totalSize = 0;
	}
	
	
	
	/**
	* 	Add the specified window to the end of the window list.
	*/
	public void addWindow(ByteBlockWindow newWindow)
	{
		windowList.insertElementAt(newWindow, windowList.size());
		totalSize += newWindow.getSize();
	}
	
	
	/**
	*	Return the total number of bytes, which equals the sum of the sizes of the windows
	*	in the window list.
	*/
	public int getSize()
	{
		return totalSize;
	}
	
	
	
	
	/**
	* 	Return the sequence of bytes in the windows, in the order in which the windows
	* 	appear in the window list.
	*/
	public byte[] getBytes()
	{
		// create new array of correct size
		byte[] returnBytes = new byte[totalSize];
		int k = 0;
		// get bytes from windows in list, copy bytes into new array
		for (int i = 0; i < windowList.size(); i++)
		{
			byte[] nextBytes = ((ByteBlockWindow)windowList.elementAt(i)).getBytes();
			for (int j = 0; j < nextBytes.length; j++)
			{
				returnBytes[k] = nextBytes[j];
				k++;
			}
		}
		
		return returnBytes;
	}
	
	
	
	/**
	* 	Set the bytes in all of the windows in the window list to 0x00.
	*/
	public void clearBytes()
	{
		// clear windows in list
		for (int i = 0; i < windowList.size(); i++)
			((ByteBlockWindow)windowList.elementAt(i)).clearBytes();
	}
	
	
	
	
	/**
	* 	Write a sequence of bytes into the window, writing the bytes sequentially
	*	into the windows in the window list. Note that this automatically clears 
	*	the other bytes.
	*/
	public void writeBytes(byte[] byteString)
	{
		// write bytes sequentially into windows in list
		int k = 0;		// current array index
		for (int i = 0; i < windowList.size(); i++)
		{
			ByteBlockWindow nextWindow = (ByteBlockWindow)windowList.elementAt(i);
			int nextWindowSize = nextWindow.getSize();
			
			// see if have enough bytes left in list to write into window
			if (k + nextWindowSize > byteString.length)
				nextWindowSize = byteString.length - k;
			
			// create new array to write into window
			byte[] subString = new byte[nextWindowSize];
			
			// fill array
			for (int j = 0; j < nextWindowSize; j++)
			{
				subString[j] = byteString[k];
				k++;
			}
				
			nextWindow.writeBytes(subString);
			
			// if no more bytes in input, break
			if (k >= byteString.length)
				break;
		}
	}
	
	
	
	/**
	* 	Return the sequence of bytes in the windows in the window list, with spaces.
	*/
	public String toString()
	{
		String returnString = new String();
		
		for (int i = 0; i < windowList.size(); i++)
		{
			returnString += ((ByteBlockWindow)windowList.elementAt(i)).toString();
		}
		
		return returnString;
	}
	
	
	
	/**
	* 	Return the sequence of bytes in the windows in the window list, without spaces.
	*/
	public String toHexString()
	{
		String returnString = new String();
		
		for (int i = 0; i < windowList.size(); i++)
		{
			returnString += ((ByteBlockWindow)windowList.elementAt(i)).toHexString();
		}
		
		return returnString;
	}
	

	
}