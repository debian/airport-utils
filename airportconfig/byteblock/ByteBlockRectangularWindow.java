/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package byteblock;


/**
*	A rectangular window into a byte block. 
*/
public class ByteBlockRectangularWindow extends ByteBlockWindow
{
	
	protected int numRows, numCols;
	protected int baseStartIndex;
	
	
	
	
	/**
	* Create a window based on an existing base block, with the same number of rows and columns.
	*/
	public ByteBlockRectangularWindow(ByteBlock block)
	{
		
		this.numRows = block.numRows;
		this.numCols = block.numCols;
		this.baseStartIndex = 0;
		
		baseBlock = block;
		
	}
	
	
	
	/**
	* 	Create a window into the specified base block, with specified number of rows and columns,
	*	and starting at specified index of the base block. Note that the number of columns need 
	*	not be less than the number of columns in the base block, and a rectangular window can
	*	extend beyond the base block's boundary, "wrapping" into the other side.
	*/
	public ByteBlockRectangularWindow(int baseStartIndex, int numRows, int numCols, ByteBlock block)
	{
		
		this.numRows = numRows;
		this.numCols = numCols;
		this.baseStartIndex = baseStartIndex;
		baseBlock = block;
		
	}
	
	
	
	/*
	* Create a sub-window of an existing window. Note that the resulting window
	* depends on the number of rows and columns of the base block; in particular,
	* both numRows and numCols must be <= to those of base block.
	
	public ByteBlockRectangularWindow(int baseStartIndex, int numRows, int numCols, ByteBlockWindow parent)
	{
		
		this.numRows = numRows;
		this.numCols = numCols;
		this.baseStartIndex = parent.baseStartIndex + baseStartIndex;
		//this.startIndex = 0;
		//this.endIndex = numRows*numCols;
		
		baseBlock = parent.baseBlock;
		
	}
	*/
	
	
	/**
	*	Return the total number of bytes in the window (= numRows * numCols).
	*/
	public int getSize()
	{
		return numRows * numCols;
	}
	
	
	
	
	/**
	* 	Return the sequence of bytes in the window.
	*/
	public byte[] getBytes()
	{
		return getBytes(0, numRows*numCols);
	}
	
	
	
	
	
	/**
	* 	Return the sequence of bytes in the window between the specified indices.
	*/
	public byte[] getBytes(int startIndex, int endIndex)
	{
		
		// set endIndex so don't try to read outside of window delimiter
		if (endIndex > numRows*numCols)
			endIndex = numRows*numCols;
		
		byte[] byteString = new byte[endIndex - startIndex];
		
		// get starting row and column of this window in its base block
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			byteString[i-startIndex] = baseBlock.bytes[baseIndex];
		}
		
		return byteString;
	}
	
	
	
	/**
	* 	Set all bytes in window to 0x00.
	*/
	public void clearBytes()
	{
		clearBytes(0, numRows*numCols);
	}
	
	
	
	
	/**
	* 	Set all bytes in window from (and including) startByte to (and excluding)
	* 	endByte to 0x00.
	*/
	public void clearBytes(int startIndex, int endIndex)
	{
		// set endIndex so don't write outside of window delimiter
		if (endIndex > numRows*numCols)
			endIndex = numRows*numCols;
		
		// get starting row and column of this window in its base block
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			baseBlock.bytes[baseIndex] = 0;
		}
	}
	
	
	
	
	/**
	* 	Write the sequence of bytes supplied into the window, starting from the beginning. Note
	* 	that this automatically clears all the other bytes in the case in which the number of
	*	bytes supplied is less than the total number of bytes in the window.
	*/
	public void writeBytes(byte[] byteString)
	{
		writeBytes(0, byteString);
	}
	
	
	
	
	/**
	* 	Write a sequence of bytes into the window, starting at the specified index. Note
	* 	that this automatically clears all the other bytes in the window.
	*/
	public void writeBytes(int startIndex, byte[] byteString)
	{
		
		// clear all bytes first
		clearBytes();
		
		// set endIndex so don't write outside of window delimiter
		int endIndex = startIndex + byteString.length;
		if (endIndex > numRows*numCols)
			endIndex = numRows*numCols;
		
		// get starting row and column of this window in its base block
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			baseBlock.bytes[baseIndex] = byteString[i-startIndex];
		}
	}
	
	
	/**
	*	Return a String representing the bytes in the window, with spaces between pairs of hex digits.
	*/
	public String toString()
	{
		return toString(0, numRows*numCols);
	}
	
	
	
	
	/**
	*	Return a String representing the bytes in the window between the specified indices,
	*	with spaces between pairs of hex digits.
	*/
	public String toString(int startIndex, int endIndex)
	{
		
		String returnString = new String();
		
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			int value = baseBlock.bytes[baseIndex];
			if (value < 0)
				value += 256;
			returnString += getHex(value) + " ";
			
			if (col == numCols - 1)
				returnString += "\n";
		
		}
		
		return returnString;
	}
	
	
	
	
	/**
	*	Return a String representing the bytes in the window, without spaces between bytes.
	*/
	public String toHexString()
	{
		return toHexString(0, numRows*numCols);
	}
	
	
	
	
	/**
	*	Return a String representing the bytes in the window between the specified indices,
	*	without spaces between bytes.
	*/
	public String toHexString(int startIndex, int endIndex)
	{
		
		String returnString = new String();
		
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			int value = baseBlock.bytes[baseIndex];
			if (value < 0)
				value += 256;
			returnString += getHex(value);
			
		}
		
		return returnString;
	}
	
	
	
}