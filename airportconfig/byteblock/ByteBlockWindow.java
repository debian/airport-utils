/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package byteblock;


/**
*	Abstract base class for "windows" into byte blocks, which are used to
*	read and write data of various types.
*/
public abstract class ByteBlockWindow
{
	protected ByteBlock baseBlock;
	
	/**
	* Return the total number of bytes in the window.
	*/
	public abstract int getSize();
	
	
	
	/**
	* Return the sequence of bytes in the window.
	*/
	public abstract byte[] getBytes();
	
	
	/**
	* Set all bytes in window to 0x00.
	*/
	public abstract void clearBytes();
	
	
	/**
	* Write a sequence of bytes into the window. Note that this 
	* automatically clears the other bytes.
	*/
	public abstract void writeBytes(byte[] byteString);
	
	/**
	*	Return a String representation of the data in the window,
	*	as a string of hexadecimal pairs, separated by spaces.
	*/
	public abstract String toString();
	
	
	/**
	*	Return a String representation of the data in the window, 
	*	as a string of hexadecimal pairs, without spaces.
	*/
	public abstract String toHexString();

	
		
	
	/**
	*	Return a hexadecimal pair representation of a byte.
	*/
	protected String getHex(int b)
	{
		String returnString = new String(Integer.toHexString(b));
		
		// add leading 0 if needed
		if (returnString.length()%2 == 1)
			returnString = "0" + returnString;
			
		return returnString;
	}

	
	
	
}