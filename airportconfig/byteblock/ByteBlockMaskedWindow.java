/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package byteblock;


/**
*	Extends ByteBlockRectangularWindow to include a mask specifying only those bits in each byte
*	which will be read or written (corresponding to the positions of 1's in the mask byte).
*/
public class ByteBlockMaskedWindow extends ByteBlockRectangularWindow
{
	
	private byte mask;
	
	/**
	* 	Create a window for the specified ByteBlock which uses the specified mask.
	*/
	public ByteBlockMaskedWindow(ByteBlock block, byte mask)
	{
		super(block);
		this.mask = mask;
	}
	
	
	
	/**
	* 	Create a rectangular window into the specified ByteBlock using the specified mask.
	*	The resulting window will have the specified number of
	*	rows and columns, and will start at the specified index of the base block.
	*/
	public ByteBlockMaskedWindow(int baseStartIndex, int numRows, int numCols, ByteBlock block, byte mask)
	{
		super(baseStartIndex, numRows, numCols, block);
		this.mask = mask;
	}
	
	
	
	/*
	* 	Create a sub-window of an existing window. Note that the resulting window
	* 	depends on the number of rows and columns of the base block; in particular,
	* both numRows and numCols must be <= to those of base block.
	
	public MaskedByteBlockWindow(int numRows, int numCols, int baseStartIndex, ByteBlockRectangularWindow parent, byte mask)
	{
		super(numRows, numCols, baseStartIndex, parent);
		this.mask = mask;
	}
	*/
	
	
	
	
	/**
	* 	Return the sequence of bytes in the window between the specified indices, masking
	* 	each with this instance's byte mask.
	*/
	public byte[] getBytes(int startIndex, int endIndex)
	{
		
		byte[] byteString = super.getBytes(startIndex, endIndex);
		
		for (int i = 0; i < byteString.length; i++)
		{
			byteString[i] = (byte)(byteString[i] & mask);
		}
		
		return byteString;
	}
	
	
	
	/**
	* 	Set all bytes in window from (and including) startByte to (and excluding)
	* 	endByte to 0x00, masking with this instance's byte mask.
	*/
	public void clearBytes(int startIndex, int endIndex)
	{
		// create inverse mask so can erase only appropriate bits in each byte
		byte notMask = (byte)(0xFF ^ mask); // exclusive OR
		
		// set endIndex so don't write outside of window delimiter
		if (endIndex > numRows*numCols)
			endIndex = numRows*numCols;
		
		// get starting row and column of this window in its base block
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			// now erase only those bits allowed by mask
			baseBlock.bytes[baseIndex] = (byte)(baseBlock.bytes[baseIndex] & notMask);
		}
	}
	
	
	
	
	/**
	* 	Write a sequence of bytes into the window, starting at the specified index and
	*	masking with this instance's byte mask. Note that this automatically clears all 
	*	the other bytes - masked, of course.
	*/
	public void writeBytes(int startIndex, byte[] byteString)
	{
		
		// clear all bytes first - but just those screened by mask, since will be using
		// this class's clear() method
		clearBytes();
		
		// set endIndex so don't write outside of window delimiter
		int endIndex = startIndex + byteString.length;
		if (endIndex > numRows*numCols)
			endIndex = numRows*numCols;
		
		// get starting row and column of this window in its base block
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			baseBlock.bytes[baseIndex] = (byte)(baseBlock.bytes[baseIndex] | (byteString[i-startIndex] & mask));
		}
	}
	
	
	
	/**
	*	Return a String representing the bytes in the window between the specified indices,
	*	masked with this instance's byte mask, with spaces between pairs of hex digits.
	*/
	public String toString(int startIndex, int endIndex)
	{
		
		String returnString = new String();
		
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			int value = baseBlock.bytes[baseIndex] & mask;
			if (value < 0)
				value += 256;
			returnString += getHex(value) + " ";
			
			if (col == numCols - 1)
				returnString += "\n";
		
		}
		
		return returnString;
	}
	
	
	
	
	/**
	*	Return a String representing the bytes in the window between the specified indices,
	*	masked with this instance's byte mask, without spaces between pairs of hex digits.
	*/
	public String toHexString(int startIndex, int endIndex)
	{
		
		String returnString = new String();
		
		int blockStartRow = baseStartIndex/baseBlock.numCols;
		int blockStartCol = baseStartIndex - blockStartRow * baseBlock.numCols;
		
		for (int i = startIndex; i < endIndex; i++)
		{
			
			// find row and column of byteString index in this window
			int row = i/numCols;
			int col = i - row*numCols;
			
			// convert this to index in base block
			int baseIndex = (col + blockStartCol) + (row + blockStartRow) * baseBlock.numCols;
			
			int value = baseBlock.bytes[baseIndex] & mask;
			if (value < 0)
				value += 256;
			returnString += getHex(value);
			
		}
		
		return returnString;
	}
	
	
	
}