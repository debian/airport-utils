/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package snmp;

import java.util.*;




/**
*	The SNMPVarBindList class is a specialization of SNMPSequence that contains a list of
*	SNMPVariablePair objects.
*	@see snmp.SNMPVariablePair

          -- variable bindings

          VarBind ::=
                  SEQUENCE {
                      name
                          ObjectName,

                      value
                          ObjectSyntax
                  }

         VarBindList ::=
                  SEQUENCE OF
                     VarBind

         END

*/

public class SNMPVarBindList extends SNMPSequence
{
	
	/** 
	*	Create a new empty variable binding list.
	*/
	
	
	public SNMPVarBindList()
	{
		super();
	}
	
	
	
	
	
	/**
	*	Return the variable pairs in the list, separated by newlines.
	*/
	
	public String toString()
	{
		Vector sequence = (Vector)(this.getValue());
		
		String valueString = new String();
		
		for (int i = 0; i < sequence.size(); ++i)
		{
			valueString += ((SNMPObject)sequence.elementAt(i)).toString() + "\n";
		}
		
		return valueString;
	}
	
	
	
}