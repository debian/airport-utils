/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package snmp;


/**
*	SNMP datatype used to represent time value. Just extension of SNMPInteger.
*/


public class SNMPTimeTicks extends SNMPInteger
{
	protected byte tag = SNMPBERCodec.SNMPTIMETICKS;
	
	public SNMPTimeTicks()
	{
		this(0);	// initialize value to 0
	}
	
	
	public SNMPTimeTicks(long value)
	{
		super(value);
	}
	
	
	public SNMPTimeTicks(byte[] enc)
		throws SNMPBadValueException
	{
		super(enc);
	}
	
}