/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package snmp;

import java.util.*;




/**
*	The SNMPVariablePair class implements the VarBind specification detailed below from RFC 1157.
*	It is a specialization of SNMPSequence, defining a 2-element sequence containing a single
*	(object identifier, value) pair. Note that the values are themselves SNMPObjects.


          -- variable bindings

          VarBind ::=
                  SEQUENCE {
                      name
                          ObjectName,

                      value
                          ObjectSyntax
                  }



*/


public class SNMPVariablePair extends SNMPSequence
{
	
	/**
	*	Create a new variable pair having the supplied object identifier and vale.
	*/
	
	public SNMPVariablePair(SNMPObjectIdentifier objectID, SNMPObject value)
		throws SNMPBadValueException
	{
		super();
		Vector contents = new Vector();
		contents.insertElementAt(objectID, 0);
		contents.insertElementAt(value, 1);
		this.setValue(contents);
	}

	
	
}