/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package snmp;

import java.util.*;



/**
*	 Defines the SNMPMessage class as a special case of SNMPSequence. Defines a
* 	top-level SNMP message, as per the following definitions from RFC 1157 and
* 	RFC 1901.


RFC1157-SNMP DEFINITIONS

     IMPORTS FROM RFC1155-SMI;

     -- top-level message

             Message ::=
                     SEQUENCE {
                          version        -- version-1 for this RFC
                             INTEGER {
                                 version-1(0)
                             },

                         community      -- community name
                             OCTET STRING,

                         data           -- e.g., PDUs if trivial
                             ANY        -- authentication is being used
                     }
                     
                     
  -- From RFC 1901:
  
  COMMUNITY-BASED-SNMPv2 DEFINITIONS ::= BEGIN

    -- top-level message

        Message ::=
                SEQUENCE {
                     version
                        INTEGER {
                            version(1)  -- modified from RFC 1157
                        },

                    community           -- community name
                        OCTET STRING,

                    data                -- PDUs as defined in [4]
                        ANY
                }
        }

    END                   

*/


public class SNMPMessage extends SNMPSequence
{
	
	
	/**
	*	Create an SNMP message with specified version, community, and pdu.
	*	Use version = 0 for SNMP version 1, or version = 1 for enhanced capapbilities
	*	provided through RFC 1157.
	*/
	
	public SNMPMessage(int version, String community, SNMPPDU pdu)
		throws SNMPBadValueException
	{
		super();
		Vector contents = new Vector();
		contents.insertElementAt(new SNMPInteger(version), 0);
		contents.insertElementAt(new SNMPOctetString(community), 1);
		contents.insertElementAt(pdu, 2);
		this.setValue(contents);
	}
	
	
	
	
	/**
	*	Construct an SNMPMessage from a received ASN.1 byte representation.
	*	@throws SNMPBadValueException Indicates invalid SNMP message encoding supplied.
	*/
	
	public SNMPMessage(byte[] enc)
		throws SNMPBadValueException
	{
		super(enc);
	}
	
	
	
	/** 
	*	Utility method which returns the PDU contained in the SNMP message. The pdu is the third component
	* 	of the sequence, after the version and community name.
	*/
	
	public SNMPPDU getPDU()
	{
		Vector contents = (Vector)(this.getValue());
		return (SNMPPDU)(contents.elementAt(2));
	}
	
}