/*
 * AirportBaseStationConfigurator
 *
 * Copyright (C) 2000, Jonathan Sevy <jsevy@mcs.drexel.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


package snmp;



/** 
*	Abstract base class of all SNMP data type classes.
*/


public abstract class SNMPObject
{
	
	/** 
	*	Must return a Java object appropriate to represent the value/data contained
	* 	in the SNMP object
	*/
	
	public abstract Object getValue();
	
	
	
	/** 
	*	Must set the value of the SNMP object when supplied with an appropriate
	* 	Java object containing an appropriate value.
	*/
	
	public abstract void setValue(Object o)
		throws SNMPBadValueException;
	
	
	
	/** 
	*	Must return the BER byte encoding (type, length, value) of the SNMP object.
	*/
		
	public abstract byte[] getBEREncoding();
	
	
	
	
	/** 
	*	Should return an appropriate human-readable representation of the stored value.
	*/
		
	public abstract String toString();
	
}